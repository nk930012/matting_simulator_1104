import numpy as np
from numpy.linalg import inv

class KalmanFilter(object):
    # Kalman filter.


    def __init__(self, state, F=None, P=None, Q=None, H=None, R=None):
        '''
        state: array-like
            State vector.
        F: matrix
            State transition.
        P: matrix
            Predicted error variance.
        Q: matrix
            Process error variance.
        H: matrix
            Observe.
        R: matrix
            Observation error varaince.
        '''
        self.x = state # state vector
        self.x_priori = None

        self.F = F # State transition.
        self.P = P # Predicted error variance.
        self.P_priori = None # Priori predicted error variance.
        self.Q = Q # Process error variance.
        self.H = H # Observe.
        self.R = R # Observation error varaince.
        self.K = None # Gain factor.
        self.z = None # Observation.
        self.y = None # Residual.

        self.ndims = self.F.shape[0]

    def predict(self):

        self.x_priori = np.dot(self.F, self.x)
        self.P_priori = np.dot(self.F, self.P).dot(self.F.T) + self.Q

    def update(self, z):

        self.y = z - np.dot(self.H, self.x_priori)
        S = np.dot(self.H, self.P_priori).dot(self.H.T) + self.R
        self.K = np.dot(self.P_priori, self.H.T).dot(inv(S))
        self.x = self.x_priori + np.dot(self.K, self.y)
        I = np.eye(self.ndims)
        self.P = (I - np.dot(self.K, self.H)).dot(self.P_priori)

    def get_state(self):
        # Get the state vector.
        return self.x

