#include "frame_averager.h"

FrameAverager::FrameAverager() {
}

FrameAverager::FrameAverager(int width, int height, int type,int frameCnt) {
    m_width = width;
    m_height = height;
    m_frameCnt = frameCnt;
    m_type = type;
}

FrameAverager::~FrameAverager() {}

void FrameAverager::addFrame(cv::Mat image) {

    cv::Mat copy;

    image.copyTo(copy);

    int type = image.type();

    if(CV_8UC3 == m_type)
        copy.convertTo(copy, CV_32FC3, (1 / 255.0f));
    else if(CV_8UC1 == m_type)
        copy.convertTo(copy, CV_32FC1, (1 / 255.0f));

    m_frames.push_back(copy);

    if (m_frames.size() > m_frameCnt)
        m_frames.erase(m_frames.begin());

    copy.release();

}

cv::Mat FrameAverager::getAverageFrame() {

    cv::Mat sum, mul;

    if(CV_8UC3 == m_type)
        sum = cv::Mat(m_height, m_width, CV_32FC3, cv::Scalar(0.0f, 0.0f, 0.0f));
    else if(CV_8UC1==m_type)
        sum = cv::Mat(m_height, m_width, CV_32FC1, cv::Scalar(0.0f));

    for (int i = 0; i < m_frames.size(); ++i) {
        mul = m_frames[i];
        cv::add(sum, mul, sum);
    }

    if(0 != m_frames.size())
        sum /= m_frames.size();


    mul.release();
    sum.convertTo(sum, m_type, (255.0f));
    return sum;
}
