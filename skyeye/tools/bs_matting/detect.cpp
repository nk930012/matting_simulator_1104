﻿#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "bs_matting.h"
#include "sharedmatting_gpu.h"
//#include "trimap_generator.h"
#include "timer.h"

using namespace std;
using namespace cv;

int main()
{
    Timer timer;

    float timeCost = 0.f;

    int width = 640;
    int height = 480;
    //int width = 1280;
    //int height = 720;

    int deviceId = 0;
    VideoCapture capture(deviceId, CAP_V4L2);

    //capture.set(CAP_PROP_FOURCC, cv::VideoWriter::fourcc('M', 'J', 'P', 'G'));
    //capture.set(CAP_PROP_FOURCC, cv::VideoWriter::fourcc('Y', 'U', 'Y', 'V'));

    capture.set(CAP_PROP_FRAME_WIDTH, width);
    capture.set(CAP_PROP_FRAME_HEIGHT, height);
    //capture.set(CAP_PROP_FPS, 60);

    int result;
	result = (int)capture.get(CAP_PROP_AUTO_EXPOSURE);
	cout << "CAP_PROP_AUTO_EXPOSURE(original) = " << result << endl;
	result = (int)capture.get(CAP_PROP_EXPOSURE);
	cout << "CAP_PROP_EXPOSURE(original) = " << result << endl;
    //capture.set(CAP_PROP_AUTOFOCUS, 0); // Turn off auto focus
    capture.set(CAP_PROP_AUTO_EXPOSURE, 1); // 1: Turn 0ff, 3: Turn on auto exposure for OpenCV 4
    capture.set(CAP_PROP_EXPOSURE, 100); 

	result = (int)capture.get(CAP_PROP_AUTO_EXPOSURE);
	cout << "CAP_PROP_AUTO_EXPOSURE(after) = " << result << endl;
	result = (int)capture.get(CAP_PROP_EXPOSURE);
	cout << "CAP_PROP_EXPOSURE(after) = " << result << endl;

    if(!capture.isOpened()) {
        printf("Failed to open webcam. \n");
        return 1;
    }

    // Load the scene image
    Mat sceneImage = imread("sceneImage.png");
    resize(sceneImage, sceneImage, Size(width, height));

    BsMatting matting = BsMatting(width, height);

    bool takeBackground = true;

    int keyCode = -1;
    Mat frame;
    Mat inputImage;
    Mat bgImage;

    Mat diff, thresh, mask;
    Mat trimap, alpha, blended;
    Mat imageWithThresh, imageWithMask, imageWithTrimap; 
    while (true) {

        // Read frame 
        if (!capture.read(frame))
        {
            printf("There is no frame available.");
            break;
        }

        keyCode = waitKey(1);

        // Show current frame
        imshow("image", frame);

        // Take a snapshot as the background
        if (char(keyCode) == 'b') {

            timer.delay(5000); // Wait for a while

            for (int k = 0; k < 10; k++) {
                capture.read(frame);
            }

            bgImage = frame.clone();

            imshow("bg", bgImage);
            imwrite("bgImage.png", bgImage);

            // Set background
            matting.setBackground(bgImage);

            takeBackground = false;

        }

        // Image matting
        if (takeBackground == false) {


            // Input image 
            inputImage = frame.clone();
            imwrite("srcImage.png", inputImage);

            timer.tic();

            // Estimate alpha
            matting.estimateAlpha(inputImage);

            timer.toc();

            diff = matting.getDiff();
            thresh = matting.getThresh();
            mask = matting.getMask();
            trimap = matting.getTrimap();
            alpha = matting.getAlpha();
            blended = matting.blend(inputImage, alpha, sceneImage);
            imageWithThresh = matting.getImageWithThresh();
            imageWithMask = matting.getImageWithMask();
            imageWithTrimap = matting.getImageWithTrimap();

            timeCost = timer.get_dt();
            int fps = int(1.0 / timeCost);

            // Write message
            string msg;
            msg = "fps: " + to_string(fps);  
            int font = cv::FONT_HERSHEY_COMPLEX;
            putText(blended, msg, Point(30, 30), font, 1.0, Scalar(255, 255, 0), 2);

            imshow("diff", diff);
            //imshow("thresh", thresh);
            //imshow("mask", mask);
            //imshow("trimap", trimap);
            imshow("alpha", alpha);
            imshow("blended", blended);
            imshow("imageWithThresh", imageWithThresh);
            imshow("imageWithMask", imageWithMask);
            imshow("imageWithTrimap", imageWithTrimap);

            imwrite("diff.png", diff);
            imwrite("mask.png", mask);
            imwrite("trimap.png", trimap);
            imwrite("alpha.png", alpha);
            imwrite("blended.png", blended);
        }

        // Quit
        if (char(keyCode) == 'q') {
            break;
        }

    }

    // Finalization
    inputImage.release();
    bgImage.release();
    blended.release();
    capture.release();

    return 0;

}
