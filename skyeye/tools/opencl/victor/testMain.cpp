﻿#include <iostream>
#undef MATTING_EXPORTS
#include "Matting/Matting.h"
#include "timer.h"


#if 1
BYTE* matToBytes(Mat image)
{
    int size = image.total() * image.elemSize();
    unsigned char* bytes = new unsigned char[size];  // you will have to delete[] that later
    std::memcpy(bytes, image.data, size * sizeof(char));
    return bytes;
}

Mat bytesToMat(BYTE* bytes, int width, int height, int type)
{
    Mat image = Mat(height, width, type, bytes).clone(); // make a copy
    return image;
}
#endif

using namespace std;
using namespace cv;

int main()
{

    Timer timer;

    Mat frame;

    cout << "useOpenCL:" << ocl::useOpenCL() << endl;

    frame = imread("test.png", IMREAD_COLOR);

    size_t num = 1000;

    double hueMin[] = { 40,30,20 };

    double hueMax[] = { 80,90,100 };

    double satMin[] = { 55,55,55 };

    double satMax[] = { 255,255,255 };

    double valueMin[] = { 85,85,85 };

    double valueMax[] = { 255,255,255 };

    int hsvSize = sizeof(hueMin) / sizeof(double);

    Mat src, out;

    cvtColor(frame, src, cv::COLOR_BGR2RGB);

    timer.tic();

    for (size_t i = 0; i < num; i++)
    {

        BYTE* input = matToBytes(src);

        BYTE* output = new BYTE[src.total() * 4];

        int width = src.cols;

        int height = src.rows;

        int type = src.type();

        OpenCLVMatting(input, width, height, type,
            hueMin, hueMax, satMin, satMax, valueMin, valueMax, hsvSize, true, output);

        out = bytesToMat(output, width, height, CV_8UC4);

        delete[] input;

        delete[] output;

    }

    timer.toc();

    float time_cost = timer.get_dt() / num;
    cout << "dt = " << time_cost << "[s]" << endl;

    std::vector<cv::Mat> outChannels(4);

    cv::split(out, outChannels);

    //RGBA

    outChannels[0].setTo(cv::Scalar(255), outChannels[3] == 0);

    outChannels[1].setTo(cv::Scalar(0), outChannels[3] == 0);

    outChannels[2].setTo(cv::Scalar(0), outChannels[3] == 0);

    cv::merge(outChannels, out);

    cvtColor(out, out, cv::COLOR_RGBA2BGR);

    imshow("result", out);

    waitKey(0);
}
