# -*- coding: utf-8 -*-
import sys
import os
import argparse
import logging
import tornado.ioloop
import numpy as np
import time
import json 
import imutils
import cv2 as cv
import yaml
from time import perf_counter
from skyeye.detect.decorator import FaceDepthBox
from skyeye.detect.decorator.mp_pose_decorate import MpPoseDecorate
from skyeye.utils.camera import CameraBuilder

from skyeye.detect.headpose_rs import FaceDepthTracker
from skyeye.detect.headpose_rs import WebcamPos


class FaceBoxHandler():

    def __init__(self, WsHandler, debug=logging.ERROR):

        input_file = "web_position.yaml"
        with open(input_file, 'r', encoding='utf-8') as f:
            inputs = yaml.load(f, Loader=yaml.Loader)

        len_x = inputs['len_x']
        len_y = inputs['len_y']
        dx = inputs['dx']
        dy = inputs['dy']
        theta = inputs['theta_deg']
        self.num_bodies_max = inputs['num_bodies_max']
        self.disappeared_max = inputs['disappeared_max']

        self.webcam_pos = WebcamPos(theta=theta, len_x=len_x, len_y=len_y, dx=dx, dy=dy)

        self.ws = WsHandler
        self.scheduler = None

        self.active = 0
        self.frame_count = 0
        

    def face_box_scheduler(self):


        if self.active == 1:

            if( 1 == self.frame_count ):
                self.time_end = perf_counter()
                time_duration = self.time_end - self.time_start
                self.fps = int( 1 / time_duration )
                self.frame_count = -1
            elif( 0 == self.frame_count ):
                self.time_start = perf_counter()
        
            self.frame_count += 1

            data = []
            
            self.detector.detect()
            face_depth = self.detector.face_depth_boxes
            bodies = self.tracker.update(face_depth)
            num_bodies = len(bodies)

            if(len(bodies) > 0):

                for body in bodies:
                    dict_body = {}
                    body_id = body.id

                    face_point_x = round(body.face_point[0], 3)
                    face_point_y = round(body.face_point[1], 3) 
                    face_point = (face_point_x, face_point_y)
                    distance = body.distance
                
                    dict_body[ 'id' ] = body_id
                    dict_body[ 'face' ] = face_point
                    dict_body[ 'distance' ] = distance
                    data.append(dict_body)
 
                #print('id:{}, distance:{}, gaze:{}, face:{}'.format(body_id, distance, gaze_point, face_point))

                wsData = {'size': (self.frame_width, self.frame_height), 'bodies': data}
                self.ws.send_updates(wsData)

                print("fps:",self.fps)

            

    def open(self, id = 0):
        
        if self.active == 0:
            self.camera = CameraBuilder.buildWithYaml('camera_config.yaml')
            self.camera.open()
            self.frame_width = self.camera.width
            self.frame_height = self.camera.height
            #self.detector = FaceDepthBox(MpPoseDecorate(self.camera,flip=1))
            self.detector = FaceDepthBox(MpPoseDecorate(self.camera))
            self.tracker = FaceDepthTracker(self.camera.depth_intrin, frame_width=self.frame_width, frame_height=self.frame_height,
            webcam_pos=self.webcam_pos, num_bodies_max=self.num_bodies_max, disappeared_max=self.disappeared_max)
            
            self.active = 1
            self.scheduler = tornado.ioloop.PeriodicCallback(self.face_box_scheduler, 20)
            self.scheduler.start()
        ret = True
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)
        
    def close(self, id):
        
        self.active = 0
        if self.scheduler is not None:
            self.camera.close()
            self.scheduler.stop()
            self.scheduler = None
        status_str = "successful"
        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)
        
    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close cam device.')
                self.close(id=1)
            elif params["command"] == "open":
                if params["property"] == "face_box":
                    self.open(id=id)
            elif params["command"] == "close":
                if params["property"] == "face_box":
                    self.close(id=id)
        elif method == "get":
            pass
        elif method == "set":
            pass
