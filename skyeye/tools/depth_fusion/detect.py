import numpy as np
import cv2 as cv
from skyeye.detect.depth_fusion import DepthFusion
from skyeye.utils.opencv import wait_key


img_dep = cv.imread('img_dep.jpg')  # Depth camera
img_cam = cv.imread('img_cam.jpg')  # Camera

img_w = 1920
img_h = 1080

img_dep = cv.resize(img_dep, (img_w, img_h))
img_cam = cv.resize(img_cam, (img_w, img_h))

h, w, c = img_cam.shape

depth_raw = cv.cvtColor(img_dep, cv.COLOR_BGR2GRAY) # Fake depth data

print('h, w, c: ', h, w, c)

# Depth fusion
fusion = DepthFusion()
fusion.initialize(img_dep, img_cam, load_h=True)
info = fusion.get_depth_info(depth_raw)

# Plot
info_3c = cv.cvtColor(info, cv.COLOR_GRAY2BGR)
plot = np.concatenate((img_dep, img_cam, info_3c), axis=1)
cv.imshow("win", plot)

# Save plot
cv.imwrite("out.jpg", plot)

# Exit while 'q' or 'Esc' is pressed
#key = wait_key(1)
cv.waitKey(3000)
