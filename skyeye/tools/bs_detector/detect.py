import argparse
import yaml
import datetime
import imutils
import cv2 as cv
from time import perf_counter

from skyeye.utils.opencv import Webcam, wait_key
from skyeye.detect.bs import BsDetector, BsTracker

blue = (255, 0, 0)
green = (0, 255, 0)
red = (0, 0, 255)

def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

def draw_bbox(image, bbox):

    xa, ya, width, height = bbox
    xa = int(xa)
    ya = int(ya)
    xz = int(xa + width)
    yz = int(ya + height)
    cv.rectangle(image, (xa, ya), (xz, yz), (0, 255, 0), 2)

if __name__ == '__main__':

    input_file = "inputs.yaml"
    with open(input_file, 'r', encoding='utf-8') as f:
        inputs = yaml.load(f, Loader=yaml.Loader)

    frame_width = inputs['frame_width']
    frame_height = inputs['frame_height']
    width_rmin = inputs['width_rmin']
    disappeared_max = inputs['disappeared_max']
    use_V4L2 = True if inputs['use_V4L2'] == 1 else False
    auto_exposure = False

    webcam = Webcam()
    if webcam.is_open():
        webcam.release()

    device = 0 # Device ID
    webcam.open(device, width=frame_width, height=frame_height,
        use_V4L2=use_V4L2, auto_exposure=auto_exposure)

    detector = BsDetector(frame_width, frame_height, bs_name="MOG2", history=50)
    tracker = BsTracker(frame_width, frame_height, width_rmin=width_rmin, disappeared_max=disappeared_max)

    frame_count = 0
    cv.namedWindow('Frame', cv.WINDOW_NORMAL)
    #cv.namedWindow('fg', cv.WINDOW_NORMAL)
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        time_start = perf_counter()

        frame = webcam.read()
        if frame is None:
            break


        bboxes = detector.detect(frame)
        fg = detector.fg

        bodies = tracker.update(bboxes)
        num_bodies = len(bodies)

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        # Draw information
        image_out = frame

        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "fps: {}, num_bodies: {}".format(fps, num_bodies)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        for body in bodies:
            draw_bbox(image_out, body.bbox)

        # show the frame and record if the user presses a key
        cv.imshow("Frame", frame)
        #cv.imshow("fg", fg)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break



    # cleanup the camera and close any open windows
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
