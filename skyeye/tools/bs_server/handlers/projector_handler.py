# -*- coding: utf-8 -*-
import logging
import telnetlib
import time

class projector_handler():
    def __init__(self, WsHandler, debug = logging.ERROR):
        print('Projector init')
        self.ws = WsHandler

    def handler(self, data):
        print('into projector_handler', type(data), data)

        id = data['id']
        method = data['method']
        params = data['params']
        if ((method == 'set') and "handler" in data):
            handler = data['handler']
            if handler == 'projector':
                print('Into Projector')
                print('Parameters = ', params)
                ip = params['target']
                action = params['action']
                print(ip, action)
                self.set_it(ip, action)

    def set_it(self, ip, action):
        host = str(ip)
        print('Set {} {}'.format(host, action))

        telnet = telnetlib.Telnet(host)
        echo = telnet.read_some().decode('ascii')
        
        if (action == 'On'):
            command = '~0000 1\r'
        elif (action == 'Off'):
            command = '~0000 0\r'
        elif (action == 'AV_Mute_On'):
            command = '~0002 1\r'
        elif (action == 'AV_Mute_Off'):
            command = '~0002 0\r'
        elif (action == 'Source_VGA'):
            command = '~0012 5\r'
        elif (action == 'Source_HDMI1'):
            command = '~0012 1\r'
        elif (action == 'Source_HDMI2'):
            command = '~0012 15\r'
        elif (action == 'Get_Info'):
            command = '~00150 1\r'
        else:
            print('Command Error!')

        telnet.write(str.encode(command))
        echo = telnet.read_some().decode('ascii')
        telnet.close()

