#include <iostream>
#include <string>
#include "trimap_generator_gpu.h"
#include "timer.h"
#include "kernels.h"

cv::Mat TrimapGeneratorGpu::GaussianBlur(cv::Mat img)
{
    cv::Mat gaussianBlur;

    cv::GaussianBlur(img, gaussianBlur, cv::Size(m_kernelSize, m_kernelSize), 0);

    return gaussianBlur;
}

TrimapGeneratorGpu::TrimapGeneratorGpu() {
}

TrimapGeneratorGpu::TrimapGeneratorGpu(int width, int height) {

    m_width = width;

    m_height = height;

    m_kernelSize = 7;

    m_channels = 3;

}

TrimapGeneratorGpu::~TrimapGeneratorGpu() {
}

void TrimapGeneratorGpu::init() {

    if (m_isInit)
        return;

    if (initBuf() && initGpu()) {
        m_isInit = true;
    }
    else {
        finalize();
        exit(1);
    }
}

void TrimapGeneratorGpu::finalize() {

    finalizeGpu();

    finalizeBuf();

    m_isInit = false;
}

bool TrimapGeneratorGpu::initBuf() {

    bool success = true;

    if (!m_imageData)
        m_imageData = (uchar*)malloc(m_width * m_height * m_channels * sizeof(uchar));

    if (!m_trimapData)
        m_trimapData = (uchar*)malloc(m_width * m_height * sizeof(uchar));

    if (!m_keyColorData)
        m_keyColorData = (KeyColor*)malloc(m_maxKeyColorSize * sizeof(KeyColor));

    if (!m_fTrimapData)
        m_fTrimapData = (float*)malloc(m_width * m_height * sizeof(float));

    if (!m_imageData || !m_trimapData || !m_keyColorData || !m_fTrimapData)
        success = false;
    return success;
}

bool TrimapGeneratorGpu::initGpu() {

    cl_int err = 0;

    bool success = true;

    cl_device_id device = create_device();

    m_context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create a context, err : %d.\n", err);

    m_queue = clCreateCommandQueue(m_context, device, CL_QUEUE_PROFILING_ENABLE, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create a command queue, err : %d.\n", err);


#ifndef _DEBUG_IN_CL_FILE

    m_program = build_program_from_source(m_context, device, kernel_source, kernel_source_len());
    if (!m_program)
        printf("Couldn't build program from source.\n");

#else
    m_program = build_program(m_context, device, "./kernels.cl");
    if (!m_program)
        printf("Couldn't build program in kernel.cl.\n");
#endif

    m_trimapCl = clCreateKernel(m_program, "trimap", &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the kernel(trimap), err : %d.\n", err);

    m_imageBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, m_width * m_height * m_channels * sizeof(uchar),
        m_imageData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the buffer(m_imageBuffer), err : %d.\n", err);

    m_keyColorBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, m_maxKeyColorSize * sizeof(KeyColor), m_keyColorData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the buffer(m_keyColorBuffer), err : %d.\n", err);

    m_trimapBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, m_width * m_height * sizeof(uchar),
        m_trimapData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the buffer(m_trimapBuffer), err : %d.\n", err);

    m_fTrimapBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, m_width * m_height * sizeof(float),
        m_fTrimapData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the buffer(m_fTrimapBuffer), err : %d.\n", err);

    if (device)
        clReleaseDevice(device);

    if (!m_context || !m_queue || !m_program || !m_trimapCl || 
        !m_imageBuffer || !m_trimapBuffer || !m_keyColorBuffer ||
        !m_fTrimapBuffer)
        success = false;

    return success;
}

void TrimapGeneratorGpu::finalizeBuf()
{
    if (m_imageData) {
        free(m_imageData);
        m_imageData = NULL;
    }

    if (m_trimapData) {
        free(m_trimapData);
        m_trimapData = NULL;
    }

    if (m_fTrimapData) {
        free(m_fTrimapData);
        m_fTrimapData = NULL;
    }

    if (m_keyColorData) {
        free(m_keyColorData);
        m_keyColorData = NULL;
    }
}

void TrimapGeneratorGpu::finalizeGpu()
{

    if (m_keyColorBuffer) {
        clReleaseMemObject(m_keyColorBuffer);
        m_keyColorBuffer = NULL;
    }

    if (m_imageBuffer) {
        clReleaseMemObject(m_imageBuffer);
        m_imageBuffer = NULL;
    }

    if (m_trimapBuffer) {
        clReleaseMemObject(m_trimapBuffer);
        m_trimapBuffer = NULL;
    }

    if (m_fTrimapBuffer) {
        clReleaseMemObject(m_fTrimapBuffer);
        m_fTrimapBuffer = NULL;
    }

    if (m_trimapCl) {
        clReleaseKernel(m_trimapCl);
        m_trimapCl = NULL;
    }

    if (m_queue) {
        clReleaseCommandQueue(m_queue);
        m_queue = NULL;
    }

    if (m_program) {
        clReleaseProgram(m_program);
        m_program = NULL;
    }

    if (m_context) {
        clReleaseContext(m_context);
        m_context = NULL;
    }
}

void TrimapGeneratorGpu::trimapGpu() {

    cl_int err = 0;

    cl_event event;

    err = clEnqueueWriteBuffer(m_queue, m_imageBuffer, CL_TRUE, 0,
        m_width * m_height * m_channels * sizeof(uchar),
        m_imageData, 0, NULL, NULL);

    err |= clEnqueueWriteBuffer(m_queue, m_keyColorBuffer, CL_TRUE, 0,
        m_keyColorSize * sizeof(*m_keyColorData),
        m_keyColorData, 0, NULL, NULL);

    if (CL_SUCCESS != err) {
        printf("Couldn't write buffer(trimapCl).\n");
        exit(1);
    }

    err = clSetKernelArg(m_trimapCl, 0, sizeof(cl_mem), &m_imageBuffer);
    err |= clSetKernelArg(m_trimapCl, 1, sizeof(int), &m_width);
    err |= clSetKernelArg(m_trimapCl, 2, sizeof(int), &m_height);
    err |= clSetKernelArg(m_trimapCl, 3, sizeof(cl_mem), &m_keyColorBuffer);
    err |= clSetKernelArg(m_trimapCl, 4, sizeof(int), &m_keyColorSize);
    err |= clSetKernelArg(m_trimapCl, 5, sizeof(m_lowerBound), &m_lowerBound);
    err |= clSetKernelArg(m_trimapCl, 6, sizeof(m_upperBound), &m_upperBound);
    err |= clSetKernelArg(m_trimapCl, 7, sizeof(cl_mem), &m_trimapBuffer);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(trimapCl) arguments.\n");
        exit(1);
    }

    globalSize[0] = m_width; globalSize[1] = m_height;

    err = clEnqueueNDRangeKernel(m_queue, m_trimapCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(trimapCl).\n");
        exit(1);
    }

    clWaitForEvents(1, &event);

    err = clEnqueueReadBuffer(m_queue, m_trimapBuffer, CL_TRUE, 0,
        m_width * m_height * sizeof(uchar),
        m_trimapData, 0, NULL, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't read buffer from kernel(trimapCl).\n");
        exit(1);
    }
}

cv::Mat TrimapGeneratorGpu::estimateTrimap(
    cv::Mat image,
    KeyColor* keyColor,
    int keyColorSize,
    float lowerBound,
    float upperBound,
    cv::Size blurSize
    )
{
    cv::GaussianBlur(image, image, cv::Size(5, 5), 0, 0);

    std::memcpy(m_imageData, image.data, m_width * m_height * m_channels);

    if (keyColorSize > m_maxKeyColorSize) {

        clReleaseMemObject(m_keyColorBuffer);

        m_keyColorBuffer = NULL;

        free(m_keyColorData);

        m_keyColorData = NULL;

        m_maxKeyColorSize = keyColorSize;

        m_keyColorData = (KeyColor*)malloc(m_maxKeyColorSize * sizeof(KeyColor));

        if (!m_keyColorData) {
            printf("Reallocate keyColor data fail");
            exit(1);
        }

        cl_int err = 0;

        m_keyColorBuffer = clCreateBuffer(m_context,
            CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, m_maxKeyColorSize * sizeof(KeyColor), m_keyColorData, &err);

        if (err != CL_SUCCESS) {
            printf("Reallocate keyColor buf fail");
            exit(1);
        }
    }

    m_keyColorSize = keyColorSize;

    std::memcpy(m_keyColorData, keyColor, m_keyColorSize * sizeof(*keyColor));

    m_lowerBound = lowerBound;

    m_upperBound = upperBound;

    trimapGpu();

    cv::Mat trimap = cv::Mat(m_height, m_width, CV_8UC1, m_trimapData);

    //cv::blur(trimap, trimap, Size(10, 10));

    cv::blur(trimap, trimap, blurSize);

    cv::Mat mask;

    cv::inRange(trimap, cv::Scalar(1), cv::Scalar(254), mask);

    trimap.setTo(127, mask);

    //cv::imshow("trimap---gen", trimap);

    mask.release();

    return trimap;
}


#if 0
cv::Mat TrimapGeneratorGpu::estimateTrimap(
    cv::Mat image,
    KeyColor* keyColor,
    int keyColorSize,
    float lowerBound,
    float upperBound
    )
{
    cv::GaussianBlur(image, image, cv::Size(5, 5), 0,0);

    std::memcpy(m_imageData, image.data, m_width * m_height * m_channels);

    if (keyColorSize > m_maxKeyColorSize) {

        clReleaseMemObject(m_keyColorBuffer);

        m_keyColorBuffer = NULL;

        free(m_keyColorData);

        m_keyColorData = NULL;

        m_maxKeyColorSize = keyColorSize;

        m_keyColorData = (KeyColor*)malloc(m_maxKeyColorSize * sizeof(KeyColor));

        if (!m_keyColorData) {
            printf("Reallocate keyColor data fail");
            exit(1);
        }

        cl_int err = 0;

        m_keyColorBuffer = clCreateBuffer(m_context,
            CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, m_maxKeyColorSize * sizeof(KeyColor), m_keyColorData, &err);

        if (err != CL_SUCCESS) {
            printf("Reallocate keyColor buf fail");
            exit(1);
        }
    }

    m_keyColorSize = keyColorSize;

    std::memcpy(m_keyColorData, keyColor, m_keyColorSize * sizeof(*keyColor));

    m_lowerBound = lowerBound;

    m_upperBound = upperBound;

    trimapGpu();

    cv::Mat trimap = cv::Mat(m_height, m_width, CV_8UC1, m_trimapData);

    cv::waitKey(1);

    //cv::blur(trimap, trimap, Size(10, 10));

    cv::blur(trimap, trimap, Size(4, 5));

    cv::Mat mask;

    cv::inRange(trimap, cv::Scalar(1), cv::Scalar(254), mask);

    trimap.setTo(127, mask);

    //cv::imwrite("./trimap.png", trimap);

    mask.release();

    return trimap;
}
#endif










