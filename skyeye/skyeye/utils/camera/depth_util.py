import numpy as np

class DepthUtil(object):
    def __init__(self):
        pass


    @staticmethod
    def depth_grayscale(depth_info, d_min=0.5, d_max=5.0):

        values = depth_info
        values = np.where(values < d_min, -1, values)
        values = np.where(values > d_max, -1, values)

        diff = values - d_min
        diff_total = d_max - d_min
        ratio = 1.0 - diff/diff_total
        gray_values = np.rint(ratio*255)

        gray = np.where(values > 0, gray_values, -1)
        gray = np.clip(gray, 0, 255)

        return gray