#ifndef OPENCL_MATTING_H
#define OPENCL_MATTING_H

#if 0
#ifdef __APPLE__
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.hpp>
#endif

#include <CL/opencl.h>

#include <opencv2/opencv.hpp>

#include <opencv2/core/ocl.hpp>

#include <opencv2/core/mat.hpp>

#include <opencv2/imgproc.hpp>

#include <vector>

#include <map>

using namespace std;

typedef std::string clFileName;
typedef std::string clKernelName;

class AOpenclBuilder
{
public:

    AOpenclBuilder() {}

    ~AOpenclBuilder() {}

    virtual bool Init() = 0;

    //virtual bool DeInit() = 0;

    //virtual void Run() = 0;

    bool isInit()
    {
        return m_isInit;
    }

protected:

    vector<cl_device_id>    m_deviceID;

    vector<map<cl_device_id, cl_context>> m_context;

    //string1 : program name, string2 : kernel name
    vector<map<string, map<string, cl_kernel>>>  m_kernel;

    //string1: program name
    vector<map<string,cl_program>>  m_program;

    vector<cl_command_queue> m_queue;



    bool m_isInit;

};

class OpenclSimpleBuilder : AOpenclBuilder
{
public:

    OpenclSimpleBuilder();

    ~OpenclSimpleBuilder();

    //bool Init();

    //bool DeInit();

    //void Run();

private:
    bool GetGpuDevice();
    bool CreateContext(size_t deviceIndex);
    bool BuildProgram(cl_context ctx, cl_device_id dev, string filename);
    bool CreateQueue(cl_context context, cl_device_id device);
};
#endif
#endif