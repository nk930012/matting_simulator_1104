#include "color_shared_matting.h"
#include "timer.h"
#include "mixed_alpha_generator.h"

ColorSharedMatting::ColorSharedMatting() { }

ColorSharedMatting::ColorSharedMatting(
    int originalWidth, int originalHeight,
    int scaledWidth, int scaledHeight,
    bool useAverage
    )
{

    m_originalWidth = originalWidth;
    m_originalHeight = originalHeight;

    m_scaledWidth = scaledWidth;
    m_scaledHeight = scaledHeight;

    m_ct = TrimapGeneratorGpu(m_scaledWidth, m_scaledHeight);
    m_sm = SharedMatting(m_scaledWidth, m_scaledHeight);
    htg = HsvTrimapGenerator();

    m_skipNum = m_maxSkipNum + 1;
    m_useAverage = useAverage;

    if (m_useAverage)
        m_frmAver = FrameAverager(m_originalWidth, m_originalHeight, CV_8UC3, 2);
}

ColorSharedMatting::~ColorSharedMatting(){ }

void ColorSharedMatting::init()
{
    m_sm.init();
    m_ct.init();
}

double diffTime1 = 0.0;
double diffTime2 = 0.0;

void ColorSharedMatting::finalize()
{
    if(!m_image.empty())
        m_image.release();

    if(!m_trimap.empty())
        m_trimap.release();

    if(!m_alpha.empty())
        m_alpha.release();

    //printf("Diff time1 : %f\n", diffTime1 / 500);
    //printf("Diff time2 : %f\n", diffTime2 / 500);
    m_ct.finalize();

    m_sm.finalize();
}

cv::Mat ColorSharedMatting::getAlpha() {
    return m_alpha;
}



//#define USING_PRE_ALPHA

#if 1

void rgb2Hsv(
    uchar r,
    uchar g,
    uchar b,
    int* hue,
    int* sat,
    int* val
    )
{

    float fR = ((float)r / 255.0), fG = ((float)g / 255.0), fB = ((float)b / 255.0);

    float fH = 0.0f, fS = 0.0f, fV = 0.0f;

    float fCMax = max(max(fR, fG), fB), fCMin = min(min(fR, fG), fB), fDelta = fCMax - fCMin;

    if (fDelta > 0)
    {

        if (fCMax == fR)
            fH = 60 * (fmod(((fG - fB) / fDelta), 6));
        else if (fCMax == fG)
            fH = 60 * (((fB - fR) / fDelta) + 2);
        else if (fCMax == fB)
            fH = 60 * (((fR - fG) / fDelta) + 4);


        if (fCMax > 0)
            fS = fDelta / fCMax;
        else
            fS = 0;
        fV = fCMax;
    }
    else
    {
        fH = 0;
        fS = 0;
        fV = fCMax;
    }

    if (fH < 0)
        fH = 360 + fH;

    int h = (180 * (fH / 360)), s = (255 * fS), v = (255 * fV);

    *hue = h;
    *sat = s;
    *val = v;

}

static void KeyColor2HsvBound(KeyColor* keyColor, int keyColorSize, cv::Scalar& lowerBound, cv::Scalar& upperBound) {

    int hMin = 255, hMax = 0, sMin = 255, sMax = 0, vMin = 255, vMax = 0;

#if 0

    for (int i = 0; i < keyColorSize; ++i) {
        unsigned char b = keyColor[i].b;
        unsigned char g = keyColor[i].g;
        unsigned char r = keyColor[i].r;
        int h = 0, s = 0, v = 0;
        rgb2Hsv(b, g, r, &h, &s, &v);

        if (h < hMin)
            hMin = h;

        if (h > hMax)
            hMax = h;

        if (s < sMin)
            sMin = s;

        if (s > sMax)
            sMax = s;

        if (v < vMin)
            vMin = v;

        if (v > vMax)
            vMax = v;
    }

#else
    cv::Mat bgrM = cv::Mat(1, 1, CV_8UC3, cv::Scalar(0, 0, 0));
    cv::Mat hsvM = cv::Mat(1, 1, CV_8UC3, cv::Scalar(0, 0, 0));
    for (int i = 0; i < keyColorSize; ++i) {

        bgrM.at<cv::Vec3b>(0, 0) = cv::Vec3b(keyColor[i].b, keyColor[i].g, keyColor[i].r);

        cv::cvtColor(bgrM, hsvM, COLOR_BGR2HSV);

        cv::Vec3b hsv = hsvM.at<cv::Vec3b>(0, 0);

        if (hsv.val[0] < hMin)
            hMin = hsv.val[0];

        if (hsv.val[0] > hMax)
            hMax = hsv.val[0];

        if (hsv.val[1] < sMin)
            sMin = hsv.val[1];

        if (hsv.val[1] > sMax)
            sMax = hsv.val[1];

        if (hsv.val[2] < vMin)
            vMin = hsv.val[2];

        if (hsv.val[2] > vMax)
            vMax = hsv.val[2];

    }
#endif
    lowerBound = cv::Scalar(hMin, sMin, vMin);
    upperBound = cv::Scalar(hMax, sMax, vMax);
}

void ColorSharedMatting::process(
    uchar* imgBuf,
    int width,
    int height,
    KeyColor* keyColor,
    int keyColorSize,
    float lowerBound,
    float upperBound,
    uchar* blendBuf
    )
{
    cv::Mat image = cv::Mat(height, width, CV_8UC3, imgBuf);

    image.copyTo(m_image);

    if(m_originalWidth != width || m_originalHeight != height)
        exit(1);

    if (m_useAverage) {
        m_frmAver.addFrame(m_image);
        m_image.release();
        m_image = m_frmAver.getAverageFrame();
        m_image.copyTo(image);
    }

    //do gaussian before resize
    cv::GaussianBlur(m_image, m_image, cv::Size(9,9), 0.0, 0.0);

    cv::resize(m_image, m_image, cv::Size(m_scaledWidth, m_scaledHeight), 0, 0, cv::INTER_AREA);


#ifdef USING_PRE_ALPHA
    if (m_skipNum < m_maxSkipNum) {
#else
    if(false){
#endif
        MixedAlphaGenerator mag;
        m_alpha = mag.approximateAlpha(m_image, m_previousAlpha);
        ++m_skipNum;
    }
    else {

        //HsvTrimapGenerator htg = HsvTrimapGenerator();

        htg.setColorBound(cv::Scalar(GREEN_HUE_MIN, GREEN_SAT_MIN, GREEN_VAL_MIN), cv::Scalar(GREEN_HUE_MAX, GREEN_SAT_MAX, GREEN_VAL_MAX));

        cv::Mat mask = htg.estimateMask(m_image);

        //cv::imshow("mask", mask);

        if (0 != keyColorSize) {

            cv::Scalar lowerBound(0.0, 0.0, 0.0);

            cv::Scalar upperBound(0.0, 0.0, 0.0);

            KeyColor2HsvBound(keyColor, keyColorSize, lowerBound, upperBound);

            htg.setColorBound(lowerBound, upperBound);

            cv::Mat keyColorMask = htg.estimateMask(m_image);

            cv::bitwise_and(mask, keyColorMask, mask);

            keyColorMask.release();

        }

        m_trimap = htg.estimateTrimap(mask, cv::Size(9,9));

        //imshow("trimap", m_trimap);

        mask.release();

        m_sm.estimateAlpha(m_image, m_trimap);

        m_alpha = m_sm.getAlpha();

        //imshow("alpha", m_alpha);

#ifdef USING_PRE_ALPHA
        m_alpha.copyTo(m_previousAlpha);
#endif
        m_skipNum = 0;
    }

    cv::resize(m_alpha, m_alpha, cv::Size(m_originalWidth, m_originalHeight), 0, 0, cv::INTER_CUBIC);

    m_trimap.release();

    m_image.release();

    image.release();

}

void ColorSharedMatting::OnePageTrimap(
    std::string outputs, 
    cv::Mat image, 
    int& width, 
    int& height, 
    float lowerBound, 
    float upperBound, 
    KeyColor* keyColor, 
    int& keyColorSize)

{
    //cv::Mat image = cv::Mat(height, width, CV_8UC3, imgBuf);
    image.copyTo(m_image);

    if (m_originalWidth != width || m_originalHeight != height)
        exit(1);

    if (m_useAverage) {
        m_frmAver.addFrame(m_image);
        m_image.release();
        m_image = m_frmAver.getAverageFrame();
        m_image.copyTo(image);
    }

    //do gaussian before resize
    cv::GaussianBlur(m_image, m_image, cv::Size(9, 9), 0.0, 0.0);

    cv::resize(m_image, m_image, cv::Size(m_scaledWidth, m_scaledHeight), 0, 0, cv::INTER_AREA);


#ifdef USING_PRE_ALPHA
    if (m_skipNum < m_maxSkipNum) {
#else
    if (false) {
#endif
        MixedAlphaGenerator mag;
        m_alpha = mag.approximateAlpha(m_image, m_previousAlpha);
        ++m_skipNum;
    }
    else {

        htg.setColorBound(cv::Scalar(GREEN_HUE_MIN, GREEN_SAT_MIN, GREEN_VAL_MIN), cv::Scalar(GREEN_HUE_MAX, GREEN_SAT_MAX, GREEN_VAL_MAX));

        cv::Mat mask = htg.estimateMask(m_image);

        imwrite(outputs + "test_mask.png", mask);

        if (0 != keyColorSize) {

            cv::Scalar lowerBound(0.0, 0.0, 0.0);

            cv::Scalar upperBound(0.0, 0.0, 0.0);

            KeyColor2HsvBound(keyColor, keyColorSize, lowerBound, upperBound);

            htg.setColorBound(lowerBound, upperBound);

            cv::Mat keyColorMask = htg.estimateMask(m_image);

            cv::bitwise_and(mask, keyColorMask, mask);

            keyColorMask.release();

        }

        m_trimap = htg.estimateTrimap(mask, cv::Size(9, 9));

        imwrite(outputs + "test_trimap.png", m_trimap);
        mask.release();
    }
}


void ColorSharedMatting::OnePageBlend(
    cv::Mat& image,
    std::string outputs,
    KeyColor* keyColor, 
    int& keyColorSize,
    float lowerBound,
    float upperBound)

{

    int width = image.cols;

    int height = image.rows;

    int channels = image.channels();

    uchar* blendBuf = (uchar*)malloc(width * height * 4);
    
    process(image.data, width, height, keyColor, keyColorSize, lowerBound, upperBound, blendBuf);
    

    cv::Mat LighBlueBg = cv::Mat(height, width, CV_8UC3, cv::Scalar(255, 180, 130));

    cv::Mat alpha = getAlpha();

    cv::Mat blendblue = blendWithBg(image, alpha, LighBlueBg);

    imwrite(outputs + "test_blend.png", blendblue);

    LighBlueBg.release();
    blendblue.release();

    alpha.release();

    free(blendBuf);

    blendBuf = NULL;

    image.release();
}

void ColorSharedMatting::AllBlend(
    string  filePath,
    string outputsRemoveBG,
    float lowerBound,
    float upperBound,
    KeyColor* keyColor,
    int keyColorSize,
    int& width,
    int& height)
{

    int index1 = filePath.find_last_of("/");

    int index2 = filePath.find_last_of(".");

    string imageName = filePath.substr(index1 + 1, index2 - index1 - 1);

    cv::Mat image = cv::Mat::zeros(cv::Size(width, height), CV_8UC3);

    image = cv::imread(filePath, cv::IMREAD_COLOR);

    int channels = image.channels();

    uchar* blendBuf = (uchar*)malloc(width * height * 4);

    process(image.data, width, height, keyColor, keyColorSize, lowerBound, upperBound, blendBuf);

    cv::Mat LighBlueBg = cv::Mat(height, width, CV_8UC3, cv::Scalar(255, 180, 130));

    cv::Mat alpha = getAlpha();

    cv::Mat blendblue = blendWithBg(image, alpha, LighBlueBg);

    imwrite(outputsRemoveBG + imageName + ".png", blendblue);

    blendblue.release();
    LighBlueBg.release();
    alpha.release();

    free(blendBuf);

    blendBuf = NULL;

    image.release();


}

void ColorSharedMatting::AllTrimap(
    string  filePath,
    string outputsTrimap,
    float lowerBound,
    float upperBound,
    KeyColor* keyColor,
    int keyColorSize,
    int &width,
    int &height)
{
    Timer timer;
    //timer.tic();
    int index1 = filePath.find_last_of("/");

    int index2 = filePath.find_last_of(".");

    string imageName = filePath.substr(index1 + 1, index2 - index1 - 1);

    cv::Mat image = cv::Mat::zeros(cv::Size(width, height), CV_8UC3);

    image = cv::imread(filePath, cv::IMREAD_COLOR);
    
    image.copyTo(m_image);

    if (m_originalWidth != width || m_originalHeight != height)
        exit(1);

    if (m_useAverage) {
        m_frmAver.addFrame(m_image);
        m_image.release();
        m_image = m_frmAver.getAverageFrame();
        m_image.copyTo(image);
    }

    //do gaussian before resize
    cv::GaussianBlur(m_image, m_image, cv::Size(9, 9), 0.0, 0.0);

    cv::resize(m_image, m_image, cv::Size(m_scaledWidth, m_scaledHeight), 0, 0, cv::INTER_AREA);


#ifdef USING_PRE_ALPHA
    if (m_skipNum < m_maxSkipNum) {
#else
    if (false) {
#endif
        MixedAlphaGenerator mag;
        m_alpha = mag.approximateAlpha(m_image, m_previousAlpha);
        ++m_skipNum;
    }
    else {

        htg.setColorBound(cv::Scalar(GREEN_HUE_MIN, GREEN_SAT_MIN, GREEN_VAL_MIN), cv::Scalar(GREEN_HUE_MAX, GREEN_SAT_MAX, GREEN_VAL_MAX));

        cv::Mat mask = htg.estimateMask(m_image);

        if (0 != keyColorSize) {

            cv::Scalar lowerBound(0.0, 0.0, 0.0);

            cv::Scalar upperBound(0.0, 0.0, 0.0);

            KeyColor2HsvBound(keyColor, keyColorSize, lowerBound, upperBound);

            htg.setColorBound(lowerBound, upperBound);

            cv::Mat keyColorMask = htg.estimateMask(m_image);

            cv::bitwise_and(mask, keyColorMask, mask);

            keyColorMask.release();

        }

        m_trimap = htg.estimateTrimap(mask, cv::Size(9, 9));

        imwrite(outputsTrimap + imageName + ".png", m_trimap);
        mask.release();
    }
}

cv::Mat ColorSharedMatting::blendWithBg(
    cv::Mat srcIn,
    cv::Mat alphaIn,
    cv::Mat bgIn)

{
    cv::Mat src = normalize(srcIn);

    cv::Mat alphaC3;

    cv::cvtColor(alphaIn, alphaC3, cv::COLOR_GRAY2BGR);

    cv::Mat alpha = normalize(alphaC3);

    cv::Mat bg = normalize(bgIn);
    resize(bg, bg, src.size(), 0, 0);

    //cv::Mat bg2 = normalize(bgIn);
    //resize(bg2, bg2, src.size(), 0, 0);

    cv::Mat tmp1, tmp2;

    cv::Scalar ones = cv::Scalar(1.0, 1.0, 1.0);

    multiply(alpha, src, tmp1, 1.0);

    multiply((ones - alpha), bg, tmp2, 1.0);

    cv::Mat blended = tmp1 + tmp2;

    cv::Mat out = unnormalize(blended);

    return out;

};

cv::Mat ColorSharedMatting::normalize(Mat src)
{
    Mat out;
    src.convertTo(out, CV_32F, 1.0 / 255, 0);
    return out;
}

cv::Mat ColorSharedMatting::unnormalize(Mat src)
{
    Mat out;
    src.convertTo(out, CV_8U, 255, 0);
    return out;
}



#else

#define SCALE 0.2f//0.15f


void ColorSharedMatting::process(
    uchar* imgBuf,
    int width,
    int height,
    KeyColor* keyColor,
    int keyColorSize,
    float lowerBound,
    float upperBound,
    uchar* blendBuf
    )
{
    cv::Mat image = cv::Mat(height, width, CV_8UC3, imgBuf);

    image.copyTo(m_image);

    if (m_originalWidth != width || m_originalHeight != height)
        exit(1);

    float lower = SCALE * lowerBound;

    float upper = SCALE * upperBound;

    if (upper < lower)
        upper = lower;

    cv::resize(m_image, m_image, cv::Size(m_scaledWidth, m_scaledHeight), 0, 0, cv::INTER_AREA);

    cv::Size blurSize(9, 9);

    //cv::GaussianBlur(m_image, m_image, blurSize, 0.0, 0.0);

    m_trimap = m_ct.estimateTrimap(m_image, keyColor, keyColorSize, lower, upper, cv::Size(4,5));

    m_sm.estimateAlpha(m_image, m_trimap);

    m_alpha = m_sm.getAlpha();

    cv::resize(m_alpha, m_alpha, cv::Size(m_originalWidth, m_originalHeight), 0, 0, cv::INTER_CUBIC);

    std::vector<cv::Mat> imgChannel(4);

    std::vector<cv::Mat> alphaChannel(1);

    cv::split(image, imgChannel);

    cv::split(m_alpha, alphaChannel);

    imgChannel.push_back(alphaChannel[0]);

    cv::Mat blend;

    merge(imgChannel, blend);

    imgChannel.clear();

    alphaChannel.clear();

    m_alpha.release();

    m_trimap.release();

    m_image.release();

    image.release();

    std::memcpy(blendBuf, blend.data, m_originalWidth * m_originalHeight * 4);

    blend.release();
}
#endif

