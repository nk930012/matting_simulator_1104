#ifndef SHAREDMSTTING_H
#define SHAREDMSTTING_H

#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cmath>
#include <vector>

#include "cl_utilities.h"
#include "typedef.h"


using namespace std;
using namespace cv;


const int kI = 10;
const int kG = 4;
const double kC = 5.0;

//const int numUnknownPixelsMax = 921600;
const int numPickedPointsMax = kG;


class SharedMatting
{
public:
    SharedMatting();
    ~SharedMatting();

    void loadImage(Mat img);
    void loadTrimap(Mat img);

    void initialize(); 
    void finalize(); 

    void expandKnown();
    //void sample(int width, int height, int numUnknownPixels, PixelPoint p, vector<PixelPoint> &f, vector<PixelPoint> &b);
    void gathering();
    void refineSample();
    void localSmooth();
    void solveAlpha();
    void sample(int width, int height, int numUnknownPixels,
        vector<vector<PixelPoint> > &F, vector<vector<PixelPoint> > &B);
    void release();

    double mP(int i, int j, ColorPoint f, ColorPoint b);
    double nP(int i, int j, ColorPoint f, ColorPoint b);
    double eP(int i1, int j1, int i2, int j2);
    double pfP(PixelPoint p, vector<PixelPoint>& f, vector<PixelPoint>& b);
    double aP(int i, int j, double pf, ColorPoint f, ColorPoint b);
    double gP(PixelPoint p, PixelPoint fp, PixelPoint bp, double pf);
    double gP(PixelPoint p, PixelPoint fp, PixelPoint bp, double dpf, double pf);
    double dP(PixelPoint s, PixelPoint d);
    double sigma2(PixelPoint p);
    double distanceColor2(ColorPoint cs1, ColorPoint cs2);
    double comalpha(ColorPoint c, ColorPoint f, ColorPoint b);


    Mat normalize(Mat src);
    Mat unnormalize(Mat src);
    Mat getAlpha();
    Mat blend(Mat src, Mat alpha, Mat bg);
    Mat getImageBlended();

private:

    int m_width = 0;
    int m_height = 0;
    int m_channels = 0;
    int m_step = 0;

    int numUnknownPixels = 0;
    int numUnknownPixelsMax = 0;

    Mat m_image;
    Mat m_trimap;
    Mat m_alpha;

    uchar* data;
    uchar* m_imageData = NULL;
    uchar* m_trimapData = NULL;
    uchar* m_alphaData = NULL;

    Tuple* m_tuples = NULL;
    Ftuple* m_ftuples = NULL;

    int* m_unknownIndexes = NULL;
    vector<PixelPoint> m_unknownPixels;
    //PixelPoint* m_unknownPixels = NULL;
    PixelPoint* m_fgPoint = NULL;
    PixelPoint* m_bgPoint = NULL;
    int* m_numFgPoints = NULL;
    int* m_numBgPoints = NULL;

    vector<PixelPoint> uT;
    vector<Tuple> tuples;
    vector<Ftuple> ftuples;

    int kI;
    int kG;
    int ** unknownIndex;//Unknown的索引信息；
    int ** tri;
    int ** alpha;
    double kC;


};



#endif
