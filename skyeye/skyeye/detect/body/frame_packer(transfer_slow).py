import json
import sys
from dataclasses_json import dataclass_json
from dataclasses import dataclass
from dataclasses import field
from typing import List

sys.path.append("../../../touch")
from touch.detect.body import body_constant as BC
from touch.detect.body import Body as BCLS

@dataclass_json
@dataclass
class Pixel:
    x: int = field(default_factory=int)
    y: int = field(default_factory=int)

@dataclass_json
@dataclass
class PixelFlt:
    x: float = field(default_factory=float)
    y: float = field(default_factory=float)



@dataclass_json
@dataclass
class BodyBox:
    x: int = field(default_factory=int)
    y: int = field(default_factory=int)
    width: int = field(default_factory=int)
    height: int = field(default_factory=int)


@dataclass_json
@dataclass
class Node:
    nose: Pixel = field(default_factory=Pixel)
    neck: Pixel = field(default_factory=Pixel)
    right_shoulder: Pixel = field(default_factory=Pixel)
    right_elbow: Pixel = field(default_factory=Pixel)
    right_wrist: Pixel = field(default_factory=Pixel)
    left_shoulder: Pixel = field(default_factory=Pixel)
    left_elbow: Pixel = field(default_factory=Pixel)
    left_wrist: Pixel = field(default_factory=Pixel)
    right_hip: Pixel = field(default_factory=Pixel)
    right_knee: Pixel = field(default_factory=Pixel)
    right_ankle: Pixel = field(default_factory=Pixel)
    left_hip: Pixel = field(default_factory=Pixel)
    left_knee: Pixel = field(default_factory=Pixel)
    left_ankle: Pixel = field(default_factory=Pixel)
    right_eye: Pixel = field(default_factory=Pixel)
    left_eye: Pixel = field(default_factory=Pixel)
    right_ear: Pixel = field(default_factory=Pixel)
    left_ear: Pixel = field(default_factory=Pixel)


@dataclass_json
@dataclass
class Gesture:
    lw_rn: bool = field(default_factory=bool)
    lw_rw: bool = field(default_factory=bool)
    lw_bc: bool = field(default_factory=bool)
    lw_bb: bool = field(default_factory=bool)
    box_left: BodyBox = field(default_factory=BodyBox)
    box_right: BodyBox = field(default_factory=BodyBox)
    pos_right: PixelFlt = field(default_factory=PixelFlt)
    pos_left: PixelFlt = field(default_factory=PixelFlt)



@dataclass_json
@dataclass
class Body:
    id: int = field(default_factory=int)
    keyPoint: Node = field(default_factory=Node)
    gesture: Gesture = field(default_factory=Gesture)


@dataclass_json
@dataclass
class Bodies:
    data: List[Body] = field(default_factory=list)


@dataclass_json
@dataclass
class FramePacker:
    bodies: List[Body] = field(default_factory=list)
    width: int = field(default_factory=int)
    height: int = field(default_factory=int)



def body_converter(body):

    converted_body = Body()

    converted_body.id = body.id
    
    #nose
    converted_body.keyPoint.nose.x = body.keypoints[BC.BODY_NOSE][BC.PIXEL_X]
    converted_body.keyPoint.nose.y = body.keypoints[BC.BODY_NOSE][BC.PIXEL_Y]

    #neck
    converted_body.keyPoint.neck.x = body.keypoints[BC.BODY_NECK][BC.PIXEL_X]
    converted_body.keyPoint.neck.y = body.keypoints[BC.BODY_NECK][BC.PIXEL_Y]

    #right_shoulder
    converted_body.keyPoint.right_shoulder.x = body.keypoints[BC.BODY_RIGHT_SHOULDER][BC.PIXEL_X]
    converted_body.keyPoint.right_shoulder.y = body.keypoints[BC.BODY_RIGHT_SHOULDER][BC.PIXEL_Y]

    #right_elbow
    converted_body.keyPoint.right_elbow.x = body.keypoints[BC.BODY_RIGHT_ELBOW][BC.PIXEL_X]
    converted_body.keyPoint.right_elbow.y = body.keypoints[BC.BODY_RIGHT_ELBOW][BC.PIXEL_Y]
          
    #right_wrist
    converted_body.keyPoint.right_wrist.x = body.keypoints[BC.BODY_RIGHT_WRIST][BC.PIXEL_X]
    converted_body.keyPoint.right_wrist.y = body.keypoints[BC.BODY_RIGHT_WRIST][BC.PIXEL_Y]

    #left_shoulder
    converted_body.keyPoint.left_shoulder.x = body.keypoints[BC.BODY_LEFT_SHOULDER][BC.PIXEL_X]
    converted_body.keyPoint.left_shoulder.y = body.keypoints[BC.BODY_LEFT_SHOULDER][BC.PIXEL_Y]

    #left_elbow
    converted_body.keyPoint.left_elbow.x = body.keypoints[BC.BODY_LEFT_ELBOW][BC.PIXEL_X]
    converted_body.keyPoint.left_elbow.y = body.keypoints[BC.BODY_LEFT_ELBOW][BC.PIXEL_Y]

    #left_wrist
    converted_body.keyPoint.left_wrist.x = body.keypoints[BC.BODY_LEFT_WRIST][BC.PIXEL_X]
    converted_body.keyPoint.left_wrist.y = body.keypoints[BC.BODY_LEFT_WRIST][BC.PIXEL_Y]

    #right_hip
    converted_body.keyPoint.right_hip.x = body.keypoints[BC.BODY_RIGHT_HIP][BC.PIXEL_X]
    converted_body.keyPoint.right_hip.y = body.keypoints[BC.BODY_RIGHT_HIP][BC.PIXEL_Y]

    #right_knee
    converted_body.keyPoint.right_knee.x = body.keypoints[BC.BODY_RIGHT_KNEE][BC.PIXEL_X]
    converted_body.keyPoint.right_knee.y = body.keypoints[BC.BODY_RIGHT_KNEE][BC.PIXEL_Y]

    #right_ankle
    converted_body.keyPoint.right_ankle.x = body.keypoints[BC.BODY_RIGHT_ANKLE][BC.PIXEL_X]
    converted_body.keyPoint.right_ankle.y = body.keypoints[BC.BODY_RIGHT_ANKLE][BC.PIXEL_Y]

    #left_hip
    converted_body.keyPoint.left_hip.x = body.keypoints[BC.BODY_LEFT_HIP][BC.PIXEL_X]
    converted_body.keyPoint.left_hip.y = body.keypoints[BC.BODY_LEFT_HIP][BC.PIXEL_Y]

    #left_knee
    converted_body.keyPoint.left_knee.x = body.keypoints[BC.BODY_LEFT_KNEE][BC.PIXEL_X]
    converted_body.keyPoint.left_knee.y = body.keypoints[BC.BODY_LEFT_KNEE][BC.PIXEL_Y]

    #left_ankle
    converted_body.keyPoint.left_ankle.x = body.keypoints[BC.BODY_LEFT_ANKLE][BC.PIXEL_X]
    converted_body.keyPoint.left_ankle.y = body.keypoints[BC.BODY_LEFT_ANKLE][BC.PIXEL_Y]

    #right_eye
    converted_body.keyPoint.right_eye.x = body.keypoints[BC.BODY_RIGHT_EYE][BC.PIXEL_X]
    converted_body.keyPoint.right_eye.y = body.keypoints[BC.BODY_RIGHT_EYE][BC.PIXEL_Y]

    #left_eye
    converted_body.keyPoint.left_eye.x = body.keypoints[BC.BODY_LEFT_EYE][BC.PIXEL_X]
    converted_body.keyPoint.left_eye.y = body.keypoints[BC.BODY_LEFT_EYE][BC.PIXEL_Y]

    #right_ear
    converted_body.keyPoint.right_ear.x = body.keypoints[BC.BODY_RIGHT_EAR][BC.PIXEL_X]
    converted_body.keyPoint.right_ear.y = body.keypoints[BC.BODY_RIGHT_EAR][BC.PIXEL_Y]

    #left_ear
    converted_body.keyPoint.left_ear.x = body.keypoints[BC.BODY_LEFT_EAR][BC.PIXEL_X]
    converted_body.keyPoint.left_ear.y = body.keypoints[BC.BODY_LEFT_EAR][BC.PIXEL_Y]

                
    x, y, width, height = body.mouse_left.get_box()
    converted_body.gesture.box_left.x = x
    converted_body.gesture.box_left.y = y
    converted_body.gesture.box_left.width = width
    converted_body.gesture.box_left.height = height

    x, y, width, height = body.mouse_right.get_box()
    converted_body.gesture.box_right.x = x
    converted_body.gesture.box_right.y = y
    converted_body.gesture.box_right.width = width
    converted_body.gesture.box_right.height = height


    converted_body.gesture.lw_rn = body.is_lw_rn_connected()
    converted_body.gesture.lw_rw = body.is_lw_rw_connected()
    converted_body.gesture.lw_bc = body.is_lw_bc_connected()
    converted_body.gesture.lw_bb = body.is_lw_bb_connected()

    x, y = body.get_pos_right()
    converted_body.gesture.pos_right.x = round((x + 0.5), 2)
    converted_body.gesture.pos_right.y = round((y + 0.5), 2)


    x, y = body.get_pos_left()
    converted_body.gesture.pos_left.x = round((x + 0.5), 2)
    converted_body.gesture.pos_left.y = round((y + 0.5), 2)

    return converted_body



