import numpy as np
import cv2 as cv
from time import perf_counter

from skyeye.utils.opencv import wait_key
from skyeye.utils.camera import CameraBuilder
from pymatting import estimate_alpha_cf, estimate_foreground_ml, blend

from trimap_generator import TrimapGenerator

def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

def normalize(image):

    out = image/255.0
    out = np.array(out, dtype=np.float64)

    return out

def unnormalize(image):

    out = image*255
    out = np.array(out, dtype=np.uint8)

    return out

def blend_with_background(fg, alpha):

    bg = np.ones(image.shape, dtype=np.float32)

    out = blend(fg, bg, alpha)
    out = unnormalize(out)

    return out

def bs_color(image, bg):

    im_b = image[:, :, 0]
    im_g = image[:, :, 1]
    im_r = image[:, :, 2]

    bg_b = bg[:, :, 0]
    bg_g = bg[:, :, 1]
    bg_r = bg[:, :, 2]

    diff_b = im_b.astype('float32') - bg_b.astype('float32')
    diff_g = im_g.astype('float32') - bg_g.astype('float32')
    diff_r = im_r.astype('float32') - bg_r.astype('float32')

    diff = np.sqrt(diff_b*diff_b + diff_g*diff_g + diff_r*diff_r)  
    max_value = np.max(diff)

    diff = diff / max_value * 255
    diff = np.array(diff, dtype=np.uint8)

    return diff

class BsMultiFrames:

    def __init__(self, num_frames=5):

        self.num_frames = num_frames 
        self.frames = []
        self.frame = None
        self.bg_frame = None
        self.diff = None
        self.mask = None

    def set_background(self, bg_frame):
        self.bg_frame = bg_frame

    def update(self, frame):

        self.frame = frame
        self.frames.append(self.frame)
        if len(self.frames) > self.num_frames:
            self.frames.pop(0)

        diff_avg = np.zeros(frame.shape, dtype=np.float32)
        for f in self.frames:
            f = f.astype('float32')
            bg = self.bg_frame.astype('float32')
            diff = f - bg
            #diff = np.abs(f - bg)
            diff_avg += diff

        diff_avg /= self.num_frames    
        diff_avg = np.abs(diff_avg)
        diff_avg = np.where(diff_avg < 255, diff_avg, 255)
        self.diff = diff_avg.astype('uint8')

    def get_diff(self):
        return self.diff 

def get_body_mask(mask): 

    contours, hierarchy = cv.findContours(mask, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

    height = mask.shape[0]  
    width = mask.shape[1]  
    img_area = height*width
    min_area = img_area * 0.005
    body_area = 0

    candidates = []
    index = -1
    body_index = -1
    for c in contours:
        area = cv.contourArea(c)

        if area > min_area:
            index += 1
            candidates.append(c)

            if area > body_area:
                body_area = area
                body_index = index

    body_mask = np.zeros(mask.shape, dtype=np.uint8)   
    for i, c in enumerate(candidates):
        if i == body_index:
            cv.fillPoly(body_mask, pts=[c], color=255)
        else:    
            cv.fillPoly(body_mask, pts=[c], color=0)

    return body_mask



if __name__ == '__main__':

    camera = CameraBuilder.buildWithYaml('camera.yaml')
    camera.open()
    frame_width = camera.width
    frame_height = camera.height

 
    bs_multi_frames = BsMultiFrames(num_frames=30)

    subtractor = cv.createBackgroundSubtractorMOG2(history=50, detectShadows=True)
    #subtractor = cv.createBackgroundSubtractorKnn(detectShadows=True)
    #subtractor = cv.bgsegm.createBackgroundSubtractorGSOC()
    bg_frame = None
    trimap_generator = TrimapGenerator(num_erosion=5, num_dilation=5)

    image = None
    trimap = None

    frame_count = 0
    while True:

        bg = None
        while True: # Grab background

            camera.process()
            frame = camera.color_image.copy()

            cv.imshow("Win", frame)

            # Press 'b' to take a snapshot as the background 
            key = wait_key(1)
            if key == ord('b'):
                bg_frame = cv.imread('background.png')
                break
            elif key == ord('q'):
                exit()

        while True: # Matting

            camera.process()
            frame = camera.color_image.copy()

            frame_count += 1
            print("frame_count = {}".format(frame_count))
            time_start = perf_counter()

            # Binary mask
            gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
            gray = cv.GaussianBlur(gray, (21, 21), 0)
            fg_mask = subtractor.apply(gray)

            #bg_gray = cv.cvtColor(bg_frame, cv.COLOR_BGR2GRAY)
            #bg_gray = cv.GaussianBlur(bg_gray, (21, 21), 0)
            #diff = cv.absdiff(gray, bg_gray)

            '''  
            bs_multi_frames.set_background(bg_gray)
            bs_multi_frames.update(gray)
            diff = bs_multi_frames.get_diff()
            '''

            frame_blur = cv.GaussianBlur(frame, (21, 21), 0)
            bg_blur = cv.GaussianBlur(bg_frame, (21, 21), 0)
            diff = bs_color(frame_blur, bg_blur)

            thresh = cv.threshold(diff, 20, 255, cv.THRESH_BINARY)[1]
            thresh = cv.dilate(thresh, None, iterations=3)
            thresh = cv.erode(thresh, None, iterations=3)
            mask = thresh

            mask = get_body_mask(mask) 

            # Generate trimap
            trimap_im = trimap_generator.generate(mask)
            trimap = normalize(trimap_im)
            cv.imwrite('trimap.png', trimap_im)

            image = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
            image = normalize(image)

            # Estimate alpha
            alpha = estimate_alpha_cf(image, trimap)

            alpha_im = unnormalize(alpha)
            cv.imwrite('alpha.png', alpha_im)

            # Blend image with white background
            fg = normalize(frame)
            blended = blend_with_background(fg, alpha)
            cv.imwrite('blended.png', blended)

            time_end = perf_counter()
            time_duration = time_end - time_start
            fps = int(1.0/time_duration)

            #image_out = diff
            #image_out = fg_mask
            image_out = mask
            #image_out = trimap
            #image_out = blended
            
            # Show image 
            text_x = 20
            text_y = 20
            text_y_shift = 20

            msg = "fps: {}".format(fps)
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            #cv.imshow("fg_mask", fg_mask)
            cv.imshow("win", image_out)

            # Exit while 'q' is pressed
            key = wait_key(1)
            if key == ord('b'):
                break
            elif key == ord('q'):
                exit()


    # cleanup the camera and close any open windows
    camera.close()

    cv.destroyAllWindows()
