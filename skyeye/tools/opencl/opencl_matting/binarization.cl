void rgb_to_hsv(unsigned char r, unsigned char g, unsigned char b, unsigned char* hue, unsigned char* sat, unsigned char* val) {

    float fR = ((float)r / 255.0);
    float fG = ((float)g / 255.0);
    float fB = ((float)b / 255.0);

    //printf("fR:%.2f, fG:%.2f, fB:%.2f\n", fR, fG, fB);


    float fH = 0.0f;
    float fS = 0.0f;
    float fV = 0.0f;

    float fCMax = max(max(fR, fG), fB);
    float fCMin = min(min(fR, fG), fB);
    float fDelta = fCMax - fCMin;

    if (fDelta > 0)
    {

        if (fCMax == fR)
            fH = 60 * (fmod(((fG - fB) / fDelta), 6));
        else if (fCMax == fG)
            fH = 60 * (((fB - fR) / fDelta) + 2);
        else if (fCMax == fB)
            fH = 60 * (((fR - fG) / fDelta) + 4);


        if (fCMax > 0)
            fS = fDelta / fCMax;
        else
            fS = 0;
        fV = fCMax;
    }
    else 
    {
        fH = 0;
        fS = 0;
        fV = fCMax;
    }

    if (fH < 0)
        fH = 360 + fH;

    unsigned int h = (180 * (fH / 360));
    unsigned int s = (255 * fS);
    unsigned int v = (255 * fV);

    //printf("fH:%.2f, fS:%.2f, fV:%.2f\n", fH, fS, fV);
    //printf("h:%d, s:%d, v:%d\n", h, s, v);

    *hue = (unsigned char)h;
    *sat = (unsigned char)s;
    *val = (unsigned char)v;


}

__kernel void binarization(
            __global uchar* src,
            __constant uchar2* hue_bound,
            __constant uchar2* sat_bound,
            __constant uchar2* val_bound,
            __constant uint* dropper_size,
            __constant uint* p_width,
            __constant uint* p_height,
            __global uchar* dst) {

    uint x = get_global_id(0);
    uint y = get_global_id(1);

    uint width = *p_width;
    uint height = *p_height;

    uint index = y * (width) + x;

    unsigned char r = 0, g = 0, b = 0, a = 0;
    unsigned char hue = 0, sat = 0, val = 0;

    r = src[index*4];
    g = src[index*4+1];
    b = src[index*4+2];
    a = src[index*4+3];

    //printf("r:%d, g:%d, b:%d, a:%d\n", r, g, b, a);

    rgb_to_hsv(r, g, b, &hue, &sat, &val);

    //printf("hue:%d, sat:%d, val:%d\n", hue, sat, val);

    uint size = *dropper_size;

    //printf("size:%d\n", size);



    bool is_range = true;

    for(int i = 0; i < size; ++i) {

        uchar hue_min = hue_bound[i].x;
        uchar hue_max = hue_bound[i].y;

        uchar sat_min = sat_bound[i].x;
        uchar sat_max = sat_bound[i].y;

        uchar val_min = val_bound[i].x;
        uchar val_max = val_bound[i].y;

        //printf("(%d) - %d, %d, %d, %d, %d, %d\n", i,hue_min, hue_max, sat_min, sat_max, val_min, val_max);


        if((hue > hue_max) || (hue < hue_min) ||
           (sat > sat_max) || (sat < sat_min) ||
           (val > val_max) || (val < val_min))
        {
            is_range = false;
            break;
        }

    }

    if(!is_range)
        dst[index] = 255;
    else
        dst[index] = 0;
}
