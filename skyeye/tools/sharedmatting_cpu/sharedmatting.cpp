#include <stdio.h>
//#include "opencv2/opencv.hpp"
#include "sharedmatting.h"
#include "timer.h"

using namespace cv;
using namespace std;


int getDataIndex(int width, int channels, size_t i, size_t j, size_t c)
{
    return i * channels*width + j * channels + c;
}

int getTrimapIndex(int width, size_t i, size_t j)
{
    return i * width + j;
}

int getAlphaIndex(int width, size_t i, size_t j)
{
    return i * width + j;
}

void copyColorPoint(ColorPoint* target, ColorPoint* src) {

    target->val[0] = src->val[0];
    target->val[1] = src->val[1];
    target->val[2] = src->val[2];
    target->val[3] = src->val[3];
}

PixelPoint createPixelPoint(int x, int y)
{
    PixelPoint p;
    p.x = x;
    p.y = y;

    return p;
}

ColorPoint createColorPoint(double v0, double v1, double v2) {

    ColorPoint p;
    p.val[0] = v0;
    p.val[1] = v1;
    p.val[2] = v2;

    return p;
}


//构造函数
SharedMatting::SharedMatting()
{
    kI = 10;
    kC = 5.0;
    kG = 4;  //each unknown p gathers at most kG forground and background samples
    m_unknownPixels.clear();
    tuples.clear();

}
//析构函数
SharedMatting::~SharedMatting()
{
    m_image.release();
    m_trimap.release();
    m_alpha.release();
    m_unknownPixels.clear();
    tuples.clear();
    ftuples.clear();

    for (int i = 0; i < m_height; ++i)
    {
        delete[] tri[i];
        //delete[] unknownIndex[i];
        delete[] alpha[i];
    }
    delete[] tri;
    //delete[] unknownIndex;
    delete[] alpha;
}

void SharedMatting:: initialize() 
{

    m_alphaData = (uchar*)malloc(m_width*m_height * sizeof(uchar));

    m_unknownIndexes = (int*)malloc(m_width*m_height * sizeof(int));

    //m_unknownPixels = (PixelPoint*)malloc(numUnknownPixelsMax * sizeof(PixelPoint));

    m_tuples = (Tuple*)malloc(numUnknownPixelsMax * sizeof(Tuple));

    m_ftuples = (Ftuple*)malloc(numUnknownPixelsMax * sizeof(Ftuple));

}

void SharedMatting:: finalize() 
{

    //if (m_alphaData) free(m_alphaData);
     
    if (m_unknownIndexes) free(m_unknownIndexes);

    //if (m_unknownPixels) free(m_unknownPixels);

    if (m_tuples) free(m_tuples);

    if (m_ftuples) free(m_ftuples);

}

void SharedMatting::loadImage(Mat img)
{
    m_image = img;
    if (!m_image.data)
    {
        cout << "Loading Image Failed!" << endl;
        exit(-1);
    }
    m_height     = m_image.rows;
    m_width      = m_image.cols;
    m_channels   = m_image.channels();
    m_step       = m_image.step1();
    m_imageData = (uchar*)m_image.data;

    //unknownIndex  = new int*[m_height];
    tri           = new int*[m_height];
    alpha         = new int*[m_height];

    for(int i = 0; i < m_height; ++i)
    {
        //unknownIndex[i] = new int[m_width];
        tri[i]          = new int[m_width];
        alpha[i]        = new int[m_width];
    }

    m_alpha.create(Size(m_width, m_height), CV_8UC1);

    numUnknownPixelsMax = m_width * m_height;
}


void SharedMatting::loadTrimap(Mat img)
{
    m_trimap = img; 
    if (!m_trimap.data)
    {
        cout << "Loading Trimap Failed!" << endl;
        exit(-1);
    }

    m_trimapData = (uchar*)m_trimap.data;
}

/*
void SharedMatting::expandKnown()
{
    int i, j;
    int value;
    uchar * d   = (uchar *)m_trimap.data;

    for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            value = d[i * m_width + j];
            tri[i][j] = value;

            if (value != 0 && value != 255)
            {
                PixelPoint lp;
                lp.x = i;
                lp.y = j;
                m_unknownPixels.push_back(lp);
            }

        }
    }

}
*/

void SharedMatting::expandKnown()
{
    vector<LabelPoint> vp;
    int kc2 = kC * kC;
    vp.clear();
    uchar * d   = (uchar *)m_trimap.data;

    for (int i = 0; i < m_height; ++i)
    {
        for (int j = 0; j < m_width; ++j)
        {
            tri[i][j] = d[i * m_width + j];
        }
    }

    for (int i = 0; i < m_height; ++i)
    {
        for (int j = 0; j < m_width; ++j)
        {

            if (tri[i][j] != 0 && tri[i][j] != 255)
            {

                int label = -1;
                double dmin = 10000.0;
                bool flag = false;
                //int pb = data[i * m_step + j * m_channels];
                //int pg = data[i * m_step + j * m_channels + 1];
                //int pr = data[i * m_step + j * m_channels + 2];

                int pb = m_imageData[i * m_step + j * m_channels];
                int pg = m_imageData[i * m_step + j * m_channels + 1];
                int pr = m_imageData[i * m_step + j * m_channels + 2];
                ColorPoint p = createColorPoint(pb, pg, pr);

                for (int k = 0; (k <= kI) && !flag; ++k)
                {
                    int k1 = max(0, i - k);
                    int k2 = min(i + k, m_height - 1);
                    int l1 = max(0, j - k);
                    int l2 = min(j + k, m_width - 1);

                    for (int l = k1; (l <= k2) && !flag; ++l)
                    {
                        double dis;
                        double gray;


                        gray = tri[l][l1];
                        if (gray == 0 || gray == 255)
                        {
                            dis = dP(createPixelPoint(i, j), createPixelPoint(l, l1));
                            if (dis > kI)
                            {
                                continue;
                            }
                            int qb = m_imageData[l * m_step + l1 * m_channels];
                            int qg = m_imageData[l * m_step + l1 * m_channels + 1];
                            int qr = m_imageData[l * m_step + l1 * m_channels + 2];
                            ColorPoint q = createColorPoint(qb, qg, qr);

                            double distanceColor = distanceColor2(p, q);
                            if (distanceColor <= kc2)
                            {
                                flag = true;
                                label = gray;
                            }
                        }
                        if (flag)
                        {
                            break;
                        }

                        gray = tri[l][l2];
                        if (gray == 0 || gray == 255)
                        {
                            dis = dP(createPixelPoint(i, j), createPixelPoint(l, l2));
                            if (dis > kI)
                            {
                                continue;
                            }
                            int qb = m_imageData[l * m_step + l2 * m_channels];
                            int qg = m_imageData[l * m_step + l2 * m_channels + 1];
                            int qr = m_imageData[l * m_step + l2 * m_channels + 2];
                            ColorPoint q = createColorPoint(qb, qg, qr);

                            double distanceColor = distanceColor2(p, q);
                            if (distanceColor <= kc2)
                            {
                                flag = true;
                                label = gray;
                            }
                        }
                    }

                    for (int l = l1; (l <= l2) && !flag; ++l)
                    {
                        double dis;
                        double gray;

                        gray = tri[k1][l];
                        if (gray == 0 || gray == 255)
                        {
                            dis = dP(createPixelPoint(i, j), createPixelPoint(k1, l));
                            if (dis > kI)
                            {
                            continue;
                            }
                            int qb = m_imageData[k1 * m_step + l * m_channels];
                            int qg = m_imageData[k1 * m_step + l * m_channels + 1];
                            int qr = m_imageData[k1 * m_step + l * m_channels + 2];
                            ColorPoint q = createColorPoint(qb, qg, qr);

                            double distanceColor = distanceColor2(p, q);
                            if (distanceColor <= kc2)
                            {
                                flag = true;
                                label = gray;
                            }
                        }
                        gray = tri[k2][l];
                        if (gray == 0 || gray == 255)
                        {
                            dis = dP(createPixelPoint(i, j), createPixelPoint(k2, l));
                            if (dis > kI)
                            {
                            continue;
                            }
                            int qb = m_imageData[k2 * m_step + l * m_channels];
                            int qg = m_imageData[k2 * m_step + l * m_channels + 1];
                            int qr = m_imageData[k2 * m_step + l * m_channels + 2];
                            ColorPoint q = createColorPoint(qb, qg, qr);

                            double distanceColor = distanceColor2(p, q);
                            if (distanceColor <= kc2)
                            {
                                flag = true;
                                label = gray;
                            }
                        }
                    }
                }
                if (label != -1)
                {
                    LabelPoint lp;
                    lp.x = i;
                    lp.y = j;
                    lp.label = label;
                    vp.push_back(lp);
                }
                else
                {
                    PixelPoint lp;
                    lp.x = i;
                    lp.y = j;
                    m_unknownPixels.push_back(lp);
                }

            }
        }
    }

    vector<LabelPoint>::iterator it;
    for (it = vp.begin(); it != vp.end(); ++it)
    {
        int ti = it->x;
        int tj = it->y;
        int label = it->label;
        tri[ti][tj] = label;
    }
    vp.clear();
}

double SharedMatting::comalpha(ColorPoint c, ColorPoint f, ColorPoint b)
{
    double alpha = ((c.val[0] - b.val[0]) * (f.val[0] - b.val[0]) +
                    (c.val[1] - b.val[1]) * (f.val[1] - b.val[1]) +
                    (c.val[2] - b.val[2]) * (f.val[2] - b.val[2]))
                 / ((f.val[0] - b.val[0]) * (f.val[0] - b.val[0]) +
                    (f.val[1] - b.val[1]) * (f.val[1] - b.val[1]) +
                    (f.val[2] - b.val[2]) * (f.val[2] - b.val[2]) + 0.0000001);
    return min(1.0, max(0.0, alpha));
}

double SharedMatting::mP(int i, int j, ColorPoint f, ColorPoint b)
{
    int bc = m_imageData[i * m_step + j * m_channels];
    int gc = m_imageData[i * m_step + j * m_channels + 1];
    int rc = m_imageData[i * m_step + j * m_channels + 2];
    ColorPoint c = createColorPoint(bc, gc, rc);

    double alpha = comalpha(c, f, b);

    double result = sqrt((c.val[0] - alpha * f.val[0] - (1 - alpha) * b.val[0]) * (c.val[0] - alpha * f.val[0] - (1 - alpha) * b.val[0]) +
                         (c.val[1] - alpha * f.val[1] - (1 - alpha) * b.val[1]) * (c.val[1] - alpha * f.val[1] - (1 - alpha) * b.val[1]) +
                         (c.val[2] - alpha * f.val[2] - (1 - alpha) * b.val[2]) * (c.val[2] - alpha * f.val[2] - (1 - alpha) * b.val[2]));
    return result / 255.0;
}

double SharedMatting::nP(int i, int j, ColorPoint f, ColorPoint b)
{
    int i1 = max(0, i - 1);
    int i2 = min(i + 1, m_height - 1);
    int j1 = max(0, j - 1);
    int j2 = min(j + 1, m_width - 1);

    double  result = 0;

    for (int k = i1; k <= i2; ++k)
    {
        for (int l = j1; l <= j2; ++l)
        {
            double m = mP(k, l, f, b);
            result += m * m;
        }
    }

    return result;
}

double SharedMatting::eP(int i1, int j1, int i2, int j2)
{

    //int flagi = 1, flagj = 1;

    double ci = i2 - i1;
    double cj = j2 - j1;
    double z  = sqrt(ci * ci + cj * cj);

    double ei = ci / (z + 0.0000001);
    double ej = cj / (z + 0.0000001);

    double stepinc = min(1 / (abs(ei) + 1e-10), 1 / (abs(ej) + 1e-10));

    double result = 0;

    int b = m_imageData[i1 * m_step + j1 * m_channels];
    int g = m_imageData[i1 * m_step + j1 * m_channels + 1];
    int r = m_imageData[i1 * m_step + j1 * m_channels + 2];
    ColorPoint pre = createColorPoint(b, g, r);

    int ti = i1;
    int tj = j1;

    for (double t = 1; ;t += stepinc)
    {
        double inci = ei * t;
        double incj = ej * t;
        int i = int(i1 + inci + 0.5);
        int j = int(j1 + incj + 0.5);

        double z = 1;

        int b = m_imageData[i * m_step + j * m_channels];
        int g = m_imageData[i * m_step + j * m_channels + 1];
        int r = m_imageData[i * m_step + j * m_channels + 2];
        ColorPoint cur = createColorPoint(b, g, r);

        if (ti - i > 0 && tj - j == 0)
        {
            z = ej;
        }
        else if(ti - i == 0 && tj - j > 0)
        {
            z = ei;
        }

        result += ((cur.val[0] - pre.val[0]) * (cur.val[0] - pre.val[0]) +
                   (cur.val[1] - pre.val[1]) * (cur.val[1] - pre.val[1]) +
                   (cur.val[2] - pre.val[2]) * (cur.val[2] - pre.val[2])) * z;
        pre = cur;

        ti = i;
        tj = j;

        if(abs(ci) >= abs(inci) || abs(cj) >= abs(incj))
            break;

    }

    return result;
}

double SharedMatting::pfP(PixelPoint p, vector<PixelPoint>& f, vector<PixelPoint>& b)
{
    double fmin = 1e10;
    vector<PixelPoint>::iterator it;
    for (it = f.begin(); it != f.end(); ++it)
    {
        double fp = eP(p.x, p.y, it->x, it->y);
        if (fp < fmin)
        {
            fmin = fp;
        }
    }

    double bmin = 1e10;
    for (it = b.begin(); it != b.end(); ++it)
    {
        double bp = eP(p.x, p.y, it->x, it->y);
        if (bp < bmin)
        {
            bmin = bp;
        }
    }
    return bmin / (fmin + bmin + 1e-10);
}

double SharedMatting::aP(int i, int j, double pf, ColorPoint f, ColorPoint b)
{
    int bc = m_imageData[i * m_step + j * m_channels];
    int gc = m_imageData[i * m_step + j * m_channels + 1];
    int rc = m_imageData[i * m_step + j * m_channels + 2];
    ColorPoint c = createColorPoint(bc, gc, rc);

    double alpha = comalpha(c, f, b);

    return pf + (1 - 2 * pf) * alpha;
}

double SharedMatting::dP(PixelPoint s, PixelPoint d)
{
    return sqrt(double((s.x - d.x) * (s.x - d.x) + (s.y - d.y) * (s.y - d.y)));
}

double SharedMatting::gP(PixelPoint p, PixelPoint fp, PixelPoint bp, double pf)
{
    int bc, gc, rc;
    bc = m_imageData[fp.x * m_step + fp.y * m_channels];
    gc = m_imageData[fp.x * m_step + fp.y * m_channels + 1];
    rc = m_imageData[fp.x * m_step + fp.y * m_channels + 2];
    ColorPoint f = createColorPoint(bc, gc, rc);
    bc = data[bp.x * m_step + bp.y * m_channels];
    gc = data[bp.x * m_step + bp.y * m_channels + 1];
    rc = data[bp.x * m_step + bp.y * m_channels + 2];
    ColorPoint b = createColorPoint(bc, gc, rc);


    double tn = pow(nP(p.x, p.y, f, b), 3);
    double ta = pow(aP(p.x, p.y, pf, f, b), 2);
    double tf = dP(p, fp);
    double tb = pow(dP(p, bp), 4);

    return tn * ta * tf * tb;

}

double SharedMatting::gP(PixelPoint p, PixelPoint fp, PixelPoint bp, double dpf, double pf)
{
    int bc, gc, rc;
    bc = m_imageData[fp.x * m_step + fp.y * m_channels];
    gc = m_imageData[fp.x * m_step + fp.y * m_channels + 1];
    rc = m_imageData[fp.x * m_step + fp.y * m_channels + 2];
    ColorPoint f = createColorPoint(bc, gc, rc);
    bc = m_imageData[bp.x * m_step + bp.y * m_channels];
    gc = m_imageData[bp.x * m_step + bp.y * m_channels + 1];
    rc = m_imageData[bp.x * m_step + bp.y * m_channels + 2];
    ColorPoint b = createColorPoint(bc, gc, rc);


    double tn = pow(nP(p.x, p.y, f, b), 3);
    double ta = pow(aP(p.x, p.y, pf, f, b), 2);
    double tf = dpf;
    double tb = pow(dP(p, bp), 4);

    return tn * ta * tf * tb;
}

double SharedMatting::sigma2(PixelPoint p)
{
    int xi = p.x;
    int yj = p.y;
    int bc, gc, rc;
    bc = m_imageData[xi * m_step + yj * m_channels];
    gc = m_imageData[xi * m_step + yj * m_channels + 1];
    rc = m_imageData[xi * m_step + yj * m_channels + 2];
    ColorPoint pc = createColorPoint(bc, gc, rc);

    int i1 = max(0, xi - 2);
    int i2 = min(xi + 2, m_height - 1);
    int j1 = max(0, yj - 2);
    int j2 = min(yj + 2, m_width - 1);

    double result = 0;
    int    num    = 0;

    for (int i = i1; i <= i2; ++i)
    {
        for (int j = j1; j <= j2; ++j)
        {
            int bc, gc, rc;
            bc = m_imageData[i * m_step + j * m_channels];
            gc = m_imageData[i * m_step + j * m_channels + 1];
            rc = m_imageData[i * m_step + j * m_channels + 2];
            ColorPoint temp = createColorPoint(bc, gc, rc);
            result += distanceColor2(pc, temp);
            ++num;
        }
    }

    return result / (num + 1e-10);

}

double SharedMatting::distanceColor2(ColorPoint cs1, ColorPoint cs2)
{
    return (cs1.val[0] - cs2.val[0]) * (cs1.val[0] - cs2.val[0]) +
           (cs1.val[1] - cs2.val[1]) * (cs1.val[1] - cs2.val[1]) +
           (cs1.val[2] - cs2.val[2]) * (cs1.val[2] - cs2.val[2]);
}

void SharedMatting::sample(int width, int height, int numUnknownPixels,
    vector<vector<PixelPoint> > &F, vector<vector<PixelPoint> > &B)
{
    int a, b; 
    int i, j;
    int x, y, p, q;
    int gray;
    int angle;
    double z, ex, ey, t, step;

    vector<PixelPoint>::iterator iter;

    a=360/kG;
    b=1.7f*a/9;
    F.clear();
    B.clear();
    //w = m_image.cols;
    //h = m_image.rows;

    /* 
    // Obtain the number of unknown pixels 
    numUnknownPixels = 0;
    for(i = 0; i < width; i++) 
    {
        for(j = 0; j < height; i++)
        {
            if (trimap[i][j] != 0 && trimap[i][j] != 255) 
            {
                numUnknownPixels += 1;
            }

        } 
    }
    */

    for(iter=m_unknownPixels.begin();iter!=m_unknownPixels.end();++iter)
    {
        vector<PixelPoint> fPts,bPts;

        x=iter->x;
        y=iter->y;
        angle=(x+y)*b % a;
        for(i=0;i<kG;++i)
        {
            bool f1(false),f2(false);

            z=(angle+i*a)/180.0f*3.1415926f;
            ex=sin(z);
            ey=cos(z);
            step=min(1.0f/(abs(ex)+1e-10f),
                1.0f/(abs(ey)+1e-10f));

            for(t=0;;t+=step)
            {
                p=(int)(x + ex*t + 0.5f);
                q=(int)(y + ey*t + 0.5f);
                if(p < 0 || p >= height || q < 0 || q >= width)
                    break;

                gray=tri[p][q];
                if(!f1 && gray < 50)
                {
                    PixelPoint pt = createPixelPoint(p, q);
                    bPts.push_back(pt);
                    f1=true;
                }
                else
                    if(!f2 && gray > 200)
                    {
                        PixelPoint pt = createPixelPoint(p, q);
                        fPts.push_back(pt);
                        f2=true;
                    }
                    else
                        if(f1 && f2)
                            break;
            }
        }

        F.push_back(fPts);
        B.push_back(bPts);
    }
}

void SharedMatting::gathering()
{
    vector<PixelPoint> f;
    vector<PixelPoint> b;
    vector<PixelPoint>::iterator it;
    vector<PixelPoint>::iterator it1;
    vector<PixelPoint>::iterator it2;

    vector<vector<PixelPoint> > F,B;

    int k;

    sample(m_width, m_height, numUnknownPixels, F, B);

    int index = 0;
    double a;
    int size = m_unknownPixels.size();

    for (int m = 0; m < size; ++m)
    {
        int ix = m_unknownPixels[m].x;
        int iy = m_unknownPixels[m].y;

        double pfp = pfP(createPixelPoint(ix, iy), F[m], B[m]);
        double gmin = 1.0e10;

        PixelPoint tf;
        PixelPoint tb;

        bool flag = false;
        bool first = true;

        for (it1 = F[m].begin(); it1 != F[m].end(); ++it1)
        {
            double dpf = dP(createPixelPoint(ix, iy), *(it1));
            for (it2 = B[m].begin(); it2 < B[m].end(); ++it2)
            {

                double gp = gP(createPixelPoint(ix, iy), *(it1), *(it2), dpf, pfp);
                if (gp < gmin)
                {
                    gmin = gp;
                    tf   = *(it1);
                    tb   = *(it2);
                    flag = true;
                }
            }
        }

        Tuple st;
        st.flag = -1;
        if (flag)
        {
            int bc, gc, rc;
            bc = m_imageData[tf.x * m_step +  tf.y * m_channels];
            gc = m_imageData[tf.x * m_step +  tf.y * m_channels + 1];
            rc = m_imageData[tf.x * m_step +  tf.y * m_channels + 2];
            st.flag = 1;
            st.f = createColorPoint(bc, gc, rc);
            bc = m_imageData[tb.x * m_step +  tb.y * m_channels];
            gc = m_imageData[tb.x * m_step +  tb.y * m_channels + 1];
            rc = m_imageData[tb.x * m_step +  tb.y * m_channels + 2];
            st.b      = createColorPoint(bc, gc, rc);
            st.sigmaf = sigma2(tf);
            st.sigmab = sigma2(tb);
        }

        tuples.push_back(st);
        //unknownIndex[i][j] = index;
        m_unknownIndexes[getTrimapIndex(m_width, iy, ix)] = index;
        ++index;
    }
    f.clear();
    b.clear();

}

void SharedMatting::refineSample()
{

    ftuples.resize(m_width * m_height + 1);
    for (int i = 0; i < m_height; ++i)
    {
        for (int j = 0; j < m_width; ++j)
        {
            int b, g, r;
            b = m_imageData[i * m_step +  j* m_channels];
            g = m_imageData[i * m_step +  j * m_channels + 1];
            r = m_imageData[i * m_step +  j * m_channels + 2];
            ColorPoint c = createColorPoint(b, g, r);
            int indexf = i * m_width + j;
            int gray = tri[i][j];
            if (gray == 0 )
            {
                ftuples[indexf].f = c;
                ftuples[indexf].b = c;
                ftuples[indexf].alphar = 0;
                ftuples[indexf].confidence = 1;
                alpha[i][j] = 0;
            }
            else if (gray == 255)
            {
                ftuples[indexf].f = c;
                ftuples[indexf].b = c;
                ftuples[indexf].alphar = 1;
                ftuples[indexf].confidence = 1;
                alpha[i][j] = 255;
            }

        }
    }
    vector<PixelPoint>::iterator it;
    for (it = m_unknownPixels.begin(); it != m_unknownPixels.end(); ++it)
    {
        int xi = it->x;
        int yj = it->y;
        int i1 = max(0, xi - 5);
        int i2 = min(xi + 5, m_height - 1);
        int j1 = max(0, yj - 5);
        int j2 = min(yj + 5, m_width - 1);

        double minvalue[3] = {1e10, 1e10, 1e10};
        //PixelPoint p[3];
        PixelPoint *p = new PixelPoint[3];
        int num = 0;
        for (int k = i1; k <= i2; ++k)
        {
            for (int l = j1; l <= j2; ++l)
            {
                int temp = tri[k][l];

                if (temp == 0 || temp == 255)
                {
                    continue;
                }

                //int index = unknownIndex[k][l];
                int index = m_unknownIndexes[getTrimapIndex(m_width, l, k)];
                Tuple t   = tuples[index];
                if (t.flag == -1)
                {
                    continue;
                }

                double m  = mP(xi, yj, t.f, t.b);

                if (m > minvalue[2])
                {
                    continue;
                }

                if (m < minvalue[0])
                {
                    minvalue[2] = minvalue[1];
                    p[2]   = p[1];

                    minvalue[1] = minvalue[0];
                    p[1]   = p[0];

                    minvalue[0] = m;
                    p[0].x = k;
                    p[0].y = l;

                    ++num;

                }
                else if (m < minvalue[1])
                {
                    minvalue[2] = minvalue[1];
                    p[2]   = p[1];

                    minvalue[1] = m;
                    p[1].x = k;
                    p[1].y = l;

                    ++num;
                }
                else if (m < minvalue[2])
                {
                    minvalue[2] = m;
                    p[2].x = k;
                    p[2].y = l;

                    ++num;
                }
            }
        }

        num = min(num, 3);


        double fb = 0;
        double fg = 0;
        double fr = 0;
        double bb = 0;
        double bg = 0;
        double br = 0;
        double sf = 0;
        double sb = 0;

        for (int k = 0; k < num; ++k)
        {
            //int i  = unknownIndex[p[k].x][p[k].y];
            int i = m_unknownIndexes[getTrimapIndex(m_width, p[k].y, p[k].x)];
            fb += tuples[i].f.val[0];
            fg += tuples[i].f.val[1];
            fr += tuples[i].f.val[2];
            bb += tuples[i].b.val[0];
            bg += tuples[i].b.val[1];
            br += tuples[i].b.val[2];
            sf += tuples[i].sigmaf;
            sb += tuples[i].sigmab;
        }

        fb /= (num + 1e-10);
        fg /= (num + 1e-10);
        fr /= (num + 1e-10);
        bb /= (num + 1e-10);
        bg /= (num + 1e-10);
        br /= (num + 1e-10);
        sf /= (num + 1e-10);
        sb /= (num + 1e-10);

        ColorPoint fc = createColorPoint(fb, fg, fr);
        ColorPoint bc = createColorPoint(bb, bg, br);
        int b, g, r;
        b = m_imageData[xi * m_step +  yj * m_channels];
        g = m_imageData[xi * m_step +  yj * m_channels + 1];
        r = m_imageData[xi * m_step +  yj * m_channels + 2];
        ColorPoint pc = createColorPoint(b, g, r);
        double df = distanceColor2(pc, fc);
        double db = distanceColor2(pc, bc);
        ColorPoint tf = fc;
        ColorPoint tb = bc;

        int index = xi * m_width + yj;
        if (df < sf)
        {
            fc = pc;
        }
        if (db < sb)
        {
            bc = pc;
        }
        if (fc.val[0] == bc.val[0] && fc.val[1] == bc.val[1] && fc.val[2] == bc.val[2])
        {
            ftuples[index].confidence = 0.00000001;
        }
        else
        {
            ftuples[index].confidence = exp(-10 * mP(xi, yj, tf, tb));
        }


        ftuples[index].f = fc;
        ftuples[index].b = bc;


        ftuples[index].alphar = max(0.0, min(1.0,comalpha(pc, fc, bc)));
    }
    tuples.clear();

}

void SharedMatting::localSmooth()
{
    vector<PixelPoint>::iterator it;
    double sig2 = 100.0 / (9 * 3.1415926);
    double r = 3 * sqrt(sig2);
    for (it = m_unknownPixels.begin(); it != m_unknownPixels.end(); ++it)
    {
        int xi = it->x;
        int yj = it->y;

        int i1 = max(0, int(xi - r));
        int i2 = min(int(xi + r), m_height - 1);
        int j1 = max(0, int(yj - r));
        int j2 = min(int(yj + r), m_width - 1);

        int indexp = xi * m_width + yj;
        Ftuple ptuple = ftuples[indexp];

        ColorPoint wcfsumup = createColorPoint(0., 0., 0.);
        ColorPoint wcbsumup = createColorPoint(0., 0., 0.);
        double wcfsumdown = 0;
        double wcbsumdown = 0;
        double wfbsumup   = 0;
        double wfbsundown = 0;
        double wasumup    = 0;
        double wasumdown  = 0;

        for (int k = i1; k <= i2; ++k)
        {
            for (int l = j1; l <= j2; ++l)
            {
                int indexq = k * m_width + l;
                Ftuple qtuple = ftuples[indexq];

                double d = dP(createPixelPoint(xi, yj), createPixelPoint(k, l));

                if (d > r)
                {
                    continue;
                }

                double wc;
                if (d == 0)
                {
                    wc = exp(-(d * d) / sig2) * qtuple.confidence;
                }
                else
                {
                    wc = exp(-(d * d) / sig2) * qtuple.confidence * abs(qtuple.alphar - ptuple.alphar);
                }
                wcfsumdown += wc * qtuple.alphar;
                wcbsumdown += wc * (1 - qtuple.alphar);

                wcfsumup.val[0] += wc * qtuple.alphar * qtuple.f.val[0];
                wcfsumup.val[1] += wc * qtuple.alphar * qtuple.f.val[1];
                wcfsumup.val[2] += wc * qtuple.alphar * qtuple.f.val[2];

                wcbsumup.val[0] += wc * (1 - qtuple.alphar) * qtuple.b.val[0];
                wcbsumup.val[1] += wc * (1 - qtuple.alphar) * qtuple.b.val[1];
                wcbsumup.val[2] += wc * (1 - qtuple.alphar) * qtuple.b.val[2];


                double wfb = qtuple.confidence * qtuple.alphar * (1 - qtuple.alphar);
                wfbsundown += wfb;
                wfbsumup   += wfb * sqrt(distanceColor2(qtuple.f, qtuple.b));

                double delta = 0;
                double wa;
                if (tri[k][l] == 0 || tri[k][l] == 255)
                {
                    delta = 1;
                }
                wa = qtuple.confidence * exp(-(d * d) / sig2) + delta;
                wasumdown += wa;
                wasumup   += wa * qtuple.alphar;
            }
        }

        int b, g, r;
        b = m_imageData[xi * m_step +  yj * m_channels];
        g = m_imageData[xi * m_step +  yj * m_channels + 1];
        r = m_imageData[xi * m_step +  yj * m_channels + 2];
        ColorPoint cp = createColorPoint(b, g, r);
        ColorPoint fp;
        ColorPoint bp;

        double dfb;
        double conp;
        double alp;

        bp.val[0] = min(255.0, max(0.0,wcbsumup.val[0] / (wcbsumdown + 1e-200)));
        bp.val[1] = min(255.0, max(0.0,wcbsumup.val[1] / (wcbsumdown + 1e-200)));
        bp.val[2] = min(255.0, max(0.0,wcbsumup.val[2] / (wcbsumdown + 1e-200)));

        fp.val[0] = min(255.0, max(0.0,wcfsumup.val[0] / (wcfsumdown + 1e-200)));
        fp.val[1] = min(255.0, max(0.0,wcfsumup.val[1] / (wcfsumdown + 1e-200)));
        fp.val[2] = min(255.0, max(0.0,wcfsumup.val[2] / (wcfsumdown + 1e-200)));

        //double tempalpha = comalpha(cp, fp, bp);
        dfb  = wfbsumup / (wfbsundown + 1e-200);

        conp = min(1.0, sqrt(distanceColor2(fp, bp)) / dfb) * exp(-10 * mP(xi, yj, fp, bp));
        alp  = wasumup / (wasumdown + 1e-200);

        double alpha_t = conp * comalpha(cp, fp, bp) + (1 - conp) * max(0.0, min(alp, 1.0));

        alpha[xi][yj] = alpha_t * 255;
    }
    ftuples.clear();
}

Mat SharedMatting::getAlpha()
{
    int h     = m_alpha.rows;
    int w     = m_alpha.cols;
    int s     = m_alpha.step1();

    uchar* d  = (uchar *)m_alpha.data;
    for (int i = 0; i < h; ++i)
        for (int j = 0; j < w; ++j)
            d[i*s+j] = alpha[i][j];

    return m_alpha;
}


Mat SharedMatting::normalize(Mat src)
{
    Mat out;
    src.convertTo(out, CV_32F, 1.0/255, 0);
    return out;
} 

Mat SharedMatting::unnormalize(Mat src)
{
    Mat out;
    src.convertTo(out, CV_8U, 255, 0);
    return out;
}

Mat SharedMatting::blend(Mat srcIn, Mat alphaIn, Mat bgIn) 
{
    Mat src = normalize(srcIn);
    Mat alphaC3;
    cvtColor(alphaIn, alphaC3, COLOR_GRAY2BGR);
    Mat alpha = normalize(alphaC3);
    Mat bg = normalize(bgIn);
    Mat tmp1, tmp2;
    
    Scalar ones = Scalar(1.0, 1.0, 1.0);

    multiply(alpha, src, tmp1, 1.0);
    multiply((ones - alpha), bg, tmp2, 1.0);
    Mat blended = tmp1 + tmp2;
      
    Mat out = unnormalize(blended);

    return out; 

};

Mat SharedMatting::getImageBlended()
{
    Mat alpha = getAlpha();
    Mat bg(m_image.size(), CV_8UC3, Scalar(255, 255, 255));
    Mat out = blend(m_image, alpha, bg);
    return out;
}

//主干方法
void SharedMatting::solveAlpha()
{
    clock_t start, finish;
    //expandKnown()
    start = clock();
    cout << "Expanding...";
    expandKnown();
    cout << "    over!!!" << endl;
    finish = clock();
    cout <<  double(finish - start) / (CLOCKS_PER_SEC * 2.5) << endl;

    //gathering()
    start = clock();
    cout << "Gathering...";
    gathering();
    cout << "    over!!!" << endl;
    finish = clock();
    cout <<  double(finish - start) / (CLOCKS_PER_SEC * 2.5) << endl;

    //refineSample()
    start = clock();
    cout << "Refining...";
    refineSample();
    cout << "    over!!!" << endl;
    finish = clock();
    cout <<  double(finish - start) / (CLOCKS_PER_SEC * 2.5) << endl;

    //localSmooth()
    start = clock();
    cout << "LocalSmoothing...";
    localSmooth();
    cout << "    over!!!" << endl;
    finish = clock();
    cout <<  double(finish - start) / (CLOCKS_PER_SEC * 2.5) << endl;

    getAlpha();
}
