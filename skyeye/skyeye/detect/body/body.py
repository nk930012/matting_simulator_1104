import numpy as np
from .body_model import BodyModel
from .dyn_gesture import DynGesture
from .connect_sensor import ConnectSensor
from .target_sensor import TargetSensor
from .air_mouse import AirMouse
from .get_distance import get_distance
from .exist import exist

def get_num_nonzeros(keypoints):

    n = 0
    for kp in keypoints:

        x, y = kp
        if x != 0:
            n += 1

    return n

class Body(BodyModel):

    def __init__(self):

        self.id = None
        self.right_gesture = None
        self.left_gesture = None
        self.len_c_default = 50 # Default characteristic length
        self.len_c = self.len_c_default # Characteristic length

        self.dyn_gesture = DynGesture()

        self.target_sensor = TargetSensor(num_c=30)
        self.lw_rn_sensor = ConnectSensor()
        self.lw_rw_sensor = ConnectSensor()
        self.lw_re_sensor = ConnectSensor()
        self.lw_rs_sensor = ConnectSensor()
        self.lw_bc_sensor = ConnectSensor()
        self.lw_bb_sensor = ConnectSensor()

        self.mouse_right = AirMouse(type_name='right')
        self.mouse_left = AirMouse(type_name='left')

    def get_ref_point(self):
        # Reference point.
        return self.get_neck()

    def get_body_center(self):

        neck = self.get_neck()
        x = neck[0]
        y = neck[1]
        y += 0.5*self.len_c

        return [x, y]

    def get_body_belly(self):

        neck = self.get_neck()
        x = neck[0]
        y = neck[1]
        y += 1.0*self.len_c

        return [x, y]

    def update(self, keypoints):

        self.set_keypoints(keypoints)

        shoulder_width = self.get_shoulder_width()
        self.set_len_c(shoulder_width)

        # Dynamic guesture
        self.dyn_gesture.set_len_c(self.len_c)
        p = self.get_right_wrist()
        self.dyn_gesture.update(p)

        # Sensors
        d = 0.25*self.len_c
        neck = self.get_neck()
        left_wrist = self.get_left_wrist()
        right_wrist = self.get_right_wrist()
        right_elbow = self.get_right_elbow()
        left_shoulder = self.get_left_shoulder()
        right_shoulder = self.get_right_shoulder()
        body_center = self.get_body_center()
        body_belly = self.get_body_belly()

        self.target_sensor.update(left_wrist, right_wrist, left_shoulder, right_shoulder, 0.333*self.len_c)
        self.lw_rn_sensor.update(left_wrist, neck, d)
        self.lw_rw_sensor.update(left_wrist, right_wrist, d)
        self.lw_re_sensor.update(left_wrist, right_elbow, d)
        self.lw_rs_sensor.update(left_wrist, right_shoulder, d)
        self.lw_bc_sensor.update(left_wrist, body_center, d)
        self.lw_bb_sensor.update(left_wrist, body_belly, d)

        # Air mouse
        self.mouse_right.update(right_shoulder, right_wrist, self.len_c)
        self.mouse_left.update(left_shoulder, left_wrist, self.len_c)

    def set_len_c(self, value):

        self.len_c = value

    def is_valid(self):

        neck = self.get_neck()
        hip = self.get_hip()

        neck_exist = exist(neck)
        hip_exist = exist(hip)
        height = self.get_height()

        if neck_exist and hip_exist and height > 0:
            out = True
        else:
            out = False

        return out

    def is_front(self):

        result = True
        if self.get_nose() is None:
            result = False

        return result

    def is_target(self):
        return self.target_sensor.is_triggered

    def get_right_wrist_bbox(self):

        len_c = self.len_c

        wrist = self.get_right_wrist()
        x = wrist[0]
        y = wrist[1]

        xa = int(x - 1.5*len_c)
        ya = int(y - 2.0*len_c)
        xz = xa + int(3.0*len_c + 0.5)
        yz = ya + int(3.0*len_c + 0.5)

        bbox = [xa, ya, xz, yz]

        return bbox

    def is_lw_rw_connected(self):
        return self.lw_rw_sensor.is_triggered

    def is_lw_rn_connected(self):
        return self.lw_rn_sensor.is_triggered

    def is_lw_re_connected(self):
        return self.lw_re_sensor.is_triggered

    def is_lw_rs_connected(self):
        return self.lw_rs_sensor.is_triggered

    def is_lw_bc_connected(self):
        return self.lw_bc_sensor.is_triggered

    def is_lw_bb_connected(self):
        return self.lw_bb_sensor.is_triggered

    def get_pos_right(self):
        return self.mouse_right.get_pos()

    def get_pos_left(self):
        return self.mouse_left.get_pos()

    def get_shoulder_width(self):

        left_shoulder = self.get_left_shoulder()
        right_shoulder = self.get_right_shoulder()

        return get_distance(left_shoulder, right_shoulder)

    def get_hip(self):

        left_hip = self.get_left_hip()
        right_hip = self.get_left_hip()

        x_left = left_hip[0]
        y_left = left_hip[1]

        x_right = right_hip[0]
        y_right = right_hip[1]

        x_out = 0.5*(x_left + x_right)
        y_out = 0.5*(y_left + y_right)

        hip = [x_out, y_out]

        return hip

    def get_height(self):

        neck = self.get_neck()
        hip = self.get_hip()
        neck_exist = exist(neck)
        hip_exist = exist(hip)

        if neck_exist and hip_exist:
            d = get_distance(neck, hip)
        else:
            d = 0

        height = 2.0*d

        return height
