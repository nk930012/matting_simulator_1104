#ifndef COLOR_SHARED_MATTING_H
#define COLOR_SHARED_MATTING_H

#define COLOR_SHARED_MATTING_API __declspec(dllexport)


#include "trimap_generator_gpu.h"

#include "sharedmatting_gpu.h"

#include "frame_averager.h"

#include "hsv_trimap.h"

class  COLOR_SHARED_MATTING_API ColorSharedMatting {

public:

    ColorSharedMatting(int originalWidth, int originalHeight,
        int scaledWidth, int scaledHeight, bool useAverage);

    ColorSharedMatting();

    ~ColorSharedMatting();

    void init();

    void finalize();

    void process(
        uchar* imgBuf,
        int width,
        int height,
        KeyColor* keyColor,
        int keyColorSize,
        float lowerBound,
        float upperBound,
        uchar* blendBuf
        );

    cv::Mat getAlpha();

    void OnePageTrimap(
        std::string outputs,
        cv::Mat image,
        int& width,
        int& height,
        float lowerBound,
        float upperBound,
        KeyColor* keyColor,
        int& keyColorSize);

    void OnePageBlend(
        cv::Mat& image,
        std::string outputs,
        KeyColor* keyColor,
        int& keyColorSize,
        float lowerBound,
        float upperBound
    );
   
    void AllBlend(
        string  filePath,
        string outputsRemoveBG,
        float lowerBound,
        float upperBound,
        KeyColor* keyColor,
        int keyColorSize,
        int& width,
        int& height
    );
    
    void AllTrimap(
        string  filePath,
        string outputsTrimap,
        float lowerBound,
        float upperBound,
        KeyColor* keyColor,
        int keyColorSize,
        int& width,
        int& height
    );
    
    cv::Mat blendWithBg(
        cv::Mat srcIn,
        cv::Mat alphaIn,
        cv::Mat bgIn
    );
    cv::Mat normalize(cv::Mat src);
    cv::Mat unnormalize(cv::Mat src);
    
private:


    TrimapGeneratorGpu m_ct;

    SharedMatting m_sm;

    FrameAverager m_frmAver;

    HsvTrimapGenerator htg;

    cv::Mat m_image;
    cv::Mat m_trimap;
    cv::Mat m_alpha;
    cv::Mat m_previousAlpha;
    cv::Mat m_preImage;

    int m_originalWidth = 0;
    int m_originalHeight = 0;

    int m_scaledWidth = 0;
    int m_scaledHeight = 0;


    bool m_useAverage = false;
    unsigned int m_maxSkipNum = 1;
    unsigned int m_skipNum = 0;
};
#endif