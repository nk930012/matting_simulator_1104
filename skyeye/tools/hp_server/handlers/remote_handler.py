# -*- coding: utf-8 -*-
import os
import logging
import paramiko
import requests
import telnetlib
import time
from pywol import wake

broadcast = '192.168.0.255'
admin, user, password = 'sis', 'User', 'sis'
delay = '0'

class remote_handler():
    def __init__(self, WsHandler, debug = logging.ERROR):
        print('Remote Control Initialize')
        self.ws = WsHandler
        self.id = 0


    def handler(self, data):
        self.id = data['id']
        method = data['method']
        params = data['params']
        
        if ((method == 'set') and ("handler" in data)):
            print('Parameters = ', params)

            handler = data['handler']
            target = params['target']
            action = params['action']

            if (handler == 'esp8266'):
                ip, gpio = target.split(':')
                self.set_esp8266(ip, gpio, action)
            elif (handler == 'projector'):
                ip = target
                self.set_projector(ip, action)
            elif (handler == 'ssh'):
                ip = target
                self.set_ssh(ip)
            elif (handler == 'wol'):
                mac, device = target.split(':')
                self.set_wol(mac, device, action)


    def set_esp8266(self, ip, gpio, action):
        if (action == 'On'):
            command = 'http://' + str(ip) + '/D' + str(gpio) + '-1'
        elif (action == 'Off'):
            command = 'http://' + str(ip) + '/D' + str(gpio) + '-0'
        
        print('Send: ', command)
        requests.get(command)
#        print(echo.text)
#        return (result)


    def set_projector(self, ip, action):
        host = str(ip)
        print('Set {} {}'.format(host, action))

        telnet = telnetlib.Telnet(host)
        echo = telnet.read_some().decode('ascii')
        
        if (action == 'On'):
            command = '~0000 1\r'
        elif (action == 'Off'):
            command = '~0000 0\r'
        elif (action == 'AV_Mute_On'):
            command = '~0002 1\r'
        elif (action == 'AV_Mute_Off'):
            command = '~0002 0\r'
        elif (action == 'Source_VGA'):
            command = '~0012 5\r'
        elif (action == 'Source_HDMI1'):
            command = '~0012 1\r'
        elif (action == 'Source_HDMI2'):
            command = '~0012 15\r'
        elif (action == 'Get_Info'):
            command = '~00150 1\r'

        telnet.write(str.encode(command))
        echo = telnet.read_some().decode('ascii')
#        print('Echo=', echo)

        info = echo.split('> ')
        status = info[1][0:2]
        power = info[1][2:3]
        lamp = info[1][3:8]
        source = info[1][8:10]
        firmware = info[1][10:14]
        display = info[1][14:16]

        print('Get Info = ', status, power, lamp, source, firmware, display)

        if status == 'Ok':
            print('This is ', self.id)
            ws_data = '{"id": '+str(self.id)+', "result": {"power": "'+power+'", "source": "'+source+'", "display": "'+display+'"}}'
#        else:
#            status_str = "failed"
#            ws_data = '{"id": '+str(self.id)+', "result": {"status": "'+status_str+'"}}'

            self.ws.send_updates(ws_data)
        telnet.close()

    def set_ssh(self, ip, user, password):
        try: 
            ssh_client = paramiko.SSHClient() 
            ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy()) 
            ssh_client.connect(ip, 22, admin, password) 
            
            print("command: " + command)
            std_in, std_out, std_err = ssh_client.exec_command(command, get_pty=True) 
            std_in.write(password + '\n')
            
            for line in std_out: 
                print(line.strip("\n"))
            for line in std_err: 
                print(line.strip("\n")) 
                
            ssh_client.close() 

        except Exception as e: 
            print("error: " + str(e)) 


    def set_wol(self, mac, device, action):
        if (action == 'On'):
            wake(str(mac), ip_address=str(broadcast))
        elif (action == 'Off'):
            os.system(r'net use \\' + device +' /delete')
#            print(r'net use \\' + device +' /user:' + user + ' ' + password)
            os.system(r'net use \\' + device +' /user:' + user + ' ' + password)
            time.sleep(2)
#            print(r'shutdown -s -f -m \\' + device + ' -t ' + delay)
            os.system(r'shutdown -s -f -m \\' + device + ' -t ' + delay)


            
