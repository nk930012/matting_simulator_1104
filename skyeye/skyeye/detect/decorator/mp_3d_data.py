from .data_decorate import DataDecorate
from skyeye.utils.opencv import InfoDrawer

class Mp3dData(DataDecorate):
    
    def detect(self):
        super().detect()

        self._mp_result = self.component.result
        self._mp_3d_data = self.component.mp_3d_data
        self._mp_3d_pixel = self.component.mp_3d_pixel
        self._imageOut = self.component.image_out

    @property
    def mp_result(self):
        return self._mp_result

    @property
    def mp_3d_data(self):
        return self._mp_3d_data

    @property
    def mp_3d_pixel(self):
        return self._mp_3d_pixel

    def postProcess(self):
        InfoDrawer.draw_mp_3d(self._imageOut, self._mp_3d_pixel)