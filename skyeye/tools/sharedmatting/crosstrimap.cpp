#include <iostream>
#include "timer.h"
#include "crosstrimap.h"

CrossTrimap::CrossTrimap() {}

CrossTrimap::~CrossTrimap()
{
    release();
}


#if 0

Mat binarization(
    cv::Mat img,
    double hueMin, double hueMax,
    double satMin, double satMax,
    double valueMin, double valueMax)
{
    cv::Mat hsv, inRangeHsv;

    Scalar lowerBound(hueMin, satMin, valueMin);

    Scalar upperBound(hueMax, satMax, valueMax);

    cv::cvtColor(img, hsv, COLOR_BGR2HSV);

    cv::inRange(hsv, lowerBound, upperBound, inRangeHsv);

    cv::bitwise_not(inRangeHsv, inRangeHsv);

    inRangeHsv.convertTo(inRangeHsv, CV_32FC1, (1.0f / 255));

    hsv.release();

    return inRangeHsv;
}


void CrossTrimap::binarizationGpu() {

    cv::Mat binary(m_height, m_width, CV_32FC1, Scalar(1.0));

    for (int i = 0; i < m_dropperSize; ++i)
        binary = binary.mul(binarization(m_image, m_hue[i].x, m_hue[i].y, m_sat[i].x,
            m_sat[i].y, m_val[i].x, m_val[i].y));

    binary.convertTo(binary, CV_8UC1, 255);

    memcpy(m_binaryImg, binary.data, m_width * m_height * sizeof(uchar));

    cl_int err = clEnqueueWriteBuffer(m_queue, m_binaryMem, CL_TRUE, 0,
        m_width * m_height * sizeof(uchar),
        m_binaryImg, 0, NULL, NULL);

    if (CL_SUCCESS != err) 
    {
        printf("Couldn't write buffer(binarizationCl).\n");
        exit(1);
    }

    imwrite("binarization(opencv).png", binary);

    binary.release();
}

#else

void CrossTrimap::binarizationGpu()
{

    cl_int err;

    cl_event event;

    err = clEnqueueWriteBuffer(m_queue, m_colorMem, CL_TRUE, 0,
        m_width * m_height * m_channels * sizeof(uchar),
        m_colorImg, 0, NULL, NULL);

    err |= clEnqueueWriteBuffer(m_queue, m_hueMem, CL_TRUE, 0,
        m_dropperSize * sizeof(*m_hue),
        m_hue, 0, NULL, NULL);

    err |= clEnqueueWriteBuffer(m_queue, m_satMem, CL_TRUE, 0,
        m_dropperSize * sizeof(*m_sat),
        m_sat, 0, NULL, NULL);

    err |= clEnqueueWriteBuffer(m_queue, m_valMem, CL_TRUE, 0,
        m_dropperSize * sizeof(*m_val),
        m_val, 0, NULL, NULL);

    if (CL_SUCCESS != err) {
        printf("Couldn't write buffer(binarizationCl).\n");
        exit(1);
    }
    err = clSetKernelArg(m_binarizationCl, 0, sizeof(cl_mem), &m_colorMem);
    err |= clSetKernelArg(m_binarizationCl, 1, sizeof(cl_mem), &m_hueMem);
    err |= clSetKernelArg(m_binarizationCl, 2, sizeof(cl_mem), &m_satMem);
    err |= clSetKernelArg(m_binarizationCl, 3, sizeof(cl_mem), &m_valMem);
    err |= clSetKernelArg(m_binarizationCl, 4, sizeof(int), &m_dropperSize);
    err |= clSetKernelArg(m_binarizationCl, 5, sizeof(int), &m_width);
    err |= clSetKernelArg(m_binarizationCl, 6, sizeof(int), &m_height);
    err |= clSetKernelArg(m_binarizationCl, 7, sizeof(int), &m_channels);
    err |= clSetKernelArg(m_binarizationCl, 8, sizeof(cl_mem), &m_binaryMem);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(binarizationCl) arguments.\n");
        exit(1);
    }

    globalSize[0] = m_width; globalSize[1] = m_height;

    err = clEnqueueNDRangeKernel(m_queue, m_binarizationCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(binarizationCl).\n");
        exit(1);
    }

    clWaitForEvents(1, &event);

    err = clEnqueueReadBuffer(m_queue, m_binaryMem, CL_TRUE, 0,
        m_width * m_height * sizeof(uchar),
        m_binaryImg, 0, NULL, &event);

    cv::Mat binary = Mat(m_height, m_width, CV_8UC1, m_binaryImg);

    //imwrite("binary(opencl).png", binary);
    //binary.release();
}
#endif

#if 0

void CrossTrimap::trimapGpu()
{
    Mat erosion, dilation;

    cv::Mat binary = Mat(m_height, m_width, CV_8UC1, m_binaryImg);

    binary.convertTo(binary, CV_32FC1);

    //cv::Mat kernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, Size(5, 5));

    cv::Mat kernel(3, 3, CV_32FC1, Scalar(1.0));

    Point anchor(-1, -1);

    int iterations = 5;

    cv::erode(binary, erosion, kernel, anchor, iterations);

    //cv::imshow("erosion", erosion);

    iterations = 3;

    cv::dilate(binary, dilation, kernel, anchor, iterations);

    //cv::imshow("dilation", dilation);

    Mat trimap;

    add(erosion, dilation, trimap);

    Mat mask1, mask2, mask3;

    inRange(trimap, Scalar(0.0), Scalar(254.0), mask1);

    trimap.setTo(Scalar(0.0), mask1);

    inRange(trimap, Scalar(255.0), Scalar(509.0), mask2);

    trimap.setTo(Scalar(127.0), mask2);

    inRange(trimap, Scalar(509.0), Scalar(600.0), mask3);

    trimap.setTo(Scalar(255.0), mask3);

    trimap.convertTo(trimap, CV_8UC1);

    memcpy(m_trimapImg, trimap.data, m_width * m_height);

    //cv::imshow("trimap", trimap);

    trimap.release();

}

#else

void CrossTrimap::trimapGpu()
{

    cl_int err;

    cl_event event;

    err = clSetKernelArg(m_trimapCl, 0, sizeof(cl_mem), &m_binaryMem);
    err |= clSetKernelArg(m_trimapCl, 1, sizeof(int), &m_width);
    err |= clSetKernelArg(m_trimapCl, 2, sizeof(int), &m_height);
    err |= clSetKernelArg(m_trimapCl, 3, sizeof(int), &m_trimapSize);
    err |= clSetKernelArg(m_trimapCl, 4, sizeof(cl_mem), &m_trimapMem);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(trimapCl) arguments.\n");
        exit(1);
    }

    globalSize[0] = m_width; globalSize[1] = m_height;

    err = clEnqueueNDRangeKernel(m_queue, m_trimapCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(trimapCl).\n");
        exit(1);
    }

    clWaitForEvents(1, &event);

    err = clEnqueueReadBuffer(m_queue, m_trimapMem, CL_TRUE, 0,
        m_width * m_height * sizeof(uchar),
        m_trimapImg, 0, NULL, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't read buffer from kernel(trimapCl).\n");
        exit(1);
    }

    //Mat trimap = Mat(m_height, m_width, CV_8UC1, m_trimapImg);
    //imwrite("trimap.png", trimap);
    //trimap.release();

}
#endif

cv::Mat GaussianBlur(cv::Mat img)
{
    cv::Mat gaussianBlur;

    cv::GaussianBlur(img, gaussianBlur, Size(21, 21), 0);

    return gaussianBlur;
}

cv::Mat CrossTrimap::estimateTrimap(
    double* hueMin, double* hueMax,
    double* satMin, double* satMax,
    double* valMin, double* valMax,
    int dropperSize)
{
    m_dropperSize = dropperSize;

    init();

#if 1

    cv::Mat blur = GaussianBlur(m_image);

    memcpy(m_colorImg, blur.data, m_width * m_height * m_channels);

    blur.release();

#else

    memcpy(m_colorImg, m_image.data, m_width * m_height * m_channels);

#endif

    for (int i = 0; i < m_dropperSize; ++i)
    {
        m_hue[i].x = (int)hueMin[i];
        m_hue[i].y = (int)hueMax[i];

        m_sat[i].x = (int)satMin[i];
        m_sat[i].y = (int)satMax[i];

        m_val[i].x = (int)valMin[i];
        m_val[i].y = (int)valMax[i];
    }

    binarizationGpu();

    trimapGpu();

    cv::Mat trimap = cv::Mat(m_height, m_width, CV_8UC1, m_trimapImg);

    return trimap;
}

bool CrossTrimap::initBuf()
{
    if (!m_colorImg)
        m_colorImg = (uchar*)malloc(m_width * m_height * m_channels * sizeof(uchar));

    if(!m_binaryImg)
        m_binaryImg = (uchar*)malloc(m_width * m_height * sizeof(uchar));

    if (!m_trimapImg)
        m_trimapImg = (uchar*)malloc(m_width * m_height * sizeof(uchar));

    if (!m_hue)
        m_hue = (cl_int2*)malloc(MAX_DROPPER_SIZE * sizeof(*m_hue));

    if (!m_sat)
        m_sat = (cl_int2*)malloc(MAX_DROPPER_SIZE * sizeof(*m_sat));

    if (!m_val)
        m_val = (cl_int2*)malloc(MAX_DROPPER_SIZE * sizeof(*m_val));
    return true;
}

bool CrossTrimap :: initGpu()
{
    cl_int err = 0;

    bool success = true;

    cl_device_id device = create_device();

    m_context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);

    if (CL_SUCCESS != err) {
        printf("Couldn't create a context, err : %d.\n", err);
        //exit(1);
    }

    m_queue = clCreateCommandQueue(m_context, device, CL_QUEUE_PROFILING_ENABLE, &err);

    if (CL_SUCCESS != err) {
        printf("Couldn't create a command queue, err : %d.\n", err);
        //exit(1);
    }

    m_program = build_program(m_context, device, "kernels.cl");

    m_binarizationCl = clCreateKernel(m_program, "binarization", &err);

    if (CL_SUCCESS != err) {
        printf("Couldn't create the kernel(binarization), err : %d.\n", err);
        //exit(1);
    }

    m_trimapCl = clCreateKernel(m_program, "trimap", &err);

    if (CL_SUCCESS != err) {
        printf("Couldn't create the kernel(trimap), err : %d.\n", err);
        //exit(1);
    }


    m_colorMem = clCreateBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, m_width * m_height * m_channels * sizeof(uchar),
        m_colorImg, &err);

    if (CL_SUCCESS != err) {
        printf("Couldn't create the buffer(m_colorMem), err : %d.\n", err);
        //exit(1);
    }

    m_binaryMem = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, m_width * m_height * sizeof(uchar),
        m_binaryImg, &err);

    if (CL_SUCCESS != err) {
        printf("Couldn't create the buffer(m_binaryMem), err : %d.\n", err);
        //exit(1);
    }

    m_trimapMem = clCreateBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, m_width * m_height * sizeof(uchar),
        m_trimapImg, &err);

    if (CL_SUCCESS != err) {
        printf("Couldn't create the buffer(m_trimapMem), err : %d.\n", err);
        //exit(1);
    }

    m_hueMem = clCreateBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, MAX_DROPPER_SIZE * sizeof(*m_hue), m_hue, &err);

    if (CL_SUCCESS != err)
    {
        printf("Couldn't create the buffer(m_hueMem), err : %d.\n", err);
        //exit(1);
    }

    m_satMem = clCreateBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, MAX_DROPPER_SIZE * sizeof(*m_sat), m_sat, &err);

    if (CL_SUCCESS != err)
    {
        printf("Couldn't create the buffer(m_satMem), err : %d.\n", err);
        //exit(1);
    }

    m_valMem = clCreateBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, MAX_DROPPER_SIZE * sizeof(*m_val), m_val, &err);

    if (CL_SUCCESS != err) {
        printf("Couldn't create the buffer(m_valMem), err : %d.\n", err);
        //exit(1);
    }

    if (!m_context || !m_queue || !m_program || !m_binarizationCl ||
        !m_trimapCl || !m_colorMem || !m_binaryMem || !m_trimapMem ||
        !m_hueMem || !m_satMem || !m_valMem) {
        success = false;
        releaseGpu();
    }

    return success;

}

void CrossTrimap::releaseBuf()
{
    if (m_colorImg)
    {
        free(m_colorImg);
        m_colorImg = NULL;
    }

    if (m_binaryImg)
    {
        free(m_binaryImg);
        m_binaryImg = NULL;
    }

    if (m_trimapImg)
    {
        free(m_trimapImg);
        m_trimapImg = NULL;
    }

    if (m_hue)
    {
        free(m_hue);
        m_hue = NULL;
    }

    if (m_sat)
    {
        free(m_sat);
        m_sat = NULL;
    }

    if (m_val)
    {
        free(m_val);
        m_val = NULL;
    }



}

void CrossTrimap::releaseGpu()
{

    clReleaseMemObject(m_colorMem);
    clReleaseMemObject(m_binaryMem);
    clReleaseMemObject(m_trimapMem);

    clReleaseMemObject(m_hueMem);
    clReleaseMemObject(m_satMem);
    clReleaseMemObject(m_valMem);



    clReleaseKernel(m_binarizationCl);
    clReleaseKernel(m_trimapCl);

    clReleaseCommandQueue(m_queue);
    clReleaseProgram(m_program);
    clReleaseContext(m_context);
}

void CrossTrimap::release()
{
    releaseGpu();
    releaseBuf();
    m_isInit = false;
}

void CrossTrimap::init()
{
    if (m_isInit)
        return;

    if (initBuf() && initGpu())
    {
        m_isInit = true;
    }
    else
    {
        release();
        exit(1);
    }
}