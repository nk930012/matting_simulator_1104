#ifndef CROSS_TRIMAP_H
#define CROSS_TRIMAP_H

#include <string>
#include "opencv2/opencv.hpp"
#include "cl_utilities.h"
#include "typedef.h"

class TrimapGeneratorGpu
{
public:

    TrimapGeneratorGpu();

    TrimapGeneratorGpu(int width, int height);

    ~TrimapGeneratorGpu();

    void init();

    void finalize();

    cv::Mat estimateTrimap(
        cv::Mat image,
        KeyColor* color,
        int colorSize,
        float lowerBound,
        float upperBound
        );


    cv::Mat estimateTrimap(
        cv::Mat image,
        KeyColor* color,
        int colorSize,
        float lowerBound,
        float upperBound,
        cv::Size blurSize
        );


private:

    bool initGpu();
    bool initBuf();

    void finalizeGpu();
    void finalizeBuf();

    cv::Mat GaussianBlur(cv::Mat img);

    void trimapGpu();

    int m_trimapSize = 8;//15;

    size_t globalSize[2];

    cl_program m_program = NULL;

    cl_context m_context = NULL;

    cl_command_queue m_queue = NULL;

    cl_kernel m_binarizationCl = NULL, m_trimapCl = NULL;

    cl_mem m_imageBuffer = NULL;

    cl_mem m_trimapBuffer = NULL;

    cl_mem m_keyColorBuffer = NULL;

    cl_mem m_fTrimapBuffer = NULL;

    uchar* m_imageData = NULL, *m_trimapData = NULL;

    KeyColor* m_keyColorData = NULL;

    float* m_fTrimapData = NULL;

    cv::Mat m_image;

    int m_maxKeyColorSize = 10;

    int m_dropperSize = 0;
    int m_keyColorSize = 10;
    int m_width = 0;
    int m_height = 0;
    int m_channels = 0;
    int m_kernelSize = 0;
    bool m_isInit = false;
    //double m_reduceRatio = 1.0f;
    float m_lowerBound = 0.0f;
    float m_upperBound = 0.0f;
};

#endif