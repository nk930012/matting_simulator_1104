from .data_decorate import DataDecorate
from skyeye.utils.opencv import InfoDrawer

class KeyPointFace(DataDecorate):
    
    def detect(self):
        super().detect()

        self.key_points = self.component.key_points
        self._imageOut = self.component.image_out
        self.updateFace()

    def updateFace(self):
        boxes = []
        noses = []
        for body in self.key_points:
            nose = body[0]
            x , y = nose
            depth = self.component.getDepthValue(x, y)
            face_box = [x - 20, y - 20, 40, 40, depth]
            d_nose = [x, y, depth]

            boxes.append(face_box)
            noses.append(d_nose)

        self.boxes = boxes
        self.noses = noses

    def getFaceBox(self):
        return self.boxes
    def getNoses(self):
        return self.noses

    def postProcess(self):
        #InfoDrawer.draw_key_points(self._imageOut, self.key_points)
        for bbox in self.boxes:
            InfoDrawer.draw_depth_box(self._imageOut, bbox)