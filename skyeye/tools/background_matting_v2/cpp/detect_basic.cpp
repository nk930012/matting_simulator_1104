#include <torch/script.h>
#include <stdio.h>
#include <iostream>

using namespace std;


int main() {

    auto device = torch::Device("cuda");
    auto precision = torch::kFloat16;


    printf("Loading model ... \n");   
    auto model = torch::jit::load("torchscript_mobilenetv2_fp16.pth");
    model.setattr("backbone_scale", 0.25);
    model.setattr("refine_mode", "sampling");
    model.setattr("refine_sample_pixels", 80000);
    model.to(device);

    auto src = torch::rand({1, 3, 1080, 1920}).to(device).to(precision);
    auto bgr = torch::rand({1, 3, 1080, 1920}).to(device).to(precision);

    auto outputs = model.forward({src, bgr}).toTuple()->elements();
    auto pha = outputs[0].toTensor();
    auto fgr = outputs[1].toTensor();

    cout << "pha: " << pha << endl; 
    cout << "fgr: " << fgr << endl; 

    printf("End of run. \n");
}
