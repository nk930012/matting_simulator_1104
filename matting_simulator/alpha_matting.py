import numpy as np
import cv2 as cv

from libmatting import get_min_max_dists, gray_to_binary

class AlphaMatting: 

    def __init__(self):

        self.pixels = []
        self.min_dist = None
        self.max_dist = None

    def set_pixels(self, pixels):
        self.pixels = pixels

    def solve_alpha(self, min_dist, lower_bound):
    
        min_dist = cv.blur(min_dist, (10, 10))
        height, width = min_dist.shape

        alpha = np.zeros((height, width), dtype=np.float32)
        alpha  = np.where(min_dist > lower_bound, 1.0, alpha) # Forground

        kernel = np.ones((3,3), np.int32)
        alpha = cv.erode(alpha, kernel, iterations=2)

        return alpha

    def estimate_alpha(self, image, lower_bound, upper_bound):
        '''
        Genetate the alpha channel.

        image: nd array
            Image with BGR channels.

        lower_bound: float
            Lower bound. (normalized)

        upper_bound: float
            Upper bound. (normalized)
        '''

        self.min_dist, self.max_dist = get_min_max_dists(image, self.pixels)
        alpha = self.solve_alpha(self.min_dist, lower_bound)

        return alpha

    def get_gray_dist(self):

        dist = np.array(self.min_dist * 255, dtype=np.uint8)

        return dist
