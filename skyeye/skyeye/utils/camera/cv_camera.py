from .abstract_camera import AbstractCamera
from .camera_constants import *
import cv2 as cv
import platform
import subprocess

class CvCamera(AbstractCamera):
    def __init__(self):
        self.device = None # Device
        self.vc = None # Video capture
        self.frame = None

    def init(self, params):
        super().init(params)

        self.device = params[DEVICE_ID]
        self.use_V4L2 = params[USE_V4L2]
        self.autofocus = params[AUTO_FOCUS]
        self.auto_exposure = params[AUTO_EXPOSURE]
        self.brightness = params[BRIGHTNESS]
        self.contrast = params[CONTRAST]
        self.saturation = params[SATURATION]
        self.hue = params[HUE]
        self.exposure = params[EXPOSURE]
        self.use_MJPEG = params[USE_MJPEG]

        (self.major, minor, _) = cv.__version__.split(".")
        print('opencv ver. ', self.major)

        # check Os and VID/PID
        if platform.system() == 'Windows':
            print('OS is windows')

            self.use_V4L2 = False
            vendor_id = ""
            product_id = ""

        elif platform.system()  == 'Linux':
            print('OS is linux')
            dev_path = "/sys/class/video4linux/video"+str(self.device)+"/device/uevent"
            dev_info = subprocess.check_output(["cat", dev_path])
            dev_info = str(dev_info).split('PRODUCT=', 1)[1]
            dev_info = str(dev_info).split('TYPE=', 1)[0]
            vendor_id = dev_info.split('/')[0]
            product_id = dev_info.split('/')[1]
            #print(dev_info, vendor_id, product_id)
        else:
            self.use_V4L2 = False
            vendor_id = ""
            product_id = ""

        if self.exposure == None:
            if vendor_id in '0c45' or vendor_id in '0C45':
                if product_id in '6368':
                    self.exposure = 0.05
                elif product_id in '6536':
                    self.exposure = 0.01
            else:
                self.exposure = 0.5

    def open(self):
        print('cv.open')

        if self.use_V4L2:
            self.vc = cv.VideoCapture(self.device, cv.CAP_V4L2)  
        else:
            self.vc = cv.VideoCapture(self.device)
            
        if self.use_MJPEG:
            print('use MJPEG')
            fourcc_cap = cv.VideoWriter_fourcc(*'MJPG')
            self.vc.set(cv.CAP_PROP_FOURCC , fourcc_cap )
        else:
            print('use YUYV')
            fourcc_cap = cv.VideoWriter_fourcc(*'YUYV')
            self.vc.set(cv.CAP_PROP_FOURCC , fourcc_cap )

        # Set the camera parameters
        self.vc.set(cv.CAP_PROP_FRAME_WIDTH, self._width)
        self.vc.set(cv.CAP_PROP_FRAME_HEIGHT, self._height)

        if self.autofocus:
            self.vc.set(cv.CAP_PROP_AUTOFOCUS, 1)
        else:
            self.vc.set(cv.CAP_PROP_AUTOFOCUS, 0)

        if self.auto_exposure:
            self.vc.set(cv.CAP_PROP_AUTO_EXPOSURE, 3)
        else: # Manual
            if int(self.major) == 4:
                self.vc.set(cv.CAP_PROP_AUTO_EXPOSURE, 1)
                self.vc.set(cv.CAP_PROP_EXPOSURE, self.exposure)
            else: # For version 3
                self.vc.set(cv.CAP_PROP_AUTO_EXPOSURE, 0.25)
                self.vc.set(cv.CAP_PROP_EXPOSURE, self.exposure)
        self.vc.set(cv.CAP_PROP_FPS, self._fps)

        print("get exposure: ", self.vc.get(cv.CAP_PROP_EXPOSURE))
        

    def close(self):
        print('cv.close')
        self.vc.release()
        

    def process(self):
        is_capturing, frame = self.vc.read()
        #frame = cv.cvtColor(frame, cv.COLOR_BGR2RGB)

        # Save the frame
        self.frame = frame
        

    def getColorImage(self):
        return self.frame

    def getDepthInfo(self):
        return None

    def getDepthValue(self, x, y):
        return -1
