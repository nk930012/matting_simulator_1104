﻿#include "pch.h"
#include "Matting.h"
#include <chrono>
#if 0
Mat bytesToMat(BYTE* bytes, int width, int height, int type)
{
    Mat image = Mat(height, width, type, bytes).clone(); // make a copy
    return image;
}

BYTE* matToBytes(Mat image)
{
    int size = image.total() * image.elemSize();
    unsigned char* bytes = new unsigned char[size];  // you will have to delete[] that later
    std::memcpy(bytes, image.data, size * sizeof(char));
    return bytes;
}
#endif
cv::UMat OpenCLVMattingImp::MixChannels(cv::UMat src, cv::UMat mix)
{

    //if index over bound, return src;
    int width = src.cols;
    int height = src.rows;

    int srcChannelSize = src.channels();

    int mixChannelSize = mix.channels();

    vector<cv::UMat> srcChannels;

    vector<cv::UMat> mixChannels;

    vector<cv::UMat> copyChannels;

    split(src, srcChannels);

    split(mix, mixChannels);

    for (int i = 0; i < srcChannelSize; ++i)
        copyChannels.push_back(srcChannels[i]);

    copyChannels.push_back(mixChannels[0]);

    cv::UMat copy(height, width, CV_8UC4, Scalar(0, 0, 0, 0));

    merge(copyChannels, copy);

    srcChannels.clear();

    mixChannels.clear();

    copyChannels.clear();

    return copy;
}

cv::UMat OpenCLVMattingImp::GetHsvMask(
    cv::UMat src,
    double hueMin, double hueMax,
    double satMin, double satMax,
    double valueMin, double valueMax)
{
    cv::UMat hsv, inRangeHsv;

    Scalar lowerBound(hueMin, satMin, valueMin);

    Scalar upperBound(hueMax, satMax, valueMax);

    cv::cvtColor(src, hsv, COLOR_RGB2HSV);

    cv::inRange(hsv, lowerBound, upperBound, inRangeHsv);

    cv::bitwise_not(inRangeHsv, inRangeHsv);

    inRangeHsv.convertTo(inRangeHsv, CV_32FC1, (1.0f / 255));

    hsv.release();

    return inRangeHsv;
}

vector<vector<Point>> OpenCLVMattingImp::FindTargetContours(cv::UMat alpha)
{

    int width = alpha.cols;
    int height = alpha.rows;

    cv::UMat gray(height, width, CV_32FC1);
    alpha.convertTo(gray, CV_32FC1, 255);
    gray.convertTo(gray, CV_8UC1);

    cv::threshold(gray, gray, 127, 255, THRESH_BINARY);

    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;

    cv::findContours(gray, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);

    int imgArea = height * width;
    double minArea = imgArea * 0.05;

    vector<vector<Point>> targetContours;

    for (int i = 0; i < contours.size(); ++i)
    {
        vector<Point> c = contours[i];
        double area = contourArea(c);
        if (area > minArea)
            targetContours.push_back(c);
    }
    return targetContours;
}

cv::UMat OpenCLVMattingImp::Cutting(int width, int height, double top, double bottom, double left, double right)
{

    int leftCuttingArea = (int)(left * width);
    int rightCuttingArea = (int)((1 - right) * width);

    int topCuttingArea = (int)(top * height);
    int bottomCuttingArea = (int)((1 - bottom) * height);

    cv::UMat cutting(height, width, CV_32FC1, Scalar(0.0));
    Range colRange(leftCuttingArea, rightCuttingArea);
    Range rowRange(topCuttingArea, bottomCuttingArea);

    cv::UMat roi(cutting, rowRange, colRange);
    roi.setTo(Scalar(1.0));
    return cutting;
}

cv::UMat GetTransparentAlpha(cv::UMat alpha, vector<vector<Point>> contours)
{

    drawContours(alpha, contours, -1, Scalar(0.8), 2);

    //#修改這邊
    //Size size = new Size(5, 5);
    //Imgproc.GaussianBlur(alpha, alpha, size, 0);
    return alpha;
}




#define MIN_VALUE_INDEX 0

#define MAX_VALUE_INDEX 1

#define USING_GAUSSIAN_BLUR

//#define _DEBUG

cv::UMat OpenCLVMattingImp::Core(
    cv::UMat src,
    vector<vector<double>> hues,
    vector<vector<double>> sats,
    vector<vector<double>> values,
    int hsvSize,
    bool isFindContours)

{
    int blurSize = 5;

    int width = src.cols;

    int height = src.rows;
#ifdef _DEBUG
    chrono::steady_clock::time_point start;

    chrono::steady_clock::time_point end;
#endif


#ifdef USING_GAUSSIAN_BLUR

#ifdef _DEBUG
    start = chrono::steady_clock::now();
#endif
    cv::GaussianBlur(src, src, Size(blurSize, blurSize), 0);

#ifdef _DEBUG

    end = chrono::steady_clock::now();

    std::cout << "Time(GaussianBlur) : " << chrono::duration_cast<chrono::microseconds>(end - start).count()/1000.0 << "[ms]" << std::endl;

#endif

#endif

#ifdef _DEBUG

    start = chrono::steady_clock::now();

#endif

    cv::UMat alpha(height, width, CV_32FC1, Scalar(1.0));

#ifdef _DEBUG

    end = chrono::steady_clock::now();
    std::cout << "Time(Creat alpha) : " << chrono::duration_cast<chrono::microseconds>(end - start).count()/1000.0 << "[ms]" << std::endl;

#endif

#ifdef _DEBUG

    start = chrono::steady_clock::now();

#endif

    for (int i = 0; i < hsvSize; ++i)
    {
        double hueMin = hues[MIN_VALUE_INDEX][i];
        double hueMax = hues[MAX_VALUE_INDEX][i];
        double satMin = sats[MIN_VALUE_INDEX][i];
        double satMax = sats[MAX_VALUE_INDEX][i];
        double valueMin = values[MIN_VALUE_INDEX][i];
        double valueMax = values[MAX_VALUE_INDEX][i];
        alpha = alpha.mul(GetHsvMask(src, hueMin, hueMax, satMin, satMax, valueMin, valueMax));
    }

#ifdef _DEBUG

    end = chrono::steady_clock::now();

    std::cout << "Time(HsvMask("<< hsvSize <<")) : " << chrono::duration_cast<chrono::microseconds>(end - start).count()/1000.0 << "[ms]" << std::endl;

    start = chrono::steady_clock::now();

#endif

    cv::UMat kernel(5, 5, CV_32FC1, Scalar(1.0));

    Point anchor(-1, -1);

    int iteration = 1;

    cv::erode(alpha, alpha, kernel, anchor, iteration);

#ifdef _DEBUG

    end = chrono::steady_clock::now();

    std::cout << "Time(Erode) : " << chrono::duration_cast<chrono::microseconds>(end - start).count()/1000.0 << "[ms]" << std::endl;

    start = chrono::steady_clock::now();

#endif

    if (isFindContours)
    {
        vector<vector<Point>> contours = FindTargetContours(alpha);
        cv::UMat contourAlpha(alpha.rows, alpha.cols, alpha.type());
        drawContours(contourAlpha, contours, -1, Scalar(1.0), cv::FILLED);
        alpha = alpha.mul(contourAlpha);

        //alpha = GetTransparentAlpha(alpha, contours);
        contourAlpha.release();
    }

#ifdef _DEBUG

    end = chrono::steady_clock::now();

    std::cout << "Time(Contours) : " << chrono::duration_cast<chrono::microseconds>(end - start).count()/1000.0 << "[ms]" << std::endl;

#endif

#ifdef USING_GAUSSIAN_BLUR

#ifdef _DEBUG

    start = chrono::steady_clock::now();

#endif

    cv::GaussianBlur(alpha, alpha, Size(blurSize, blurSize), 0);

#ifdef _DEBUG

    end = chrono::steady_clock::now();

    std::cout << "Time(GaussianBlur) : " << chrono::duration_cast<chrono::microseconds>(end - start).count()/1000.0 << "[ms]" << std::endl;

#endif

#endif

#ifdef _DEBUG

    start = chrono::steady_clock::now();

#endif

    alpha.convertTo(alpha, CV_8UC1, 255.0, 0);

#ifdef _DEBUG

    end = chrono::steady_clock::now();

    std::cout << "Time(ConvertTo) : " << chrono::duration_cast<chrono::microseconds>(end - start).count()/1000.0 << "[ms]" << std::endl;

    start = chrono::steady_clock::now();

#endif

    cv::UMat matting = MixChannels(src, alpha);

#ifdef _DEBUG

    end = chrono::steady_clock::now();

    std::cout << "Time(MixChannels) : " << chrono::duration_cast<chrono::microseconds>(end - start).count()/1000.0 << "[ms]" << std::endl;

#endif

    alpha.release();

    return matting;
}


void OpenCLVMatting(BYTE* bytes, int width, int height, int type,
    double* hueMin, double* hueMax,
    double* satMin, double* satMax,
    double* valueMin, double* valueMax,
    int hsvSize, bool isFindContours, BYTE* output)
{
#ifdef _DEBUG
    chrono::steady_clock::time_point start = chrono::steady_clock::now();
    chrono::steady_clock::time_point end = chrono::steady_clock::now();
    AllocConsole();
    FILE *fDummy;
    freopen_s(&fDummy, "CONOUT$", "w", stdout);

#endif

    cv::ocl::setUseOpenCL(true);

#ifdef _DEBUG
    start = chrono::steady_clock::now();
#endif
    //RGB format
    cv::Mat img = Mat(height, width, type, bytes).clone();

    cv::UMat src = img.getUMat(AccessFlag::ACCESS_RW);

#ifdef _DEBUG

    end = chrono::steady_clock::now();

    std::cout << "width:" << src.cols << ", height:" << src.rows << std::endl;

    std::cout << "Time(ToUMat & Clone) : " << chrono::duration_cast<chrono::microseconds>(end - start).count()/1000.0 << "[ms]" << std::endl;

#endif

    OpenCLVMattingImp process;

    vector<vector<double>> hues{ vector<double>(hueMin, hueMin+hsvSize), vector<double>(hueMax, hueMax + hsvSize) };

    vector<vector<double>> sats{ vector<double>(satMin, satMin + hsvSize), vector<double>(satMax, satMax + hsvSize) };

    vector<vector<double>> values{ vector<double>(valueMin, valueMin + hsvSize), vector<double>(valueMax, valueMax + hsvSize) };

#ifdef _DEBUG

    start = chrono::steady_clock::now();

#endif
    //RGBA format

    cv::UMat matting = process.Core(src, hues, sats, values, hsvSize, isFindContours);
#ifdef _DEBUG

    end = chrono::steady_clock::now();

    std::cout << "Time(Core) : " << chrono::duration_cast<chrono::microseconds>(end - start).count()/1000.0 << "[ms]" << std::endl;

#endif

    hues.clear();

    sats.clear();

    values.clear();

    int size = matting.total() * matting.elemSize();

#ifdef _DEBUG

    start = chrono::steady_clock::now();

#endif

    std::memcpy(output, matting.getMat(AccessFlag::ACCESS_READ).data, size * sizeof(char));

#ifdef _DEBUG

    end = chrono::steady_clock::now();

    std::cout << "Time(Copy) : " << chrono::duration_cast<chrono::microseconds>(end - start).count()/1000.0 << "[ms]" << std::endl;

    start = chrono::steady_clock::now();

#endif

    matting.release();

    src.release();

    img.release();

#ifdef _DEBUG

    end = chrono::steady_clock::now();

    std::cout << "Time(Release) : " << chrono::duration_cast<chrono::microseconds>(end - start).count()/1000.0 << "[ms]" << std::endl;

    std::cout << std::endl;

#endif
    //end = chrono::steady_clock::now();
    //std::cout << "Time : " << chrono::duration_cast<chrono::microseconds>(end - start).count() / 1000.0 << "[ms]" << std::endl;
}