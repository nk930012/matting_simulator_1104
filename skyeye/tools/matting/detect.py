import os
import cv2 as cv
import numpy as np
import yaml
from skyeye.utils.opencv import wait_key
from skyeye.utils.file import to_str_digits

if __name__ == '__main__':


    input_file = "inputs.yaml"
    with open(input_file, 'r', encoding='utf-8') as f:
        inputs = yaml.load(f, Loader=yaml.Loader)

    # Set input parameters
    image_dir_path = inputs['image_dir_path']
    output_dir_path = inputs['output_dir_path']

    # Checking
    if not os.path.exists(image_dir_path):
        print("There is no {}.".format(image_dir_path))
        exit(0)

    if not os.path.exists(output_dir_path):
        os.mkdir(output_dir_path)  
        exit(0)

    bg_path = 'images/img_00001.png'
    image_path = 'images/img_00001.png'

    bg = cv.imread(bg_path)
    bg_gray = cv.cvtColor(bg, cv.COLOR_BGR2GRAY)

    height = bg.shape[0]
    width = bg.shape[1]

    image = cv.imread(image_path)
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

    #image_out = np.concatenate(bg_image, fg_image) 

    
    #cv.namedWindow('Win', cv.WINDOW_NORMAL)
    print(bg)

    #while True:
    cv.imshow('Win', bg)



    '''
    # Exit while 'q' or 'Esc' is pressed
    key = wait_key(1)
    if key == ord("q") or key == 27: break
    '''

    cv.destroyAllWindows()
