import cv2 as cv
import numpy as np
import mediapipe as mp
mp_drawing = mp.solutions.drawing_utils
mp_selfie_segmentation = mp.solutions.selfie_segmentation

from time import perf_counter

def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

# For webcam input:
BG_COLOR = (192, 192, 192) # gray
cap = cv.VideoCapture(0)

with mp_selfie_segmentation.SelfieSegmentation(
    model_selection=1) as selfie_segmentation:

  bg_image = None
  while cap.isOpened():
    success, image = cap.read()
    if not success:
      print("Ignoring empty camera frame.")
      # If loading a video, use 'break' instead of 'continue'.
      continue

    time_start = perf_counter()

    # Flip the image horizontally for a later selfie-view display, and convert
    # the BGR image to RGB.
    image = cv.cvtColor(cv.flip(image, 1), cv.COLOR_BGR2RGB)

    # To improve performance, optionally mark the image as not writeable to
    # pass by reference.
    image.flags.writeable = False
    results = selfie_segmentation.process(image)

    image.flags.writeable = True
    image = cv.cvtColor(image, cv.COLOR_RGB2BGR)

    # Draw selfie segmentation on the background image.
    # To improve segmentation around boundaries, consider applying a joint
    # bilateral filter to "results.segmentation_mask" with "image".
    condition = np.stack(
      (results.segmentation_mask,) * 3, axis=-1) > 0.1

    # The background can be customized.
    #   a) Load an image (with the same width and height of the input image) to
    #      be the background, e.g., bg_image = cv.imread('/path/to/image/file')
    #   b) Blur the input image by applying image filtering, e.g.,
    #      bg_image = cv.GaussianBlur(image,(55,55),0)
    if bg_image is None:
      bg_image = np.zeros(image.shape, dtype=np.uint8)
      bg_image[:] = BG_COLOR

    output_image = np.where(condition, image, bg_image)

    time_end = perf_counter()
    time_duration = time_end - time_start
    fps = int(1.0/time_duration)

    # Show image 
    text_x = 20
    text_y = 20
    text_y_shift = 20

    msg = "fps: {}".format(fps)
    (text_x, text_y) = draw_msg(output_image, msg, text_x, text_y)

    cv.imshow('MediaPipe Selfie Segmentation', output_image)
    if cv.waitKey(5) & 0xFF == 27:
      break

cap.release()