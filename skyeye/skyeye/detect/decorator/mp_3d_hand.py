from .mp_3d_data import Mp3dData
from skyeye.utils.opencv import InfoDrawer
from skyeye.detect.hand import HandFactory
from skyeye.detect.hand.hand_model import HandModel
import numpy as np

class Mp3dHand(Mp3dData):
    
    def detect(self):
        super().detect()

        factory = HandFactory() 

        self._hands_3d = factory.generate_with_camera(self._mp_result, self.component.camera)

        # debug message
        #for hand in self._hands_3d:
        #    print(hand.WRIST_DEPTH_CAMERA)

    @property
    def mp_3d_hands(self):
        return self._hands_3d

