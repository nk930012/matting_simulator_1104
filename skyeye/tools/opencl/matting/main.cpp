#include <chrono>

#include "opencv2/opencv.hpp"
#include "opencv2/core/ocl.hpp"
#include <CL/cl.h>
#include "timer.h"

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{

    Timer timer;

    size_t rows = 720;
    size_t cols = 1280;

    Mat frame;
    //UMat image, gray, hsv, mask; 

    cout << "useOpenCL:" << ocl::useOpenCL() << endl;  

    UMat image = UMat(rows, cols, CV_8UC3); 
    UMat hsv = UMat(rows, cols, CV_8UC3); 
    UMat mask = UMat(rows, cols, CV_8UC1); 
    UMat binary = UMat(rows, cols, CV_8UC1); 


    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    frame = imread("test.png", IMREAD_COLOR);
    Size frame_size = Size(1280, 720);
    resize(frame, frame, frame_size);


    timer.tic();

    size_t num = 10000;
    for (size_t i=0; i < num; i++) 
    {

    frame.copyTo(image);

    GaussianBlur(image, image, Size(5, 5), 0);
    cvtColor(image, hsv, COLOR_BGR2HSV);

    //Scalar lower_bound = Scalar(40, 55, 85);
    //Scalar upper_bound = Scalar(80, 255, 255);

    inRange(hsv, Scalar(40, 55, 85), Scalar(80, 255, 255), mask);
    bitwise_not(mask, mask);

    inRange(hsv, Scalar(30, 55, 85), Scalar(90, 255, 255), mask);
    bitwise_not(mask, mask);

    inRange(hsv, Scalar(20, 55, 85), Scalar(100, 255, 255), mask);
    bitwise_not(mask, mask);

    // Find contours
    threshold(mask, binary, 127, 255, THRESH_BINARY);
    findContours(binary, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);

    }

    // Draw contours
    drawContours(image, contours, -1, Scalar(0, 0, 255), 2);

    imshow("image", image);
    imshow("mask", mask);

    timer.toc();

    float time_cost = timer.get_dt() / num;
    cout << "dt = " << time_cost << "[s]" << endl;

    waitKey();
    return 0;
}
