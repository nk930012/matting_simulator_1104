import numpy as np
import cv2 as cv
import torch
from time import perf_counter

from skyeye.utils.opencv import Webcam, wait_key
from skyeye.matting.background_matting_v2 import BackgroundMattingV2


def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)


if __name__ == '__main__':

    frame_width = 640
    frame_height = 480
    use_V4L2 = True
    autofocus=False
    auto_exposure=True

    webcam = Webcam()
    if webcam.is_open():
        webcam.release()

    device = 0 # Device ID
    webcam.open(device, width=frame_width, height=frame_height,
        use_V4L2=use_V4L2, autofocus=autofocus, auto_exposure=auto_exposure, exposure=0.005)

    matting = BackgroundMattingV2(backbone='mobilenetv2')
    matting.load_checkpoint('pytorch_mobilenetv2.pth')
    #matting = BackgroundMattingV2(backbone='resnet50')
    #matting.load_checkpoint('pytorch_resnet50.pth')
    #matting = BackgroundMattingV2(backbone='resnet101')
    #matting.load_checkpoint('pytorch_resnet101.pth')

    frame_count = 0
    while True:

        bg = None

        while True: # Grab background

            frame = webcam.read()
            rgb = cv.cvtColor(frame, cv.COLOR_BGR2RGB)

            cv.imshow("Win", frame)

            # Press 'b' to take a snapshot as the background 
            key = wait_key(1)
            if key == ord('b'):
                matting.set_init_background(rgb) 
                break
            elif key == ord('q'):
                exit()

        while True: # Matting

            frame = webcam.read()
            rgb = cv.cvtColor(frame, cv.COLOR_BGR2RGB)

            frame_count += 1
            print("frame_count = {}".format(frame_count))
            time_start = perf_counter()

            # Matting
            alpha, fg = matting.predict(rgb)  
            composed = matting.blend_white_background(alpha, fg)

            time_end = perf_counter()
            time_duration = time_end - time_start
            fps = int(1.0/time_duration)

            # Show image 
            image_out = cv.cvtColor(composed, cv.COLOR_RGB2BGR)

            text_x = 20
            text_y = 20
            text_y_shift = 20

            msg = "fps: {}".format(fps)
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            cv.imshow("Win", image_out)

            # Exit while 'q' is pressed
            key = wait_key(1)
            if key == ord('b'):
                break
            elif key == ord('q'):
                exit()


    # cleanup the camera and close any open windows
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
