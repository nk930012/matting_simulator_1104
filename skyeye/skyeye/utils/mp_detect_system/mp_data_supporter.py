from skyeye.utils.camera import CameraBuilder
from skyeye.detect.decorator import Mp3dHand
from skyeye.detect.decorator.mp_hands_decorate import MpHandsDecorate
from skyeye.utils.opencv import Webcam, wait_key
from skyeye.utils.mp_detect_system import MPHand2DBuilder
from skyeye.utils.mp_detect_system import MPHand2DParser



class MPHand2DDataSupporter(object):

    def __init__(self, cam_yaml='camera2d.yaml', detect_yaml=''):
        self.camera = CameraBuilder.buildWithYaml(cam_yaml)
        self.camera.open()
        hand2DBuilder = MPHand2DBuilder(self.camera)
        if(not detect_yaml):
            self.detector = hand2DBuilder.build()
        else:
            self.detector = hand2DBuilder.buildWithYaml(detect_yaml)
        self.parser = MPHand2DParser()


    def update(self):
        self.detector.detect()
        data = self.detector.mp_3d_hands
        return self.parser.parse(data)

'''
if __name__ == '__main__':

    supporter = MPHand2DDataSupporter(cam_yaml = 'camera2d.yaml')
    
    while True:
        wsData = supporter.update()
        print(wsData)
        key = wait_key(1)
        if key == ord("q") or key == 27: break
'''
