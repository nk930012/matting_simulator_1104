#ifndef DETECTORCLIENT_H
#define DETECTORCLIENT_H

#include <QObject>
#include <QtWebSockets/QWebSocket>

class DetectorClient :  public QObject
{
    Q_OBJECT

public:
    DetectorClient();
    ~DetectorClient();

    void connectToServer(QString address);
    void close();
    void sendMessage(QString message);

Q_SIGNALS:
    void connectSucceed();
    void textMessageReceived(QString message);

public Q_SLOTS:
    void onConnected();
    void onClosed();
    void onTextMessageReceived(QString message);

private:
    QWebSocket m_webSocket;
    QUrl m_url;
};

#endif // DETECTORCLIENT_H
