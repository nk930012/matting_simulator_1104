# -*- coding: utf-8 -*-
import sys
import os
import argparse
import logging
import tornado.ioloop
import numpy as np
import time
import json 
import imutils
import cv2 as cv
import collections

class mp_data_handler():
    def __init__(self, WsHandler, supporter, debug=logging.ERROR):
        self.ws = WsHandler
        num_points_max = 10000000
        self.points = []
        self.active = 0
        self.recordData = False
        self.scheduler = None
        self.supporter = supporter

 
    def mp_data_scheduler(self):
        wsData = self.supporter.update()
        #print(wsData)
        self.ws.send_updates(wsData)

    def open(self, id=0):
        if self.active == 0:
            self.active = 1
            self.scheduler = tornado.ioloop.PeriodicCallback(self.mp_data_scheduler, 20)
            self.scheduler.start()
        ret = True
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)

    def close(self, id):
        self.active = 0
        if self.scheduler is not None:
            self.scheduler.stop()
            self.scheduler = None
        if len(self.points) > 0 and self.recordData == True:
            filename = time.strftime("pose_coco_%Y%m%d-%H%M%S") + '.json'
            f = open("./data/" + filename, "w")
            f.write(json.dumps(self.points))
            f.close()
            self.points.clear()
        status_str = "successful"
        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)

    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close cam device.')
                self.close(id=1)
            elif params["command"] == "open":
                if params["property"] == "jetson_device":
                    self.open(id=id)
            elif params["command"] == "close":
                if params["property"] == "jetson_device":
                    self.close(id=id)
        elif method == "get":
            pass
        elif method == "set":
            pass
