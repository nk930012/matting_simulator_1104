import csv

def str_to_int(frame):

    num_rows = len(frame)
    num_cols = len(frame[0])

    for i in range(num_rows):
        for j in range(num_cols):
            frame[i][j] = int(frame[i][j])

    return frame

def read_dumped_csv(file_path):
    # Read the dumped data in csv file from the ShowVoltage.

    lines = []
    with open(file_path, newline='') as csvfile:

        reader = csv.reader(csvfile)

        for line in reader:
            lines.append(line)

    tool_name = lines[0][0]
    num_frames = int(lines[1][1])
    num_rows = int(lines[2][1])
    num_cols = int(lines[3][1])
    data_type = lines[4][1]

    frames = []
    i = 5
    for i_frame in range(num_frames):
        frame = []
        i = i+2 # Skip the blank line and message
        for row in range(num_rows):
            frame.append(lines[i][:-1])
            i = i + 1

        # Convert string into integer
        frame = str_to_int(frame)

        frames.append(frame)

    # Prepare the output
    data = {}
    data['tool_name'] = tool_name
    data['num_frames'] = int(num_frames)
    data['num_rows'] = int(num_rows)
    data['num_cols'] = int(num_cols)
    data['data_type'] = data_type
    data['frames'] = frames
 
    return data
