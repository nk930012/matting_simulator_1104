#ifndef SIMPLE_OPENCL_BUILDER_H
#define SIMPLE_OPENCL_BUILDER_H
#include "abstract_opencl_builder.h"

using namespace std;

class SimpleOpenclBuilder : public AOpenclBuilder
{
public:
    SimpleOpenclBuilder() {};
    ~SimpleOpenclBuilder() {};
    std::vector<cl_device_id> createDevice();
    cl_context createContext(cl_device_id device);
    cl_program buildProgram(cl_context context, cl_device_id device, std::string name);
    cl_kernel  createKernel(cl_program program, std::string functionName);
    cl_command_queue createCommandQueue(cl_context context, cl_device_id device);
    cl_int setKernelArg(cl_kernel kernel, cl_uint index, size_t size, const void* value);
    std::string clErrMessage(cl_int error);
    cl_int finish(cl_command_queue queue);

    void releaseMemObject(cl_mem memObj);
    void releaseKernel(cl_kernel kernel);
    void releaseCommandQueue(cl_command_queue queue);
    void releaseProgram(cl_program program);
    void releaseContext(cl_context context);
    cl_mem createBuffer(cl_context context, cl_mem_flags flags, size_t size, void *host_ptr);
    bool enqueueWriteBuffer(cl_command_queue queue, cl_mem memObj,
        cl_bool block, size_t offset, size_t size, const void* hostPtr);
    bool enqueueReadBuffer(cl_command_queue queue, cl_mem memObj,
        cl_bool block, size_t offset, size_t size, void* hostPtr);
private:
    void writeBuildLog(cl_program program, cl_device_id device);
};

#endif