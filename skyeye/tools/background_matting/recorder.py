import os
import cv2 as cv
from skyeye.utils.file import to_str_digits
from time import perf_counter

from skyeye.detect.face import HaarFaceDetector
from skyeye.utils.opencv import Webcam, wait_key

blue = (255, 0, 0)
green = (0, 255, 0)
red = (0, 0, 255)

def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

def draw_bbox(image, bbox):

    xa, ya, width, height = bbox
    xa = int(xa)
    ya = int(ya)
    xz = int(xa + width)
    yz = int(ya + height)
    cv.rectangle(image, (xa, ya), (xz, yz), (0, 255, 0), 2)

def save_image(dir, image, index, prefix='', tail='_img', ext='png'):

    str_num = to_str_digits(index, num_digits=3)
    filename = prefix + str_num + tail + '.' + ext
    path = os.path.join(dir, filename)
    cv.imwrite(path, image)

if __name__ == '__main__':


    frame_width = 800
    frame_height = 600
    use_V4L2 = True
    auto_exposure=False

    # images
    num_images = 10
    num_back = 2
    num_back_wait = 100
    image_dir = 'images'

    if not os.path.isdir(image_dir):
        os.mkdir(image_dir)

    webcam = Webcam()
    if webcam.is_open():
        webcam.release()

    device = 0 # Device ID
    webcam.open(device, width=frame_width, height=frame_height,
        use_V4L2=use_V4L2, autofocus=False, auto_exposure=auto_exposure)

    # Face detector
    face_detector = HaarFaceDetector(frame_width, frame_height, ratio_target=1)

    image_index = 0
    back_index = 0
    back_wait_index = 0
    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        time_start = perf_counter()

        frame = webcam.read()
        if frame is None:
            break

        # Draw information
        image_out = frame.copy()

        # Detect faces
        bboxes = face_detector.detect(frame)
        num_faces = len(bboxes)
        print("num_faces: ", num_faces)

        # Save images
        if num_faces > 0:

            back_wait_index = 0

            if image_index < num_images and frame_count % 3 == 0:

                save_image(image_dir, frame, image_index, tail='_img', ext='png')
                image_index += 1

        else: # Save background

            if back_index < num_back and back_wait_index >= num_back_wait:

                save_image(image_dir, frame, back_index, tail='_back', ext='png')
                back_index += 1
                back_wait_index = 0

            back_wait_index += 1

        if image_index >= num_images and back_index >= num_back:
            break

        # Draw faces
        for bbox in bboxes:
            draw_bbox(image_out, bbox)

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "fps: {}".format(fps)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
        msg = "image: {}/{}".format(image_index, num_images)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
        msg = "back: {}/{}".format(back_index, num_back)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
        msg = "back_wait: {}/{}".format(back_wait_index, num_back_wait)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        # show the frame and record if the user presses a key
        cv.imshow("Frame", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()

    print("Done.")
