﻿// opencl_trimap.cpp : 此檔案包含 'main' 函式。程式會於該處開始執行及結束執行。
//
#if 1
#include "opencl_fun.h"
#include <iostream>
#include <string.h>

#define CL_HPP_TARGET_OPENCL_VERSION 120 

#include <CL/opencl.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/ocl.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;
using namespace std;

cv::Mat GetHsvMask(
    cv::Mat src,
    double hueMin, double hueMax,
    double satMin, double satMax,
    double valueMin, double valueMax)
{

    Scalar lowerBound(hueMin, satMin, valueMin);

    Scalar upperBound(hueMax, satMax, valueMax);

    cv::Mat hsv;

    cv::cvtColor(src, hsv, COLOR_RGB2HSV);

    cv::Mat inRangeHsv;

    cv::inRange(hsv, lowerBound, upperBound, inRangeHsv);

    cv::bitwise_not(inRangeHsv, inRangeHsv);

    hsv.release();

    return inRangeHsv;
}

#define PROGRAM_FILE "trimap_generator.cl"

#include <chrono>

int main()
{
    chrono::steady_clock::time_point begin = chrono::steady_clock::now();

    chrono::steady_clock::time_point init_begin = chrono::steady_clock::now();

    chrono::steady_clock::time_point measure_begin = chrono::steady_clock::now();

    chrono::steady_clock::time_point measure_end = chrono::steady_clock::now();

    measure_begin = chrono::steady_clock::now();

    cl_device_id device = create_device();

    measure_end = chrono::steady_clock::now();

    std::cout << "Time(create_device) : " << chrono::duration_cast<chrono::milliseconds>(measure_end - measure_begin).count() << "[ms]" << std::endl;

    if (NULL == device)
        exit(1);

    size_t size = 0;// sizeof(cl_bool) / sizeof(cl_char);

    cl_int err = 0;

    cl_int trimap_size = 5;

    cl_device_type device_type = 0;

    size = sizeof(device_type) / sizeof(cl_char);

    measure_begin = chrono::steady_clock::now();

    err = clGetDeviceInfo(device, CL_DEVICE_TYPE, size, &device_type, &size);

    measure_end = chrono::steady_clock::now();

    std::cout << "Time(clGetDeviceInfo) : " << chrono::duration_cast<chrono::milliseconds>(measure_end - measure_begin).count() << "[ms]" << std::endl;


    if (err < 0) {
        printf("Couldn't get device info: %d", err);
        exit(1);
    }

    if((device_type & CL_DEVICE_TYPE_GPU) == CL_DEVICE_TYPE_GPU)
        printf("  CL_DEVICE_TYPE:\t\t\t%s\n", "CL_DEVICE_TYPE_GPU");
    if ((device_type & CL_DEVICE_TYPE_CPU )== CL_DEVICE_TYPE_CPU)
        printf("  CL_DEVICE_TYPE:\t\t\t%s\n", "CL_DEVICE_TYPE_CPU");

    cl_bool is_support_image = false;

    size = sizeof(is_support_image) / sizeof(cl_uchar);

    measure_begin = chrono::steady_clock::now();

    err = clGetDeviceInfo(device, CL_DEVICE_IMAGE_SUPPORT, size, &is_support_image, &size);

    measure_end = chrono::steady_clock::now();

    std::cout << "Time(clGetDeviceInfo) : " << chrono::duration_cast<chrono::milliseconds>(measure_end - measure_begin).count() << "[ms]" << std::endl;


    if (err < 0) {
        printf("Couldn't get device info: %d", err);
        exit(1);
    }

    if (!is_support_image) {
        printf("This device does not support image\n");
    }

    measure_begin = chrono::steady_clock::now();

    cl_context context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);

    measure_end = chrono::steady_clock::now();

    std::cout << "Time(clCreateContext) : " << chrono::duration_cast<chrono::milliseconds>(measure_end - measure_begin).count() << "[ms]" << std::endl;


    if (err < 0) {
        printf("Couldn't create a kernel: %d", err);
        exit(1);
    }

    measure_begin = chrono::steady_clock::now();

    cl_program program = build_program(context, device, PROGRAM_FILE);

    measure_end = chrono::steady_clock::now();

    std::cout << "Time(build_program) : " << chrono::duration_cast<chrono::milliseconds>(measure_end - measure_begin).count() << "[ms]" << std::endl;

    if (NULL == program) {
        system("pause");
        exit(1);
    }

    const char kernel_function_name[] = "trimap_generator";

    measure_begin = chrono::steady_clock::now();

    cl_kernel kernel = clCreateKernel(program, kernel_function_name, &err);

    measure_end = chrono::steady_clock::now();

    std::cout << "Time(clCreateKernel) : " << chrono::duration_cast<chrono::milliseconds>(measure_end - measure_begin).count() << "[ms]" << std::endl;

    if (err < 0)
    {
        printf("Couldn't create a kernel: %d", err);
        exit(1);
    }

    const std::string file_name = "input";

    const std::string file_format = ".png";

    const std::string input_file_name = file_name + file_format;

    measure_begin = chrono::steady_clock::now();

    cv::Mat input_src = cv::imread(input_file_name, cv::IMREAD_COLOR);

    measure_end = chrono::steady_clock::now();

    std::cout << "Time(imread) : " << chrono::duration_cast<chrono::milliseconds>(measure_end - measure_begin).count() << "[ms]" << std::endl;

    cv::Mat input_rgb;

    measure_begin = chrono::steady_clock::now();

    cv::cvtColor(input_src, input_rgb, cv::COLOR_BGR2RGB);

    measure_end = chrono::steady_clock::now();

    std::cout << "Time(cvtColor) : " << chrono::duration_cast<chrono::milliseconds>(measure_end - measure_begin).count() << "[ms]" << std::endl;

    double hueMin = 40, hueMax = 80, satMin = 55, satMax = 255, valueMin = 85, valueMax = 255;

    measure_begin = chrono::steady_clock::now();

    cv::Mat hsv = GetHsvMask(input_rgb, hueMin, hueMax, satMin, satMax, valueMin, valueMax);

    measure_end = chrono::steady_clock::now();

    std::cout << "Time(GetHsvMask) : " << chrono::duration_cast<chrono::milliseconds>(measure_end - measure_begin).count() << "[ms]" << std::endl;

    int width = hsv.cols;

    int height = hsv.rows;

    uchar* hsv_data_buffer = (uchar *)malloc(width * height * sizeof(uchar));

    chrono::steady_clock::time_point init_end = chrono::steady_clock::now();

    std::cout << "Time(init) : " << chrono::duration_cast<chrono::milliseconds>(init_end - init_begin).count() << "[ms]" << std::endl;

    chrono::steady_clock::time_point copy_begin = chrono::steady_clock::now();

    uchar* start = hsv_data_buffer;
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            uchar pixel = hsv.at<uchar>(i, j);
            *start++ = pixel;
        }
    }

    chrono::steady_clock::time_point copy_end = chrono::steady_clock::now();

    std::cout << "Time(copy) : " << chrono::duration_cast<chrono::milliseconds>(copy_end - copy_begin).count() << "[ms]" << std::endl;


    const cl_image_format image_format = {CL_R, CL_UNSIGNED_INT8 };

    const cl_image_desc image_desc = { CL_MEM_OBJECT_IMAGE2D , width, height, 0, 1, 0, 0, 0, NULL };


    cl_mem input_image = clCreateImage(context,
        CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
        &image_format,
        &image_desc,
        (void*)hsv_data_buffer,
        &err);

    if (err < 0) {
        printf("Couldn't create a input_image: %d", err);
        exit(1);
    }

    cl_mem output_image = clCreateImage(context,
        CL_MEM_WRITE_ONLY, &image_format, &image_desc, NULL, &err);

    if (err < 0)
    {
        printf("Couldn't create a output_image: %d", err);
        exit(1);
    }

    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &input_image);
    err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &output_image);

    if (err < 0) {
        printf("Couldn't set a kernel argument");
        exit(1);
    }

    //cl_command_queue queue = clCreateCommandQueue(context, device, 0, &err);

    cl_command_queue queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err);


    if (err < 0) {
        perror("Couldn't create a command queue");
        exit(1);
    };

    /* Enqueue kernel */
    size_t global_size[2] = { width, height };

    //err = clEnqueueNDRangeKernel(queue, kernel, 2, NULL, global_size,
        //NULL, 0, NULL, NULL);

    cl_event event;

    err = clEnqueueNDRangeKernel(queue, kernel, 2, NULL, global_size, NULL, 0, NULL, &event);

    clWaitForEvents(1, &event);

    if (err < 0) {
        perror("Couldn't enqueue the kernel");
        exit(1);
    }

    size_t origin[3] = {0,0,0};
    size_t region[3] = { width, height,1 };
    uchar* trimap = (uchar *)malloc(width * height * sizeof(uchar));

    if (NULL == trimap) {
        printf("Can not allocate trimap data buffer\n");
        exit(1);
    }

    err = clEnqueueReadImage(queue, output_image, CL_TRUE, origin,
        region, 0, 0, (void*)trimap, 0, NULL, NULL);

    if (err < 0) {
        perror("Couldn't read from the image object");
        exit(1);
    }

    clFinish(queue);

    cl_ulong time_start;
    cl_ulong time_end;

    clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
    clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);

    double nanoSeconds = time_end - time_start;
    printf("OpenCl Execution time is: %0.3f milliseconds \n", nanoSeconds / 1000000.0);

    chrono::steady_clock::time_point end = chrono::steady_clock::now();

    std::cout << "Time : " << chrono::duration_cast<chrono::milliseconds>(end - begin).count() << "[ms]" << std::endl;

    Mat check = Mat(height, width, CV_8UC1, trimap).clone(); // make a copy

    imshow("hsv", hsv);

    imshow("check", check);

    imwrite("mask.png", check);

    cv::waitKey(0);

    free(trimap);

    free(hsv_data_buffer);

    clReleaseMemObject(input_image);

    clReleaseMemObject(output_image);

    clReleaseKernel(kernel);

    clReleaseCommandQueue(queue);

    clReleaseProgram(program);

    clReleaseContext(context);

    system("pause");

}
#endif