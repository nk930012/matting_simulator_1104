#pragma once
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <windows.h>
#include <string>
#include <exception>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <fstream>

//QT related
#include <QtWidgets/QMainWindow>

//opencv related
#include <opencv2/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui.hpp>

// 自定義
#include "ui_VideoMattingUI.h"
#include "color_shared_matting.h"
#include "init_program.h"  
#include "timer.h"

using namespace std;

QT_BEGIN_NAMESPACE
namespace Ui { class VideoMattingUI; }
QT_END_NAMESPACE


class VideoMattingUI : public QMainWindow
{
    Q_OBJECT

public:
    string videoName, inputs,outputs, vtoi_outputs;
    int run_num,do_notdo;
    int page_start=0,page_end=0;
    int width, height, set_col, set_row,totalFrameCnt;
    int time_vtoi, time_trimap_matting, time_blend_matting, time_trimap_video, time_blend_video;
    //KeyColor keyColor[5];
    int img_index;
    int width_trimap, height_trimap, width_blend, height_blend;


    std::vector<string> value_path;
    char value[10000];
    float lowerBound = 0.104f / 0.2f;
    float upperBound = 0.11399999999999999f / 0.2f;


    VideoMattingUI(QWidget *parent = Q_NULLPTR);
    ~VideoMattingUI();

    void update_image_info(int& totalFrameCnt, int& width, int& height);
    void show_fig(QPixmap pixImg);
    void check_run(int& do_notdo);

    void ConvertVideoToImage(int& frameCnt, int& width, int& height, int& totalFrameCnt);
    void ConvertImageToVideo(std::string outputspath, int& frameCnt, int& width, int& height);

    void get_time_process(int& time_vtoi, int& time_trimap_matting, int& time_blend_matting, int& time_trimap_video, int& time_blend_video);
    void get_outputs(string& inputs, string& videoName, string& outputs, string& vtoi_outputs);
    void get_image_info(int& width, int& height, int& totalFrameCnt);
    void get_trimap_image_info(int& width, int& height);
    void get_blend_image_info(int& width, int& height);

    void get_pixel_color(int set_col, int set_row);
    void get_color_vector(int* color1_data, int* color2_data, int* color3_data, int* color4_data, int* color5_data);
    void get_HSV_color_vector(int* color1_data, int* color2_data, int* color3_data, int* color4_data, int* color5_data);
    void get_page(int& page_start, int& page_end);
    void get_image_num(char* value);
    void get_string(string input_string, QString& out);
    void change_index(QPlainTextEdit* plaina, QPlainTextEdit* plainb);
    //void choose_keycolor(KeyColor* keyColor);

protected:
    void mousePressEvent(QMouseEvent* event);


private slots:
    void on_pushButton_begin_clicked();
    void on_pushButton_fw_clicked();
    void on_pushButton_bw_clicked();
    void on_pushButton_savecolor1_clicked();
    void on_pushButton_savecolor2_clicked();
    void on_pushButton_savecolor3_clicked();
    void on_pushButton_savecolor4_clicked();
    void on_pushButton_savecolor5_clicked();
    void on_pushButton_trimap_clicked();
    void on_pushButton_blend_clicked();
    void on_pushButton_org_clicked();
    void on_pushButton_matting_clicked();
    void on_pushButton_trimapmatting_clicked();
    void on_pushButton_video_clicked();
    void on_pushButton_trimapvideo_clicked();
    void on_pushButton_log_clicked();

private:
    Ui::VideoMattingUIClass* ui;
};
#endif // MAINWINDOW_H