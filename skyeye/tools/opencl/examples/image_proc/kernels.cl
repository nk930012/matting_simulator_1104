__constant sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |
           CLK_ADDRESS_CLAMP | CLK_FILTER_NEAREST;


__kernel void process(read_only image2d_t input, read_only image2d_t trimap, 
  write_only image2d_t output) {

    int ix = get_global_id(0);
    int iy = get_global_id(1);

	  uint width = get_global_size(0);
	  uint height = get_global_size(1);

    int2 coord = (int2)(ix, iy);

    uint4 pixel = read_imageui(input, sampler, coord);

    write_imageui(output, coord, pixel);

}

/*
__kernel void process(__global uchar4* in, __global uchar* trimap, __global uchar4* out,
  uint width, uint height) {

    size_t ix = get_group_id(0);
    size_t iy = get_group_id(1);

    out[ix + iy*width] = in[ix + iy*width]

}
*/
