# -*- coding: utf-8 -*-
import sys
import os
import logging
import tornado.ioloop
import numpy as np
import time
import json 
#sys.path.append("../../../touch")
from skyeye.detect.body.tp_body_detector import TpBodyDetector
#from touch.jetson_nano.body_detector import BodyDetector
import collections
from skyeye.writers import csv_dict_writer
from time import perf_counter



from skyeye.utils.opencv import Webcam, wait_key
import cv2 as cv

class jetson_handler():
    def __init__(self, WsHandler, debug=logging.ERROR):
        self.ws = WsHandler
        num_points_max = 10000000
        self.points = []
        self.body_detector = TpBodyDetector()
        self.active = 0
        self.recordData = False
        self.scheduler = None

        self.frame_count = 0
        self.time_end = 0
        self.time_start = 0
        self.fps = 0

    def jetson_scheduler(self):

        if(10 == self.frame_count):
            self.time_end = perf_counter()
            time_duration = self.time_end - self.time_start
            self.fps = int( 10 / time_duration )
            self.frame_count = 0
        elif(1 == self.frame_count):
            self.time_start = perf_counter()

        self.frame_count += 1

        #print("fps: {}".format(self.fps))

        if self.active == 1:
            data = []
            bodies = self.body_detector.detect() # Read the frame and detect body   
            
            for body in bodies:
                # Air mouse box
                box_left = body.mouse_left.get_box()
                box_right = body.mouse_right.get_box()

                # Diagnostic information
                lw_rn = body.is_lw_rn_connected()
                lw_rw = body.is_lw_rw_connected()
                lw_bc = body.is_lw_bc_connected()
                lw_bb = body.is_lw_bb_connected()
                is_target = body.is_target()

                pos_right = body.get_pos_right()
                pos_left = body.get_pos_left()

                dict_body = {}

                dict_body['id'] = body.id               
                dict_body['keypoints'] = body.keypoints 

                dict_body['box_left'] = box_left
                dict_body['box_right'] = box_right              
                
                dict_body['lw_rn'] = lw_rn
                dict_body['lw_rw'] = lw_rw 
                dict_body['lw_bc'] = lw_bc
                dict_body['lw_bb'] = lw_bb

                dict_body['pos_right'] = pos_right
                dict_body['pos_left'] = pos_left
                dict_body['is_target'] = is_target

                data.append(dict_body)

            if self.recordData == True and len(data) > 0:
                timestamp = int(time.time()*1000)
                point = {'timestamp': timestamp, 'keypoints':json.dumps(data)}
                self.points.append(point)

            # Data transfer
            #wsData = {'bodies': data, 'size': (self.body_detector.frame_width, self.body_detector.frame_height)}
            wsData = {'size': (self.body_detector.frame_width, self.body_detector.frame_height), 'bodies': data}

            logging.debug(wsData)
            self.ws.send_updates(wsData)

    def open(self, id=0):
        if self.active == 0:
            self.body_detector.open(640, 480)
            self.active = 1
            self.scheduler = tornado.ioloop.PeriodicCallback(self.jetson_scheduler, 20)
            self.scheduler.start()
        ret = True
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)

    def close(self, id):
        self.active = 0
        if self.scheduler is not None:
            self.scheduler.stop()
            self.scheduler = None
        if len(self.points) > 0 and self.recordData == True:
            filename = time.strftime("pose_coco_%Y%m%d-%H%M%S") + '.json'
            f = open("./data/" + filename, "w")
            f.write(json.dumps(self.points))
            f.close()
            self.points.clear()
        self.body_detector.stop()
        status_str = "successful"
        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)

    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close cam device.')
                self.close(id=1)
            elif params["command"] == "open":
                if params["property"] == "jetson_device":
                    self.open(id=id)
            elif params["command"] == "close":
                if params["property"] == "jetson_device":
                    self.close(id=id)
        elif method == "get":
            pass
        elif method == "set":
            pass
