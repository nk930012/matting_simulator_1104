import numpy as np
import cv2 as cv

class TrimapGeneratorV1:

    def __init__(self, kernel_size=10):

        self.kernel_size = kernel_size

    def generate(self, mask):
        '''
        Genetate the trimap.

        mask: nd array
            Mask in grayscale.
        '''

        ksize = self.kernel_size
        tmp = cv.blur(mask, (ksize, ksize))
        tmp  = np.where((tmp < 255) & (tmp > 0), 127, tmp) # Unknown region
        trimap = np.array(tmp, dtype=np.uint8)

        return trimap
