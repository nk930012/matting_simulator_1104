import sys
import os
import logging
import websocket
import optparse
import json
import websocket
import numpy as np
import cv2
from time import perf_counter
import logging
from time import sleep


from dataclasses_json import dataclass_json
from dataclasses import dataclass

sys.path.append("../../../touch")
#from typing import List

from touch.detect.body.frame_packer import Body, PixelFlt, FramePacker


index_blue = 0
index_green = 1
index_red = 2
index_cyan = 3
index_magenta = 4
index_yellow = 5
index_black = 6

#(blue, green, read)
default_color = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255), (0 , 0, 0)]

def draw_msg(image, msg, x, y, y_shift=20, color=(0, 0, 0)):

    cv2.putText(image, msg, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)


def formated_message(name, items):
    data = []
    for pos in items:
        x = pos.x
        y = pos.y
        data.append([x, y])
    l = [tuple('{0:.2f}'.format(flt) for flt in sublist) for sublist in data] 
    msg = "{}: {}".format(name, l)

    return msg 
    #l = [tuple('{0:.2f}'.format(flt) for flt in sublist) for sublist in items]
    #msg = "{}: {}".format(name, l)
    #return msg

def draw_box(image, box, color):
    xa = int(box.x)
    ya = int(box.y)
    xz = int(xa + box.width)
    yz = int(ya + box.height)
    cv2.rectangle(image, (xa, ya), (xz, yz), color, 2)


def draw_keypoint(img, keyPoint, color):
    cv2.circle( img, ( keyPoint.nose.x, keyPoint.nose.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.neck.x, keyPoint.neck.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.right_shoulder.x, keyPoint.right_shoulder.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.right_elbow.x, keyPoint.right_elbow.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.right_wrist.x, keyPoint.right_wrist.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.left_shoulder.x, keyPoint.left_shoulder.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.left_elbow.x, keyPoint.left_elbow.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.left_wrist.x, keyPoint.left_wrist.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.right_hip.x, keyPoint.right_hip.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.right_knee.x, keyPoint.right_knee.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.right_ankle.x, keyPoint.right_ankle.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.left_hip.x, keyPoint.left_hip.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.left_knee.x, keyPoint.left_knee.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.left_ankle.x, keyPoint.left_ankle.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.right_eye.x, keyPoint.right_eye.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.left_eye.x, keyPoint.left_eye.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.right_ear.x, keyPoint.right_ear.y ), 10, color, -1)
    cv2.circle( img, ( keyPoint.left_ear.x, keyPoint.left_ear.y ), 10, color, -1)    

def draw_trunk(img, keyPoint, color):
    #do right hand side
    '''        
    cv2.line(img, ( keyPoint.nose.x, keyPoint.nose.y ), ( keyPoint.right_eye.x, keyPoint.right_eye.y ), color, 3)
    cv2.line(img, ( keyPoint.right_ear.x, keyPoint.right_ear.y ), ( keyPoint.right_eye.x, keyPoint.right_eye.y ), color,3)
    cv2.line(img, ( keyPoint.right_shoulder.x, keyPoint.right_shoulder.y ), ( keyPoint.neck.x, keyPoint.neck.y ), color,3)
    cv2.line(img, ( keyPoint.right_shoulder.x, keyPoint.right_shoulder.y ), ( keyPoint.right_elbow.x, keyPoint.right_elbow.y ), color,3)
    '''        
    if((0 != keyPoint.right_wrist.x and 0 != keyPoint.right_wrist.y) and (0 != keyPoint.right_elbow.x and 0 != keyPoint.right_elbow.y)):
        cv2.line(img,(keyPoint.right_wrist.x, keyPoint.right_wrist.y), (keyPoint.right_elbow.x, keyPoint.right_elbow.y), color, 3)
    '''        
    cv2.line(img, ( keyPoint.right_knee.x, keyPoint.right_knee.y ), ( keyPoint.right_hip.x, keyPoint.right_hip.y ), color,3)
    cv2.line(img, ( keyPoint.right_knee.x, keyPoint.right_knee.y ), ( keyPoint.right_ankle.x, keyPoint.right_ankle.y ), color,3)            
    '''
    #do left hand side            
    '''
    cv2.line(img, ( keyPoint.nose.x, keyPoint.nose.y ), ( keyPoint.left_eye.x, keyPoint.left_eye.y ), color,3)
    cv2.line(img, ( keyPoint.left_ear.x, keyPoint.left_ear.y ), ( keyPoint.left_eye.x, keyPoint.left_eye.y ), color,3)
    cv2.line(img, ( keyPoint.left_shoulder.x, keyPoint.left_shoulder.y ), ( keyPoint.neck.x, keyPoint.neck.y ), color,3)
    cv2.line(img, ( keyPoint.left_shoulder.x, keyPoint.left_shoulder.y ), ( keyPoint.left_elbow.x, keyPoint.left_elbow.y ), color, 3)
    '''
    if((0 != keyPoint.left_wrist.x and 0 != keyPoint.left_wrist.y) and (0 != keyPoint.left_elbow.x and 0 != keyPoint.left_elbow.y)):
        cv2.line(img, (keyPoint.left_wrist.x, keyPoint.left_wrist.y), (keyPoint.left_elbow.x, keyPoint.left_elbow.y), color, 3)
    '''
    cv2.line(img, ( keyPoint.left_knee.x, keyPoint.left_knee.y ), ( keyPoint.left_hip.x, keyPoint.left_hip.y ), color,3)
    cv2.line(img, ( keyPoint.left_knee.x, keyPoint.left_knee.y ), ( keyPoint.left_ankle.x, keyPoint.left_ankle.y ), color,3)            
    '''


def parse_arguments():
    parser = optparse.OptionParser()
    parser.add_option('-i', '--ip', dest='ip', default="127.0.0.1",
                    help=('the IP address of server'))
    parser.add_option('-p', '--port', dest='port', default=8050,
                    help=('the port of server'))

    parser.add_option('--width', dest='width', default=800,
                    help=('width of img'))

    parser.add_option('--height', dest='height', default=600,
                    help=('height of img'))


    (options, args) = parser.parse_args()
    return options, args

class DiagnosisWebsocketData:

    def __init__(self, ip="127.0.0.1", port=8050, debug=logging.ERROR, width=800, height=600):
        self.ip = ip
        self.port = port
        self.debug = debug
        logging.basicConfig(level=self.debug, format='%(asctime)s -(%(filename)s)%(levelname)s : %(message)s')
        self.width=width
        self.height=height
        self.is_img_init = False
        self.img = None
        self.url = url = "ws://"+ip+":"+str(port)+"/ws"
        self.time_start = 0
        self.time_end = 0
        self.id = 0
        websocket.enableTrace(True)
        self.ws = websocket.WebSocketApp(self.url,
        on_message = lambda ws,msg: self.message(ws, msg),
        on_error   = lambda ws,msg: self.error(ws, msg),
        on_close   = lambda ws:     self.close(ws),
        on_open    = lambda ws:     self.open(ws))

    def message(self, ws, message):
        frame_packer = FramePacker.from_json(message)
        self.time_end = perf_counter()
        time_duration = self.time_end - self.time_start
        self.time_start = self.time_end
        fps = int( 1.0 / time_duration )
 
        if(False == self.is_img_init):     
             frame_width = frame_packer.width
             frame_height = frame_packer.height
             if(0 == frame_width or 0 == frame_height):
                 frame_width = self.width
                 frame_height = self.height
             self.img = np.zeros((frame_height, frame_width, 3), np.uint8)
             self.is_img_init = True  

        self.img.fill(255)


        if(0 < len(frame_packer.bodies)):

            bodies = frame_packer.bodies           
            text_x = 20
            text_y = 20
            text_y_shift = 20
      
            lw_rn = []
            lw_rw = []
            lw_bc = []
            lw_bb = []

            pos_right = []
            pos_left = []

            for i in range(len(bodies)):

                body = bodies[i]
                keyPoint = body.keyPoint
               
                msg = "{}".format(body.id)
                draw_msg(self.img, msg, keyPoint.nose.x, keyPoint.nose.y - 50, color = default_color[index_blue])

                draw_keypoint(self.img, keyPoint, color = default_color[index_red])
                draw_trunk(self.img, keyPoint, color = default_color[index_blue])

                box_left = body.gesture.box_left
                box_right = body.gesture.box_right


                if(0 == body.id):
                    color = default_color[index_cyan]
                else:
                    color = default_color[index_blue]



                draw_box(self.img, box_left, color)
                draw_box(self.img, box_right, color)

                lw_rn.append(body.gesture.lw_rn)
                lw_rw.append(body.gesture.lw_rw)
                lw_bc.append(body.gesture.lw_bc)
                lw_bb.append(body.gesture.lw_bb)

                pos_right.append(body.gesture.pos_right)
                pos_left.append(body.gesture.pos_left) 
               
            
            res_lw_rn = any(status == True for status in lw_rn) 
            res_lw_rw = any(status == True for status in lw_rw)
            res_lw_bc = any(status == True for status in lw_bc)
            res_lw_bb = any(status == True for status in lw_bb)

            msg = "fps: {}, num_bodies: {}".format(fps, len(bodies))
            (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])


            msg = "lw_rn: {}".format(lw_rn)
            if(True == res_lw_rn):
                (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_green])
            else:
                (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])

        
            msg = "lw_rw: {}".format(lw_rw)
            if(True == res_lw_rw):
                (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_green])
            else:
                (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])


            msg = "lw_bc: {}".format(lw_bc)
            if(True == res_lw_bc):
                (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_green])
            else:
                (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])


            msg = "lw_bb: {}".format(lw_bb)
            if(True == res_lw_bb):
                (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_green])
            else:
                (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])


            msg = formated_message("pos_right", pos_right)
            (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])


            msg = formated_message("pos_left", pos_left)
            (text_x, text_y) = draw_msg(self.img, msg, text_x, text_y, color=default_color[index_magenta])
      
        cv2.imshow('diagnosis image', self.img)       
        key = cv2.waitKey(1)
            
        if key == ord("q") or 27 == key:
            self.close(websocket)

    def error(self, ws, error):
        print(error)


    def close(self, ws):
        print("close")
        cv2.destroyAllWindows()
        self.id += 1
        id_string = '{id}'.format(id = self.id)
        close_jetson_cmd = '{"id":'+ id_string +', "method": "command","params": {"command":"close", "property":"jetson_device"}}'
        self.ws.send(close_jetson_cmd)        
        self.ws.keep_running = False
        self.ws.close()

    def open(self, ws):
        self.id += 1
        print('Open websocket server ip: {}, port: {}'.format(self.ip, self.port))
        id_string = '{id}'.format(id = self.id)
        open_jetson_cmd = '{"id":'+ id_string +', "method": "command","params": {"command":"open", "property":"jetson_device"}}'
        self.ws.send(open_jetson_cmd)
        sleep(0.05)

    def start(self):
        if None != self.ws:
            self.ws.run_forever()


if __name__ == "__main__":
    options, args = parse_arguments()
    ip = options.ip
    port = int(options.port)
    debug = logging.ERROR
    width = int(options.width)
    height = int(options.height)
    websocket = DiagnosisWebsocketData(ip, port, debug, width, height)
    websocket.start()
