﻿#ifdef MATTING_EXPORTS
#define MATTING_API __declspec(dllexport)
#else
#define MATTING_API __declspec(dllimport)
#endif

//opencl 2.0
//#define CL_HPP_TARGET_OPENCL_VERSION 200 

//opencl 1.2
#define CL_HPP_TARGET_OPENCL_VERSION 120 

#include <CL/opencl.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/ocl.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>
using namespace cv;
using namespace std;
typedef unsigned char BYTE;

class OpenCLVMattingImp
{
    public:
#if 0
        cv::UMat Core(
            cv::UMat src, double top, double bottom, double left, double right,
            vector<vector<double>> hues,
            vector<vector<double>> sats,
            vector<vector<double>> values,
            int hsvSize,
            bool isFindContours);
#endif
        cv::UMat Core(
            cv::UMat src,
            vector<vector<double>> hues,
            vector<vector<double>> sats,
            vector<vector<double>> values,
            int hsvSize,
            bool isFindContours);
    private:

        cv::UMat MixChannels(cv::UMat src, cv::UMat mix);
        cv::UMat GetHsvMask(cv::UMat src, double hueMin, double hueMax,
            double satMin, double satMax, double valueMin, double valueMax);
        vector<vector<Point>> FindTargetContours(cv::UMat alpha);
        cv::UMat Cutting(int width, int height, double top, double bottom, double left, double right);
};



#ifdef MATTING_EXPORTS
extern "C" MATTING_API void OpenCLVMatting(BYTE* bytes, int width, int height, int type,
    double* hueMin, double* hueMax,
    double* satMin, double* satMax,
    double* valueMin, double* valueMax,
    int hsvSize, bool isFindContours, BYTE* output);
#else

extern "C"
{
    void OpenCLVMatting(BYTE* bytes, int width, int height, int type,
        double* hueMin, double* hueMax,
        double* satMin, double* satMax,
        double* valueMin, double* valueMax,
        int hsvSize, bool isFindContours, BYTE* output);

#if 0
    Mat bytesToMat(BYTE* bytes, int width, int height, int type);


        BYTE* matToBytes(Mat image);
#endif
}
#endif