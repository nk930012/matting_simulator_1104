
# Ubuntu 18.04
g++ -O3 -Wall -std=c++11 $(python3 -m pybind11 --includes) -c wrappers.cpp 

g++ -O3 -Wall -std=c++11 -I/usr/include/python3.6m -I/home/andrew/.virtualenvs/cv/include/site/python3.6 -c wrappers.cpp


g++ -O3 -Wall -shared -std=c++11 -fPIC $(python3 -m pybind11 --includes) -I. -I/usr/include -I/usr/local/include/opencv4/opencv -I/usr/local/include/opencv4 -L /usr/local/lib -lOpenCL -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_imgcodecs wrappers.o sharedmatting_gpu.o cl_utilities.o -o wrappers$(python3-config --extension-suffix)