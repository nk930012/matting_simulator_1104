import cv2 as cv
from time import perf_counter

from skyeye.utils.opencv import Webcam, wait_key

blue = (255, 0, 0)
green = (0, 255, 0)
red = (0, 0, 255)

def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

if __name__ == '__main__':

    device_id = 0

    cap = cv.VideoCapture(device_id)

    #cap.set(cv.CAP_PROP_AUTOFOCUS, 0)
    #cap.set(cv.CAP_PROP_AUTO_EXPOSURE, 1) # Manual
    #cap.set(cv.CAP_PROP_EXPOSURE, 300)

    frame_width = cap.get(cv.CAP_PROP_FRAME_WIDTH)
    frame_height = cap.get(cv.CAP_PROP_FRAME_HEIGHT)
    fps = cap.get(cv.CAP_PROP_FPS)
    focus = cap.get(cv.CAP_PROP_FOCUS)
    autofocus = cap.get(cv.CAP_PROP_AUTOFOCUS)
    exposure = cap.get(cv.CAP_PROP_EXPOSURE)
    auto_exposure = cap.get(cv.CAP_PROP_AUTO_EXPOSURE)


    print("Frame_WIDTH: ", frame_width)
    print("Frame_Height: ", frame_height)
    print("FPS: ", fps)
    print("AUTOFOCUS: ", autofocus)
    print("FOCUS: ", focus)
    print("AUTO_EXPOSURE: ", auto_exposure)
    print("EXPOSURE: ", exposure)


    cap.release()
