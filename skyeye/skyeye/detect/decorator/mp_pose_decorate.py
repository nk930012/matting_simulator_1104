from .camera_decorate import CameraDecorate
import cv2 as cv
import mediapipe as mp
from skyeye.utils.mediapipe import MpConvert
import math
from typing import List, Tuple, Union

class MpPoseDecorate(CameraDecorate):

    def __init__(self, camera, flip=None):
        super().__init__(camera,ratio_target=0.5)
        self._result = None
        self.mp_drawing = mp.solutions.drawing_utils
        self.mp_pose = mp.solutions.pose


        self.pose = self.mp_pose.Pose(
            min_detection_confidence=0.5, min_tracking_confidence=0.5)

    def detect(self):
        super().detect()

        #image = self.camera.color_image.copy()
        image = self.copy_frame()
        image.flags.writeable = False
        self._result, self.boxes, self._mp_3d, self._mp_3d_pixel = self.detect_mp_pose(image)

        self.camera.countFPS()

    def detect_mp_pose(self, image):

        results = self.pose.process(image)

        #print(dir(results))
        #print(dir(results.pose_landmarks))
        bboxes = []

        pose_3d, pose_pixel_3d = MpConvert.convert_to_camera_3d_ary(results.pose_landmarks, self.camera)

        if results.pose_landmarks:
            landmark_list = results.pose_landmarks
            nose = landmark_list.landmark[self.mp_pose.PoseLandmark.NOSE]
            noseX = min(math.floor(nose.x * self.camera.width), self.camera.width - 1)
            noseY = min(math.floor(nose.y * self.camera.height), self.camera.height - 1)
            bbox = [noseX-40, noseY-40, 80, 80]
            bboxes.append(bbox)
            '''
            for idx, landmark in enumerate(landmark_list.landmark):
                #print(idx)
                #print(landmark)
                if landmark.x < 0 or landmark.x > 1:
                    continue
                if landmark.y < 0 or landmark.y > 1:
                    continue
                px = min(math.floor(landmark.x * self.camera.width), self.camera.width - 1)
                py = min(math.floor(landmark.y * self.camera.height), self.camera.height - 1)
                #px, py = self._normalized_to_pixel_coordinates(landmark.x, landmark.y, self.camera.width, self.camera.height)
                #print(px)
                #print(py)
                z = self.camera.getDepthValue(px,py)
                coordinate = [landmark.x, landmark.y, z]
                pixel = [px, py, z]
                pose_3d.append(coordinate)
                pose_pixel_3d.append(pixel)
            '''

        return results, bboxes, pose_3d, pose_pixel_3d

    def postProcess(self):
        self.mp_drawing.draw_landmarks(
            self._imageOut, self._result.pose_landmarks, self.mp_pose.POSE_CONNECTIONS)

    @property
    def result(self):
        return self._result

    @property
    def face_boxes(self):
        return self.boxes

    @property
    def mp_3d_data(self):
        return self._mp_3d

    @property
    def mp_3d_pixel(self):
        return self._mp_3d_pixel