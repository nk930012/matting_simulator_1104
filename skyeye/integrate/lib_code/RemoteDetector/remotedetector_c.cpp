#include "remotedetector_c.h"
#include "RemoteDetector.h"
#include <iostream>
#include <QCoreApplication>
using namespace std;

static QCoreApplication* app;
static int argc{1};
static const char * argv[] = { "myLibrary", nullptr };

void myLibraryInit()
{
    int a = 1;
    app = new QCoreApplication(a, (char**)argv);
    //QMetaObject::invokeMethod(qApp, "quit", Qt::QueuedConnection);
    //app->exec();
}

void myLibraryDeInit()
{
    //app.reset();
}

void remoteConnect(const char *webServiceAddr, const char *udpIP, int udpPort)
{
    QString webAddr = QString(webServiceAddr);
    QString udp_ip = QString(udpIP);
    SRemoteDetector.connect(webAddr, udp_ip, udpPort);
}

void remoteClose()
{
    SRemoteDetector.close();
}

char *poseDetect(uchar *image, int length)
{
    QString msg = SRemoteDetector.poseDetect(image, length);
    string data = msg.toStdString();
    //cout<<data<<endl;
    //cout<<"========================================================="<<endl;
    char* rData = new char[data.size()+1];
    for(int i = 0; i < data.size();i++)
    {
        rData[i] = data[i];
    }
    rData[data.size()] = '\0';
    return rData;
}

void sendMessage(char *message)
{
    SRemoteDetector.sendMessage(message);
}

void processEvent()
{
    //SRemoteDetector.processEvent();
    app->processEvents(QEventLoop::AllEvents);
    //qApp->processEvents(QEventLoop::AllEvents);
}
