import matplotlib.pyplot as plt
import cv2

def imshow_bgr(image):
    """
    Show the image with BGR order.
    """
    plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

def imshow_gray(image):
    """
    Show the image in gray scale.
    """
    plt.imshow(image, cmap='gray')
