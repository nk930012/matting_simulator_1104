#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/core.hpp"
#include <iostream>

using namespace cv;
using namespace std;

int main()
{
	int result;
	int index;
	cv::VideoCapture* cap;
	cap = new cv::VideoCapture(0);

	for (index = 0; index <= 20; index++)
	{
		result = (int)cap->get(index);
		cout << index << "," << result << endl;
	}

	result = (int)cap->get(cv::CAP_PROP_EXPOSURE);
	cout << "CAP_PROP_EXPOSURE = " << result << endl;
	result = (int)cap->get(cv::CAP_PROP_AUTO_EXPOSURE);
	cout << "CAP_PROP_AUTO_EXPOSURE = " << result << endl;

//	cap->set(cv::CAP_PROP_AUTO_EXPOSURE, 0.75);
	cap->set(cv::CAP_PROP_EXPOSURE, -6);

	result = (int)cap->get(cv::CAP_PROP_EXPOSURE);
	cout << "CAP_PROP_EXPOSURE = " << result << endl;
	result = (int)cap->get(cv::CAP_PROP_AUTO_EXPOSURE);
	cout << "CAP_PROP_AUTO_EXPOSURE = " << result << endl;

//	result = (int)cap->set(20, 0);

}