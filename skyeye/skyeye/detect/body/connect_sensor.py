import numpy as np
from .get_distance import get_distance

class ConnectSensor:

    def __init__(self, num_c=10):

        self.num_buffer = 80
        self.num_c = num_c
        self.len_c = 10
        self.states = [0 for i in range(self.num_buffer)]
        self.is_triggered = False

        self.ia = self.num_buffer - self.num_c
        self.iz = self.num_buffer

    def set_len_c(self, d):

        if d is None:
            self.len_c = 10
        else:
            self.len_c = d

    def update(self, p0, p1, len_c):

        self.set_len_c(len_c)

        if p0[0] == 0 or p1[0] == 0:
            self.is_triggered = False
            return self.is_triggered

        d = get_distance(p0, p1)

        if d < self.len_c:
            out = 1
        else:
            out = 0

        self.states.append(out)
        self.states.pop(0)

        sum = 0
        for v in self.states[self.ia:self.iz]:
            sum += 1 - v

        if sum == 0:
            self.is_triggered = True
        else:
            self.is_triggered = False

        return self.is_triggered
