import cv2 as cv
from time import perf_counter
from skyeye.utils.opencv import Webcam, wait_key

import mediapipe as mp
import yaml

mp_drawing = mp.solutions.drawing_utils
mp_hands = mp.solutions.hands

hands = mp_hands.Hands(
    max_num_hands=2, min_detection_confidence=0.8, min_tracking_confidence=0.5)


def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)


if __name__ == '__main__':


    frame_width = 640
    frame_height = 480
    use_V4L2 = True
    autofocus=False
    auto_exposure=False

    # Set mediapipe using GPU
    with open(r'mediapipe.yaml', 'r', encoding='utf-8') as f:
        inputs = yaml.load(f, Loader=yaml.Loader)
    enable_gpu = inputs['enable_gpu']

    webcam = Webcam()
    if webcam.is_open():
        webcam.release()

    device = 0 # Device ID
    webcam.open(device, width=frame_width, height=frame_height,
        use_V4L2=use_V4L2, autofocus=autofocus, auto_exposure=auto_exposure)


    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        time_start = perf_counter()

        frame = webcam.read()
        if frame is None:
            break

        frame = cv.flip(frame, 1) # Flip the frame horizontally
        image = frame.copy()
        image_out = frame.copy()

        # Flip the image horizontally for a later selfie-view display, and convert
        if enable_gpu == 1:
            # the BGR image to RGBA.
            image = cv.cvtColor(image, cv.COLOR_BGR2RGBA)
        else:
            # the BGR image to RGB.
            image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
        # To improve performance, optionally mark the image as not writeable to
        # pass by reference.
        image.flags.writeable = False
        results = hands.process(image)

        # Draw the hand annotations on the image.
        if results.multi_hand_landmarks:
            for hand_landmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(
                    image_out, hand_landmarks, mp_hands.HAND_CONNECTIONS)

            print("hand no: ", len(results.multi_hand_landmarks))
            print("========")

            print("multi_handedness: ", results.multi_handedness)
            print("========")

            print("multi_hand_lanmarks: ", results.multi_hand_landmarks)
            print("========")


            print("---------------------------")

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "fps: {}".format(fps)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        # show the frame and record if the user presses a key
        cv.imshow("Win", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
