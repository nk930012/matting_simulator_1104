#include "hsv_trimap.h"

HsvTrimapGenerator::HsvTrimapGenerator() {
}

HsvTrimapGenerator::~HsvTrimapGenerator() {
}

void HsvTrimapGenerator::setColorBound(cv::Scalar lowerBound, cv::Scalar upperBound) {
    m_lowerBound = lowerBound;
    m_upperBound = upperBound;
}

cv::Mat HsvTrimapGenerator::estimateMask(cv::Mat image) {

    cv::Mat hsv;

    cv::cvtColor(image, hsv, cv::COLOR_BGR2HSV);

    cv::Mat binary;

    cv::inRange(hsv, m_lowerBound, m_upperBound, binary);

    hsv.release();

    cv::bitwise_not(binary, binary);

    return binary;

}

cv::Mat HsvTrimapGenerator::estimateTrimap(cv::Mat binary, cv::Size blurSize=cv::Size(9,9)) {

    binary.convertTo(binary, CV_32FC1);

    cv::Mat trimap;

    cv::blur(binary, trimap, blurSize);

    cv::Mat mask;

    cv::inRange(trimap, cv::Scalar(1), cv::Scalar(254), mask);

    trimap.setTo(127.0f, mask);

    trimap.convertTo(trimap, CV_8UC1);

    mask.release();

    return trimap;

}