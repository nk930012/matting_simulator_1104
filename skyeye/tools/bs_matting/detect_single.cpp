﻿#include <string>
#include <opencv2/imgproc/imgproc.hpp>
#include "bs_matting.h"
#include "timer.h"

using namespace std;
using namespace cv;

int main()
{
    Timer timer;

    float timeCost = 0.f;

    Mat inputImage = imread("input.png");
    Mat bgImage = imread("background.png");

    // Resize images
    //resize(inputImage, inputImage, Size(inputImage.cols / 2, inputImage.rows / 2));
    //resize(bgImage, bgImage, Size(bgImage.cols / 2, bgImage.rows / 2));

    int width = inputImage.cols;
    int height = inputImage.rows;
    int channels = inputImage.channels();

    // Load the scene image
    Mat sceneImage = imread("scene.png");
    resize(sceneImage, sceneImage, Size(width, height));

    BsMatting matting = BsMatting(width, height);

    // Set background
    matting.setBackground(bgImage);
    
    timer.tic();

    // Estimate alpha
    matting.estimateAlpha(inputImage);

    Mat diff = matting.getDiff();
    Mat thresh = matting.getThresh();
    Mat mask = matting.getMask();
    Mat trimap = matting.getTrimap();
    Mat alpha = matting.getAlpha();

    Mat blended = matting.blend(inputImage, alpha, sceneImage);

    timer.toc();

    timeCost += timer.get_dt();
    printf("time cost is %8.5f [s] \n", timeCost);

    //imshow("input", inputImage);
    //imshow("background", bgImage);
    imshow("diff", diff);
    imshow("thresh", thresh);
    imshow("mask", mask);
    imshow("trimap", trimap);
    imshow("alpha", alpha);
    imshow("blended", blended);

    imwrite("mask.png", mask);
    imwrite("trimap.png", trimap);
    imwrite("alpha.png", alpha);
    imwrite("blended.png", blended);

    waitKey(0);

    // Finalization
    inputImage.release();

    return 0;

}
