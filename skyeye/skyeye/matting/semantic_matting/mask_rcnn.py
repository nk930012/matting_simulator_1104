import os
import pathlib
import numpy as np
import cv2 as cv

import torch
import detectron2
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg


class MaskRcnn:

    def __init__(self, confidence_threshold=0.8):

        self.confidence_threshold = confidence_threshold

        self.cfg = self.setup_cfg(confidence_threshold)
        self.predictor = DefaultPredictor(self.cfg)

        self.cpu_device = torch.device("cpu")

    def setup_cfg(self, confidence_threshold):

        #model_path = "COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"
        #model_path = "COCO-InstanceSegmentation/mask_rcnn_R_101_FPN_3x.yaml"
        model_path = "COCO-InstanceSegmentation/mask_rcnn_X_101_32x8d_FPN_3x.yaml"

        cfg = get_cfg()
        cfg.merge_from_file(model_zoo.get_config_file(model_path))
        cfg.MODEL.RETINANET.SCORE_THRESH_TEST = confidence_threshold
        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = confidence_threshold
        cfg.MODEL.PANOPTIC_FPN.COMBINE.INSTANCES_CONFIDENCE_THRESH = confidence_threshold
        cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(model_path)
        self.cfg = cfg
        cfg.freeze()

        return cfg

    def detect(self, image):

        predictions = self.predictor(image)
        instances = predictions["instances"].to(self.cpu_device)

        return instances

    def get_mask_from_instances(self, instances):

        classes = instances.pred_classes
        scores =  instances.scores
        masks = instances.pred_masks.numpy()
        num, height, width = masks.shape

        out = np.zeros((height, width), dtype=np.bool)
        for i in range(num):
            if classes[i] == 0: # Only person class is considered
                if scores[i] > self.confidence_threshold:
                    out = np.logical_or(out, masks[i])

        out = self.to_grayscale(out)

        return out

    def to_grayscale(self, binary):

        h, w = binary.shape
        out = np.ones((h, w), dtype=np.uint8)
        out = binary*out*255

        return out
