import json
import trt_pose.coco
import trt_pose.models
import torch
import torch2trt
from torch2trt import TRTModule
import sys
import cv2 as cv
import torchvision.transforms as transforms
import PIL.Image
from trt_pose.draw_objects import DrawObjects
from trt_pose.parse_objects import ParseObjects
import argparse
import os.path
from skyeye.utils.opencv import Webcam, wait_key
from skyeye.detect.body import to_keypoints
from skyeye.detect.body import scale_keypoints, BodyTracker
import numpy as np



class TpBodyDetector():
    def __init__(self, json_file='human_pose.json', model_file='./model/resnet18_baseline_att_224x224_A_epoch_249_trt.pth', frame_width=1280, frame_height=800):

        self.optimized_model = model_file
        self.frame_width = frame_width
        self.frame_height = frame_height
        self.body_tracker = BodyTracker(frame_width=frame_width, frame_height=frame_height)
        self.width = 224
        self.height = 224
        self.frame = None

        with open('./model/human_pose.json', 'r') as f:
            human_pose = json.load(f)

        self.topology = trt_pose.coco.coco_category_to_topology(human_pose)

        num_parts = len(human_pose['keypoints'])
        num_links = len(human_pose['skeleton'])


        if 'resnet18' in self.optimized_model:
            self.model = trt_pose.models.resnet18_baseline_att(num_parts, 2 * num_links).cuda().eval()
        else:
            self.model = trt_pose.models.densenet121_baseline_att(num_parts, 2 * num_links).cuda().eval()

        data = torch.zeros((1, 3, self.height, self.width)).cuda()
        if os.path.exists(self.optimized_model) == False:
            print("Error, can't find the model: {}".format(self.optimized_model))

        self.model_trt = TRTModule()
        self.model_trt.load_state_dict(torch.load(self.optimized_model))

        torch.cuda.current_stream().synchronize()
        for i in range(50):
            y = self.model_trt(data)
        torch.cuda.current_stream().synchronize()

        self.mean = torch.Tensor([0.485, 0.456, 0.406]).cuda()
        self.std = torch.Tensor([0.229, 0.224, 0.225]).cuda()
        self.device = torch.device('cuda')


    '''
    hnum: 0 based human index
    kpoint : keypoints (float type range : 0.0 ~ 1.0 ==> later multiply by image width, height
    '''
    def get_keypoint(self, humans, hnum, peaks):
        #check invalid human index
        kpoint = []
        human = humans[0][hnum]
        C = human.shape[0]
        for j in range(C):
            k = int(human[j])
            if k >= 0:
                peak = peaks[0][j][k]   # peak[1]:width, peak[0]:height
                peak = (j, float(peak[0]), float(peak[1]))
                kpoint.append(peak)
                #print('index:%d : success [%5.3f, %5.3f]'%(j, peak[1], peak[2]) )
            else:
                peak = (j, None, None)
                kpoint.append(peak)
                #print('index:%d : None %d'%(j, k) )
        return kpoint


    def preprocess(self, image):
        self.device = torch.device('cuda')
        image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
        image = PIL.Image.fromarray(image)
        image = transforms.functional.to_tensor(image).to(self.device)
        image.sub_(self.mean[:, None, None]).div_(self.std[:, None, None])
        return image[None, ...]

    def execute(self, img):
        data = self.preprocess(img)
        cmap, paf = self.model_trt(data)
        cmap, paf = cmap.detach().cpu(), paf.detach().cpu()
        counts, objects, peaks = self.parse_objects(cmap, paf)#, cmap_threshold=0.15, link_threshold=0.15)

        keypoints_all = []
        for i in range(counts[0]):
            keypoints = self.get_keypoint(objects, i, peaks)
            keypoints_all.append(keypoints)

        return keypoints_all

    def open(self, frame_width, frame_height):
        self.frame_width = frame_width #640
        self.frame_height = frame_height #480
        print("open with width:",self.frame_width, " height:", self.frame_height)
        self.cap = cv.VideoCapture(0, cv.CAP_V4L2)
        self.cap.set(cv.CAP_PROP_FRAME_WIDTH, self.frame_width)
        self.cap.set(cv.CAP_PROP_FRAME_HEIGHT, self.frame_height)

        ret_val, img = self.cap.read()
        #fourcc = cv.VideoWriter_fourcc('m', 'p', '4', 'v')
        #out_video = cv.VideoWriter('/tmp/output.mp4', fourcc, cap.get(cv.CAP_PROP_FPS), (640, 480))
        self.count = 0

        self.X_compress = 1.0 * self.frame_width / self.width
        self.Y_compress = 1.0 * self.frame_height / self.height


        if self.cap is None:
            print("Camera Open Error")
            sys.exit(0)

        self.parse_objects = ParseObjects(self.topology)
        self.draw_objects = DrawObjects(self.topology)


    def detect(self):
        if self.cap.isOpened():
            ret_val, frame = self.cap.read()
            self.frame = frame # Save the frame

            if ret_val == False:
                print("Camera read Error")
                return

            img = cv.resize(frame, dsize=(self.width, self.height), interpolation=cv.INTER_AREA)
            keypoints = self.execute(img)
            # Convert to standard keypoints
            keypoints = to_keypoints(keypoints, fmt="trt_pose")
            # Scale the keypoints
            keypoints = scale_keypoints(keypoints, self.frame_width, self.frame_height)
            self.count += 1

            #print("keypoints: ", keypoints)
            # Body tracker
            bodies = self.body_tracker.update(keypoints)

            return bodies

    def get_frame(self):
        return self.frame

    def stop(self):
        cv.destroyAllWindows()
        #out_video.release()
        self.cap.release()



if __name__ == '__main__':
    body_detector = body_detector()
    body_detector.open(800, 600)
    bodies = body_detector.detect() # Read the frame and detect body
    for body in bodies:
        print(body.keypoints)
    body_detector.stop()
