#ifndef SHAREDMSTTING_H
#define SHAREDMSTTING_H

#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cmath>
#include <vector>

#include "cl_utilities.h"
#include "typedef.h"


using namespace std;
using namespace cv;


const int kI = 10;
const int kG = 4;
const double kC = 5.0;

//const int numUnknownPixelsMax = 921600;
const int numPickedPointsMax = kG;

class SharedMatting
{
public:
    SharedMatting();
    ~SharedMatting();
    void loadImage(string filename);
    void loadTrimap(string filename);
    void setTrimapData(uchar* data);
    void expandKnown();

    void init();
    void estimateAlpha(cv::Mat img, cv::Mat trimap);

    void solveAlpha();
    void save(string filename);
    Mat normalize(Mat src);
    Mat unnormalize(Mat src);
    cv::Mat getMatte();
    cv::Mat blend(Mat src, Mat alpha, Mat bg); 
    cv::Mat getImageBlended();
    void release();

private:
    void sampleGpu();
    void gatherGpu();
    void refineGpu();
    void localSmoothGpu();

    void loadTrimap(cv::Mat trimap);
    void loadImage(cv::Mat src);

    bool initGpu();
    bool initBuf();

    void finalizeGpu();
    void releaseBuf();

    bool m_isInit = false;

    cv::Mat m_image;
    cv::Mat m_trimap;
    cv::Mat m_matte;

    int numUnknownPixels = 0;

    int numUnknownPixelsMax = 0;



    Tuple* m_tuples = NULL;
    Ftuple* m_ftuples = NULL;
    PixelPoint* m_unknownPixels = NULL;
    PixelPoint* m_fgPoint = NULL;
    PixelPoint* m_bgPoint = NULL;// BgData;
    int* m_numFgPoints = NULL;
    int* m_numBgPoints = NULL;
    int* m_unknownIndexes = NULL;

    cl_uchar* m_trimapBuf = NULL;
    cl_uchar* m_alphaBuf = NULL;
    cl_uchar* m_imageBuf = NULL;

    int m_width = 0;
    int m_height = 0;
    int m_channels = 0;
    int m_step = 0;

    cl_device_id m_device = NULL;
    cl_context m_context = NULL;
    cl_command_queue m_queue = NULL;
    cl_program m_program = NULL;
    cl_kernel m_sampleCl = NULL, m_gatherCl = NULL, m_refineCl = NULL, m_localSmoothCl = NULL;

    cl_int err;
    size_t globalSize[2];
    cl_event event;


    cl_mem m_unknownPixelsMem = NULL, m_fgPointsMem = NULL,
        m_bgPointsMem = NULL, m_numFgPointsMem = NULL,
        m_numBgPointsMem = NULL, m_unknownIndexesMem = NULL,
    m_trimapMem = NULL, m_tuplesMem = NULL, m_imageMem = NULL,
    m_ftuplesMem = NULL, m_alphaMem = NULL;







};



#endif
