import cv2 as cv
import numpy as np
from .wide_resnet import WideResNet


class AgDetector:

    def __init__(self, face_detector):

        self.face_detector = face_detector

        self.ag_size = 64 # image size used in this detecor.
        self.ag_margin = 0.4
        model_depth = 16
        model_width = 8
        self.model = WideResNet(self.ag_size, depth=model_depth, k=model_width)()


    def load_weights(self, file_path):

        try: 
            self.model.load_weights(file_path)

        except: 
            print("Error: there is no {}.".format(file_path))


    def detect(self, image, convert_rgb=True):
        # Detect age and gender.
        # image: array-like
        #    If image is BGR format, convert_rgb should be True. If image is RGB format, convert_rgb should be False. 

        # Detect faces
        img = image
        if convert_rgb:
            img = cv.cvtColor(image, cv.COLOR_BGR2RGB)

        image_height, image_width = image.shape[0:2]   

        bboxes = self.face_detector.detect(img)
        num_faces = len(bboxes)

        # Predict ages and genders.
        if num_faces > 0:

            ag_size = self.ag_size 
            faces = np.empty((num_faces, ag_size, ag_size, 3))

            for i, bbox in enumerate(bboxes): 

                x1, y1, w, h = bbox
                x2 = x1 + w
                y2 = y1 + h

                xw1 = max(int(x1 - self.ag_margin * w), 0)
                yw1 = max(int(y1 - self.ag_margin * h), 0)
                xw2 = min(int(x2 + self.ag_margin * w), image_width-1)
                yw2 = min(int(y2 + self.ag_margin * h), image_height-1)
                faces[i, :, :, :] = cv.resize(img[yw1:yw2 + 1, xw1:xw2 + 1, :], (ag_size, ag_size))

                xc = int(x1 + 0.5*w)
                yc = int(y1 + 0.5*h)

            # predict ages and genders of the detected faces
            results = self.model.predict(faces)
            predicted_genders = results[0]
            predicted_genders = [ 1 if g[0] < 0.5 else 0 for g in predicted_genders]

            ages = np.arange(0, 101).reshape(101, 1)
            predicted_ages = results[1].dot(ages).flatten()
            predicted_ages = [int(age + 0.5) for age in predicted_ages]


        # Prepare outputs
        out = []
        for i in range(num_faces):

            bbox = bboxes[i] 
            age = predicted_ages[i]
            gender = predicted_genders[i]

            out.append([bbox, age, gender])

        return out

