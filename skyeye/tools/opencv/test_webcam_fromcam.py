import cv2
import time

cap = cv2.CaptureFromCAM(0)
#cap = cv2.VideoCapture(0)

# Frame size: 224x224 or 800x600
#frame_width = 320 #224
#frame_height = 240 #224
frame_width = 800
frame_height = 600

cap.set(cv2.CAP_PROP_FRAME_WIDTH, frame_width)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, frame_height)

while(True):
  t0 = time.time()

  ret, frame = cap.read()
  cv2.imshow('frame', frame)

  t1 = time.time()
  fps = 1.0 / (t1 - t0)
  print("fps: {}".format(fps))

  if cv2.waitKey(1) & 0xFF == ord('q'):
    break



cap.release()
cv2.destroyAllWindows()
