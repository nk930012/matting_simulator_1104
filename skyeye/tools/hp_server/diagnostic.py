import logging
import websocket
import optparse
import json
import websocket
import numpy as np
import cv2
from time import perf_counter
import logging
from time import sleep
import sys
import os
#sys.path.append("../../../touch")
from skyeye.detect.body.body import Body 


index_blue = 0
index_green = 1
index_red = 2
index_cyan = 3
index_magenta = 4
index_yellow = 5
index_black = 6

#(blue, green, read)
default_color = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255), (0 , 0, 0)]

def draw_msg(img, msg, x, y, color):

    cv2.putText(img, msg, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y_shift = 20
    y += y_shift

    return (x, y)


def formated_message(name, items):

    l = [tuple('{0:.2f}'.format(flt) for flt in sublist) for sublist in items]
    msg = "{}: {}".format(name, l)
    
    return msg


def draw_bbox(image, x, y, xw, yh):
    cv2.rectangle(image, (xw, y), (x, yh), (0, 255, 0), 2)
    
def drawline( img, pt1, pt2, color, thickness=1, style='dotted', gap=20 ):
    dist =((pt1[0]-pt2[0])**2+(pt1[1]-pt2[1])**2)**.5
    pts= []
    for i in  np.arange(0,dist,gap):
        r=i/dist
        x=int((pt1[0]*(1-r)+pt2[0]*r)+.5)
        y=int((pt1[1]*(1-r)+pt2[1]*r)+.5)
        p = (x,y)
        pts.append(p)

    if style=='dotted':
        for p in pts:
            cv2.circle(img,p,thickness,color,-1)
    else:
        s=pts[0]
        e=pts[0]
        i=0
        for p in pts:
            s=e
            e=p
            if i%2==1:
                cv2.line(img,s,e,color,thickness)
            i+=1

def draw_grid(img, width, height):
    division_x = 10
    division_y = 10
    increament_x = int( width / division_x )
    increament_y = int( height / division_y )
    color = (240, 32, 160)
    thickness = 1 
    lineType = 4
    gap = 10
    style='dashed'
    
    for i in range(division_x):
    
        x = (increament_x * i)
        if(i != 0):
            msg = "{:3.2f}".format(i * 0.1)
            draw_msg(img, msg, x, ( height - 30 ), color)
        
        drawline( img, (x, 0), (x, height), color, thickness, style, gap)
    
    for i in range(division_y):
        y = (increament_y * i)
        if(i != 0):
            msg = "{:3.1f}".format(i * 0.1)
            draw_msg(img, msg, (width - 30), y, color)
        drawline( img, (0, y), (width, y), color, thickness, style, gap)
    

def parse_arguments():
    parser = optparse.OptionParser()
    parser.add_option('-i', '--ip', dest='ip', default="127.0.0.1",
                    help=('the IP address of server'))
    parser.add_option('-p', '--port', dest='port', default=8050,
                    help=('the port of server'))

    parser.add_option('--width', dest='width', default=800,
                    help=('width of img'))

    parser.add_option('--height', dest='height', default=600,
                    help=('height of img'))


    (options, args) = parser.parse_args()
    return options, args

class DiagnosisWebsocketData:

    def __init__(self, ip="127.0.0.1", port=8050, debug=logging.ERROR, width=640, height=680):
        self.fps = 0
        self.frame_count = 0
        self.ip = ip
        self.port = port
        self.debug = debug
        logging.basicConfig(level=self.debug, format='%(asctime)s -(%(filename)s)%(levelname)s : %(message)s')
        self.width=width
        self.height=height
        self.is_img_init = False
        self.img = np.zeros((height, width, 3), np.uint8)
        self.url = url = "ws://"+ip+":"+str(port)+"/ws"
        self.time_start = 0
        self.time_end = 0
        self.id = 0
        self.trackid = (-1)
        websocket.enableTrace(True)
        self.ws = websocket.WebSocketApp(self.url,
        on_message = lambda ws,msg: self.message(ws, msg),
        on_error   = lambda ws,msg: self.error(ws, msg),
        on_close   = lambda ws:     self.close(ws),
        on_open    = lambda ws:     self.open(ws))
    

    def message(self, ws, message):
        
        if( 1 == self.frame_count ):
            self.time_end = perf_counter()
            time_duration = self.time_end - self.time_start
            self.fps = int( 1 / time_duration )
            self.frame_count = -1
        elif( 0 == self.frame_count ):
            self.time_start = perf_counter()
        
        self.frame_count += 1
        
        json_string = json.loads( message )

        if( False == self.is_img_init ):
            if( 'size' in json_string ):
                ( width, height ) = json_string[ 'size' ]
                self.width = width
                self.height = height
                self.img = np.zeros( ( height, width, 3 ), np.uint8 )
                self.is_img_init = True
        
        self.img.fill( 255 )

        if( 'bodies' in json_string ):

            bodies = json_string[ 'bodies' ]
            
            if( 0 < len( bodies ) ):

                text_x = 20
                text_y = 20
                text_y_shift = 20
             
                num_bodies = len( bodies )
                distance_msg = ""
                id_msg = "";
                gaze_u_msg = "";
                gaze_v_msg = "";
                for i in range( num_bodies ):

                    body_id = bodies[ i ][ 'id' ]

                    gaze_point = bodies[ i ][ 'gaze' ]
                    face_point = bodies[ i ][ 'face' ]
                    distance = bodies[ i ][ 'distance' ]
                    
                    id_msg += " {}".format(body_id)
                    distance_msg += " {}".format(distance)
                    #print('width:{}, height:{}, id:{}, distance:{}, gaze:{}, face:{}'.format(self.width, self.height, body_id, distance, gaze_point, face_point))

                    face_point_x = int(round(bodies[ i ][ 'face' ][0] * self.width, 0))
                    face_point_y = int(round(bodies[ i ][ 'face' ][1] * self.height, 0))
                    msg = "{}".format( body_id )
                    draw_msg( self.img, msg, face_point_x - 5, face_point_y - 30, default_color[ 0 ] )
                    cv2.circle( self.img, ( face_point_x, face_point_y), 10, default_color[ index_blue ], -1 )


                    gaze_point_u = bodies[ i ][ 'gaze' ][0]
                    gaze_point_v = bodies[ i ][ 'gaze' ][1]


                    gaze_u_msg += " {}".format(gaze_point_u);
                    gaze_v_msg += " {}".format(gaze_point_v);



                    gaze_point_u = int(round(gaze_point_u * self.width, 0))
                    gaze_point_v = int(round(gaze_point_v * self.height, 0))
                    

                    
                    
                    draw_msg( self.img, msg, gaze_point_u - 5, gaze_point_v - 30, default_color[ index_green ] )
                    cv2.circle( self.img, ( gaze_point_u, gaze_point_v), 10, default_color[ index_green ], -1 )
 
                if(num_bodies > 0):
                    draw_grid(self.img, self.width, self.height)
                    msg = "fps : {}, num_bodies : {}".format( self.fps, num_bodies )
                    ( text_x, text_y ) = draw_msg( self.img, msg, text_x, text_y, default_color[ index_magenta ] )
                    
                    id_msg = "id : [" + id_msg +"]"
                    ( text_x, text_y ) = draw_msg( self.img, id_msg, text_x, text_y, default_color[ index_magenta ] )
                    
                    
                    gaze_u_msg = "gaze_u : [" + gaze_u_msg +"]"
                    ( text_x, text_y ) = draw_msg( self.img, gaze_u_msg, text_x, text_y, default_color[ index_magenta ] )
                    
                    
                    gaze_v_msg = "gaze_v : [" + gaze_v_msg +"]"
                    ( text_x, text_y ) = draw_msg( self.img, gaze_v_msg, text_x, text_y, default_color[ index_magenta ] )
                    
                    
                    
                    distance_msg = "distance : [" + distance_msg +"]"
                    ( text_x, text_y ) = draw_msg( self.img, distance_msg, text_x, text_y, default_color[ index_magenta ] )
        cv2.imshow( 'diagnosis image', self.img )
               
        key = cv2.waitKey( 1 )
        if key == ord( "q" ) or 27 == key:
            self.close( websocket )

    def error( self, ws, error ):
        print( error )


    def close(self, ws):
        print( "close" )
        cv2.destroyAllWindows()
        self.id += 1
        id_string = '{id}'.format( id = self.id )
        close_jetson_cmd = '{"id":'+ id_string +', "method": "command","params": {"command":"close", "property":"jetson_device"}}'
        self.ws.send( close_jetson_cmd )        
        self.ws.keep_running = False
        self.ws.close()

    def open(self, ws):
        self.id += 1
        print( 'Open websocket server ip: {}, port: {}'.format(self.ip, self.port) )
        id_string = '{id}'.format( id = self.id )
        open_jetson_cmd = '{"id":'+ id_string +', "method": "command","params": {"command":"open", "property":"jetson_device"}}'
        self.ws.send( open_jetson_cmd )
        sleep(0.05)

    def start(self):
        if None != self.ws:
            self.ws.run_forever()


if __name__ == "__main__":
    options, args = parse_arguments()
    ip = options.ip
    port = int(options.port)
    debug = logging.ERROR
    width = int(options.width)
    height = int(options.height)
    cv2.namedWindow("diagnosis image", cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("diagnosis image",cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    websocket = DiagnosisWebsocketData(ip, port, debug, width, height)
    websocket.start()
