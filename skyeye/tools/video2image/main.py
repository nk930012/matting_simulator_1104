import os
import cv2 as cv
import numpy as np
import yaml
from skyeye.utils.opencv import wait_key
from skyeye.utils.file import to_str_digits

if __name__ == '__main__':


    input_file = "inputs.yaml"
    with open(input_file, 'r', encoding='utf-8') as f:
        inputs = yaml.load(f, Loader=yaml.Loader)

    # Set input parameters
    video_path = inputs['video_path']
    output_dir_path = inputs['output_dir_path']
    output_width = inputs['output_width']
    output_height = inputs['output_height']
    output_ext = inputs['output_ext']

    # Checking
    if not os.path.exists(output_dir_path):
        os.mkdir(output_dir_path)  

    cap = cv.VideoCapture(video_path)
    ret, frame = cap.read()

    frame_height = frame.shape[0]
    frame_width = frame.shape[1]

    print("Processing ...")

    frame_count = -1
    while(True):

        frame_count += 1
        ret, frame = cap.read()

        if ret == False:
            break

        image = cv.resize(frame, (output_width, output_height), interpolation=cv.INTER_CUBIC)

        # Write out image
        prefix = 'img_'
        index = to_str_digits(frame_count, num_digits=5)
        filename = "{}{}.{}".format(prefix, index, output_ext)
        filepath = os.path.join(output_dir_path, filename) 
        cv.imwrite(filepath, image)

        cv.imshow('Win', image)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # Finalization
    print("End of run.")

    cap.release()
    cv.destroyAllWindows()
