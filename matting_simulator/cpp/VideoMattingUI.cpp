#include "VideoMattingUI.h"
#include "ui_VideoMattingUI.h"

#include <windows.h>
#include <QDebug>
#include <QMouseEvent>
#include <QColor>
#include <QDir>
#include <QCoreApplication>
#include <QVector>
#include <QDebug>
#include <QTextBlock>
#include <QImage>

VideoMattingUI::VideoMattingUI(QWidget* parent) : QMainWindow(parent), ui(new Ui::VideoMattingUIClass)
{

    ui->setupUi(this);
    ui->label_show_image->setText("程式開始，請先輸入檔案路徑，然後按begin");
}


VideoMattingUI::~VideoMattingUI()
{
    delete ui;
}

// 初始化

void VideoMattingUI::get_outputs(string& inputs, string& videoName,string &outputs, string& vtoi_outputs)
{
    inputs = ui->lineEdit_path->text().toStdString();
    videoName= ui->lineEdit_name->text().toStdString();

    int run_num = ui->spinBox->value();
    char filePath[1024]; char filePath_vtoi[1024]; char filePath_in[1024];
    sprintf_s(filePath_vtoi, (inputs + videoName + "/images/").c_str(), run_num);
    sprintf_s(filePath, (inputs + videoName + "/outputs_run_%d/").c_str(), run_num);
    vtoi_outputs = filePath_vtoi;
    outputs = filePath;

    
    ifstream myFile;
    myFile.open(inputs + videoName + ".avi",ios::in);
    if (!myFile)
    {
        string videoName = videoName + ".mp4";
    }
    else
    {
        string videoName = videoName + ".avi";

    }

}
void VideoMattingUI::on_pushButton_begin_clicked()
{
    check_run(do_notdo);
    qDebug() << "check run log exist or not"<<do_notdo;

    if (do_notdo != 0)// 沒有此檔案
    {
        ui->label_show_image->setText("這個run # 已經有產生過logfile，請更換run number");
    }
    else
    {
        ui->label_show_image->setText("開始讀取第一張圖片");
        get_outputs(inputs, videoName, outputs, vtoi_outputs);
        CreateDirectoryA((inputs + videoName + "/").c_str(), NULL);
        CreateDirectoryA((inputs + videoName + "/images/").c_str(), NULL);
        CreateDirectoryA((outputs).c_str(), NULL);
        CreateDirectoryA((outputs + "RemoveBG/").c_str(), NULL);
        CreateDirectoryA((outputs + "trimap/").c_str(), NULL);
        
        int totalFrameCnt = 0; int frameCnt = 0; int width = 0; int height = 0;//initialize value

        cv::Mat image = cv::Mat::zeros(cv::Size(width, height), CV_8UC3);
        InitProgram initP;
        initP.GetFirstImage(inputs, vtoi_outputs, videoName, width, height, image, totalFrameCnt);// 這邊就先知道總共有幾張圖了


        if (width == 0)// 確定有讀到圖片及其尺寸再往下
        {
            ui->label_show_image->setText("沒有讀到影片，請確認影片路徑及名稱是否正確");
        }

        else if (width >= 0)// 確定有讀到圖片及其尺寸再往下
        {
            QPixmap pixImg((vtoi_outputs + "00000.png").c_str());
            show_fig(pixImg);

            if (ui->checkBox->isChecked())
            {
                ui->label_information->setText("程式開始，請耐心等待，bar 停止前請不要有其他動作");
                ConvertVideoToImage(frameCnt, width, height, totalFrameCnt);//讀取影片轉圖片
            }
            else
            {
                ui->label_information->setText("目前為選擇不執行影片轉圖片的動作，若需要重新轉圖片，請勾選New ");
            }
            update_image_info(totalFrameCnt, width, height);//update

        }
        image.release();
    }    
}

void VideoMattingUI::update_image_info(
    int& totalFrameCnt,
    int& width,
    int& height
)
{
    QString s_totalFrameCnt = QString::number(totalFrameCnt-1, 10, 0);
    QString s_width = QString::number(width, 10, 0);
    QString s_height = QString::number(height, 10, 0);
    ui->lineEdit_num_image->setText(s_totalFrameCnt);
    ui->lineEdit_width->setText(s_width);
    ui->lineEdit_height->setText(s_height);
}

void VideoMattingUI::get_image_info(int& width, int& height, int& totalFrameCnt)
{
    width = ui->lineEdit_width->text().toInt();
    height = ui->lineEdit_height->text().toInt();
    totalFrameCnt=ui->lineEdit_num_image->text().toInt();
}

void VideoMattingUI::get_trimap_image_info(int& width_trimap, int& height_trimap)
{
    get_page(page_start, page_end);
    get_outputs(inputs, videoName, outputs, vtoi_outputs);

    string filePath = outputs + "trimap/" + value_path[page_start] + ".png";//input圖片存放路徑
    cv::Mat image_trimap = cv::Mat::zeros(cv::Size(width, height), CV_8UC3);

    image_trimap = imread(filePath, cv::IMREAD_COLOR);
    width_trimap = image_trimap.cols;
    height_trimap = image_trimap.rows;

    image_trimap.release();
}

void VideoMattingUI::get_blend_image_info(int& width_blend, int& height_blend)
{
    get_page(page_start, page_end);
    get_outputs(inputs, videoName, outputs, vtoi_outputs);

    string filePath = outputs + "RemoveBG/" + value_path[page_start] + ".png";//input圖片存放路徑
    cv::Mat image_blend = cv::Mat::zeros(cv::Size(width, height), CV_8UC3);

    image_blend = imread(filePath, cv::IMREAD_COLOR);
    width_blend = image_blend.cols;
    height_blend = image_blend.rows;

    image_blend.release();
}

void VideoMattingUI::ConvertVideoToImage(
    int& frameCnt,
    int& width,
    int& height,
    int& totalFrameCnt)
{
    get_outputs(inputs, videoName, outputs, vtoi_outputs);
    cv::VideoCapture cap = cv::VideoCapture(inputs + videoName+".avi");

    bool success = true;

    cv::Mat frame;

    char imageName[128];// 這是buffer
    frameCnt = 0;

    Timer timer;
    timer.tic();
    while (1) 
    {

        success = cap.read(frame);

        if (!success)
            break;
        get_outputs(inputs, videoName, outputs, vtoi_outputs);
        sprintf_s(imageName, "%s%05d.png", (vtoi_outputs).c_str(), frameCnt);

        cv::imwrite(imageName, frame);
        ++frameCnt;
        qDebug() << "影片轉圖片處理進度:" << frameCnt << "/" << totalFrameCnt;
        ui->progressBar_toimage->setMaximum(totalFrameCnt);
        ui->progressBar_toimage->setValue(frameCnt);
    }
    timer.toc();

    QString time_sec = QString::number(timer.get_dt(), 10, 0);
    ui->lineEdit_time_vtoi->setText(time_sec);
    
    frame.release();

}

void VideoMattingUI::show_fig(QPixmap pixImg)
{
    pixImg = pixImg.scaled(ui->label_show_image->width(), ui->label_show_image->height());
    ui->label_show_image->setPixmap(pixImg);
}

void VideoMattingUI::check_run(int &do_notdo)
{
    int run_num = ui->spinBox->value();
    char filePath[1024];
    get_outputs(inputs, videoName, outputs, vtoi_outputs);
    sprintf_s(filePath, (outputs + "log.txt").c_str(), run_num);
    qDebug() << "filepath" << filePath;

    ofstream myFile;
    myFile.open(filePath, ios::out);
    if (!myFile.is_open())
    {
        do_notdo = 0;// 開啟檔案失敗
        qDebug() << "這個run # OK";
    }
    else
    {
        do_notdo = 1;
        qDebug() << "需更換run #";
    }
    myFile.close();

}

// 選圖片
void VideoMattingUI::on_pushButton_fw_clicked()
{
    get_image_info(width, height, totalFrameCnt);
    if (width == 0)
    {
        ui->label_show_image->setText("請先按begin由影片轉圖片，再來選圖片");
    }

    else if (width > 0)// 確定有讀到圖片及其尺寸再往下
    {
        img_index = ui->lineEdit_image_index->text().toInt();

        if (img_index > 0)
        {
            QString s_imgindex = QString::number(img_index - 1, 10, 0);
            ui->lineEdit_image_index->setText(s_imgindex);

        }
    }

}

void VideoMattingUI::on_pushButton_bw_clicked()
{
    get_image_info(width, height, totalFrameCnt);
    if (width == 0)// 確定有讀到圖片及其尺寸再往下
    {
        ui->label_information->setText("請先按begin由影片轉圖片，再來選圖片");
    }

    else if (width > 0)// 確定有讀到圖片及其尺寸再往下
    {
        int img_index = ui->lineEdit_image_index->text().toInt();
        totalFrameCnt = ui->lineEdit_num_image->text().toInt();

        if (img_index < totalFrameCnt)
        {
            QString s_imgindex = QString::number(img_index + 1, 10, 0);
            ui->lineEdit_image_index->setText(s_imgindex);
            ui->label_information->setText("請點選要看哪種圖片:origin,trimap or blend");

        }
        else
        {
            ui->label_information->setText("圖片沒有那麼多張!!!請輸入數字小於image num");

        }
    }

}


// 點+ 存背景顏色
void VideoMattingUI::mousePressEvent(QMouseEvent* event)
{
    //click 找到對應相對位置
    //qDebug() <<"mouse clicked(global)"<< event->x() << ":" << event->y()<<"\n";;
    //qDebug() <<"mouse clicked(loacal)"<< ui->label_show_image->mapFromGlobal(event->globalPos())<<"\n";//需確認不同大小圖片的問題

    // 畫面上的相對位置
    double col = ui->label_show_image->mapFromGlobal(event->globalPos()).rx();//width
    double row = ui->label_show_image->mapFromGlobal(event->globalPos()).ry();//height
    qDebug() << "inti of col" << col << ",row" << row;
    get_image_info(width, height, totalFrameCnt);
    col = col * width*1.0 / 1152.0;// 轉成對應圖片大小的實際pixel 位置
    row = row * height*1.0 / 864.0;//(1152,864) correspond to label size
    qDebug() << "col" << col <<",row" << row;

    // 改成string 寫入
    QString s_col = QString::number(col, 10, 0);
    QString s_row = QString::number(row, 10, 0);
    ui->lineEdit_x->setText(s_col);
    ui->lineEdit_y->setText(s_row);
    qDebug() << "transfer col" << s_col << ",row" << s_row;
    get_pixel_color(col, row);// 讀取點取出顏色

}

void VideoMattingUI::get_image_num(char* value)
{
    int image_index = ui->lineEdit_image_index->text().toInt();
    sprintf(value, "%05d", image_index);

}

void VideoMattingUI::get_pixel_color(int set_col, int set_row)
{
    // 位置存取向量
    QVector<int> vector(2);
    int* data = vector.data();
    get_image_info(width, height, totalFrameCnt);

    QImage img;
    get_image_num(value);
    qDebug() << "get_image_num" << value;
    get_outputs(inputs, videoName, outputs, vtoi_outputs);
    img.load((vtoi_outputs + value + ".png").c_str());

    if ((set_col >= 0) & (set_col <= width) & (set_row >= 0) & (set_row <= height))
    {
        // 只有在圖片範圍內的點取出
        data[0] = set_col;
        data[1] = set_row;
        qDebug() << "col,row=" << data[0] << data[1];

        //取出的點得到BGR顏色資訊並存到框內
        const QColor qcolor = img.pixelColor(data[0], data[1]);
        QString s_b = QString::number(qcolor.blue(), 10, 0);
        QString s_g = QString::number(qcolor.green(), 10, 0);
        QString s_r = QString::number(qcolor.red(), 10, 0);
        ui->get_b->setText(s_b);
        ui->get_g->setText(s_g);
        ui->get_r->setText(s_r);
        qDebug() << qcolor;

        //取出的點得到HSV顏色資訊並存到框內
        const QColor qcolor_HSV = img.pixelColor(data[0], data[1]).toHsv();
        QString s_h = QString::number(qcolor_HSV.hue(), 10, 0);
        QString s_s = QString::number(qcolor_HSV.saturation(), 10, 0);
        QString s_v = QString::number(qcolor_HSV.value(), 10, 0);
        ui->get_h->setText(s_h);
        ui->get_s->setText(s_s);
        ui->get_v->setText(s_v);
        qDebug() << qcolor_HSV;

    }

    else
    {
        ui->get_b->setText("NA");
        ui->get_g->setText("NA");
        ui->get_r->setText("NA");
        ui->get_h->setText("NA");
        ui->get_s->setText("NA");
        ui->get_v->setText("NA");
    }

}

void VideoMattingUI::change_index(QPlainTextEdit* plaina, QPlainTextEdit* plainb)
{
    QString s_b = ui->get_b->text(), s_g = ui->get_g->text(), s_r = ui->get_r->text();
    if (plaina->blockCount() >= 3)
    {
        plaina->setPlainText("");
    }
    plaina->appendPlainText(s_b);
    plaina->appendPlainText(s_g);
    plaina->appendPlainText(s_r);

    //HSV color
    QString s_h = ui->get_h->text(), s_s = ui->get_s->text(), s_v = ui->get_v->text();
    if (plainb->blockCount() >= 3)
    {
        plainb->setPlainText("");
    }
    plainb->appendPlainText(s_h);
    plainb->appendPlainText(s_s);
    plainb->appendPlainText(s_v);

}


void VideoMattingUI::on_pushButton_savecolor1_clicked()
{
    change_index(ui->plainTextEdit_1, ui->plainTextEdit_H1);
}

void VideoMattingUI::on_pushButton_savecolor2_clicked()
{
    change_index(ui->plainTextEdit_2, ui->plainTextEdit_H2);
}

void VideoMattingUI::on_pushButton_savecolor3_clicked()
{
    change_index(ui->plainTextEdit_3, ui->plainTextEdit_H3);
}

void VideoMattingUI::on_pushButton_savecolor4_clicked()
{
    change_index(ui->plainTextEdit_4, ui->plainTextEdit_H4);
}

void VideoMattingUI::on_pushButton_savecolor5_clicked()
{
    change_index(ui->plainTextEdit_5, ui->plainTextEdit_H5);
}


//改變page

void VideoMattingUI::get_page(int& page_start, int& page_end)
{
    
    page_start = ui->lineEdit_page_start->text().toInt();
    page_end = ui->lineEdit_page_end->text().toInt();

}


// 存取logfile
void VideoMattingUI::get_time_process(int& time_vtoi, int& time_trimap_matting, int& time_blend_matting, int& time_trimap_video, int& time_blend_video)
{
    time_vtoi = ui->lineEdit_time_vtoi->text().toInt();
    time_trimap_matting = ui->lineEdit_time_trimap_matting->text().toInt();
    time_blend_matting = ui->lineEdit_time_blend_matting->text().toInt();
    time_trimap_video = ui->lineEdit_time_trimap_video->text().toInt();
    time_blend_video= ui->lineEdit_time_blend_video->text().toInt();

}

void VideoMattingUI::on_pushButton_log_clicked()
{

    //錯誤資訊 待確認要建立什麼??
    
    //建立路徑
    ofstream filePtr;
    int run_num = ui->spinBox->value();
    char filePath[1024];
    get_outputs(inputs, videoName, outputs, vtoi_outputs);
    sprintf_s(filePath, (outputs + "log.txt").c_str(), run_num);

    QString outputlog;
    get_string(filePath, outputlog);

    ui->label_information->setText("程式結束，logfile 存到路徑:" + outputlog);

    filePtr.open(filePath, ios::out);//開始寫入檔案

    get_image_info(width, height, totalFrameCnt);
    get_time_process(time_vtoi, time_trimap_matting, time_blend_matting, time_trimap_video, time_blend_video);
    get_page(page_start, page_end);
    get_time_process(time_vtoi, time_trimap_matting, time_blend_matting, time_trimap_video, time_blend_video);
    get_image_num(value);


    filePtr << "----file information--------------- \n";
    filePtr << "file input is:" << inputs << videoName <<"\n";
    filePtr << "image (width,height)=(" << width << "," << height << ")\n";
    filePtr << "total image number is:" << totalFrameCnt << " pages\n";
    filePtr << "output original image path is: " << vtoi_outputs << "\n";
    filePtr << "one page test image_num:" << value << "\n";
    filePtr << "output one page test path is: " << vtoi_outputs << "and correpond name is test_org/mask/trimap/blend.png\n";
    filePtr << "output trimap image path" << outputs<<"trimap/" << "\n";
    filePtr << "output blend image path" << outputs << "RemoveBG/" << "\n";
    filePtr << "----------------------------------- \n\n\n\n\n";
    
    filePtr << "---- - time of process (in sec)--------------\n";
    filePtr << "time: video->image:" << time_vtoi << " sec \n";
    filePtr << "page range:P." << page_start << " ~ " << page_end << " \n";
    filePtr << "get trimap images takes:" << time_trimap_matting << " sec\n";
    filePtr << "get blend images takes:" << time_blend_matting << " sec\n";
    filePtr << "trimap images->trimpa video takes:" << time_trimap_video << " sec\n";
    filePtr << "blend images->trimpa video takes:" << time_blend_video << " sec\n";
    filePtr << "----------------------------------- \n\n\n\n\n";



    QVector<int> keyColor_1(3), keyColor_2(3), keyColor_3(3), keyColor_4(3), keyColor_5(3);
    int* color1_data = keyColor_1.data(), * color2_data = keyColor_2.data(), * color3_data = keyColor_3.data(), * color4_data = keyColor_4.data(), * color5_data = keyColor_5.data();
    filePtr << "-----BGR color information---------------\n";
    get_color_vector(color1_data, color2_data, color3_data, color4_data, color5_data);
    filePtr << "color information(color 1):(" << *(color1_data)<<"," << *(color1_data+1) << "," << *(color1_data + 2) << ")\n";
    filePtr << "color information(color 2):(" << *(color2_data) << "," << *(color2_data + 1) << "," << *(color2_data + 2) << ")\n";
    filePtr << "color information(color 3):(" << *(color3_data) << "," << *(color3_data + 1) << "," << *(color3_data + 2) << ")\n";
    filePtr << "color information(color 4):(" << *(color4_data) << "," << *(color4_data + 1) << "," << *(color4_data + 2) << ")\n";
    filePtr << "color information(color 5):(" << *(color5_data) << "," << *(color5_data + 1) << "," << *(color5_data + 2) << ")\n";
    filePtr << "----------------------------------- \n\n\n\n\n";

    filePtr << "---- - HSV color information--------------\n";
    get_HSV_color_vector(color1_data, color2_data, color3_data, color4_data, color5_data);
    filePtr << "color information(color 1):(" << *(color1_data) << "," << *(color1_data + 1) << "," << *(color1_data + 2) << ")\n";
    filePtr << "color information(color 2):(" << *(color2_data) << "," << *(color2_data + 1) << "," << *(color2_data + 2) << ")\n";
    filePtr << "color information(color 3):(" << *(color3_data) << "," << *(color3_data + 1) << "," << *(color3_data + 2) << ")\n";
    filePtr << "color information(color 4):(" << *(color4_data) << "," << *(color4_data + 1) << "," << *(color4_data + 2) << ")\n";
    filePtr << "color information(color 5):(" << *(color5_data) << "," << *(color5_data + 1) << "," << *(color5_data + 2) << ")\n";
    filePtr << "----------------------------------- \n\n\n\n\n";

    filePtr.close();
}


//show fig

void VideoMattingUI::get_color_vector(int* color1_data, int* color2_data, int* color3_data, int* color4_data, int* color5_data)
{
    *(color1_data) = ui->plainTextEdit_1->document()->findBlockByLineNumber(0).text().toInt();
    *(color1_data + 1) = ui->plainTextEdit_1->document()->findBlockByLineNumber(1).text().toInt();
    *(color1_data + 2) = ui->plainTextEdit_1->document()->findBlockByLineNumber(2).text().toInt();
    *(color2_data) = ui->plainTextEdit_2->document()->findBlockByLineNumber(0).text().toInt();
    *(color2_data + 1) = ui->plainTextEdit_2->document()->findBlockByLineNumber(1).text().toInt();
    *(color2_data + 2) = ui->plainTextEdit_2->document()->findBlockByLineNumber(2).text().toInt();
    *(color3_data) = ui->plainTextEdit_3->document()->findBlockByLineNumber(0).text().toInt();
    *(color3_data + 1) = ui->plainTextEdit_3->document()->findBlockByLineNumber(1).text().toInt();
    *(color3_data + 2) = ui->plainTextEdit_3->document()->findBlockByLineNumber(2).text().toInt();
    
    *(color4_data) = ui->plainTextEdit_4->document()->findBlockByLineNumber(0).text().toInt();
    *(color4_data + 1) = ui->plainTextEdit_4->document()->findBlockByLineNumber(1).text().toInt();
    *(color4_data + 2) = ui->plainTextEdit_4->document()->findBlockByLineNumber(2).text().toInt();
    *(color5_data) = ui->plainTextEdit_5->document()->findBlockByLineNumber(0).text().toInt();
    *(color5_data + 1) = ui->plainTextEdit_5->document()->findBlockByLineNumber(1).text().toInt();
    *(color5_data + 2) = ui->plainTextEdit_5->document()->findBlockByLineNumber(2).text().toInt();
    
}

void VideoMattingUI::get_HSV_color_vector(int* color1_data, int* color2_data, int* color3_data, int* color4_data, int* color5_data)
{
    *(color1_data) = ui->plainTextEdit_H1->document()->findBlockByLineNumber(0).text().toInt();
    *(color1_data + 1) = ui->plainTextEdit_H1->document()->findBlockByLineNumber(1).text().toInt();
    *(color1_data + 2) = ui->plainTextEdit_H1->document()->findBlockByLineNumber(2).text().toInt();
    *(color2_data) = ui->plainTextEdit_H2->document()->findBlockByLineNumber(0).text().toInt();
    *(color2_data + 1) = ui->plainTextEdit_H2->document()->findBlockByLineNumber(1).text().toInt();
    *(color2_data + 2) = ui->plainTextEdit_H2->document()->findBlockByLineNumber(2).text().toInt();
    *(color3_data) = ui->plainTextEdit_H3->document()->findBlockByLineNumber(0).text().toInt();
    *(color3_data + 1) = ui->plainTextEdit_H3->document()->findBlockByLineNumber(1).text().toInt();
    *(color3_data + 2) = ui->plainTextEdit_H3->document()->findBlockByLineNumber(2).text().toInt();
    *(color4_data) = ui->plainTextEdit_H4->document()->findBlockByLineNumber(0).text().toInt();
    *(color4_data + 1) = ui->plainTextEdit_H4->document()->findBlockByLineNumber(1).text().toInt();
    *(color4_data + 2) = ui->plainTextEdit_H4->document()->findBlockByLineNumber(2).text().toInt();
    *(color5_data) = ui->plainTextEdit_H5->document()->findBlockByLineNumber(0).text().toInt();
    *(color5_data + 1) = ui->plainTextEdit_H5->document()->findBlockByLineNumber(1).text().toInt();
    *(color5_data + 2) = ui->plainTextEdit_H5->document()->findBlockByLineNumber(2).text().toInt();
    
}


void VideoMattingUI::on_pushButton_trimap_clicked()
{
    get_image_info(width, height, totalFrameCnt);
    QString check = QString::number(ui->plainTextEdit_1->document()->lineCount());
    qDebug() << check;
    img_index = ui->lineEdit_image_index->text().toInt();

    if (width == 0)// 確定有讀到圖片及其尺寸再往下
    {
        ui->label_information->setText("沒有讀到圖片，先按begin");
    }
    else if (img_index > totalFrameCnt)
    {
        ui->label_information->setText("圖片沒有那麼多張!!!請輸入數字小於image num");
    }
    else if (img_index<0)
    {
        ui->label_information->setText("圖片編號需大於零，請重新輸入");
    }
    else
    {
        cv::Mat image = cv::Mat::zeros(cv::Size(width, height), CV_8UC3);

        //得到圖片
        get_image_num(value);
        get_outputs(inputs, videoName, outputs, vtoi_outputs);
        image = imread((vtoi_outputs + value + ".png").c_str(), cv::IMREAD_COLOR);
        imwrite(outputs + "test_org.png", image);

        if (check == "0")
        {
            ui->label_information->setText("還沒有讀到背景顏色資訊!!!1. 請先點選origin 回到原圖背景 2. 然後在原圖上點選背景，依序按save1, save 2, save 3，然後才可以執行trimap");
        }

        else
        {
            ui->label_information->setText("SHOW　trimap");
            QVector<int> keyColor_1(3), keyColor_2(3), keyColor_3(3), keyColor_4(3), keyColor_5(3);
            int* color1_data = keyColor_1.data(), * color2_data = keyColor_2.data(), * color3_data = keyColor_3.data(), * color4_data = keyColor_4.data(), * color5_data = keyColor_5.data();
            get_color_vector(color1_data, color2_data, color3_data, color4_data, color5_data);
            KeyColor keyColor[] = { {*(color1_data),*(color1_data + 1),*(color1_data + 2)},{*(color2_data),*(color2_data + 1),*(color2_data + 2)},{*(color3_data),*(color3_data + 1),*(color3_data + 2)},{*(color4_data),*(color4_data + 1),*(color4_data + 2)},{*(color5_data),*(color5_data + 1),*(color5_data + 2)} };
            
            int keyColorSize = sizeof(keyColor) / sizeof(keyColor[0]);
            
            unsigned char* blendBuf = (unsigned char*)malloc(width * height * 4);
            ColorSharedMatting csm = ColorSharedMatting(width, height, width, height, false);// 第三格及第四格可以修改較小的數字，以利影片的縮放
            csm.init();
            csm.OnePageTrimap(outputs, image, width, height, lowerBound, upperBound, keyColor, keyColorSize);
            csm.finalize();

            QPixmap pixImg((outputs + "test_trimap.png").c_str());
            show_fig(pixImg);

            image.release();
        }
    }
}

void VideoMattingUI::on_pushButton_org_clicked()
{
    get_image_info(width, height, totalFrameCnt);
    img_index = ui->lineEdit_image_index->text().toInt();

    if (width == 0)// 確定有讀到圖片及其尺寸再往下
    {
        ui->label_information->setText("沒有讀到圖片，先按begin");
    }
    else if (img_index > totalFrameCnt)
    {
        ui->label_information->setText("圖片沒有那麼多張!!!請輸入數字小於image num");
    }
    else if (img_index < 0)
    {
        ui->label_information->setText("圖片編號需大於零，請重新輸入");
    }

    else // 確定有讀到圖片及其尺寸再往下
    {
        ui->label_information->setText("SHOW 原圖");
        get_image_num(value);
        QPixmap pixImg((vtoi_outputs + value + ".png").c_str());
        show_fig(pixImg);
    }
}

void VideoMattingUI::on_pushButton_blend_clicked()
{
    get_image_info(width, height, totalFrameCnt);
    QString check = QString::number(ui->plainTextEdit_1->document()->lineCount());
    qDebug() << check;

    img_index = ui->lineEdit_image_index->text().toInt();

    if (width == 0)// 確定有讀到圖片及其尺寸再往下
    {
        ui->label_information->setText("沒有讀到圖片，先按begin");
    }
    else if (img_index > totalFrameCnt)
    {
        ui->label_information->setText("圖片沒有那麼多張!!!請輸入數字小於image num");
    }
    else if (img_index < 0)
    {
        ui->label_information->setText("圖片編號需大於零，請重新輸入");
    }

    else
    {
        ui->label_information->setText("SHOW 合成圖");
        cv::Mat image = cv::Mat::zeros(cv::Size(width, height), CV_8UC3);

        //得到圖片
        get_image_num(value);
        get_outputs(inputs, videoName, outputs, vtoi_outputs);
        image = imread((vtoi_outputs + value + ".png").c_str(), cv::IMREAD_COLOR);


        QVector<int> keyColor_1(3), keyColor_2(3), keyColor_3(3), keyColor_4(3), keyColor_5(3);
        int* color1_data = keyColor_1.data(), * color2_data = keyColor_2.data(), * color3_data = keyColor_3.data(), * color4_data = keyColor_4.data(), * color5_data = keyColor_5.data();
        
        if (check == "0")
        {
            ui->label_information->setText("還沒有讀到背景顏色資訊!!!1. 請先點選origin 回到原圖背景 2. 然後在原圖上點選背景，依序按save1, save 2, save 3，然後才可以執行trimap");
        }
        
        else
        {

            QVector<int> keyColor_1(3), keyColor_2(3), keyColor_3(3), keyColor_4(3), keyColor_5(3);
            int* color1_data = keyColor_1.data(), * color2_data = keyColor_2.data(), * color3_data = keyColor_3.data(), * color4_data = keyColor_4.data(), * color5_data = keyColor_5.data();
            get_color_vector(color1_data, color2_data, color3_data, color4_data, color5_data);
            KeyColor keyColor[] = { {*(color1_data),*(color1_data + 1),*(color1_data + 2)},{*(color2_data),*(color2_data + 1),*(color2_data + 2)},{*(color3_data),*(color3_data + 1),*(color3_data + 2)},{*(color4_data),*(color4_data + 1),*(color4_data + 2)},{*(color5_data),*(color5_data + 1),*(color5_data + 2)} };

            int keyColorSize = sizeof(keyColor) / sizeof(keyColor[0]);
            unsigned char* blendBuf = (unsigned char*)malloc(width * height * 4);
            ColorSharedMatting csm = ColorSharedMatting(width, height, width, height, false);// 第三格及第四格可以修改較小的數字，以利影片的縮放

            csm.init();
            csm.OnePageBlend(image, outputs, keyColor, keyColorSize, lowerBound, upperBound);
            csm.finalize();

            QPixmap pixImg((outputs + "test_blend.png").c_str());
            show_fig(pixImg);

            image.release();
        }

    }

}

//matting
void VideoMattingUI::on_pushButton_matting_clicked()
{

    get_image_info(width, height, totalFrameCnt);

    QString check = QString::number(ui->plainTextEdit_1->document()->lineCount());
    qDebug() << check;

    if (width == 0)// 確定有讀到圖片及其尺寸再往下
    {
        ui->label_information->setText("沒有讀到圖片，先按begin");
    }

    else
    {
        if (check == "3")
        {
            InitProgram initP;
            initP.CreateValuePath(totalFrameCnt, value_path);
            
            get_page(page_start, page_end);
            if (page_start == page_end)
            {
                ui->label_information->setText("matting 起始頁跟中止頁面相同，請重新設定");
            }

            else if (page_start > page_end)
            {
                ui->label_information->setText("終止頁超過頁數，請重新設定");
            }

            else if (page_end >= totalFrameCnt)
            {
                ui->label_information->setText("起始頁需小於終止頁，請重新設定");
            }

            else
            {   
                ui->label_information->setText("開始產生一系列合成圖，請耐心等待，bar 停止前請不要有其他動作");

                ColorSharedMatting csm = ColorSharedMatting(width, height, width, height, false);
                csm.init();// 因為colorsharedmatting init 會有記憶體crash ，所以init 在外面只做一次

                Timer timer;
                timer.tic();
                for (int i = page_start; i <= page_end; i++)
                {
                    get_outputs(inputs, videoName, outputs, vtoi_outputs);
                    string filePath = vtoi_outputs + value_path[i] + ".png";//圖片存放路徑

                    QVector<int> keyColor_1(3), keyColor_2(3), keyColor_3(3), keyColor_4(3), keyColor_5(3);
                    int* color1_data = keyColor_1.data(), * color2_data = keyColor_2.data(), * color3_data = keyColor_3.data(), * color4_data = keyColor_4.data(), * color5_data = keyColor_5.data();
                    get_color_vector(color1_data, color2_data, color3_data, color4_data, color5_data);
                    KeyColor keyColor[] = { {*(color1_data),*(color1_data + 1),*(color1_data + 2)},{*(color2_data),*(color2_data + 1),*(color2_data + 2)},{*(color3_data),*(color3_data + 1),*(color3_data + 2)},{*(color4_data),*(color4_data + 1),*(color4_data + 2)},{*(color5_data),*(color5_data + 1),*(color5_data + 2)} };

                    int keyColorSize = sizeof(keyColor) / sizeof(keyColor[0]);
            
                    csm.AllBlend(filePath, outputs + "RemoveBG/", lowerBound, upperBound, keyColor, keyColorSize, width, height);
                   

                    qDebug() << "圖片去背處理進度:" << i + 1 << "/" << (page_end - page_start+1);

                    ui->progressBar_matting->setMaximum(page_end - page_start+1);
                    ui->progressBar_matting->setValue(i + 1 - page_start);
                }
                                
                csm.finalize();

                timer.toc();
                QString time_sec = QString::number(timer.get_dt(), 10, 0);
                ui->lineEdit_time_blend_matting->setText(time_sec);
            }
        }
        else
        {
            ui->label_information->setText("還沒有讀到背景顏色資訊!!!1. 請先點選origin 回到原圖背景 2. 然後依序按save1, save 2, save 3，然後才可以執行image mattting");
        }


    }

}

void VideoMattingUI::on_pushButton_trimapmatting_clicked()
{
    get_image_info(width, height, totalFrameCnt);
    QString check = QString::number(ui->plainTextEdit_1->document()->lineCount());
    

    if (width == 0)// 確定有讀到圖片及其尺寸再往下
    {
        ui->label_information->setText("沒有讀到圖片，先按 begin");

    }
    
    else
    {

        if (check == "3")
        {
            InitProgram initP;
            initP.CreateValuePath(totalFrameCnt, value_path);

            get_page(page_start, page_end);
            if (page_start == page_end)
            {
                ui->label_information->setText("matting 起始頁跟中止頁面相同，請重新設定");
            }

            else if (page_start > page_end)
            {
                ui->label_information->setText("終止頁超過頁數，請重新設定");
            }

            else if (page_end >=totalFrameCnt)
            {
                ui->label_information->setText("起始頁需小於終止頁，請重新設定");
            }

            else
            {
                ui->label_information->setText("開始產生一系列trimap 圖片，請耐心等待，bar 停止前請不要有其他動作");
                ColorSharedMatting csm = ColorSharedMatting(width, height, width, height, false);
                csm.init();// 因為colorsharedmatting init 會有記憶體crash ，所以init 在外面只做一次

                Timer timer;
                timer.tic();
                for (int i = page_start; i <= page_end; i++)
                {
                    get_outputs(inputs, videoName, outputs, vtoi_outputs);
                    string filePath = vtoi_outputs + value_path[i] + ".png";//input圖片存放路徑

                    //choose_keycolor(keyColor

                    QVector<int> keyColor_1(3), keyColor_2(3), keyColor_3(3), keyColor_4(3), keyColor_5(3);
                    int* color1_data = keyColor_1.data(), * color2_data = keyColor_2.data(), * color3_data = keyColor_3.data(), * color4_data = keyColor_4.data(), * color5_data = keyColor_5.data();
                    get_color_vector(color1_data, color2_data, color3_data, color4_data, color5_data);
                    KeyColor keyColor[] = { {*(color1_data),*(color1_data + 1),*(color1_data + 2)},{*(color2_data),*(color2_data + 1),*(color2_data + 2)},{*(color3_data),*(color3_data + 1),*(color3_data + 2)},{*(color4_data),*(color4_data + 1),*(color4_data + 2)},{*(color5_data),*(color5_data + 1),*(color5_data + 2)} };
                    int keyColorSize = sizeof(keyColor) / sizeof(keyColor[0]);

                    csm.AllTrimap(filePath, outputs + "trimap/", lowerBound, upperBound, keyColor, keyColorSize, width, height);

                    qDebug() << "trimap圖片處理進度:" << i + 1 << "/" << (page_end - page_start + 1);
                    ui->progressBar_trimapmatting->setMaximum(page_end - page_start + 1);
                    ui->progressBar_trimapmatting->setValue(i + 1- page_start);
                }

                csm.finalize();

                timer.toc();
                QString time_sec = QString::number(timer.get_dt(), 10, 0);
                ui->lineEdit_time_trimap_matting->setText(time_sec);


            }
        }
        else
        {
            ui->label_information->setText("還沒有讀到背景顏色資訊!!!1. 請先點選origin 回到原圖背景 2. 然後依序按save1, save 2, save 3，然後才可以執行image mattting");
        }


    }

}

//video
void VideoMattingUI::on_pushButton_video_clicked()
{
    get_image_info(width, height, totalFrameCnt);
    QString check = QString::number(ui->plainTextEdit_1->document()->lineCount());
    qDebug() << check;
    get_blend_image_info(width_blend, height_blend);

    if (width == 0)
    {
        ui->label_information->setText("沒有讀到圖片，先按begin");
    }

    else if (width_blend == 0)
    {
        ui->label_information->setText("還沒有產生一系列合成圖的圖片，請先按blend image matting");
    }
    else
    {
        if (check == "3")// 只要有讀到第一個背景顏色就繼續
        {
            ui->label_information->setText("開始產生合成圖的影片，請耐心等待，bar 停止前請不要有其他動作");
            ConvertImageToVideo(outputs + "RemoveBG/", totalFrameCnt, width, height);
        }
        else
        {
            ui->label_information->setText("還沒有讀到背景顏色資訊!!!請先點選origin 回到原圖背景，並選取背景顏色");
        }
    }

}

void VideoMattingUI::on_pushButton_trimapvideo_clicked()
{
    get_image_info(width, height, totalFrameCnt);
    QString check = QString::number(ui->plainTextEdit_1->document()->lineCount());
    qDebug() << check;
    get_trimap_image_info(width_trimap, height_trimap);

    if (width == 0)
    {
        ui->label_information->setText("沒有讀到圖片，先按begin");
    }
    else if (width_trimap == 0)
    {
        ui->label_information->setText("還沒有產生一系列 trimap 的圖片，請先按trimap image matting");
    }

    else
    {
        if (check == "3")// 只要有讀到第一個背景顏色就繼續
        {
            ui->label_information->setText("開始產生trimpa 影片，請耐心等待，bar 停止前請不要有其他動作");
            ConvertImageToVideo(outputs + "trimap/", totalFrameCnt, width, height);
        }
        else
        {
            ui->label_information->setText("還沒有讀到背景顏色資訊!!!請先點選origin 回到原圖背景，並選取背景顏色");
        }
    }

}


void VideoMattingUI::ConvertImageToVideo(std::string outputspath, int& frameCnt, int& width, int& height)
{
    char filePath[1024];
    char name_file[256];

    //int fourcc = cv::VideoWriter::fourcc('m', 'p', '4', '2');
    //int fourcc = cv::VideoWriter::fourcc('d', 'i', 'v', 'x');
    int fourcc = cv::VideoWriter::fourcc('X', 'V', 'I', 'D');// to avi

    sprintf_s(name_file, (outputspath + "output.avi").c_str());
    cv::VideoWriter  writter(name_file, fourcc, 30, cv::Size(width, height), true);

    Timer timer;
    timer.tic();
    for (int i = 0; i < frameCnt; ++i) {

        sprintf_s(filePath, (outputspath + "%05d.png").c_str(), i);//give name to each image
        cv::Mat frame = cv::imread(filePath, cv::IMREAD_COLOR);
        writter.write(frame);

        qDebug() << "圖片轉影片處理進度:" << i + 1 << "/" << frameCnt;


        if (outputspath == outputs + "trimap/")
        {
            ui->progressBar_trimaptovideo->setMaximum(frameCnt);
            ui->progressBar_trimaptovideo->setValue(i + 1);
        }
        else
        {
            ui->progressBar_tovideo->setMaximum(frameCnt);
            ui->progressBar_tovideo->setValue(i + 1);

        }

        frame.release();
    }
    timer.toc();
    QString time_sec = QString::number(timer.get_dt(), 10, 0);
    if (outputspath == outputs + "trimap/")
    {
        ui->lineEdit_time_trimap_video->setText(time_sec);
    }
    else
    {
        ui->lineEdit_time_blend_video->setText(time_sec);
    }
    writter.release();
}


//other
void VideoMattingUI::get_string(string input_string, QString& out)
{

    char* aaa = new char[input_string.size()];
    strcpy(aaa, input_string.c_str());
    out = QString::fromLocal8Bit(aaa);

}



