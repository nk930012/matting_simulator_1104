import cv2 as cv

from skyeye.utils.camera import CameraBuilder
from skyeye.utils.camera import DepthUtil
from skyeye.utils.opencv import wait_key
import numpy as np

blue = (255, 0, 0)
green = (0, 255, 0)
red = (0, 0, 255)

def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

if __name__ == '__main__':

    camera = CameraBuilder.buildWithYaml('camera_config.yaml')

    camera.open()

    while True:

        print("frame_count = {}".format(camera.frame_count))

        camera.process()

        # Draw information
        color = camera.color_image
        depth = camera.depth_info
        if depth is None:
            image_out = color
        else:
            #gray = DepthUtil.depth_grayscale(depth)
            #depth_colormap = cv.cvtColor(gray, cv.COLOR_GRAY2BGR)
            depth_colormap = cv.applyColorMap(cv.convertScaleAbs(depth, alpha=30), cv.COLORMAP_JET)
            image_out = np.concatenate((color, depth_colormap), axis=1)

        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "Camera type: {}".format(camera.camera_type)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        msg = "fps: {}".format(camera.fps)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        # show the frame and record if the user presses a key
        cv.imshow("Camera Test", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    camera.close()

    cv.destroyAllWindows()
