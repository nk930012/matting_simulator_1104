#ifndef SHAREDMSTTING_GPU_H
#define SHAREDMSTTING_GPU_H

#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/opencv.hpp"
#include <cmath>
#include <vector>
#include "cl_utilities.h"
#include "typedef.h"

using namespace std;

const int kI = 10;
const double kC = 5.0;
const int kG = 4;
const int numPickedPointsMax = kG;

class SharedMatting
{
public:


    SharedMatting(int width, int heieght);
    SharedMatting();
    ~SharedMatting();

    void save(char * filename);

    cv::Mat getAlpha();

    void init();
    void finalize();

    void estimateAlpha(cv::Mat img, cv::Mat trimap);

    cv::Mat getImageBlended();
    cv::Mat normalize(cv::Mat src);
    cv::Mat unnormalize(cv::Mat src);
    cv::Mat blend(cv::Mat srcIn, cv::Mat alphaIn, cv::Mat bgIn);



private:

    bool m_isInit = false;

    bool initGpu();
    bool initBuf();

    void finalizeBuf();
    void finalizeGpu();

    void loadImage(cv::Mat image);
    void loadTrimap(cv::Mat trimap);

    void solveAlpha();

    void classifyGpu();
    void expandKnownGpu();

    cv::Mat getImageFromData(uchar* data, int numChannels);




    void expandGpu();
    void sampleGpu();
    void gatherGpu();
    void refineGpu();
    void localSmoothGpu();

    size_t globalSize[2];
    cl_event event;

    cv::Mat m_image;
    cv::Mat m_trimap;
    cv::Mat m_matte;

    PixelPoint* m_bgPointData = NULL;
    PixelPoint* m_fgPointData = NULL;

    int* m_numBgPointsData = NULL;
    int* m_numFgPointsData = NULL;

    LabelPoint* m_labelPixelsData = NULL;

    PixelPoint* m_unknownPixelsData = NULL;

    int m_numUnknownPixelsMax = 0;

    int m_numOfUnknownPixels = 0;

    int* m_unknownIndexesData = NULL;

    Ftuple* m_ftuplesData = NULL;
    Tuple* m_tuplesData = NULL;

    int m_width;
    int m_height;
    int m_channels;
    int m_step;

    uchar* m_imageData = NULL;
    uchar* m_trimapData = NULL;
    uchar* m_alphaData = NULL;


    cl_device_id m_device = NULL;
    cl_context m_context = NULL;
    cl_command_queue m_queue = NULL;
    cl_program m_program = NULL;
    cl_kernel m_classifyCl = NULL, m_expandCl = NULL,
        m_sampleCl = NULL, m_gatherCl = NULL, m_refineCl = NULL, m_localSmoothCl = NULL;

    cl_mem m_imageBuffer = NULL;
    cl_mem m_alphaBuffer = NULL;
    cl_mem m_trimapBuffer = NULL;

    cl_mem m_unknownPixelsBuffer = NULL;
    cl_mem m_labelPixelsBuffer = NULL;


    cl_mem m_fgPointsBuffer = NULL;
    cl_mem m_bgPointsBuffer = NULL;

    cl_mem m_numFgPointsBuffer = NULL;
    cl_mem m_numBgPointsBuffer = NULL;

    cl_mem m_unknownIndexesBuffer = NULL;

    cl_mem m_tuplesBuffer = NULL;
    cl_mem m_ftuplesBuffer = NULL;
};



#endif
