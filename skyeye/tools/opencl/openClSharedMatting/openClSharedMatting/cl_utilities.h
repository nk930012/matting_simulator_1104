#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <CL/cl.h>

cl_device_id create_device();
cl_program build_program(cl_context ctx, cl_device_id dev, const char* filename);