from collections import OrderedDict
import numpy as np
from .headpose_detector import HeadposeDetector
from .gaze import Gaze
from .human_tracker import HumanTracker
from .body import Body


class HeadposeTracker:
    '''
    Headpose tracker.
    '''

    def __init__(self, frame_width=640, frame_height=480,
        webcam_pos=None, num_bodies_max= 20, disappeared_max=30):

        self.frame_width = frame_width
        self.frame_height = frame_height

        self.webcam_pos = webcam_pos

        self.disappeared_max = disappeared_max

        self.num_bodies_max = num_bodies_max
        self.num_bodies = 0
        self.body_pool = []

        # Headpose detector
        self.detector = HeadposeDetector()

        # Human tracker
        self.human_tracker = HumanTracker(disappeared_max=self.disappeared_max)

    def update(self, image):

        bboxes, poses = self.detector.detect(image)
        self.num_bodies = len(bboxes)

        theta = self.webcam_pos.theta
        len_x = self.webcam_pos.len_x
        len_y = self.webcam_pos.len_y
        dx = self.webcam_pos.dx
        dy = self.webcam_pos.dy

        # Bodies
        bodies = []
        for i in range(self.num_bodies):

            body = Body()

            gaze = Gaze(theta, dx=dx, dy=dy, len_x=len_x, len_y=len_y)
            gaze_point, face_point, distance = gaze.detect(poses[i].rvec, poses[i].tvec)

            body.update(bboxes[i], poses[i], gaze_point, face_point, distance)
            bodies.append(body)

        # Tracking
        self.human_tracker.update(bodies)
        self.bodies = self.human_tracker.get_bodies()

        return self.bodies
