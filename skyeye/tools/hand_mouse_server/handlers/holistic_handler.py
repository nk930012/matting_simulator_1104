# -*- coding: utf-8 -*-
import sys
import os
import argparse
import logging
import tornado.ioloop
import numpy as np
import time
import json 
import imutils
import cv2 as cv
from skyeye.detect.decorator import Mp3dHolistic
from skyeye.detect.decorator.mp_holistic_decorate import MpHolisticDecorate
from skyeye.detect.hand import HandFactory, GestureDetector, MouseSimulator
from skyeye.utils.camera import CameraBuilder


class HolisticHandler():

    def __init__(self, WsHandler, debug=logging.ERROR):

        self.ws = WsHandler
        self.scheduler = None

        self.active = 0

        self.snap_count = 0
        

    def dectet_scheduler(self):


        if self.active == 1:
            data = []
            
            self.detector.detect()
            face = self.detector.mp_3d_face
            left_hand = self.detector.mp_3d_left_hand
            right_hand = self.detector.mp_3d_right_hand
            pose = self.detector.mp_3d_pose

            # Draw the mouse pointer
            #if not(mouse_pos is None):
                
            wsData = {'size': (self.frame_width, self.frame_height),'face':face, 'left_hand':left_hand, 'right_hand': right_hand, 'pose': pose}
                
            self.ws.send_updates(wsData)


            

    def open(self, id = 0):
        
        if self.active == 0:
            self.camera = CameraBuilder.buildWithYaml('camera_config.yaml')
            self.camera.open()
            self.detector = Mp3dHolistic(MpHolisticDecorate(self.camera))

            self.frame_width = self.camera.width
            self.frame_height = self.camera.height

            self.active = 1
            self.scheduler = tornado.ioloop.PeriodicCallback(self.dectet_scheduler, 20)
            self.scheduler.start()
        ret = True
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)
        
    def close(self, id):
        
        self.active = 0
        if self.scheduler is not None:
            self.scheduler.stop()
            self.scheduler = None
            self.camera.close()
        status_str = "successful"
        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)
        
    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close cam device.')
                self.close(id=1)
            elif params["command"] == "open":
                if params["property"] == "holistic":
                    self.open(id=id)
            elif params["command"] == "close":
                if params["property"] == "holistic":
                    self.close(id=id)
        elif method == "get":
            pass
        elif method == "set":
            pass
