<<使用版本資訊>>
OPENCV:4.5.1_VC14_VC15
OPENCL:CUDA 11.4.1
QT:5.15.2
Visual studio:2019


<<準備工作>>
*輸入檔案路徑、名稱及 run #
* 按 Begin
1. New 打勾，把影片轉圖片，成功的話會顯示圖片資訊，progress bar 也會跟著變化
2. 如果 New 沒打勾(先前已經把影片轉成圖片了)，則指示進行進行初始化
3. 如果影片不存在路徑，會顯示錯誤訊息，請修改路徑及名稱
4. 如果輸出資料夾已存在，請增加 run #，或把資料夾下的以輸出檔案刪除

<<第一張圖片去背>>
A. 更改 imgage index 或是按 < /> 更改圖片，按完後請按 origin 圖片才會變動             ----> 後續版本會讓動作連動
B . 點選圖片上的背景，若是顯示的位置及顏色是想要的，按 save，直到選取完所有背景       -----> 後續版本會設定可以不用所有顏色都選完
C.  依序按 trimpap, blend確認去背品質 


若品質OK
D. 更改page range 修改需要做成影片的頁數範圍
E. 按 trimap image matting，進度顯示在後面。再按 Export trimap video，進度顯示在後面。
F. 按 blend image matting，進度顯示在後面。再按 Export video，進度顯示在後面。
G. 按 Logfile  存取重要參數資訊

若品質不行
1. 重複步驟 A~B，新增背景顏色
2. 重複步驟 C，直到品質可接受，再繼續執行步驟 D~G
