#include <chrono>

#include "opencv2/opencv.hpp"
#include <CL/cl.h>
#include "timer.h"

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
    UMat img, gray;
    //Mat img, gray;
  
    Timer timer;
  
    imread("test.png", IMREAD_COLOR).copyTo(img);
  
    timer.tic();
  
    cvtColor(img, gray, COLOR_BGR2GRAY);
    GaussianBlur(gray, gray,Size(7, 7), 1.5);
    Canny(gray, gray, 0, 50);
  
    imshow("edges", gray);
  
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;
  
    waitKey();
    return 0;
}