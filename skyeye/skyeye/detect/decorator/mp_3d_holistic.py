from .mp_3d_data import Mp3dData
from skyeye.utils.opencv import InfoDrawer

class Mp3dHolistic(Mp3dData):
    
    def detect(self):
        super().detect()

        self._face_boxes = self.component.face_boxes
        self._face_3d = self.component.mp_3d_face
        self._lhand_3d = self.component.mp_3d_left_hand
        self._rhand_3d = self.component.mp_3d_right_hand
        self._pose_3d = self.component.mp_3d_pose

    @property
    def face_boxes(self):
        return self._face_boxes

    @property
    def mp_3d_face(self):
        return self._face_3d

    @property
    def mp_3d_left_hand(self):
        return self._lhand_3d

    @property
    def mp_3d_right_hand(self):
        return self._rhand_3d

    @property
    def mp_3d_pose(self):
        return self._pose_3d

    def postProcess(self):
        for bbox in self.face_boxes:
            InfoDrawer.draw_depth_box(self._imageOut, bbox)