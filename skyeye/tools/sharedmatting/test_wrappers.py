import numpy as np
import cv2 as cv
from time import perf_counter

from wrappers import SharedMattingWrapper


sm = SharedMattingWrapper()

image = cv.imread("input.png")
trimap = cv.imread("trimap.png")

sm.setImage(image)
sm.setTrimap(trimap)

sm.initilize()

time_start = perf_counter()

sm.solveAlpha()

time_end = perf_counter()
time_cost = time_end - time_start

sm.finalize()

print("Time cost is {} (sec)".format(time_cost))
