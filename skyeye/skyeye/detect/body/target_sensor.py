
import numpy as np
from .body_model import BodyModel
from .dyn_gesture import DynGesture
from .exist import exist


def detect_diff(y, y0, dc, ifcmp=0):
    '''
    Detect the difference.
    ifcmp: int
        0 -> smaller.
        1 -> greater.
    '''

    dy = y - y0
    dy_abs = abs(dy)

    out = False
    if ifcmp == 0:

        if y < y0 and dy_abs > dc:
            out = True

    else:

        if y > y0 and dy_abs > dc:
            out = True

    return out


class TargetSensor:

    def __init__(self, num_c=10):

        self.num_buffer = 80
        self.num_c = num_c
        self.len_c = 10
        self.states = [0 for i in range(self.num_buffer)]
        self.is_triggered = False

        self.ia = self.num_buffer - self.num_c
        self.iz = self.num_buffer

    def set_len_c(self, d):

        if d is None:
            self.len_c = 10
        else:
            self.len_c = d

    def update(self, left_wrist, right_wrist, left_shoulder, right_shoulder, len_c):

        self.set_len_c(len_c)

        left_wrist_exist = exist(left_wrist)
        right_wrist_exist = exist(right_wrist)
        left_shoulder_exist = exist(left_shoulder)
        right_shoulder_exist = exist(right_shoulder)

        is_valid = left_wrist_exist and right_wrist_exist and \
            left_shoulder_exist and right_shoulder_exist

        if not is_valid:
            self.is_triggered = False
            return self.is_triggered

        # Left hand
        y = left_wrist[1]
        y0 = left_shoulder[1]
        result_left = detect_diff(y, y0, len_c, ifcmp=1)

        # Right hand
        y = right_wrist[1]
        y0 = right_shoulder[1]
        result_right = detect_diff(y, y0, len_c, ifcmp=0)

        if result_left and result_right:
            out = 1
        else:
            out = 0

        self.states.append(out)
        self.states.pop(0)

        sum = 0
        for v in self.states[self.ia:self.iz]:
            sum += 1 - v

        if sum == 0:
            self.is_triggered = True
        else:
            self.is_triggered = False

        return self.is_triggered
