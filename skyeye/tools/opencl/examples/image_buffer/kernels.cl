
__kernel void process(__global uchar4* input, __global uchar* trimap, __global uchar4* output)
{

    int ix = get_global_id(0);
    int iy = get_global_id(1);

	  uint width = get_global_size(0);
	  uint height = get_global_size(1);

    int pos = ix + iy*width;

    uchar4 pixel = input[pos];

    output[pos] = pixel;

}