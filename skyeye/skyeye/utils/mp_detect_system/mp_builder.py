import abc
import cv2 as cv
import numpy as np
import mediapipe as mp
import json
import enum
import yaml
from skyeye.utils.camera import CameraBuilder
from skyeye.detect.decorator import Mp3dHand
from skyeye.detect.decorator.mp_hands_decorate import MpHandsDecorate
from skyeye.utils.opencv import Webcam, wait_key

class IMPBuilder(metaclass=abc.ABCMeta):
    def __init__(self, camera):
        self.camera = camera

    def buildWithYaml(fileName):
        return NotImplemented



class MPHand2DBuilder(IMPBuilder):
    def __init__(self, camera):
        super().__init__(camera)

    def buildWithYaml(self, fileName):
        with open(fileName, 'r', encoding='utf-8') as f:
            inputs = yaml.load(f, Loader=yaml.Loader)
        #refine here
        flip = inputs['flip']
        max_num_hands = inputs['max_num_hands']
        min_detection_confidence = inputs['min_detection_confidence']
        min_tracking_confidence = inputs['min_tracking_confidence']
        #print("MPHand2DBuilder -- flip:{}, max_num_hand:{}".format(flip, max_num_hands))
        return self.build(flip, max_num_hands, min_detection_confidence, min_tracking_confidence)


    def build(self, flip=None, max_num_hands=1, min_detection_confidence=0.8, min_tracking_confidence=0.5):
        return Mp3dHand(MpHandsDecorate(self.camera, flip, max_num_hands, min_detection_confidence, min_tracking_confidence))
