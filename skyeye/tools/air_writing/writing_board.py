import numpy as np


class WritingBoard:

    def __init__(self, width, height):

        self.width = width
        self.height = height

        self.points = []

    def write(self, point): 

        point = np.array(point)
        self.points.append(point)

    def erase(self, point): 

        tmp = []

        point = np.array(point)
        for p in self.points:
            dist = np.linalg.norm(p-point)

            if dist > 30:
                 tmp.append(p)

        self.points = tmp         

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height

    def get_points(self):
        return self.points