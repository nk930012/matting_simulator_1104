from .avatar import Avatar

class AvatarFactory:

    def __init__(self):

        self.avatars = []

    def generate(self, heads, hands, bodys):

        head = None
        if len(heads) > 0:
            head = heads[0]

        left_hand = None         
        right_hand = None         
        if len(hands) > 0:

            for hand in hands:

                if hand.is_right_hand:
                    right_hand = hand
                else:
                    left_hand = hand

        body = None
        if len(bodys) > 0:
            body = bodys[0]

        avatar = Avatar()
        avatar.update(head=head, left_hand=left_hand, right_hand=right_hand, body=body)

        self.avatars = [avatar]

        return self.avatars
