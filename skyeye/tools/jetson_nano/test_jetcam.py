from jetcam.usb_camera import USBCamera
from jetcam.csi_camera import CSICamera
import cv2
import time

camera = USBCamera(width=640, height=480, format='brg8', capture_width=320, capture_height=240, capture_device=0)
# Frame size: 224x224 or 800x600
#frame_width = 320 #224
#frame_height = 240 #224
frame_width = 800
frame_height = 600

#cap.set(cv2.CAP_PROP_FRAME_WIDTH, frame_width)
#cap.set(cv2.CAP_PROP_FRAME_HEIGHT, frame_height)
#camera.gst_str()

while(True):
  t0 = time.time()

  image = camera.read()
  cv2.imshow('frame', image)

  t1 = time.time()
  fps = 1.0 / (t1 - t0)
  print("fps: {}".format(fps))

  if cv2.waitKey(1) & 0xFF == ord('q'):
    break



