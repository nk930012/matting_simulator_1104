#include "DetectorClient.h"
#include <QDebug>

DetectorClient::DetectorClient()
{
    QObject::connect(&m_webSocket, &QWebSocket::connected, this, &DetectorClient::onConnected);
    QObject::connect(&m_webSocket, &QWebSocket::disconnected, this, &DetectorClient::onClosed);


    connect(&m_webSocket, QOverload<QAbstractSocket::SocketError>::of(&QWebSocket::error),
        [=](QAbstractSocket::SocketError error)
        {
            qDebug()<<"Error:"<<error;

        });
}

DetectorClient::~DetectorClient()
{

}

void DetectorClient::connectToServer(QString address)
{
    m_url = QUrl(address);
    qDebug()<<m_url;
    if(m_webSocket.state() == QAbstractSocket::UnconnectedState)
    {
        qDebug()<<"Try to connect."<<m_url;
        m_webSocket.open(m_url);
    }
}

void DetectorClient::close()
{
    if(m_webSocket.state() == QAbstractSocket::ConnectedState)
    {
        m_webSocket.close();
    }
}

void DetectorClient::sendMessage(QString message)
{
    //qDebug()<<message;
    m_webSocket.sendTextMessage(message);
}

void DetectorClient::onConnected()
{
    qDebug()<<"connect succeed.";
    emit connectSucceed();
    QObject::connect(&m_webSocket, &QWebSocket::textMessageReceived,
                     this, &DetectorClient::onTextMessageReceived);
}

void DetectorClient::onClosed()
{
    qDebug()<<"disconnect.";
}

void DetectorClient::onTextMessageReceived(QString message)
{
    //qDebug()<<message;
    //qDebug()<<"DetectorClient::onTextMessageReceived";
    emit textMessageReceived(message);
}
