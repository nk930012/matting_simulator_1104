from .camera_decorate import CameraDecorate
import cv2 as cv
import mediapipe as mp
from skyeye.utils.mediapipe import MpConvert

class MpHandsDecorate(CameraDecorate):

    def __init__(self, camera, flip=None,
        max_num_hands=1, min_detection_confidence=0.8, min_tracking_confidence=0.5):
        super().__init__(camera,ratio_target=0.5,flip=flip)

        self._result = None
        self.mp_drawing = mp.solutions.drawing_utils
        self.mp_hands = mp.solutions.hands

        self.hands = self.mp_hands.Hands(
            max_num_hands=max_num_hands,
            min_detection_confidence=min_detection_confidence,
            min_tracking_confidence=min_tracking_confidence)

    def detect(self):
        super().detect()

        image = super().copy_frame()

        image.flags.writeable = False
        self._result, self._mp_3d, self._mp_3d_pixel = self.detect_mp_pose(image)

        self.camera.countFPS()

    def detect_mp_pose(self, image):

        results = self.hands.process(image)
        hand_3d, hand_pixel_3d = MpConvert.convert_multi_hand_to_3d(results.multi_hand_landmarks, self.camera)

        return results, hand_3d, hand_pixel_3d

    def postProcess(self):
        if self._result.multi_hand_landmarks:
            for hand_landmarks in self._result.multi_hand_landmarks:
                self.mp_drawing.draw_landmarks(
                    self._imageOut, hand_landmarks, self.mp_hands.HAND_CONNECTIONS)

    @property
    def result(self):
        return self._result

    @property
    def mp_3d_data(self):
        return self._mp_3d

    @property
    def mp_3d_pixel(self):
        return self._mp_3d_pixel