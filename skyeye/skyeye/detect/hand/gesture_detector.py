import numpy as np
from collections import Counter 


def most_frequent_event(events): 
    event_count = Counter(events) 
    return event_count.most_common(1)[0][0] 
    

class GestureDetector:

    def __init__(self):

        self.hand = None
        self.max_num = 10
        self.pool = []
        self.numeric_pool = []

        self.len_c = None
        self.snap_dist = 0

    def detect(self, hand):


        gesture = hand.get_gesture()  
        self.pool.append(gesture)
        
        if len(self.pool) > self.max_num:
            self.pool.pop(0)

        gesture = most_frequent_event(self.pool)

        return gesture

    def detect_numeric(self, hand):

        gesture = hand.get_numeric_gesture()  
        self.numeric_pool.append(gesture)
        
        if len(self.numeric_pool) > self.max_num:
            self.numeric_pool.pop(0)

        gesture = most_frequent_event(self.numeric_pool)

        return gesture

    def detect_snap(self, hand):

        thumb_tip = hand.THUMB_TIP
        middle_tip = hand.MIDDLE_FINGER_TIP

        index_mcp = hand.INDEX_FINGER_MCP
        wrist = hand.WRIST

        self.len_c = np.linalg.norm(index_mcp - wrist)

        dist = np.linalg.norm(middle_tip - thumb_tip)

        diff = dist - self.snap_dist
        self.snap_dist = dist

        is_middle_thumb_close = hand.is_two_points_close(middle_tip, thumb_tip, 0.5*self.len_c)

        out = False
        if diff > 0.3*self.len_c:
            out = True

        return out