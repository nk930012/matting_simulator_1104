#include "CL/cl.h"

cl_device_id create_device();
cl_program build_program_from_source(cl_context ctx, cl_device_id dev, const char* source, int len);
cl_program build_program(cl_context ctx, cl_device_id dev, const char* filename);