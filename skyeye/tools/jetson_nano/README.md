# Put the model file which is resnet18_baseline_att_224x224_A_epoch_249_trt.pth) to ~/projects/trt_pose/tasks/human_pose/
cp resnet18_baseline_att_224x224_A_epoch_249_trt.pth ~/projects/trt_pose/tasks/human_pose/

# Make a symbolic link under the model directory.
cd model
ln -s ../../../../trt_pose/tasks/human_pose/resnet18_baseline_att_224x224_A_epoch_249_trt.pth resnet18_baseline_att_224x224_A_epoch_249_trt.pth

# Run the test code.
python3 detect_body.py


