import numpy as np
from pyquaternion import Quaternion
from scipy.spatial.transform import Rotation as R

class HeadModel:

    def __init__(self):

        self.landmarks = None 

        self.len_c = None
        self.len_c_base = 0.1

        self.quat = Quaternion()
        self.quat_tmp = Quaternion()

        self.ref_vec_base = np.array([0.00967874, -0.93324554, -0.35910863], dtype=np.float64)

    def get_distance(self, a, b):
        return np.linalg.norm(a-b)

    def convert_landmark(self, lm, depth):
        return np.array([lm.x, lm.y, lm.z+depth], dtype=np.float32)

    def read_data(self, landmarks, base_depth=0):

        self.landmarks = landmarks

        lm = landmarks.landmark

        self.philtrum = self.convert_landmark(lm[0], base_depth)
        self.nose_tip = self.convert_landmark(lm[4], base_depth)
        self.forehead = self.convert_landmark(lm[151], base_depth)
        self.lip_up = self.convert_landmark(lm[13], base_depth)
        self.lip_down = self.convert_landmark(lm[14], base_depth)
        self.lip_left = self.convert_landmark(lm[291], base_depth)
        self.lip_right = self.convert_landmark(lm[61], base_depth)
        self.left_eye_start = self.convert_landmark(lm[362], base_depth)
        self.left_eye_end = self.convert_landmark(lm[263], base_depth)
        self.right_eye_start = self.convert_landmark(lm[133], base_depth)
        self.right_eye_end = self.convert_landmark(lm[33], base_depth)


        # Characteristic length
        self.len_c = self.get_distance(self.nose_tip, self.philtrum) 

        # Estimate the quaternion
        self.quat = self.estimate_quat(self.nose_tip, self.philtrum)

    def get_unit_vec(self, v):
        return v / np.linalg.norm(v)

    def estimate_quat(self, p1, p0): 

        ref_vec_base = self.ref_vec_base

        # Reference vector
        ref_vec = self.get_unit_vec(p1 - p0)

        # Rotation angle
        mag0 = np.linalg.norm(ref_vec_base)
        mag1 = np.linalg.norm(ref_vec)
        dot_product = np.dot(ref_vec, ref_vec_base) 
        angle = np.arccos(dot_product/(mag0*mag1))

        # Directional vector 
        cross_product = np.cross(ref_vec_base, ref_vec)
        denominator = mag0*mag1*np.sin(angle)
        if denominator < 1e-8:
            denominator = 1e-8
        direct_vec = cross_product / denominator

        # Quaternion
        self.quat_tmp.q[0] = np.cos(0.5*angle) 
        self.quat_tmp.q[1:4] = np.sin(0.5*angle)*direct_vec

        return self.quat_tmp 
 
    def get_landmarks(self): 
        return self.landmarks

    def get_len_c(self): 
        return self.len_c

    def get_quat(self): 
        return self.quat

    def get_philtrum(self):  
        return self.philtrum

    def get_nose_tip(self):  
        return self.nose_tip

    def get_lip_up(self):  
        return self.lip_up

    def get_lip_down(self):  
        return self.lip_down

    def get_face_distance(self):
        return 1.0 - self.len_c / self.len_c_base    