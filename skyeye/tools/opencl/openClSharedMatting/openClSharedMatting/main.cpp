﻿#include "sharedmatting_gpu.h"

#include "timer.h"

#include <string>

#include "crosstrimap.h"

#include "fastsharedmatting.h"

using namespace std;





int main()
{
    std::remove("alpha.png");
    std::remove("blend.png");
    cv::Mat src = imread("input.png");

    int width = src.cols;

    int height = src.rows;

    int channels = src.channels();
    //b:115, g:158, r:225, h:12, s:125, v:225
    //b:7, g : 28, r : 59, h : 12, s : 225, v : 59

#if 0
    double hueMin[5] = {  40,   0,   0,  40,  40 };

    double hueMax[5] = {  80,  20,  20,  80,  80 };

    double satMin[5] = {  55, 100,  55,  55,  55 };

    double satMax[5] = { 255, 130, 255, 255, 255 };

    double valMin[5] = {  85, 220,   0,  85,  85 };

    double valMax[5] = { 255, 255,  80, 255, 255 };
#else
    double hueMin[5] = { 40, 40, 40, 40, 40 };

    double hueMax[5] = { 80, 80, 80, 80, 80 };

    double satMin[5] = { 55, 55, 55, 55, 55 };

    double satMax[5] = { 255, 255, 255, 255, 255 };

    double valMin[5] = { 85, 85, 85, 85, 85 };

    double valMax[5] = { 255, 255, 255, 255, 255 };
#endif

    FastSharedMatting fsm;

    Timer timer;

    float timeCost = 0;

    uchar* blendBuf = (uchar*)malloc( width * height * 4 );

    int num = 100;

    Mat blend;

    for (int i = 0; i < num; ++i) {

        timer.tic();

        fsm.process(src.data, width, height, channels,
            hueMin, hueMax, satMin, satMax, valMin, valMax,
            5, blendBuf);

        timer.toc();

        timeCost += timer.get_dt();

        if (i != (num - 1))
            memset(blendBuf, 0, width * height * 4);

    }

    timeCost /= num;

    cout << "Time cost is " << timeCost << "[s]" << endl;

    blend = Mat(height, width, CV_8UC4, blendBuf);

    imwrite("blend.png", blend);

    free(blendBuf);

    blend.release();

    src.release();

    Mat alpha = fsm.getAlpha();
    imwrite("alpha.png", alpha);
    alpha.release();

    waitKey(0);

    system("pause");

}



#if 0
int main()
{


#ifdef FAST_SHARED_MATTING_H
    Timer timer;
    FastSharedMatting fsm;
    int num = 100;
    float timeCost = 0;
    double hueMin[5] = { 40,40,40,40,40 };
    double hueMax[5] = { 80,80,80,80,80 };
    double satMin[5] = { 55,55,55,55,55 };
    double satMax[5] = { 255,255,255,255,255 };
    double valMin[5] = { 85,85,85,85,85 };
    double valMax[5] = { 255,255,255,255,255 };
    int dropperSize = 5;
    cv::Mat src = imread("input.png");
    Mat rgba;
    fsm.process(src, hueMin, hueMax, satMin, satMax, valMin, valMax,
        dropperSize);

    for (size_t i = 0; i < num; i++)
    {
        timer.tic();
        rgba = fsm.process(src, hueMin, hueMax, satMin, satMax, valMin, valMax,
            dropperSize);
        timer.toc();
        timeCost += timer.get_dt();
    }

    timeCost /= num;

    cout << "Time cost is " << timeCost << "[s]" << endl;

    Mat alpha = fsm.getAlpha();

    Mat blendedWithBg = fsm.getImageBlended();

    imwrite("rgba.png", rgba);

    imwrite("alpha.png", alpha);

    imwrite("blendedWithBg.png", blendedWithBg);

#else
#if 1
    Timer timer;

    SharedMatting sm;

    int num = 100;

    float timeCost = 0;

    double hueMin[5] = { 40,40,40,40,40 };
    double hueMax[5] = { 80,80,80,80,80 };
    double satMin[5] = { 55,55,55,55,55 };
    double satMax[5] = { 255,255,255,255,255 };
    double valMin[5] = { 85,85,85,85,85 };
    double valMax[5] = { 255,255,255,255,255 };
    int dropperSize = 5;

    cv::Mat src = imread("input.png");
    int width = src.cols;
    int height = src.rows;
    int channels = src.channels();
    uchar* matte = (uchar*)malloc(width * height * 4 * sizeof(uchar));

    sm.processAlpha(
        src.data, width, height,
        channels, hueMin, hueMax,
        satMin, satMax, valMin,
        valMax, dropperSize, matte);

    for (size_t i = 0; i < num; i++)
    {
        timer.tic();
        sm.processAlpha(
        src.data, width, height,
        channels, hueMin, hueMax,
        satMin, satMax, valMin,
        valMax, dropperSize, matte);
        timer.toc();
        timeCost += timer.get_dt();
    }
    Mat blended = Mat(height, width, CV_8UC4, matte);

    timeCost /= num;

    cout << "Time cost is " << timeCost << "[s]" << endl;


#else

    sm.loadImage("input.png");
    sm.loadTrimap("trimap.png");
    sm.initGpu();
    sm.solveAlpha();
    cv::Mat blended = sm.getImageBlended();
#endif

    //imwrite("blended.png", blended);
    //sm.finalizeGpu();
    //free(matte);
#endif

    waitKey(0);
#if defined(_WIN32)
    system("pause");
#endif
    return 0;

}
#endif