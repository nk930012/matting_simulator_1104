from collections import OrderedDict
import numpy as np
from .facedepth_detector import FaceDepthDetector
from .nose_depth import NoseDepth
from .human_tracker import HumanTracker
from .body import Body


class FaceDepthTracker:
    '''
    FaceDepth tracker.
    '''

    def __init__(self, depth_intrin, frame_width=640, frame_height=480,
        webcam_pos=None, num_bodies_max= 20, disappeared_max=30):

        self.frame_width = frame_width
        self.frame_height = frame_height

        self.webcam_pos = webcam_pos

        self.disappeared_max = disappeared_max

        self.num_bodies_max = num_bodies_max
        self.num_bodies = 0
        self.body_pool = []

        # FaceDepth detector
        self.detector = FaceDepthDetector(frame_width,frame_height,0.5)
        self.nose = NoseDepth(depth_intrin, webcam_pos.theta, webcam_pos.dx, webcam_pos.dy, webcam_pos.len_x, webcam_pos.len_y)

        # Human tracker
        self.human_tracker = HumanTracker(disappeared_max=self.disappeared_max)

    def update(self, image, depth_frame):

        bboxes = self.detector.detect(image, depth_frame)
        self.num_bodies = len(bboxes)

        theta = self.webcam_pos.theta
        len_x = self.webcam_pos.len_x
        len_y = self.webcam_pos.len_y
        dx = self.webcam_pos.dx
        dy = self.webcam_pos.dy

        # Bodies
        bodies = []
        for i in range(self.num_bodies):

            x, y, w, h, distance = bboxes[i]
            cx = int(x+(w/2))
            cy = int(y+(h/2))
            body = Body()

            face_point, distance = self.nose.detect(cx, cy, distance)
            bbox = [x, y, w, h]

            body.update(bbox, face_point, distance)
            bodies.append(body)

        # Tracking
        self.human_tracker.update(bodies)
        self.bodies = self.human_tracker.get_bodies()

        return self.bodies

    def update(self, depth_box):

        bboxes = depth_box
        self.num_bodies = len(bboxes)

        theta = self.webcam_pos.theta
        len_x = self.webcam_pos.len_x
        len_y = self.webcam_pos.len_y
        dx = self.webcam_pos.dx
        dy = self.webcam_pos.dy

        # Bodies
        bodies = []
        for i in range(self.num_bodies):

            x, y, w, h, distance = bboxes[i]
            cx = int(x+(w/2))
            cy = int(y+(h/2))
            body = Body()

            face_point, distance = self.nose.detect(cx, cy, distance)
            bbox = [x, y, w, h]

            body.update(bbox, face_point, distance)
            bodies.append(body)

        # Tracking
        self.human_tracker.update(bodies)
        self.bodies = self.human_tracker.get_bodies()

        return self.bodies
