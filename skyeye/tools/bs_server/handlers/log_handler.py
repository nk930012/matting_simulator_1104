# -*- coding: utf-8 -*-
import logging
import time
import tornado.ioloop
import sys 
import collections
#sys.path.append("../../../touch")
from touch.writers import csv_dict_writer

# Version
__version__ = '0.1.0'
class log_handler():
    def __init__(self, WsHandler, debug=logging.ERROR):
        self.ws = WsHandler
        num_points_max = 10000000
        self.points = collections.deque(maxlen=num_points_max)
        
    def open(self, id=0):
        ret = True
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)

    def close(self, id):
        # Save file
        if len(self.points) > 0:
            csv_dict_writer("output.csv", self.points)
            print("wirte out file: output.csv") 
        status_str = "successful"
        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)

    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close log device.')
                self.close(0)
            elif params["command"] == "open":
                if params["property"] == "log_device":
                    self.open(id=id)
            elif params["command"] == "close":
                if params["property"] == "log_device":
                    self.close(id=id)
        elif method == "get":
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'", "data":"[1, 2, 1]"}}'
            self.ws.send_updates(ws_data)
        elif method == "set":
            if params["property"] == "log_pen":
                status_str = "successful"
                ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'", "data":"[1, 2, 1]"}}'
                value = params["value"]
                timestamp = int(time.time()*1000)
                point = {'timestamp': timestamp, 'x': value[1], 'y': value[2], 'transducer_index':1, 'tip_pressure':value[3]}
                self.points.append(point)


