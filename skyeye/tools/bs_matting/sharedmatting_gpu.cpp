#include "sharedmatting_gpu.h"
#include <time.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
using namespace cv;


SharedMatting::SharedMatting(int width, int height)
{
    m_width = width;
    m_height = height;
    m_channels = 3;
    m_step = m_width * m_channels;
    m_numUnknownPixelsMax = m_width * m_height;
}

SharedMatting::SharedMatting()
{
}

SharedMatting::~SharedMatting()
{
    m_image.release();
    m_trimap.release();
    m_matte.release();
}

void SharedMatting::init()
{
    if (m_isInit)
        return;

    if (initBuf() && initGpu()) {
        m_isInit = true;
    }
    else {
        finalize();
        exit(1);
    }
}

bool SharedMatting::initGpu()
{
    bool success = true;

    cl_int err;

    m_device = create_device();

    m_context = clCreateContext(NULL, 1, &m_device, NULL, NULL, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create a context, err : %d.\n", err);

    m_queue = clCreateCommandQueue(m_context, m_device, CL_QUEUE_PROFILING_ENABLE, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create a command queue, err : %d.\n", err);

    m_program = build_program(m_context, m_device, "kernels.cl");

    m_classifyCl = clCreateKernel(m_program, "classifyPixel", &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the kernel(classifyPixel), err : %d.\n", err);

    m_expandCl = clCreateKernel(m_program, "expandKnown", &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the kernel(expandKnown), err : %d.\n", err);

    m_sampleCl = clCreateKernel(m_program, "sample", &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the kernel(sample), err : %d.\n", err);

    m_gatherCl = clCreateKernel(m_program, "gathering", &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the kernel(gather), err : %d.\n", err);

    m_refineCl = clCreateKernel(m_program, "refineSample", &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the kernel(refine), err : %d.\n", err);

    m_localSmoothCl = clCreateKernel(m_program, "localSmooth", &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the kernel(localSmooth), err : %d.\n", err);

    m_trimapBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, m_width * m_height * sizeof(uchar),
        m_trimapData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_trimapBuffer), err : %d.\n", err);

    m_unknownPixelsBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, m_width * m_height * sizeof(PixelPoint),
        m_unknownPixelsData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_unknownPixelsBuffer), err : %d.\n", err);

    m_fgPointsBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, m_width * m_height * kG * sizeof(PixelPoint),
        m_fgPointData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_fgPointsBuffer), err : %d.\n", err);

    m_bgPointsBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, m_width * m_height  * kG * sizeof(PixelPoint),
        m_bgPointData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_bgPointsBuffer), err : %d.\n", err);

    m_numFgPointsBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, m_width * m_height * sizeof(*m_numFgPointsData),
        m_numFgPointsData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_numFgPointsBuffer), err : %d.\n", err);

    m_numBgPointsBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, m_width * m_height * sizeof(int),
        m_numBgPointsData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_numBgPointsBuffer), err : %d.\n", err);

    m_imageBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, m_width * m_height * m_channels * sizeof(uchar),
        m_imageData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_imageBuffer), err : %d.\n", err);

    m_tuplesBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, m_width * m_height * sizeof(Tuple),
        m_tuplesData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_tuplesBuffer), err : %d.\n", err);

    m_ftuplesBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, m_width * m_height * sizeof(Ftuple),
        m_ftuplesData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_ftuplesBuffer), err : %d.\n", err);

    m_unknownIndexesBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, m_width * m_height * sizeof(int),
        m_unknownIndexesData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_unknownIndexesBuffer), err : %d.\n", err);

    m_alphaBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, m_width * m_height * sizeof(uchar),
        m_alphaData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_alphaBuffer), err : %d.\n", err);

    m_labelPixelsBuffer = clCreateBuffer(m_context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, m_width * m_height * sizeof(LabelPoint),
        m_labelPixelsData, &err);

    if (CL_SUCCESS != err)
        printf("Couldn't create the memory(m_labelPixelsBuffer), err : %d.\n", err);

    if (!m_device || !m_context || !m_queue || !m_program || !m_expandCl ||
        !m_sampleCl || !m_gatherCl || !m_refineCl || !m_localSmoothCl ||
        !m_unknownPixelsBuffer || !m_fgPointsBuffer || !m_bgPointsBuffer || !m_numFgPointsBuffer ||
        !m_numBgPointsBuffer || !m_unknownIndexesBuffer || !m_trimapBuffer || !m_tuplesBuffer ||
        !m_imageBuffer || !m_ftuplesBuffer || !m_alphaBuffer || !m_labelPixelsBuffer)
        success = false;

    return success;
}

bool SharedMatting::initBuf()
{
    bool success = true;

    if(m_matte.empty())
        m_matte.create(Size(m_width, m_height), CV_8UC1);

    if (!m_imageData)
        m_imageData = (cl_uchar*)malloc(m_width * m_height * m_channels * sizeof(uchar));

    if(!m_ftuplesData)
        m_ftuplesData = (Ftuple*)malloc(m_width * m_height * sizeof(*m_ftuplesData));

    if(!m_tuplesData)
        m_tuplesData = (Tuple*)malloc(m_width * m_height * sizeof(*m_tuplesData));

    if(!m_unknownPixelsData)
        m_unknownPixelsData = (PixelPoint*)malloc(m_width * m_height * sizeof(*m_unknownPixelsData));

    if(!m_fgPointData)
        m_fgPointData = (PixelPoint*)malloc(m_width * m_height * kG * sizeof(*m_fgPointData));

    if(!m_bgPointData)
        m_bgPointData = (PixelPoint*)malloc(m_width * m_height * kG * sizeof(*m_bgPointData));

    if (!m_numFgPointsData)
        m_numFgPointsData = (int*)malloc(m_width * m_height * sizeof(*m_numFgPointsData));

    if (!m_numBgPointsData)
        m_numBgPointsData = (int*)malloc(m_width * m_height * sizeof(*m_numBgPointsData));

    if (!m_unknownIndexesData)
        m_unknownIndexesData = (int*)malloc(m_width * m_height * sizeof(*m_unknownIndexesData));

    if (!m_alphaData)
        m_alphaData = (uchar*)malloc(m_width * m_height * sizeof(*m_alphaData));

    if (!m_trimapData)
        m_trimapData = (uchar*)malloc(m_width * m_height * sizeof(*m_trimapData));

    if (!m_labelPixelsData)
        m_labelPixelsData = (LabelPoint*)malloc(m_width*m_height * sizeof(*m_labelPixelsData));

    if (m_matte.empty() || !m_ftuplesData || !m_tuplesData || !m_unknownPixelsData ||
        !m_fgPointData || !m_bgPointData || !m_numFgPointsData || !m_numBgPointsData ||
        !m_unknownIndexesData || !m_alphaData || !m_trimapData || !m_imageData || !m_labelPixelsData)
        success = false;

    return success;

}

void SharedMatting::finalize()
{
    finalizeGpu();
    finalizeBuf();
    m_isInit = false;
}

void SharedMatting::finalizeGpu()
{
    if (m_labelPixelsBuffer) {
        clReleaseMemObject(m_labelPixelsBuffer);
        m_labelPixelsBuffer = NULL;
    }

    if (m_unknownPixelsBuffer) {
        clReleaseMemObject(m_unknownPixelsBuffer);
        m_unknownPixelsBuffer = NULL;
    }

    if (m_fgPointsBuffer) {
        clReleaseMemObject(m_fgPointsBuffer);
        m_fgPointsBuffer = NULL;
    }

    if (m_bgPointsBuffer) {
        clReleaseMemObject(m_bgPointsBuffer);
        m_bgPointsBuffer = NULL;
    }

    if (m_numFgPointsBuffer) {
        clReleaseMemObject(m_numFgPointsBuffer);
        m_numFgPointsBuffer = NULL;
    }

    if (m_numBgPointsBuffer) {
        clReleaseMemObject(m_numBgPointsBuffer);
        m_numBgPointsBuffer = NULL;
    }

    if (m_unknownIndexesBuffer) {
        clReleaseMemObject(m_unknownIndexesBuffer);
        m_unknownIndexesBuffer = NULL;
    }

    if (m_trimapBuffer) {
        clReleaseMemObject(m_trimapBuffer);
        m_trimapBuffer = NULL;
    }

    if (m_tuplesBuffer) {
        clReleaseMemObject(m_tuplesBuffer);
        m_tuplesBuffer = NULL;
    }

    if (m_imageBuffer) {
        clReleaseMemObject(m_imageBuffer);
        m_imageBuffer = NULL;
    }

    if (m_ftuplesBuffer) {
        clReleaseMemObject(m_ftuplesBuffer);
        m_ftuplesBuffer = NULL;
    }

    if (m_alphaBuffer) {
        clReleaseMemObject(m_alphaBuffer);
        m_alphaBuffer = NULL;
    }

    if (m_localSmoothCl) {
        clReleaseKernel(m_localSmoothCl);
        m_localSmoothCl = NULL;
    }

    if (m_refineCl) {
        clReleaseKernel(m_refineCl);
        m_refineCl = NULL;
    }

    if (m_gatherCl) {
        clReleaseKernel(m_gatherCl);
        m_gatherCl = NULL;
    }

    if (m_sampleCl) {
        clReleaseKernel(m_sampleCl);
        m_sampleCl = NULL;
    }

    if (m_expandCl) {
        clReleaseKernel(m_expandCl);
        m_expandCl = NULL;
    }

    if (m_classifyCl) {
        clReleaseKernel(m_classifyCl);
        m_classifyCl = NULL;
    }

    if (m_program) {
        clReleaseProgram(m_program);
        m_program = NULL;
    }

    if (m_queue) {
        clReleaseCommandQueue(m_queue);
        m_queue = NULL;
    }

    if (m_context) {
        clReleaseContext(m_context);
        m_context = NULL;
    }

    if (m_device) {
        clReleaseDevice(m_device);
        m_device = NULL;
    }
}

void SharedMatting::finalizeBuf()
{
    if (m_trimapData) {
        free(m_trimapData);
        m_trimapData = NULL;
    }

    if (m_alphaData) {
        free(m_alphaData);
        m_alphaData = NULL;
    }

    if (m_unknownIndexesData) {
        free(m_unknownIndexesData);
        m_unknownIndexesData = NULL;
    }

    if (m_unknownPixelsData) {
        free(m_unknownPixelsData);
        m_unknownPixelsData = NULL;
    }

    if (m_tuplesData) {
        free(m_tuplesData);
        m_tuplesData = NULL;
    }

    if (m_ftuplesData) {
        free(m_ftuplesData);
        m_ftuplesData = NULL;
    }

    if (m_numFgPointsData) {
        free(m_numFgPointsData);
        m_numFgPointsData = NULL;
    }

    if (m_numBgPointsData) {
        free(m_numBgPointsData);
        m_numBgPointsData = NULL;
    }

    if (m_fgPointData) {
        free(m_fgPointData);
        m_fgPointData = NULL;
    }

    if (m_bgPointData) {
        free(m_bgPointData);
        m_bgPointData = NULL;
    }

    if (m_labelPixelsData) {
        free(m_labelPixelsData);
        m_labelPixelsData = NULL;
    }

    if(!m_matte.empty())
        m_matte.release();

    if (!m_trimap.empty())
        m_trimap.release();

    if (!m_image.empty())
        m_image.release();
}

void SharedMatting::classifyGpu()
{
    cl_int err = clEnqueueWriteBuffer(m_queue, m_imageBuffer, CL_TRUE, 0,
        m_width * m_height * m_channels * sizeof(uchar),
        m_imageData, 0, NULL, NULL);

    err |= clEnqueueWriteBuffer(m_queue, m_trimapBuffer, CL_TRUE, 0,
        m_width * m_height * sizeof(uchar),
        m_trimapData, 0, NULL, NULL);

    if (CL_SUCCESS != err) {
        printf("Couldn't write buffer(classifyCl).\n");
        exit(1);
    }

    err = clSetKernelArg(m_classifyCl, 0, sizeof(cl_mem), &m_imageBuffer);
    err |= clSetKernelArg(m_classifyCl, 1, sizeof(cl_mem), &m_trimapBuffer);
    err |= clSetKernelArg(m_classifyCl, 2, sizeof(int), &m_width);
    err |= clSetKernelArg(m_classifyCl, 3, sizeof(int), &m_height);
    err |= clSetKernelArg(m_classifyCl, 4, sizeof(int), &m_channels);
    err |= clSetKernelArg(m_classifyCl, 5, sizeof(cl_mem), &m_unknownPixelsBuffer);
    err |= clSetKernelArg(m_classifyCl, 6, sizeof(cl_mem), &m_labelPixelsBuffer);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(classifyCl) arguments.\n");
        exit(1);
    }

    globalSize[0] = m_width; globalSize[1] = m_height;

    err = clEnqueueNDRangeKernel(m_queue, m_classifyCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    clWaitForEvents(1, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(classifyCl). \n");
        exit(1);
    }

#if 0
    cv::Mat trimap = getImageFromData(m_trimapData, 1);
    imwrite("trimap(Before).png", trimap);
    trimap.release();
#endif
}

void SharedMatting::expandKnownGpu()
{

    cl_int err = clSetKernelArg(m_expandCl, 0, sizeof(cl_mem), &m_trimapBuffer);

    err |= clSetKernelArg(m_expandCl, 1, sizeof(int), &m_width);

    err |= clSetKernelArg(m_expandCl, 2, sizeof(int), &m_height);

    err |= clSetKernelArg(m_expandCl, 3, sizeof(cl_mem), &m_labelPixelsBuffer);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(expandCl) arguments.\n");
        exit(1);
    }

    globalSize[0] = m_width; globalSize[1] = m_height;

    err = clEnqueueNDRangeKernel(m_queue, m_expandCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    clWaitForEvents(1, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(expandCl). \n");
        exit(1);
    }

#if 0
    err = clEnqueueReadBuffer(m_queue, m_trimapBuffer, CL_TRUE, 0,
        m_width * m_height * sizeof(cl_uchar),
        m_trimapData, 0, NULL, &event);

    clWaitForEvents(1, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't read from the buffer(m_trimapBuffer).");
        exit(1);
    }


    cv::Mat trimap = getImageFromData(m_trimapData, 1);
    imwrite("trimap(After).png", trimap);
    trimap.release();
#endif

}

void SharedMatting::expandGpu()
{
    classifyGpu();

    expandKnownGpu();
}

void SharedMatting::sampleGpu()
{
    //printf("sampleGpu\n");

    cl_int err = clSetKernelArg(m_sampleCl, 0, sizeof(cl_mem), &m_trimapBuffer);
    err |= clSetKernelArg(m_sampleCl, 1, sizeof(int), &m_width);
    err |= clSetKernelArg(m_sampleCl, 2, sizeof(int), &m_height);
    err |= clSetKernelArg(m_sampleCl, 3, sizeof(cl_mem), &m_unknownPixelsBuffer);
    err |= clSetKernelArg(m_sampleCl, 4, sizeof(int), &m_numOfUnknownPixels);
    err |= clSetKernelArg(m_sampleCl, 5, sizeof(cl_mem), &m_fgPointsBuffer);
    err |= clSetKernelArg(m_sampleCl, 6, sizeof(cl_mem), &m_numFgPointsBuffer);
    err |= clSetKernelArg(m_sampleCl, 7, sizeof(cl_mem), &m_bgPointsBuffer);
    err |= clSetKernelArg(m_sampleCl, 8, sizeof(cl_mem), &m_numBgPointsBuffer);


    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(sampleCl) arguments.\n");
        exit(1);
    }

    globalSize[0] = m_width; globalSize[1] = m_height;

    err = clEnqueueNDRangeKernel(m_queue, m_sampleCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    clWaitForEvents(1, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(sampleCl). \n");
        exit(1);
    }
}

void SharedMatting::gatherGpu()
{
    //printf("gatherGpu\n");

    cl_int err = clSetKernelArg(m_gatherCl, 0, sizeof(cl_mem), &m_imageBuffer);
    err |= clSetKernelArg(m_gatherCl, 1, sizeof(int), &m_width);
    err |= clSetKernelArg(m_gatherCl, 2, sizeof(int), &m_height);
    err |= clSetKernelArg(m_gatherCl, 3, sizeof(int), &m_channels);
    err |= clSetKernelArg(m_gatherCl, 4, sizeof(cl_mem), &m_unknownPixelsBuffer);
    err |= clSetKernelArg(m_gatherCl, 5, sizeof(int), &m_numOfUnknownPixels);
    err |= clSetKernelArg(m_gatherCl, 6, sizeof(cl_mem), &m_fgPointsBuffer);
    err |= clSetKernelArg(m_gatherCl, 7, sizeof(cl_mem), &m_numFgPointsBuffer);
    err |= clSetKernelArg(m_gatherCl, 8, sizeof(cl_mem), &m_bgPointsBuffer);
    err |= clSetKernelArg(m_gatherCl, 9, sizeof(cl_mem), &m_numBgPointsBuffer);
    err |= clSetKernelArg(m_gatherCl, 10, sizeof(cl_mem), &m_tuplesBuffer);
    err |= clSetKernelArg(m_gatherCl, 11, sizeof(cl_mem), &m_unknownIndexesBuffer);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(gahterCl) arguments. \n");
        exit(1);
    }

    globalSize[0] = m_width; globalSize[1] = m_height;

    err = clEnqueueNDRangeKernel(m_queue, m_gatherCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    clWaitForEvents(1, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(gatherCl). \n");
        exit(1);
    }
}

void SharedMatting::refineGpu()
{
    //printf("refineGpu\n");

    cl_int err = clSetKernelArg(m_refineCl, 0, sizeof(cl_mem), &m_imageBuffer);

    err |= clSetKernelArg(m_refineCl, 1, sizeof(cl_mem), &m_trimapBuffer);

    err |= clSetKernelArg(m_refineCl, 2, sizeof(int), &m_width);

    err |= clSetKernelArg(m_refineCl, 3, sizeof(int), &m_height);

    err |= clSetKernelArg(m_refineCl, 4, sizeof(int), &m_channels);

    err |= clSetKernelArg(m_refineCl, 5, sizeof(cl_mem), &m_unknownPixelsBuffer);

    err |= clSetKernelArg(m_refineCl, 6, sizeof(int), &m_numOfUnknownPixels);

    err |= clSetKernelArg(m_refineCl, 7, sizeof(cl_mem), &m_tuplesBuffer);

    err |= clSetKernelArg(m_refineCl, 8, sizeof(cl_mem), &m_unknownIndexesBuffer);

    err |= clSetKernelArg(m_refineCl, 9, sizeof(cl_mem), &m_ftuplesBuffer);

    err |= clSetKernelArg(m_refineCl, 10, sizeof(cl_mem), &m_alphaBuffer);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(refineCl) arguments. \n");
        exit(1);
    }

    globalSize[0] = m_width; globalSize[1] = m_height;

    err = clEnqueueNDRangeKernel(m_queue, m_refineCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    clWaitForEvents(1, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(refineCl). \n");
        exit(1);
    }
}

void SharedMatting::localSmoothGpu()
{
    //printf("localSmoothGpu\n");

    cl_int err = clSetKernelArg(m_localSmoothCl, 0, sizeof(cl_mem), &m_imageBuffer);

    err |= clSetKernelArg(m_localSmoothCl, 1, sizeof(cl_mem), &m_trimapBuffer);

    err |= clSetKernelArg(m_localSmoothCl, 2, sizeof(int), &m_width);

    err |= clSetKernelArg(m_localSmoothCl, 3, sizeof(int), &m_height);

    err |= clSetKernelArg(m_localSmoothCl, 4, sizeof(int), &m_channels);

    err |= clSetKernelArg(m_localSmoothCl, 5, sizeof(cl_mem), &m_unknownPixelsBuffer);

    err |= clSetKernelArg(m_localSmoothCl, 6, sizeof(int), &m_numOfUnknownPixels);

    err |= clSetKernelArg(m_localSmoothCl, 7, sizeof(cl_mem), &m_ftuplesBuffer);

    err |= clSetKernelArg(m_localSmoothCl, 8, sizeof(cl_mem), &m_alphaBuffer);

    if (CL_SUCCESS != err) {
        printf("Couldn't set the kernel(localSmoothCl) arguments. \n");
        exit(1);
    }

    globalSize[0] = m_width; globalSize[1] = m_height;

    err = clEnqueueNDRangeKernel(m_queue, m_localSmoothCl, 2, NULL, globalSize,
        NULL, 0, NULL, &event);

    clWaitForEvents(1, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't enqueue the kernel(localSmoothCl). \n");
        exit(1);
    }

    err = clEnqueueReadBuffer(m_queue, m_alphaBuffer, CL_TRUE, 0,
        m_width * m_height * sizeof(cl_uchar),
        m_alphaData, 0, NULL, &event);

    clWaitForEvents(1, &event);

    if (CL_SUCCESS != err) {
        printf("Couldn't read from the buffer.");
        exit(1);
    }
}

void SharedMatting::loadImage(cv::Mat image)
{
    if (!image.data)
    {
        cout << "Loading Image Failed!" << endl;
        exit(-1);
    }

    if (image.rows != m_height || image.cols != m_width)
        exit(-1);

    image.copyTo(m_image);

    std::memcpy(m_imageData, m_image.data, m_width * m_height * m_channels);
}

void SharedMatting::loadTrimap(cv::Mat trimap)
{
    if (!trimap.data) {
        cout << "Loading Trimap Failed!" << endl;
        exit(-1);
    }

    trimap.copyTo(m_trimap);

    int step = m_trimap.step1();

    int width = m_trimap.cols;

    int channels = m_trimap.channels();

    uchar * d = (uchar *)m_trimap.data;

    for (int i = 0; i < m_height; ++i)
        for (int j = 0; j < m_width; ++j)
            m_trimapData[i*width + j] = d[i * step + j * channels];
}

void SharedMatting::estimateAlpha(cv::Mat img, cv::Mat trimap)
{
    loadImage(img);
    loadTrimap(trimap);
    solveAlpha();
}

void SharedMatting::save(char * filename)
{
    imwrite(filename, m_matte);
}

cv::Mat SharedMatting::getAlpha()
{
    int h = m_matte.rows;
    int w = m_matte.cols;
    int s = m_matte.step1();

    uchar* d = (uchar *)m_matte.data;
    for (int i = 0; i < h; ++i)
        for (int j = 0; j < w; ++j)
            d[i*s + j] = m_alphaData[i*s + j];

    return m_matte;
}

void SharedMatting::solveAlpha()
{
    expandGpu();

    sampleGpu();

    gatherGpu();

    refineGpu();

    localSmoothGpu();

    getAlpha();

}



Mat SharedMatting::normalize(Mat src)
{
    Mat out;
    src.convertTo(out, CV_32F, 1.0 / 255, 0);
    return out;
}

Mat SharedMatting::unnormalize(Mat src)
{
    Mat out;
    src.convertTo(out, CV_8U, 255, 0);
    return out;
}

Mat SharedMatting::blend(Mat srcIn, Mat alphaIn, Mat bgIn)
{
    Mat src = normalize(srcIn);
    Mat alphaC3;
    cvtColor(alphaIn, alphaC3, COLOR_GRAY2BGR);
    Mat alpha = normalize(alphaC3);
    Mat bg = normalize(bgIn);
    Mat tmp1, tmp2;

    Scalar ones = Scalar(1.0, 1.0, 1.0);
    //ColorPoint ones = createColorPoint(1, 1, 1);

    multiply(alpha, src, tmp1, 1.0);
    multiply((ones - alpha), bg, tmp2, 1.0);
    Mat blended = tmp1 + tmp2;

    Mat out = unnormalize(blended);

    return out;

};

cv::Mat SharedMatting::getImageBlended()
{
    Mat alpha = getAlpha();
    Mat bg(m_image.size(), CV_8UC3, Scalar(255, 255, 255));
    Mat out = blend(m_image, alpha, bg);
    return out;
}

cv::Mat SharedMatting::getImageFromData(uchar* data, int numChannels)
{
    int rows = m_height;
    int cols = m_width;

    Mat img;
    if (numChannels == 3) {
        img = Mat(rows, cols, CV_8UC3);
    }
    else {
        img = Mat(rows, cols, CV_8UC1);
    }
    img.data = data;

    return img;

}