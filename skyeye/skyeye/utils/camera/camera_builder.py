from .camera_constants import *
from .camera import Camera
import yaml

class CameraBuilder(object):
    def __init__(self):
        pass

    @staticmethod
    def createCamera(params):
        impl = None

        print("Camera Type: " + params[CAMERA_TYPE])

        if params[CAMERA_TYPE] == TYPE_REALSENSE:
            from .depth_rs_camera import DepthRsCamera
            impl = DepthRsCamera()
        elif params[CAMERA_TYPE] == TYPE_UDP:
            from .udp_camera import UdpCamera
            impl = UdpCamera()
        else:
            from .cv_camera import CvCamera
            impl = CvCamera()
        impl.init(params)
        
        return Camera(impl)

    @staticmethod
    def buildWithYaml(fileName):
        with open(fileName, 'r', encoding='utf-8') as f:
            inputs = yaml.load(f, Loader=yaml.Loader)

        ckeys = ConstantChecking.getConstantKeys()

        params = {}
        for key in ckeys:
            if key in inputs:
                value = inputs[key]
            else:
                value = ConstantChecking.defaultValue(key)
            params[key] = value

        return CameraBuilder.createCamera(params)

    @staticmethod
    def buildRealSense(width=640, height=480, fps=30):
        params = {}
        params[CAMERA_TYPE] = TYPE_REALSENSE
        params[FRAME_WIDTH] = width
        params[FRAME_HEIGHT] = height
        params[FPS] = fps

        return CameraBuilder.createCamera(params)

    @staticmethod
    def buildCV(device, width=640, height=480, use_V4L2=True, autofocus=True, auto_exposure=False, brightness=1, contrast=40, saturation=50, hue=50, fps=60, exposure=None, use_MJPEG=True):
        params = {}
        params[CAMERA_TYPE] = TYPE_CV
        params[FRAME_WIDTH] = width
        params[FRAME_HEIGHT] = height
        params[FPS] = fps
        params[DEVICE_ID] = device
        params[USE_V4L2] = use_V4L2
        params[AUTO_FOCUS] = autofocus
        params[AUTO_EXPOSURE] = autofocus
        params[BRIGHTNESS] = brightness
        params[CONTRAST] = contrast
        params[SATURATION] = saturation
        params[HUE] = hue
        params[EXPOSURE] = exposure
        params[USE_MJPEG] = use_MJPEG

        return CameraBuilder.createCamera(params)
