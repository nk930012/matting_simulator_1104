# -*- coding: utf-8 -*-
import sys
import os
import argparse
import logging
import tornado.ioloop
import numpy as np
import time
import json 
import imutils
import cv2 as cv

from touch.utils.opencv import Webcam, wait_key


#sys.path.append("../../../touch")
from touch.detect.bs import BsDetector, BsTracker

import collections
from touch.writers import csv_dict_writer
from time import perf_counter


class bs_handler():
    def __init__(self, WsHandler, debug=logging.ERROR):
        self.ws = WsHandler
        num_points_max = 10000000
        self.points = []
        self.active = 0
        self.recordData = False
        self.scheduler = None

        self.frame_count = 0
        self.time_end = 0
        self.time_start = 0
        self.fps = 0
 
        input_file = "./handlers/input.json"

        with open(input_file, 'r') as f:
            inputs = json.load(f)

        self.frame_width = inputs['frame_width']
        self.frame_height = inputs['frame_height']
        self.disappeared_max = inputs['disappeared_max']
        self.width_rmin =  inputs["width_rmin"]
        self.use_V4L2 = True if inputs['use_V4L2'] == 1 else False

        print("frame_width", self.frame_width)
        print("frame_height", self.frame_height)
        print("disappeared_max", self.disappeared_max)
        print("width_rmin", self.width_rmin)
        print("use_V4L2", self.use_V4L2)


        self.bs_detector = BsDetector(self.frame_width, self.frame_height, bs_name="MOG2", history=50)
        self.bs_tracker =  BsTracker(self.frame_width, self.frame_height, self.width_rmin, self.disappeared_max)

        self.webcam = Webcam()
        if self.webcam.is_open():
            self.webcam.release()

        device = 0 # Device ID
        self.webcam.open(device, self.frame_width, self.frame_height, self.use_V4L2)




    def bs_scheduler(self):

        if(10 == self.frame_count):
            self.time_end = perf_counter()
            time_duration = self.time_end - self.time_start
            self.fps = int( 10 / time_duration )
            self.frame_count = 0
        elif(1 == self.frame_count):
            self.time_start = perf_counter()

        self.frame_count += 1

        #print("fps: {}".format(self.fps))

        if self.active == 1:
            data = []
            frame = self.webcam.read()
            bboxes = self.bs_detector.detect(frame)
            fg = self.bs_detector.fg

            print("bboxes: ", bboxes)

            bodies = self.bs_tracker.update(bboxes)
            num_bodies = len(bodies)

            print("bodies: ", bodies)

            for body in bodies:
                
                dict_body = {}
                dict_body['id'] = body.id               
                dict_body['bbox'] = body.bbox 
                data.append(dict_body)
    
            # Data transfer
            wsData = {'size': (self.frame_width, self.frame_height), 'bodies': data}

            #logging.debug(wsData)
            self.ws.send_updates(wsData)

    def open(self, id=0):
        if self.active == 0:
            self.active = 1
            self.scheduler = tornado.ioloop.PeriodicCallback(self.bs_scheduler, 20)
            self.scheduler.start()
        ret = True
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)

    def close(self, id):
        self.active = 0
        if self.scheduler is not None:
            self.scheduler.stop()
            self.scheduler = None
        if len(self.points) > 0 and self.recordData == True:
            filename = time.strftime("pose_coco_%Y%m%d-%H%M%S") + '.json'
            f = open("./data/" + filename, "w")
            f.write(json.dumps(self.points))
            f.close()
            self.points.clear()
        status_str = "successful"
        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)

    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close cam device.')
                self.close(id=1)
            elif params["command"] == "open":
                if params["property"] == "jetson_device":
                    self.open(id=id)
            elif params["command"] == "close":
                if params["property"] == "jetson_device":
                    self.close(id=id)
        elif method == "get":
            pass
        elif method == "set":
            pass
