from .camera_decorate import CameraDecorate
import cv2 as cv
import mediapipe as mp

class MpFaceMeshDecorate(CameraDecorate):

    def __init__(self, camera, flip=None):
        super().__init__(camera,ratio_target=0.5)
        self._result = None
        self.mp_drawing = mp.solutions.drawing_utils
        self.mp_face_mesh = mp.solutions.face_mesh


        self.face_mesh = self.mp_face_mesh.FaceMesh(
            max_num_faces=2,  min_detection_confidence=0.5, min_tracking_confidence=0.5)
        self.drawing_spec = self.mp_drawing.DrawingSpec(thickness=1, circle_radius=1)

    def detect(self):
        super().detect()

        #image = self.camera.color_image.copy()
        image = self.copy_frame()
        image.flags.writeable = False
        self._result = self.detect_mp_pose(image)

        self.camera.countFPS()

    def detect_mp_pose(self, image):

        results = self.face_mesh.process(image)

        return results

    def postProcess(self):
        # Draw the face mesh annotations on the image.
        if self._result.multi_face_landmarks:
            for face_landmarks in self._result.multi_face_landmarks:
                self.mp_drawing.draw_landmarks(
                image=self._imageOut,
                landmark_list=face_landmarks,
                connections=self.mp_face_mesh.FACE_CONNECTIONS,
                landmark_drawing_spec=self.drawing_spec,
                connection_drawing_spec=self.drawing_spec)

    @property
    def result(self):
        return self._result