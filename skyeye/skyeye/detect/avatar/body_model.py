import numpy as np
from pyquaternion import Quaternion
from scipy.spatial.transform import Rotation as R

class BodyModel:

    def __init__(self):

        self.landmarks = None 

    def get_distance(self, a, b):
        return np.linalg.norm(a-b)

    def convert_landmark(self, lm, depth):
        return np.array([lm.x, lm.y, lm.z+depth], dtype=np.float32)

    def get_landmarks(self): 
        return self.landmarks

    def read_data(self, landmarks, base_depth=0):

        self.landmarks = landmarks

        lm = landmarks.landmark

        # pose data will reverse with spec after image filp. 

        self.nose = lm[0]
        self.left_eye_inner = lm[4]
        self.right_eye_inner = lm[1]
        self.left_eye = lm[5]
        self.right_eye = lm[2]
        self.left_eye_outer = lm[6]
        self.right_eye_outer = lm[3]
        self.left_ear = lm[8]
        self.right_ear = lm[7]
        self.mouth_left = lm[10]
        self.mouth_right = lm[9]
        self.left_shoulder = lm[12]
        self.right_shoulder = lm[11]
        self.left_elbow = lm[14]
        self.right_elbow = lm[13]
        self.left_wrist = lm[16]
        self.right_wrist = lm[15]
        self.left_pinky = lm[18]
        self.right_pinky = lm[17]
        self.left_index = lm[20]
        self.right_index = lm[19]
        self.left_thumb = lm[22]
        self.right_thumb = lm[21]
        self.left_hip = lm[24]
        self.right_hip = lm[23]
        self.left_knee = lm[26]
        self.right_knee = lm[25]
        self.left_ankle = lm[28]
        self.right_ankle = lm[27]
        self.left_heel = lm[30]
        self.right_heel = lm[29]
        self.left_foot_index = lm[32]
        self.right_foot_index = lm[31]