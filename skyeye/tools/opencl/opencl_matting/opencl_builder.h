#ifndef OPENCL_BUILDER_H
#define OPENCL_BUILDER_H
#include "simple_opencl_builder.h"
#include <map>

typedef struct _KERNEL_INFO
{
    string programName;
    string kernelName;
    cl_program program;
    cl_kernel  kernel;

}KERNEL_INFO, *PKERNEL_INFO;



class OpenclBuilder : public SimpleOpenclBuilder
{
public:
    OpenclBuilder(){}
    ~OpenclBuilder() {}
    bool init(vector<string> clFileSet);
    const bool isInit();
    virtual void releaseResource();
    const vector<KERNEL_INFO> GetKernels();
    const cl_command_queue GetCommandQueue();
    const cl_context       GetContext();
    const vector<KERNEL_INFO> GetKernelByName(string kernelName);
private:
    vector<cl_device_id>    m_deviceSet;
    cl_context              m_context;
    cl_command_queue        m_queue;
    map<string, cl_program> m_programSet;
    vector<KERNEL_INFO>     m_kernelSet;
    //vector<cl_mem>          m_memorySet;
    cl_device_id            m_deviceUsed;
    bool                    m_isInit;
    bool    getValidGpuDevice();
    bool    buildAllProgram(vector<string> clFileSet);
    bool    buildAllKernel();
    void    releaseAllProgram();
    void    releaseAllKernel();
};
#endif
