#include <stdio.h>
#include "opencv2/opencv.hpp"

#include "sharedmatting_gpu.h"
#include "libsharedmatting.h"
#include "timer.h"

using namespace std;
using namespace cv;

//构造函数
SharedMatting::SharedMatting()
{
    unknownPixels = (PixelPoint*) malloc(numUnknownPixelsMax * sizeof(PixelPoint));
    tuples = (Tuple*) malloc(numUnknownPixelsMax * sizeof(Tuple));
    ftuples = (Ftuple*) malloc(numUnknownPixelsMax * sizeof(Ftuple));
    numFgPoints = (int*) malloc(numUnknownPixelsMax * sizeof(int));
    numBgPoints = (int*) malloc(numUnknownPixelsMax * sizeof(int));
    fgPoints = (PixelPoint*) malloc(numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint));
    bgPoints = (PixelPoint*) malloc(numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint));

}
//析构函数
SharedMatting::~SharedMatting()
{
    image.release();
    trimap.release();
    matte.release();
    free(unknownPixels);
    free(numFgPoints);
    free(numBgPoints);
    free(fgPoints);
    free(bgPoints);
    free(tuples);
    free(ftuples);

    free(tri);
    free(alpha);
    free(unknownIndexes);

}

/*
void SharedMatting::loadImage(string filename)
{
    printf("Load image \n");  
    image = imread(filename);
    if (!image.data)
    {
        cout << "Loading Image Failed!" << endl;
        exit(-1);
    }
    height     = image.rows;
    width      = image.cols;
    step       = image.step1();
    channels   = image.channels();
    imageData       = (cl_uchar *) image.data;

    tri = (cl_uchar*) malloc(height*width * sizeof(cl_uchar));
    alpha = (cl_uchar*) malloc(height*width * sizeof(cl_uchar));
    unknownIndexes = (int*) malloc(numUnknownPixelsMax * sizeof(int));

    matte.create(Size(width, height), CV_8UC1);

    printf("height, width, step: %d, %d, %d \n", height, width, step);  

}

void SharedMatting::loadTrimap(string filename)
{
    printf("load trimap \n");
    trimap = imread(filename);
    if (!trimap.data)
    {
        cout << "Loading Trimap Failed!" << endl;
        exit(-1);
    }

    int step = trimap.step1();
    int channels = trimap.channels();
    uchar* d  = (uchar *)trimap.data;

    for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            tri[getTrimapIndex(i, j)] = d[i * step + j * channels];
        }
    }

}
*/

void SharedMatting::initGpu()
{

    /* Create a device and context */
    device = create_device();
    context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
    if(err < 0) {
        perror("Couldn't create a context.");
        exit(1);
    }

    // Create a command queue
    queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err);
    if(err < 0) {
        printf("Couldn't create a command queue. \n");
        exit(1);   
    };

    // Build the program
    program = build_program(context, device, "kernels.cl");

    // Create the kernels
    process = clCreateKernel(program, "process", &err);
    if(err < 0) {
        printf("Couldn't create the kernel(process): %d \n", err);
        exit(1);
    };

    sampleCl = clCreateKernel(program, "sample", &err);
    if(err < 0) {
        printf("Couldn't create the kernel(sample): %d \n", err);
        exit(1);
    };

    gatherCl = clCreateKernel(program, "gather", &err);
    if(err < 0) {
        printf("Couldn't create the kernel(gather): %d \n", err);
        exit(1);
    };

    refineCl = clCreateKernel(program, "refine", &err);
    if(err < 0) {
        printf("Couldn't create the kernel(refine): %d \n", err);
        exit(1);
    };

    localSmoothCl = clCreateKernel(program, "localSmooth", &err);
    if(err < 0) {
        printf("Couldn't create the kernel(localSmooth): %d \n", err);
        exit(1);
    };

    // Allocate buffer
    imageBuffer = clCreateBuffer(context,
        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, width * height * 3 * sizeof(cl_uchar),
        imageData, &err);

    if (err < 0) {
        printf("Couldn't create image buffer: %d", err);
        exit(1);
    }
    
    trimapBuffer = clCreateBuffer(context,
        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, width * height * sizeof(cl_uchar),
        tri, &err);

    if (err < 0) {
        printf("Couldn't create image buffer: %d", err);
        exit(1);
    }
    
    alphaBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, width * height * sizeof(cl_uchar),
        alpha, &err);

    if (err < 0)
    {
        printf("Couldn't create a output image buffer: %d", err);
        exit(1);
    }

    unknownPixelsBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * sizeof(PixelPoint),
        unknownPixels, &err);

    if (err < 0)
    {
        printf("Couldn't create unknownPixels buffer: %d", err);
        exit(1);
    }

    fgPointsBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint),
        fgPoints, &err);

    if (err < 0)
    {
        printf("Couldn't create fgPointsBuffer: %d", err);
        exit(1);
    }

    bgPointsBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint),
        bgPoints, &err);

    if (err < 0)
    {
        printf("Couldn't create bgPointsBuffer: %d", err);
        exit(1);
    }

    numFgPointsBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * sizeof(int),
        numFgPoints, &err);

    if (err < 0)
    {
        printf("Couldn't create numFgPoints buffer: %d", err);
        exit(1);
    }

    numBgPointsBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, numUnknownPixelsMax * sizeof(int),
        numBgPoints, &err);

    if (err < 0)
    {
        printf("Couldn't create numBgPoints buffer: %d", err);
        exit(1);
    }

    unknownIndexesBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE| CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * sizeof(int),
        unknownIndexes, &err);


    tuplesBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * sizeof(Tuple),
        tuples, &err);

    ftuplesBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, numUnknownPixelsMax * sizeof(Ftuple),
        ftuples, &err);

    if (err < 0)
    {
        printf("Couldn't create unknownIndexes buffer: %d", err);
        exit(1);
    }

    numUnknownPixelsBuffer = clCreateBuffer(context,
        CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(int), &numUnknownPixels, &err);

    if (err < 0)
    {
        printf("Couldn't create numUnknownPixels buffer: %d", err);
        exit(1);
    }


}

void SharedMatting::finalizeGpu()
{

    clReleaseMemObject(imageBuffer);
    clReleaseMemObject(trimapBuffer);
    clReleaseMemObject(alphaBuffer);
    clReleaseMemObject(unknownPixelsBuffer);
    clReleaseMemObject(fgPointsBuffer);
    clReleaseMemObject(bgPointsBuffer);
    clReleaseMemObject(numFgPointsBuffer);
    clReleaseMemObject(numBgPointsBuffer);
    clReleaseMemObject(unknownIndexesBuffer);
    clReleaseMemObject(tuplesBuffer);
    clReleaseMemObject(ftuplesBuffer);

    clReleaseKernel(process);
    clReleaseKernel(sampleCl);
    clReleaseKernel(gatherCl);
    clReleaseKernel(refineCl);
    clReleaseKernel(localSmoothCl);

    //clReleaseEvent(event);
    clReleaseCommandQueue(queue);
    clReleaseProgram(program);
    clReleaseContext(context);

}

void SharedMatting::setImage(Mat imageIn)
{

    image = imageIn; 
    imageData = (cl_uchar *) image.data;

    if (isInitFrame) 
    {
        height     = image.rows;
        width      = image.cols;
        step       = image.step1();
        channels   = image.channels();

        tri = (cl_uchar*) malloc(height*width * sizeof(cl_uchar));
        alpha = (cl_uchar*) malloc(height*width * sizeof(cl_uchar));
        unknownIndexes = (int*) malloc(numUnknownPixelsMax * sizeof(int));

        matte.create(Size(width, height), CV_8UC1);
    }

    isInitFrame = false;
    
}

void SharedMatting::setTrimap(Mat trimapIn)
{
    trimap = trimapIn;

    int step = trimap.step1();
    int channels = trimap.channels();
    uchar* d  = (uchar *)trimap.data;

    for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            tri[getTrimapIndex(i, j)] = d[i * step + j * channels];
        }
    }

}

void SharedMatting::expandKnownGpu()
{

    numUnknownPixels = 0;

    for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            int value = tri[getTrimapIndex(i, j)];
            if (value != 0 && value != 255)
            {
                PixelPoint lp;
                lp.x = i;
                lp.y = j;

                numUnknownPixels += 1;
                unknownPixels[numUnknownPixels-1] = lp;

            }
        }
    }

}

void SharedMatting::sampleGpu()
{

    // Write the image data to buffer
	err = clEnqueueWriteBuffer(queue, unknownPixelsBuffer, CL_TRUE, 0,
        numUnknownPixelsMax * sizeof(PixelPoint),
        unknownPixels, 0, NULL, NULL);

	err = clEnqueueWriteBuffer(queue, trimapBuffer, CL_TRUE, 0,
        width * height * sizeof(cl_uchar),
        tri, 0, NULL, NULL);

    // Set kernel arguments
    err = clSetKernelArg(sampleCl, 0, sizeof(int), &numUnknownPixels);
    err = clSetKernelArg(sampleCl, 1, sizeof(cl_mem), &unknownPixelsBuffer);
    err = clSetKernelArg(sampleCl, 2, sizeof(cl_mem), &trimapBuffer );
    err = clSetKernelArg(sampleCl, 3, sizeof(cl_mem), &numFgPointsBuffer);
    err = clSetKernelArg(sampleCl, 4, sizeof(cl_mem), &numBgPointsBuffer);
    err = clSetKernelArg(sampleCl, 5, sizeof(cl_mem), &fgPointsBuffer);
    err = clSetKernelArg(sampleCl, 6, sizeof(cl_mem), &bgPointsBuffer);
    if(err < 0) {
        printf("Couldn't set the kernel(sampleCl) arguments. \n");
        exit(1);   
    };

    // Enqueue kernel
    globalSize[0] = width; globalSize[1] = height;
    err = clEnqueueNDRangeKernel(queue, sampleCl, 2, NULL, globalSize, 
        NULL, 0, NULL, &event);  
    if(err < 0) {
        printf("Couldn't enqueue the kernel(sampleCl). \n");
        exit(1);
    }

    // Synchronization
    clWaitForEvents(1, &event);

    err = clEnqueueReadBuffer(queue, unknownPixelsBuffer, CL_TRUE, 0,
        numUnknownPixelsMax * sizeof(PixelPoint),
        unknownPixels, 0, NULL, &event);

    err = clEnqueueReadBuffer(queue, numFgPointsBuffer, CL_TRUE, 0,
        numUnknownPixelsMax * sizeof(int),
        numFgPoints, 0, NULL, &event);

    err = clEnqueueReadBuffer(queue, numBgPointsBuffer, CL_TRUE, 0,
        numUnknownPixelsMax * sizeof(int),
        numBgPoints, 0, NULL, &event);

    err = clEnqueueReadBuffer(queue, fgPointsBuffer, CL_TRUE, 0,
        numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint),
        fgPoints, 0, NULL, &event);

    err = clEnqueueReadBuffer(queue, bgPointsBuffer, CL_TRUE, 0,
        numUnknownPixelsMax * numPickedPointsMax * sizeof(PixelPoint),
        bgPoints, 0, NULL, &event);

    if(err < 0) {
        printf("Couldn't read from the buffer.");
        exit(1);   
    }
 
}

void SharedMatting::gatherGpu()
{
    // Set kernel arguments
    err = clSetKernelArg(gatherCl, 0, sizeof(int), &numUnknownPixels);
    err = clSetKernelArg(gatherCl, 1, sizeof(cl_mem), &unknownPixelsBuffer);
    err = clSetKernelArg(gatherCl, 2, sizeof(cl_mem), &numFgPointsBuffer);
    err = clSetKernelArg(gatherCl, 3, sizeof(cl_mem), &numBgPointsBuffer);
    err = clSetKernelArg(gatherCl, 4, sizeof(cl_mem), &fgPointsBuffer);
    err = clSetKernelArg(gatherCl, 5, sizeof(cl_mem), &bgPointsBuffer);
    err = clSetKernelArg(gatherCl, 6, sizeof(cl_mem), &imageBuffer);
    err = clSetKernelArg(gatherCl, 7, sizeof(cl_mem), &tuplesBuffer);
    err = clSetKernelArg(gatherCl, 8, sizeof(cl_mem), &unknownIndexesBuffer);
    if(err < 0) {
        printf("Couldn't set the kernel(gahterCl) arguments. \n");
        exit(1);   
    };

    // Enqueue kernel
    globalSize[0] = width; globalSize[1] = height;
    err = clEnqueueNDRangeKernel(queue, gatherCl, 2, NULL, globalSize, 
        NULL, 0, NULL, &event);
    if(err < 0) {
        printf("Couldn't enqueue the kernel(gatherCl). \n");
        exit(1);
    }

    // Synchronization
    clWaitForEvents(1, &event);

    err = clEnqueueReadBuffer(queue, tuplesBuffer, CL_TRUE, 0,
        numUnknownPixelsMax * sizeof(Tuple),
        tuples, 0, NULL, &event);

   err = clEnqueueReadBuffer(queue, unknownIndexesBuffer, CL_TRUE, 0,
        numUnknownPixelsMax * sizeof(int),
        unknownIndexes, 0, NULL, &event);

    if(err < 0) {
        printf("Couldn't read from the buffer.");
        exit(1);   
    }
 
}

void SharedMatting::refineGpu()
{


    // Set kernel arguments
    err = clSetKernelArg(refineCl, 0, sizeof(int), &numUnknownPixels);
    err = clSetKernelArg(refineCl, 1, sizeof(cl_mem), &unknownPixelsBuffer);
    err = clSetKernelArg(refineCl, 2, sizeof(cl_mem), &unknownIndexesBuffer);
    err = clSetKernelArg(refineCl, 3, sizeof(cl_mem), &imageBuffer);
    err = clSetKernelArg(refineCl, 4, sizeof(cl_mem), &trimapBuffer);
    err = clSetKernelArg(refineCl, 5, sizeof(cl_mem), &tuplesBuffer);
    err = clSetKernelArg(refineCl, 6, sizeof(cl_mem), &ftuplesBuffer);
    err = clSetKernelArg(refineCl, 7, sizeof(cl_mem), &alphaBuffer);
    if(err < 0) {
        printf("Couldn't set the kernel(refineCl) arguments. \n");
        exit(1);   
    };

    // Enqueue kernel
    globalSize[0] = width; globalSize[1] = height;
    err = clEnqueueNDRangeKernel(queue, refineCl, 2, NULL, globalSize, 
        NULL, 0, NULL, &event);  
    if(err < 0) {
        printf("Couldn't enqueue the kernel(refineCl). \n");
        exit(1);
    }

    // Synchronization
    clWaitForEvents(1, &event);

    err = clEnqueueReadBuffer(queue, ftuplesBuffer, CL_TRUE, 0,
        numUnknownPixelsMax * sizeof(Ftuple),
        ftuples, 0, NULL, &event);

    err = clEnqueueReadBuffer(queue, alphaBuffer, CL_TRUE, 0,
        width * height * sizeof(cl_uchar),
        alpha, 0, NULL, &event);

    if(err < 0) {
        printf("Couldn't read from the buffer.");
        exit(1);   
    }
 
}

void SharedMatting::localSmoothGpu()
{

    // Set kernel arguments
    err = clSetKernelArg(localSmoothCl, 0, sizeof(int), &numUnknownPixels);
    err = clSetKernelArg(localSmoothCl, 1, sizeof(cl_mem), &unknownPixelsBuffer);
    err = clSetKernelArg(localSmoothCl, 2, sizeof(cl_mem), &ftuplesBuffer);
    err = clSetKernelArg(localSmoothCl, 3, sizeof(cl_mem), &imageBuffer);
    err = clSetKernelArg(localSmoothCl, 4, sizeof(cl_mem), &trimapBuffer);
    err = clSetKernelArg(localSmoothCl, 5, sizeof(cl_mem), &alphaBuffer);
    if(err < 0) {
        printf("Couldn't set the kernel(localSmoothCl) arguments. \n");
        exit(1);   
    };

    // Enqueue kernel
    globalSize[0] = width; globalSize[1] = height;
    err = clEnqueueNDRangeKernel(queue, localSmoothCl, 2, NULL, globalSize, 
        NULL, 0, NULL, &event);  
    if(err < 0) {
        printf("Couldn't enqueue the kernel(localSmoothCl). \n");
        exit(1);
    }

    // Synchronization
    clWaitForEvents(1, &event);

    err = clEnqueueReadBuffer(queue, alphaBuffer, CL_TRUE, 0,
        width * height * sizeof(cl_uchar),
        alpha, 0, NULL, &event);

    if(err < 0) {
        printf("Couldn't read from the buffer.");
        exit(1);   
    }
 
}

void SharedMatting::save(string filename)
{
    imwrite(filename, matte);
}

Mat SharedMatting::getMatte()
{
    int h     = matte.rows;
    int w     = matte.cols;
    int s     = matte.step1();

    uchar* d  = (uchar *)matte.data;
    for (int i = 0; i < h; ++i)
    {
        for (int j = 0; j < w; ++j)
        {
            //d[i*s+j] = alpha[i][j];
            d[i*s+j] = alpha[getAlphaIndex(i, j)];
        }
    }

    return matte;

}

Mat SharedMatting::normalize(Mat src)
{
    Mat out;
    src.convertTo(out, CV_32F, 1.0/255, 0);

    return out;
} 

Mat SharedMatting::unnormalize(Mat src)
{
    Mat out;
    src.convertTo(out, CV_8U, 255, 0);

    return out;

}

Mat SharedMatting::blend(Mat srcIn, Mat alphaIn, Mat bgIn) 
{
    Mat src = normalize(srcIn);
    Mat alphaC3;
    cvtColor(alphaIn, alphaC3, COLOR_GRAY2BGR);
    Mat alpha = normalize(alphaC3);
    Mat bg = normalize(bgIn);
    Mat tmp1, tmp2;
    Scalar ones = Scalar(1.0, 1.0, 1.0);
    multiply(alpha, src, tmp1, 1.0);
    multiply((ones - alpha), bg, tmp2, 1.0);
    Mat blended = tmp1 + tmp2;
      
    Mat out = unnormalize(blended);

    return out; 

};

Mat SharedMatting::getImageBlended()
{

    Mat alpha = getMatte();
    Mat bg(image.size(), CV_8UC3, Scalar(255, 255, 255));
    Mat out = blend(image, alpha, bg);

    return out;

}

void SharedMatting::solveAlpha()
{
    Timer timer;

    timer.tic();
    cout << "Set trimap data..." << endl;
    //setTrimapData(tri);
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;

    timer.tic();
    cout << "Expanding..." << endl;
    expandKnownGpu();
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;

 
    /*
    timer.tic();
    cout << "Solve alpha ..." << endl;
    // Write the image data to buffer
	err = clEnqueueWriteBuffer(queue, unknownPixelsBuffer, CL_TRUE, 0,
        numUnknownPixelsMax * sizeof(PixelPoint),
        unknownPixels, 0, NULL, NULL);

	err = clEnqueueWriteBuffer(queue, trimapBuffer, CL_TRUE, 0,
        width * height * sizeof(cl_uchar),
        tri, 0, NULL, NULL);

    // Create kernel arguments
    err = clSetKernelArg(process, 0, sizeof(cl_mem), &imageBuffer);
    err = clSetKernelArg(process, 1, sizeof(cl_mem), &trimapBuffer);
    err = clSetKernelArg(process, 2, sizeof(cl_mem), &alphaBuffer);
    err = clSetKernelArg(process, 3, sizeof(int), &numUnknownPixels);
    err = clSetKernelArg(process, 4, sizeof(cl_mem), &unknownPixelsBuffer);
    err = clSetKernelArg(process, 5, sizeof(cl_mem), &unknownIndexesBuffer);
    err = clSetKernelArg(process, 6, sizeof(cl_mem), &numFgPointsBuffer);
    err = clSetKernelArg(process, 7, sizeof(cl_mem), &numBgPointsBuffer);
    err = clSetKernelArg(process, 8, sizeof(cl_mem), &fgPointsBuffer);
    err = clSetKernelArg(process, 9, sizeof(cl_mem), &bgPointsBuffer);
    err = clSetKernelArg(process, 10, sizeof(cl_mem), &tuplesBuffer);
    err = clSetKernelArg(process, 11, sizeof(cl_mem), &ftuplesBuffer);
    if(err < 0) {
        printf("Couldn't set the kernel(process) arguments. \n");
        exit(1);   
    };

    // Enqueue kernel
    globalSize[0] = width; globalSize[1] = height;
    err = clEnqueueNDRangeKernel(queue, process, 2, NULL, globalSize, 
        NULL, 0, NULL, &event);  
    if(err < 0) {
        printf("Couldn't enqueue the kernel(process). \n");
        exit(1);
    }

    // Synchronization
    clWaitForEvents(1, &event);

    err = clEnqueueReadBuffer(queue, alphaBuffer, CL_TRUE, 0,
        width * height * sizeof(cl_uchar),
        alpha, 0, NULL, &event);

    if(err < 0) {
        printf("Couldn't read from the buffer.");
        exit(1);   
    }

    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;
    */

    timer.tic();
    cout << "Sampling..." << endl;
    sampleGpu();
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;

    timer.tic();
    cout << "Gathering..." << endl;
    gatherGpu();
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;

    timer.tic();
    cout << "Refining..." << endl;
    refineGpu();
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;

    timer.tic();
    cout << "LocalSmoothing..." << endl;
    localSmoothGpu();
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;


    timer.tic();
    cout << "Get Matte..." << endl;
    getMatte();
    timer.toc();
    cout << "dt = " << timer.get_dt() << "[s]" << endl;

}
