import csv

def csv_writer(file_name, data, field_names=None):

    with open(file_name, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        if field_names:
            writer.writerow(field_names)

        for d in data:
            writer.writerow(d)

def csv_dict_writer(file_name, data):
    # Writer for dict data

    with open(file_name, mode='w') as csv_file:

        fieldnames = data[0].keys()
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()

        for d in data:
            writer.writerow(d)
