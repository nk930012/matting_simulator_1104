#ifndef OPENCL_MATTING_PROCESS_H
#define OPENCL_MATTING_PROCESS_H

#include "opencl_builder.h"
#include <opencv2/core/mat.hpp>

class OpenclMattingProcess
{
public:
    OpenclMattingProcess() {}
    ~OpenclMattingProcess() {}
    char* process(cv::Mat src, uchar** hueBound, uchar** satBound, uchar** valBound, uint size);

    bool init(uint width, uint height, uint channel, uint dropperSize);

    bool isInit();
    void releaseResource();
private:
    bool m_isInit;
    const vector<string> m_clFileSet = { "binarization.cl", "trimap_generator.cl" };
    OpenclBuilder m_builder;


    cl_mem inputImage = NULL, outputImage = NULL;
    cl_mem m_hueMem = NULL, m_satMem = NULL, m_valMem = NULL, m_sizeMem = NULL,
        m_widthMem = NULL, m_heightMem = NULL, trimapImage = NULL;
    char* m_srcBuffer = NULL, *m_outputBuffer = NULL, *m_trimapBuffer = NULL;

    cl_command_queue m_queue = NULL;
    cl_kernel m_kernel = NULL, m_trimapKernel = NULL;
    cl_context m_context = NULL;
    cl_uchar2* hue = NULL, *sat = NULL, *val = NULL;
    uint m_dropperSize = 0, m_width = 0, m_height = 0, m_channel = 0;
};
#endif

