import numpy as np
import cv2 as cv
from time import perf_counter

import mediapipe as mp

from .head_detector import HeadDetector
from .body_detector import BodyDetector
from .avatar_factory import AvatarFactory

from skyeye.detect.hand import HandDetector


class AvatarDetector:

    def __init__(self, isDetectFace=True, isDetectHand=True, isDetectBody=True):

        self.is_detect_face = isDetectFace
        self.is_detect_hand = isDetectHand
        self.is_detect_body = isDetectBody
        self.head_detector = HeadDetector(max_num_heads=1, min_detection_confidence=0.8, min_tracking_confidence=0.5) 
        self.hand_detector = HandDetector(max_num_hands=2, min_detection_confidence=0.8, min_tracking_confidence=0.5) 
        self.body_detector = BodyDetector(min_detection_confidence=0.8, min_tracking_confidence=0.5)

        self.avatar_factory = AvatarFactory() 

        self.avatars = []

    def detect(self, image):

        image.flags.writeable = False
        heads = []
        hands = []
        bodys = []
        if self.is_detect_face:
            heads = self.head_detector.detect(image)
        if self.is_detect_hand:
            hands = self.hand_detector.detect(image)
        if self.is_detect_body:
            bodys = self.body_detector.detect(image)

        self.avatars = self.avatar_factory.generate(heads, hands, bodys)

        return self.avatars








