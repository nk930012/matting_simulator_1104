#include "opencl_builder.h"

typedef std::map<string, cl_program>::iterator clProgramItr;
typedef std::vector<KERNEL_INFO>::iterator clKernelItr;

bool OpenclBuilder::getValidGpuDevice()
{
    vector<cl_device_id> deviceSet = createDevice();

    int size = deviceSet.size();

    if (0 == size)
        return false;

    for (int i = 0; i < size; ++i)
    {
        cl_device_type deviceType = 0;

        size_t dataSize = sizeof(deviceType) / sizeof(cl_char);

        cl_int err = clGetDeviceInfo(deviceSet[i], CL_DEVICE_TYPE, dataSize, &deviceType, &dataSize);

        if (CL_SUCCESS != err) {
            printf("%s( %s )\n", "clGetDeviceInfo", clErrMessage(err).c_str());
            continue;
        }

        if ((deviceType & CL_DEVICE_TYPE_GPU) == CL_DEVICE_TYPE_GPU)
            m_deviceSet.push_back(deviceSet[i]);
    }

    if (0 == m_deviceSet.size())
        return false;

    return true;
}

void OpenclBuilder::releaseAllProgram()
{
    for (clProgramItr it = m_programSet.begin(); it != m_programSet.end(); ++it)
        releaseProgram(it->second);
    m_programSet.clear();
}

void OpenclBuilder::releaseAllKernel()
{
    for (clKernelItr it = m_kernelSet.begin(); it != m_kernelSet.end(); ++it)
        releaseKernel(it->kernel);
    releaseAllProgram();
    m_kernelSet.clear();
}

bool OpenclBuilder::buildAllProgram(vector<string> clFileSet)
{
    int size = clFileSet.size();
    bool isBuild = true;
    int i = 0;

    //build program
    for (i = 0; i < size; ++i)
    {
        string fileName = clFileSet[i];

        cl_program program = buildProgram(m_context, m_deviceUsed, fileName);

        if (NULL == program)
        {
            isBuild = false;
            break;
        }
         m_programSet[fileName] = program;
    }
    return isBuild;
}

vector<string> splitStr2Vec(string s)
{
    int current = 0; //最初由 0 的位置開始找
    int next;
    vector<string> buf;

    while (1)
    {
        next = s.find_first_of(";", current);
        if (next != current)
        {
            string tmp = s.substr(current, next - current);
            if (tmp.size() != 0) //忽略空字串
                buf.push_back(tmp);
        }
        if (next == string::npos) break;
        current = next + 1; //下次由 next + 1 的位置開始找起。
    }
    return buf;
}

bool OpenclBuilder::buildAllKernel()
{
    bool isBuild = true;

    for (clProgramItr it = m_programSet.begin(); it != m_programSet.end(); ++it)
    {

        size_t dataSize = 0;

        cl_int err = clGetProgramInfo(it->second, CL_PROGRAM_KERNEL_NAMES, 0, NULL, &dataSize);

        if (CL_SUCCESS != err)
        {
            isBuild = false;
            break;
        }

        char* kernelNamesChar = (char*)malloc(sizeof(char) * (dataSize + 1));

        kernelNamesChar[dataSize] = '\0';

        err = clGetProgramInfo(it->second, CL_PROGRAM_KERNEL_NAMES, dataSize, kernelNamesChar, &dataSize);

        if (CL_SUCCESS != err)
        {
            free(kernelNamesChar);
            kernelNamesChar = NULL;
            isBuild = false;
            break;
        }

        string kernelName(kernelNamesChar);
        free(kernelNamesChar);
        vector<string> kernelNameList = splitStr2Vec(kernelName);
        int listSize = kernelNameList.size();
        for (int j = 0; j < listSize; ++j)
        {
            cl_kernel kernel = createKernel(it->second, kernelNameList[j]);
            if (NULL == kernel)
            {
                isBuild = false;
                break;
            }

            KERNEL_INFO info = { it->first, kernelNameList[j], it->second, kernel };
            m_kernelSet.push_back(info);

        }

        if (!isBuild)
            break;
    }

    return isBuild;
}

bool OpenclBuilder::init(vector<string> clFileSet)
{
    bool isInit = true;
    int i = 0;
    int size = 0;

    if (!getValidGpuDevice()) {
        isInit = false;
        goto EXIT;
    }

    m_deviceUsed = m_deviceSet[0];
    m_context = createContext(m_deviceUsed);

    if (NULL == m_context) {
        isInit = false;
        goto EXIT;
    }

    m_queue = createCommandQueue(m_context, m_deviceUsed);

    if (NULL == m_queue)
    {
        isInit = false;
        goto EXIT;
    }

    if (!buildAllProgram(clFileSet)) {
        releaseAllProgram();
        isInit = false;
        goto EXIT;
    }

    if (!buildAllKernel()) {
        isInit = false;
    }

    if (!isInit)
        releaseResource();

EXIT:
    m_isInit = isInit;
    return isInit;
}

void OpenclBuilder::releaseResource()
{
    releaseAllKernel();
    releaseAllProgram();
    if (m_queue)
        releaseCommandQueue(m_queue);

    if (m_context)
        releaseContext(m_context);
}

const vector<KERNEL_INFO> OpenclBuilder::GetKernels()
{
    return m_kernelSet;
}

const cl_command_queue OpenclBuilder::GetCommandQueue()
{
    return m_queue;
}

const bool OpenclBuilder::isInit()
{
    return m_isInit;
}


const cl_context OpenclBuilder::GetContext()
{
    return m_context;
}

const vector<KERNEL_INFO> OpenclBuilder::GetKernelByName(string kernelName)
{
    vector<KERNEL_INFO> kernel;
    for (clKernelItr it = m_kernelSet.begin(); it != m_kernelSet.end(); ++it)
    {
        if (kernelName == it->kernelName)
            kernel.push_back(*it);
    }
    return kernel;
}