#
#   Headpose Detection Utils
#   Written by Qhan
#   Last Update: 2019.1.9
#

import numpy as np
import cv2 as cv


class Color():
    blue = (255, 0, 0)
    green = (0, 255, 0)
    red = (0, 0, 255)
    yellow = (0, 255, 255)
    white = (255, 255, 255)
    black = (0, 0, 0)
    cyan = (255, 255, 0)


class Annotator():

    def __init__(self, im, id, bbox, pose, b=10.0):

        self.im = im
        self.id = id
        self.bbox = bbox
        self.pose = pose

        self.lm = pose.landmarks_2d
        self.rvec = pose.rvec
        self.tvec = pose.tvec
        self.cm = pose.cm
        self.dc = pose.dc

        self.nose = tuple(self.lm[0].astype(int))
        self.box = np.array([
            ( b,  b,  b), ( b,  b, -b), ( b, -b, -b), ( b, -b,  b),
            (-b,  b,  b), (-b,  b, -b), (-b, -b, -b), (-b, -b,  b)
        ])
        self.b = b

        h, w, c = im.shape
        self.fs = ((h + w) / 2) / 500
        self.ls = round(self.fs * 2)
        self.ps = self.ls


    def draw_all(self):
        self.draw_bbox()
        self.draw_landmarks()
        self.draw_axes()
        self.draw_direction()
        return self.im

    def get_image(self):
        return self.im


    def draw_bbox(self):

        x, y, w, h = np.array(self.bbox).astype(int)
        x2 = x + w
        y2 = y + h

        cv.rectangle(self.im, (x, y), (x2, y2), Color.green, self.ls)
        cv.putText(self.im, str(self.id), (x, y), cv.FONT_HERSHEY_SIMPLEX, 1.0, Color.green, 2)

    def draw_landmarks(self):
        for p in self.lm:
            point = tuple(p.astype(int))
            cv.circle(self.im, point, self.ps, Color.cyan, -1)


    # axis lines index
    box_lines = np.array([
        (0, 1), (1, 2), (2, 3), (3, 0),
        (4, 5), (5, 6), (6, 7), (7, 4),
        (0, 4), (1, 5), (2, 6), (3, 7)
    ])
    def draw_axes(self):
        (projected_box, _) = cv.projectPoints(self.box, self.rvec, self.tvec, self.cm, self.dc)
        pbox = projected_box[:, 0]
        for p in self.box_lines:
            p1 = tuple(pbox[p[0]].astype(int))
            p2 = tuple(pbox[p[1]].astype(int))
            cv.line(self.im, p1, p2, Color.blue, self.ls)


    def draw_direction(self):
        (nose_end_point2D, _) = cv.projectPoints(np.array([(0.0, 0.0, self.b)]), self.rvec, self.tvec, self.cm, self.dc)
        p1 = self.nose
        p2 = tuple(nose_end_point2D[0, 0].astype(int))
        cv.line(self.im, p1, p2, Color.red, self.ls)
