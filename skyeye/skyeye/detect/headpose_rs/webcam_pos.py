

class WebcamPos:

    def __init__(self, theta=None, len_x=None, len_y=None, dx=None, dy=None):

        self.theta = theta
        self.len_x = len_x
        self.len_y = len_y
        self.dx = dx
        self.dy = dy
