#include "RemoteDetector.h"
#include <QDebug>
#include <string>
#include <iostream>
#include <QtMath>
#include <QCoreApplication>
using namespace std;

RemoteDetector::RemoteDetector(QObject *parent)
    : QObject(parent)
{
    m_webServiceAddress = "ws://127.0.0.1:8050/ws";
    m_udpIP = "127.0.0.1";
    m_udpPort = 8888;
    m_udpSender = new QUdpSocket(this);
    m_udpAddress = QHostAddress(m_udpIP);

    QObject::connect(&m_client, &DetectorClient::connectSucceed, this, &RemoteDetector::onConnected);
    QObject::connect(&m_client, &DetectorClient::textMessageReceived, this, &RemoteDetector::onTextMessageReceived);

    //int a = 0;
    //char** b;
    //app = new QCoreApplication(a,b);
}

RemoteDetector::~RemoteDetector()
{
    //delete app;
}

void RemoteDetector::connect(QString webServiceAddr, QString udpIP, int udpPort)
{
    m_webServiceAddress = webServiceAddr;
    m_udpIP = udpIP;
    m_udpPort = udpPort;
    m_udpAddress = QHostAddress(m_udpIP);

    m_client.connectToServer(webServiceAddr);

    //app->processEvents(QEventLoop::AllEvents);
}

void RemoteDetector::close()
{
    m_client.close();
    m_udpSender->close();
}

QString RemoteDetector::poseDetect(uchar *image, int length)
{
    //m_receiveMsg = "";
    vector<uchar> imgBuf(image, image + length);
    qDebug()<<"image size in poseDetect:"<<imgBuf.size();
    sendImage(imgBuf);
    //app->processEvents(QEventLoop::AllEvents);
    return m_receiveMsg;
}

void RemoteDetector::sendImage(std::vector<uchar> imgBuf)
{
    //int MAX_DGRAM = 65536;
    int MAX_DGRAM = 8192;
    int MAX_IMAGE_DGRAM = MAX_DGRAM - 64;
    int size = imgBuf.size();
    int count = qCeil((double)size/(MAX_IMAGE_DGRAM));

    int array_pos_start = 0;
    //std::cout<<"count:"<<count<<endl;

    // split image
    while(count)
    {
        int array_pos_end = qMin(size, array_pos_start + MAX_IMAGE_DGRAM);
        std::vector<uchar>::const_iterator first = imgBuf.begin() + array_pos_start;
        std::vector<uchar>::const_iterator last = imgBuf.begin() + array_pos_end;
        std::vector<uchar> newVec(first, last);
        qDebug()<<"first:"<<array_pos_start<<" ,last:"<<array_pos_end;

        //cout<<"array end:"<<array_pos_end<<endl;

        QByteArray subArray(reinterpret_cast<const char*>(newVec.data()), newVec.size());
        QByteArray sendData;
        sendData.append(count);
        sendData.append(subArray);
        //cout<<"send data size:"<<sendData.size()<<endl;
        int result = m_udpSender->writeDatagram(sendData.data(),sendData.size(), m_udpAddress,m_udpPort);
        //cout<<"udp return:"<<result<<endl;


        array_pos_start = array_pos_end;
        count -= 1;
    }
}

//void RemoteDetector::processEvent()
//{
//    //app->processEvents(QEventLoop::AllEvents);
//}

void RemoteDetector::sendMessage(char *message)
{
    QString msg = QString(message);
    m_client.sendMessage(msg);
    //app->processEvents(QEventLoop::AllEvents);
}

void RemoteDetector::onConnected()
{
    //cout<<"connect succeed."<<endl;

}

void RemoteDetector::onTextMessageReceived(QString message)
{
    //cout<<"get data."<<endl;
    //qDebug()<<"RemoteDetector::onTextMessageReceived";
    m_receiveMsg = message;
    //qDebug()<<m_receiveMsg;
}
