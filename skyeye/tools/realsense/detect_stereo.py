import dlib
import numpy as np
import json
import datetime
import imutils
import pyrealsense2 as rs
import numpy as np
import yaml
import cv2 as cv
from time import perf_counter
from skyeye.detect.stereo_camera import RsStereo
from skyeye.utils.opencv import Webcam, wait_key


def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

def draw_bbox(image, bbox):

    xa, ya, width, height, z_dist = bbox
    xa = int(xa)
    ya = int(ya)
    xz = int(xa + width)
    yz = int(ya + height)
    x = int((xa+xz)/2)
    y = int((ya+yz)/2)
    cv.rectangle(image, (xa, ya), (xz, yz), (0, 255, 0), 2)
    color = (0, 255, 0)

    msg = "[depth:{:3f}]".format(z_dist)
    cv.putText(image, msg, (x, y),
               cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

if __name__ == '__main__':

    frame_width = 1280
    frame_height = 720

    stereo = RsStereo(width=frame_width, height=frame_height)
    stereo.open()

    frame_count = 0
    while True:

        frame_count += 1
        #print("frame_count = {}".format(frame_count))
        time_start = perf_counter()

        color_image, depth_info = stereo.update()
        depth_gray = stereo.get_depth_grayscale(d_min=0.5, d_max=1.5)

        #depth_3c = np.dstack((depth_gray, depth_gray, depth_gray))
        depth_3c = cv.cvtColor(depth_gray, cv.COLOR_GRAY2BGR)

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)


        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "fps: {}".format(fps)
        (text_x, text_y) = draw_msg(color_image, msg, text_x, text_y)

        (text_x, text_y) = draw_msg(depth_3c, msg, text_x, text_y)

        # Draw distance of the center
        cx = int(0.5*frame_width + 0.5)
        cy = int(0.5*frame_height + 0.5)
        msg = "d: {}".format(depth_info[cy, cx])
        (text_x, text_y) = draw_msg(depth_3c, msg, cx, cy)

        image_out = depth_3c


        cv.imshow("win", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    stereo.close()

    cv.destroyAllWindows()
