import mediapipe as mp
from .head_factory import HeadFactory

class HeadDetector:

    def __init__(self, max_num_heads=1, min_detection_confidence=0.5, min_tracking_confidence=0.5):

        self.mp_face_mesh = mp.solutions.face_mesh
        self.mp_drawing = mp.solutions.drawing_utils
        self.drawing_spec = self.mp_drawing.DrawingSpec(thickness=1, circle_radius=1)

        self.face_mesh = self.mp_face_mesh.FaceMesh(
            max_num_faces=max_num_heads,
            min_detection_confidence=min_detection_confidence,
            min_tracking_confidence=min_tracking_confidence)

        self.results = None

        self.head_factory = HeadFactory()
        self.heads = []

    def detect(self, image):

        image.flags.writeable = False
        self.results = self.face_mesh.process(image)
        self.heads = self.head_factory.generate(self.results)

        return self.heads


    def draw_landmarks(self, image):

        for landmarks in self.results.multi_face_landmarks:
            self.mp_drawing.draw_landmarks(
            image=image, landmark_list=landmarks,
            connections=self.mp_face_mesh.FACE_CONNECTIONS,
            landmark_drawing_spec=self.drawing_spec,
            connection_drawing_spec=self.drawing_spec)

        return image            

