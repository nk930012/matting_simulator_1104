#ifndef SHAREDMSTTING_H
#define SHAREDMSTTING_H

#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cmath>
#include <vector>

#include "typedef.h"
#include "cl_utilities.h"

using namespace std;
using namespace cv;

class SharedMatting
{
public:
    SharedMatting();
    ~SharedMatting();

    //void loadImage(string filename);
    //void loadTrimap(string filename);
    void setImage(Mat imageIn);
    void setTrimap(Mat trimapIn);
    void expandKnownGpu();
    void expandKnown();
    void sampleGpu();
    void gatherGpu();
    void refineGpu();
    void localSmoothGpu();
    void solveAlpha();
    void save(string filename);
    Mat normalize(Mat src);
    Mat unnormalize(Mat src);
    cv::Mat getMatte();
    cv::Mat blend(Mat src, Mat alpha, Mat bg); 
    cv::Mat getImageBlended();
    void release();

    int getDataIndex(size_t i, size_t j, size_t c) { return i*channels*width + j*channels + c; };
    int getTrimapIndex(size_t i, size_t j) { return i*width + j; };
    int getAlphaIndex(size_t i, size_t j) { return i*width + j; };

    void initGpu();
    void finalizeGpu();

private:

    cv::Mat image;
    cv::Mat trimap;
    cv::Mat matte;
    bool isInitFrame = true;

    const int kI = 10;
    const int kG = 4; //each unknown p gathers at most kG forground and background samples
    const double kC = 5.0;

    const int numUnknownPixelsMax = 921600/2; // 1280x720 / 2
    const int numPickedPointsMax = kG;
    int numUnknownPixels = 0;

    Tuple* tuples;
    Ftuple* ftuples;
    PixelPoint* unknownPixels;
    PixelPoint* fgPoints;
    PixelPoint* bgPoints;
    int* numFgPoints;
    int* numBgPoints;
    int* unknownIndexes;//Unknown的索引信息；
    cl_uchar* imageData;  // Image data
    cl_uchar* tri;   // Trimap data
    cl_uchar* alpha; // Alpha data

    int height;
    int width;
    int step;
    int channels;

    cl_device_id device;
    cl_context context;
    cl_command_queue queue;
    cl_program program;
    cl_kernel process;
    cl_kernel sampleCl;
    cl_kernel gatherCl;
    cl_kernel refineCl;
    cl_kernel localSmoothCl;
    cl_int err;
    size_t globalSize[2];
    cl_event event;

    cl_mem imageBuffer;
    cl_mem trimapBuffer;
    cl_mem alphaBuffer;
    cl_mem unknownPixelsBuffer;
    cl_mem fgPointsBuffer;
    cl_mem bgPointsBuffer;
    cl_mem numFgPointsBuffer;
    cl_mem numBgPointsBuffer;
    cl_mem unknownIndexesBuffer;
    cl_mem tuplesBuffer;
    cl_mem ftuplesBuffer;
    cl_mem numUnknownPixelsBuffer;


};



#endif
