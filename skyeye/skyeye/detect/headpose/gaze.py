import numpy as np
from skyeye.tracers import KalmanTracer2d
import cv2 as cv


class Gaze:
    """
    Detect the point of gaze.
    """

    def __init__(self, theta, dx=None, dy=None, len_x=None, len_y=None):

        self.theta = theta
        self.dx = dx
        self.dy = dy
        self.len_x = len_x
        self.len_y = len_y

        # Tracer
        dt = 1.0/30
        self.gaze_tracer = KalmanTracer2d(0.5, 0.5, dt, P_var=1.0, Q_var=1.0, R_var=2.0)
        self.face_tracer = KalmanTracer2d(0.5, 0.5, dt, P_var=1.0, Q_var=1.0, R_var=1.0)

    def get_gaze_point(self, nose, dvec):
        '''
        nose: array-like.
            Nose tip.
        dvec: array-like.
            Direction vector.
        '''

        np_vec = np.array([0, 0, 0], dtype=np.float64)
        np_vec[1] = np.sin(self.theta)
        np_vec[2] = np.cos(self.theta)

        p0 = np.array([0, 0, 0], dtype=np.float64)

        lvec0 = np.array(nose, dtype=np.float64)
        lvec = np.array(dvec, dtype=np.float64)

        denominator = np_vec.dot(lvec)
        t = np_vec.dot(p0 - lvec0) / denominator
        p = lvec0 + lvec*t

        x = p[0]
        y = p[1]

        xp = -x + self.dx
        yp = y - self.dy

        u = xp / self.len_x
        v = yp / self.len_y

        # Apply Kalman filter
        self.gaze_tracer.update(u, v)
        u_kf, v_kf = self.gaze_tracer.get_pos()
        gaze_point = (u_kf, v_kf)

        return gaze_point


    def get_face_point(self, nose):

        p0 = np.array([0, 0, 0], dtype=np.float64)
        lvec0 = np.array(nose, dtype=np.float64)

        np_vec = np.array([0, 0, 0], dtype=np.float64)
        np_vec[1] = np.sin(self.theta)
        np_vec[2] = np.cos(self.theta)

        s = np_vec.dot(p0 - lvec0)
        q = lvec0 + np_vec*s

        x = q[0]
        y = q[1]

        xp = -x + self.dx
        yp = y - self.dy

        u = xp / self.len_x
        v = yp / self.len_y

        # Apply Kalman filter
        self.face_tracer.update(u, v)
        u_kf, v_kf = self.face_tracer.get_pos()
        face_point = (u_kf, v_kf)

        # Distance to the perpendicular plane
        distance = np_vec.dot(lvec0)

        return face_point, distance


    def detect(self, rvec, tvec):
        '''
        nose: array-like.
            Nose tip.
        dvec: array-like.
            Direction vector.
        '''

        z_w = np.array([0, 0, -1]).reshape((3, 1))
        rmat = cv.Rodrigues(rvec)[0]

        z_c = rmat.dot(z_w)
        nose = -tvec[:, 0]
        dvec = z_c[:, 0]

        # Apply Kalman filter
        gaze_point = self.get_gaze_point(nose, dvec)
        face_point, distance = self.get_face_point(nose)

        return gaze_point, face_point, distance
