# Background Matting

# Set up
ln -s ../../../models/deeplab_model deeplab_model
ln -s ../../../models/background_matting/Models Models

# Recorder

python recorder.py

# Segment
python segment.py -i images/

# Matting
python matting.py -m real-fixed-cam -i images/ -b images/000_back.png -tb images/000_back.png -o images/
