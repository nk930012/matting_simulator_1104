from .data_decorate import DataDecorate
from skyeye.utils.opencv import InfoDrawer

class FaceBox(DataDecorate):
    
    def detect(self):
        super().detect()

        self.boxes = self.component.face_boxes
        self._imageOut = self.component.image_out

    def getFaceBox(self):
        return self.boxes

    def getNoses(self):
        noses = []
        for box in self.boxes:
            xa, ya, width, height = box
            xa = int(xa)
            ya = int(ya)
            xz = int(xa + width)
            yz = int(ya + height)
            x = int((xa+xz)/2)
            y = int((ya+yz)/2)
            depth = self.component.getDepthValue(x, y)

            nose = [x, y, depth]
            noses.append(nose)
        return noses

    def postProcess(self):
        for bbox in self.boxes:
            InfoDrawer.draw_bbox(self._imageOut, bbox)