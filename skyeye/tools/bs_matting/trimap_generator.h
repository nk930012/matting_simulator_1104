#ifndef TRIMAP_GENERATOR_H
#define TRIMAP_GENERATOR_H

#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;


class TrimapGenerator
{
public:
    
    TrimapGenerator();
    TrimapGenerator(int width, int height);
    ~TrimapGenerator();

    Mat generate(Mat mask);
    void fillValues(Mat target, Mat diff, int ix, int iy);
    void fillValuesSquare(Mat target, int ix, int iy, int squareSize);

private:

    int mWidth = 0;
    int mHeight = 0;

    int mLenLong = 0;
    int mLenShort = 0;

    Mat mTrimap;

};

#endif
