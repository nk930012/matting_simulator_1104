import cv2 as cv
import platform
import subprocess

class Webcam(object):

    def __init__(self):

        self.device = None # Device
        self.vc = None # Video capture
        self.frame = None

    def open(self, device, width=640, height=480, use_V4L2=True, autofocus=True, auto_exposure=False, brightness=1, contrast=40, saturation=50, hue=50, fps=60, exposure=None, use_MJPEG=True):
        # Set the target device.

        (major, minor, _) = cv.__version__.split(".")
        print('opencv ver. ', major)

        # check Os and VID/PID
        if platform.system() == 'Windows':
            print('OS is windows')

            use_V4L2 = False
            vendor_id = ""
            product_id = ""

        elif platform.system()  == 'Linux':
            print('OS is linux')
            dev_path = "/sys/class/video4linux/video"+str(device)+"/device/uevent"
            dev_info = subprocess.check_output(["cat", dev_path])
            dev_info = str(dev_info).split('PRODUCT=', 1)[1]
            dev_info = str(dev_info).split('TYPE=', 1)[0]
            vendor_id = dev_info.split('/')[0]
            product_id = dev_info.split('/')[1]
            #print(dev_info, vendor_id, product_id)
        else:
            use_V4L2 = False
            vendor_id = ""
            product_id = ""

        self.device = device
        if use_V4L2:
            self.vc = cv.VideoCapture(self.device, cv.CAP_V4L2)  
        else:
            self.vc = cv.VideoCapture(self.device)
            
        if use_MJPEG:
            print('use MJPEG')
            fourcc_cap = cv.VideoWriter_fourcc(*'MJPG')
            self.vc.set(cv.CAP_PROP_FOURCC , fourcc_cap )
        else:
            print('use YUYV')
            fourcc_cap = cv.VideoWriter_fourcc(*'YUYV')
            self.vc.set(cv.CAP_PROP_FOURCC , fourcc_cap )

        # Set the camera parameters
        self.vc.set(cv.CAP_PROP_FRAME_WIDTH, width)
        self.vc.set(cv.CAP_PROP_FRAME_HEIGHT, height)

        if autofocus:
            self.vc.set(cv.CAP_PROP_AUTOFOCUS, 1)
        else:
            self.vc.set(cv.CAP_PROP_AUTOFOCUS, 0)

        if exposure == None:
            if vendor_id in '0c45' or vendor_id in '0C45':
                if product_id in '6368':
                    exposure = 0.05
                elif product_id in '6536':
                    exposure = 0.01
            else:
                exposure = 0.5

        if auto_exposure:
            self.vc.set(cv.CAP_PROP_AUTO_EXPOSURE, 3)
        else: # Manual
            if int(major) == 4:
                self.vc.set(cv.CAP_PROP_AUTO_EXPOSURE, 1)
                self.vc.set(cv.CAP_PROP_EXPOSURE, exposure)
            else: # For version 3
                self.vc.set(cv.CAP_PROP_AUTO_EXPOSURE, 0.25)
                self.vc.set(cv.CAP_PROP_EXPOSURE, exposure)
        self.vc.set(cv.CAP_PROP_FPS, fps)

        print("CAP_PROP_FOURCC: ", self.vc.get(cv.CAP_PROP_FOURCC))
        print("CAP_PROP_AUTO_EXPOSURE: ", self.vc.get(cv.CAP_PROP_AUTO_EXPOSURE))
        print("CAP_PROP_EXPOSURE: ", self.vc.get(cv.CAP_PROP_EXPOSURE))

    def is_open(self):
        # Device is open or not.

        if self.vc:
            return self.vc.isOpened()
        else:
            return False


    def read(self):
        # Read the frame.

        is_capturing, frame = self.vc.read()
        #frame = cv.cvtColor(frame, cv.COLOR_BGR2RGB)

        # Save the frame
        self.frame = frame

        return frame

    def get_frame(self):
        return self.frame

    def release(self):
        # Release the resource.

        if self.is_open():
            self.vc.release()
