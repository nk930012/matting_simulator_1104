import abc
import cv2 as cv
import numpy as np
import mediapipe as mp
import json
import enum
from skyeye.utils.camera import CameraBuilder
from skyeye.detect.decorator import Mp3dHand
from skyeye.detect.decorator.mp_hands_decorate import MpHandsDecorate
from skyeye.utils.opencv import Webcam, wait_key

from skyeye.detect.hand import HandFactory, GestureDetector, MouseSimulator

class IParser(metaclass=abc.ABCMeta):
    def __init__(self):
        return NotImplemented

    def parse(self, data):
        return NotImplemented

class MPHand2DParser(IParser):
    def __init__(self):
        super().__init__()
        self.mouse_detector = MouseSimulator()
         
        self.gesture_detector = GestureDetector()

    def parse(self, data):
        ret = []     
        for hand in data:
            dict_hand = {}
            if hand.get_handedness() == 'Right':
                dict_hand['SIDE'] = 'Right'
            else:
                dict_hand['SIDE'] = 'Left'

            dict_hand['WRIST'] = hand.WRIST.tolist()

            dict_hand['THUMB_CMC'] = hand.THUMB_CMC.tolist()
            dict_hand['THUMB_MCP'] = hand.THUMB_MCP.tolist()
            dict_hand['THUMB_IP'] =  hand.THUMB_IP.tolist()
            dict_hand['THUMB_TIP'] = hand.THUMB_TIP.tolist()

            dict_hand['INDEX_FINGER_MCP'] = hand.INDEX_FINGER_MCP.tolist() 
            dict_hand['INDEX_FINGER_PIP'] = hand.INDEX_FINGER_PIP.tolist()
            dict_hand['INDEX_FINGER_DIP'] = hand.INDEX_FINGER_DIP.tolist()
            dict_hand['INDEX_FINGER_TIP'] = hand.INDEX_FINGER_TIP.tolist()

            dict_hand['MIDDLE_FINGER_MCP'] = hand.MIDDLE_FINGER_MCP.tolist()
            dict_hand['MIDDLE_FINGER_PIP'] = hand.MIDDLE_FINGER_PIP.tolist()
            dict_hand['MIDDLE_FINGER_DIP'] = hand.MIDDLE_FINGER_DIP.tolist()
            dict_hand['MIDDLE_FINGER_TIP'] = hand.MIDDLE_FINGER_TIP.tolist()

            dict_hand['RING_FINGER_MCP'] = hand.RING_FINGER_MCP.tolist()
            dict_hand['RING_FINGER_PIP'] = hand.RING_FINGER_PIP.tolist()
            dict_hand['RING_FINGER_DIP'] = hand.RING_FINGER_DIP.tolist()
            dict_hand['RING_FINGER_TIP'] = hand.RING_FINGER_TIP.tolist()
            
            dict_hand['PINKY_FINGER_MCP'] = hand.PINKY_FINGER_MCP.tolist()
            dict_hand['PINKY_FINGER_PIP'] = hand.PINKY_FINGER_PIP.tolist()
            dict_hand['PINKY_FINGER_DIP'] = hand.PINKY_FINGER_DIP.tolist()
            dict_hand['PINKY_FINGER_TIP'] = hand.PINKY_FINGER_TIP.tolist()

            dict_hand['GESTURE'] = self.gesture_detector.detect(hand)
            dict_hand['NUMERIC'] = hand.get_numeric_gesture()
            
            mouse_pos, click_type = self.mouse_detector.detect(hand)
            dict_hand['MOUSE_POS'] = mouse_pos
            dict_hand['CLICK_TYPE'] = click_type

            #mouse_box = self.mouse_detector.get_box()
            #dict_hand['MOUSE_BOX'] = mouse_box
        
            depth = self.mouse_detector.get_depth()
            #print("The type of depth is {}".format(type(depth)))            
            depth = str(depth)
            dict_hand['DEPTH'] = depth
            ret.append(dict_hand)
        wsData = { 'TYPE': 'Hand2D', 'LEN': len(data), 'DATA': ret}
        return wsData
