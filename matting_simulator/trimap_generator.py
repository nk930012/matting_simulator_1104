import numpy as np
import cv2 as cv

from libmatting import get_min_max_dists, gray_to_binary

class TrimapGenerator:

    def __init__(self, num_erosion=5, num_dilation=5):

        self.num_erosion = num_erosion
        self.num_dilation = num_dilation

        self.pixels = []
        self.min_dist = None
        self.max_dist = None

    def set_pixels(self, pixels):
        self.pixels = pixels

    def estimate_trimap(self, min_dist, max_dist, lower_bound, upper_bound):
        
        #min_dist = cv.blur(min_dist, (10, 10))
        #max_dist = cv.blur(max_dist, (10, 10))
        height, width = min_dist.shape

        trimap = np.zeros((height, width), dtype=np.uint8)
        trimap[:, :]  = 127 # Unknown
        trimap = np.where(max_dist < lower_bound, 0, trimap) # Background
        trimap = np.where(min_dist > upper_bound, 255, trimap) # Forground

        tmp = cv.blur(trimap, (10, 10))
        tmp  = np.where((tmp < 255) & (tmp > 0), 127, tmp) # Unknown region
        trimap = np.array(tmp, dtype=np.uint8)

        return trimap

    def generate(self, image, lower_bound, upper_bound):
        '''
        Genetate the trimap.

        image: nd array
            Image with BGR channels.

        lower_bound: float
            Lower bound. (normalized)

        upper_bound: float
            Upper bound. (normalized)
        '''

        self.min_dist, self.max_dist = get_min_max_dists(image, self.pixels)
        trimap = self.estimate_trimap(self.min_dist, self.max_dist, lower_bound, upper_bound)

        return trimap


    def get_gray_dist(self):

        dist = 0.5*(self.min_dist + self.max_dist) 
        dist = np.array(dist * 255, dtype=np.uint8)

        return dist