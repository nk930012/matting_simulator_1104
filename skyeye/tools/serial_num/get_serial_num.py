import datetime as dt
import os
import yaml
import skyeye.utils.software_protector as sp
import sys

def is_valid_date(year, month, day):
    is_valid_date = False
    try:
        expired = dt.datetime(year, month, day, 0, 0, 0)
    except:
        print("Create expired date failed")
    else:
        is_valid_date = True
    finally:
        return is_valid_date

def reparse_mac(mac):
    mac = mac.replace("-", "")
    mac = mac.replace(":", "")
    mac = mac.replace(" ", "")
    return mac


if __name__ == '__main__':


    year = int(input("Please enter year : "))
    month = int(input("Please enter month : "))
    day = int(input("Please enter day : "))

    mac = input("Please enter mac address(on board) : ")
    mac = reparse_mac(mac)

    if(not is_valid_date(year, month, day) or len(mac) != 12):
        sys.exit(0)

    manufacture_date = dt.datetime.utcnow()
    last_open_date = manufacture_date + dt.timedelta(seconds=10)
    expired_date = dt.datetime(year, month, day, 0, 0, 0)


    time_codec = sp.TimeInfoCodec()
    manufacture_second = time_codec.get_second(manufacture_date)
    last_open_second = time_codec.get_second(last_open_date)
    expired_second = time_codec.get_second(expired_date)

    if(expired_second < 0):
        print("Expired date must be bigger than 1911-01-01")
        sys.exit(0)

    encrypted_manufacture = time_codec.encode_second(manufacture_second)
    encrypted_last_open = time_codec.encode_second(last_open_second)
    encrypted_expired = time_codec.encode_second(expired_second)

    hw_codec = sp.HwInfoCodec()
    encrypted_mac = hw_codec.encode_mac_addr(mac)


    print("initSN: {0}".format(encrypted_manufacture))
    print("currentSN: {0}".format(encrypted_last_open))
    print("expiredSN: {0}".format(encrypted_expired))
    print("hwSN: {0}".format(encrypted_mac))

    lock_file = "./lock"
    if(os.path.isfile(lock_file)):
        os.remove(lock_file)

    output = {'initSN':encrypted_manufacture,
              'currentSN':encrypted_last_open,
              'expiredSN':encrypted_expired,
              'hwSN':encrypted_mac}

    with open(lock_file, 'w', encoding='utf-8') as f:
        documents = yaml.dump(output, f, default_flow_style=False, sort_keys=False)

    while True:
        input('Press any key to exit...')
        break

