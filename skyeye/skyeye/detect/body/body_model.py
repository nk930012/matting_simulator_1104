import numpy as np


class BodyModel:
    '''
    Body model with COCO format.
    '''

    def __init__(self):

        self.keypoints = None
        self.has_body = False

    def set_keypoints(self, keypoints):
        self.keypoints = keypoints
        try:
            if len(keypoints) > 0:
                self.has_body = True
        except:
            self.has_body = False

    def get_nose(self):
        return self.get_position(self.keypoints[0])

    def get_neck(self):
        return self.get_position(self.keypoints[1])

    def get_right_shoulder(self):
        return self.get_position(self.keypoints[2])

    def get_right_elbow(self):
        return self.get_position(self.keypoints[3])

    def get_right_wrist(self):
        return self.get_position(self.keypoints[4])

    def get_left_shoulder(self):
        return self.get_position(self.keypoints[5])

    def get_left_elbow(self):
        return self.get_position(self.keypoints[6])

    def get_left_wrist(self):
        return self.get_position(self.keypoints[7])

    def get_right_hip(self):
        return self.get_position(self.keypoints[8])
    
    def get_right_knee(self):
        return self.get_position(self.keypoints[9])

    def get_right_ankle(self):
        return self.get_position(self.keypoints[10])

    def get_left_hip(self):
        return self.get_position(self.keypoints[11])
    
    def get_left_knee(self):
        return self.get_position(self.keypoints[12])

    def get_left_ankle(self):
        return self.get_position(self.keypoints[13])

    def get_right_eye(self):
        return self.get_position(self.keypoints[14])

    def get_left_eye(self):
        return self.get_position(self.keypoints[15])

    def get_right_ear(self):
        return self.get_position(self.keypoints[16])

    def get_left_ear(self):
        return self.get_position(self.keypoints[17])
    
    def get_right_wrist_mirrored(self):
        return self.get_left_wrist()

    def get_left_wrist_mirrored(self):
        return self.get_right_wrist()

    def get_position(self, obj):

        if self.has_body:
            x = int(obj[0]+0.5)
            y = int(obj[1]+0.5)
        else:
            x = 0
            y = 0

        return [x, y]
