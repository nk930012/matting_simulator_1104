#ifndef FRAME_AVERAGER
#define FRAME_AVERAGER

#include <vector>
#include "opencv2/opencv.hpp"

class FrameAverager {
public:
    FrameAverager();
    FrameAverager(int width, int height, int type,int frameCnt);
    ~FrameAverager();
    void addFrame(cv::Mat image);
    cv::Mat getAverageFrame();

private:
    int m_width = 0;
    int m_height = 0;
    int m_frameCnt = 0;
    int m_type = 0;
    std::vector<cv::Mat> m_frames;
};

#endif
