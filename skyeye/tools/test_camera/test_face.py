import cv2 as cv

from skyeye.utils.camera import CameraBuilder
from skyeye.utils.camera import DepthUtil
from skyeye.utils.opencv import wait_key
from skyeye.utils.opencv import InfoDrawer
from skyeye.detect.decorator import Mp3dData
from skyeye.detect.decorator import Mp3dHolistic
from skyeye.detect.decorator import Mp3dHand
from skyeye.detect.decorator import FaceDepthBox
from skyeye.detect.decorator.mp_pose_decorate import MpPoseDecorate
from skyeye.detect.decorator.mp_holistic_decorate import MpHolisticDecorate
from skyeye.detect.decorator.mp_hands_decorate import MpHandsDecorate
from skyeye.detect.decorator.mp_face_mesh_decorate import MpFaceMeshDecorate
import numpy as np
import yaml
from skyeye.detect.headpose_rs import FaceDepthTracker
from skyeye.detect.headpose_rs import WebcamPos

index_blue = 0
index_green = 1
index_red = 2
index_cyan = 3
index_magenta = 4
index_yellow = 5
index_black = 6

default_color = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255), (0 , 0, 0)]

def draw_msg(img, msg, x, y, color):

    cv.putText(img, msg, (x, y), cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y_shift = 20
    y += y_shift

    return (x, y)

def drawline( img, pt1, pt2, color, thickness=1, style='dotted', gap=20 ):
    dist =((pt1[0]-pt2[0])**2+(pt1[1]-pt2[1])**2)**.5
    pts= []
    for i in  np.arange(0,dist,gap):
        r=i/dist
        x=int((pt1[0]*(1-r)+pt2[0]*r)+.5)
        y=int((pt1[1]*(1-r)+pt2[1]*r)+.5)
        p = (x,y)
        pts.append(p)

    if style=='dotted':
        for p in pts:
            cv.circle(img,p,thickness,color,-1)
    else:
        s=pts[0]
        e=pts[0]
        i=0
        for p in pts:
            s=e
            e=p
            if i%2==1:
                cv.line(img,s,e,color,thickness)
            i+=1

def draw_grid(img, width, height):
    division_x = 4
    division_y = 10
    increament_x = int( width / division_x )
    increament_y = int( height / division_y )
    color = (240, 32, 160)
    thickness = 1 
    lineType = 4
    gap = 10
    style='dashed'
    
    for i in range(division_x):
    
        x = (increament_x * i)
        if(i != 0):
            msg = "{:3.2f}".format(i * 0.25)
            draw_msg(img, msg, x, ( height - 30 ), color)
        
        drawline( img, (x, 0), (x, height), color, thickness, style, gap)
    
    for i in range(division_y):
        y = (increament_y * i)
        if(i != 0):
            msg = "{:3.1f}".format(i * 0.1)
            draw_msg(img, msg, (width - 30), y, color)
        drawline( img, (0, y), (width, y), color, thickness, style, gap)

if __name__ == '__main__':

    input_file = "web_position.yaml"
    with open(input_file, 'r', encoding='utf-8') as f:
        inputs = yaml.load(f, Loader=yaml.Loader)

    len_x = inputs['len_x']
    len_y = inputs['len_y']
    dx = inputs['dx']
    dy = inputs['dy']
    theta = inputs['theta_deg']
    num_bodies_max = inputs['num_bodies_max']
    disappeared_max = inputs['disappeared_max']

    webcam_pos = WebcamPos(theta=theta, len_x=len_x, len_y=len_y, dx=dx, dy=dy)

    camera = CameraBuilder.buildWithYaml('camera_config.yaml')

    camera.open()

    #detector = MpHolisticDecorate(camera)
    #detector = MpPoseDecorate(camera)
    #detector = FaceDepthBox(MpPoseDecorate(camera))
    #detector = MpHandsDecorate(camera)
    #detector = MpFaceMeshDecorate(camera)


    # MP 3d sample
    detector = FaceDepthBox(MpPoseDecorate(camera))
    tracker = FaceDepthTracker(camera.depth_intrin, frame_width=camera.width, frame_height=camera.height,
    webcam_pos=webcam_pos, num_bodies_max=num_bodies_max, disappeared_max=disappeared_max)
    #detector = Mp3dData(MpPoseDecorate(camera))
    #detector = Mp3dHand(MpHandsDecorate(camera))
    #detector = Mp3dHolistic(MpHolisticDecorate(camera))

    width = 1920
    height = 1080

    image_out = np.zeros( ( height, width, 3 ), np.uint8 )

    while True:

        print("frame_count = {}".format(camera.frame_count))

        detector.detect()
        #image_out = detector.image_out

        text_x = 20
        text_y = 20
        text_y_shift = 20

        face_depth = detector.face_depth_boxes
        bodies = tracker.update(face_depth)
        num_bodies = len(bodies)

        image_out.fill( 255 )

        draw_grid( image_out, width, height )

        if(len(bodies) > 0):

            for body in bodies:
                body_id = body.id

                face_point_x = round(body.face_point[0], 3)
                face_point_y = round(body.face_point[1], 3) 
                face_point = (face_point_x, face_point_y)
                distance = body.distance

                msg = "[X:{},Y:{},Z:{:.3F}]".format( face_point_x,face_point_y, distance )

                x = int(round(face_point_x * width, 0))
                y = int(round(face_point_y * height, 0))
  
                InfoDrawer.draw_msg( image_out, msg, x-120, y+20 )

                cv.circle( image_out, ( x-5, y-5 ), 10, default_color[ index_red ], -1 )
                

        msg = "Camera type: {}".format(camera.camera_type)
        (text_x, text_y) = InfoDrawer.draw_msg(image_out, msg, text_x, text_y)

        msg = "fps: {}".format(camera.fps)
        (text_x, text_y) = InfoDrawer.draw_msg(image_out, msg, text_x, text_y)

        # show the frame and record if the user presses a key
        cv.imshow("Camera Test", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    camera.close()

    cv.destroyAllWindows()
