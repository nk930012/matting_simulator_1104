import numpy as np
import cv2 as cv

from .deep_lab_v3 import DeepLabV3
from .trimap_generator_v1 import TrimapGeneratorV1
#from .trimap_generator import TrimapGenerator
from ..deep_image_matting import DeepImageMatting

class SemanticMatting:

    def __init__(self, matting_model_path='BEST_checkpoint.tar', confidence_threshold=0.8):

        self.matting_model_path = matting_model_path
        self.confidence_threshold = confidence_threshold

        # Foreground image
        self.image = None

        # Mask detector
        self.mask_detector = DeepLabV3(confidence_threshold)
        self.mask = None

        # Trimap
        self.trimap_generator = TrimapGeneratorV1()
        #self.trimap_generator = TrimapGenerator()
        self.trimap = None

        # Matte
        self.matting_model = DeepImageMatting(matting_model_path)
        self.matte = None

    def predict(self, image):

        self.image = image

        # Mask
        predictions = self.mask_detector.detect(self.image)
        self.mask = self.mask_detector.get_mask(predictions)

        # Trimap
        self.trimap = self.trimap_generator.generate(self.mask)

        # Predicted matte
        self.matte = self.matting_model.predict(image, self.trimap)

        return self.matte

    def compose(self, background):
        # Compose a new image with the specific baground image.

        fg = self.image
        h, w, c = fg.shape

        matte_3c = cv.cvtColor(self.matte, cv.COLOR_GRAY2BGR)
        alpha = 1.0*matte_3c/255

        bg = cv.resize(background, (w, h))

        out = alpha*fg + (1.0-alpha)*bg
        out = np.array(out, dtype=np.uint8)

        return out

    def apply_mask(self):

        fg = self.image
        h, w, c = fg.shape

        mask_3c = cv.cvtColor(self.mask, cv.COLOR_GRAY2BGR)
        alpha = 1.0*mask_3c/255

        out = alpha*fg
        out = np.array(out, dtype=np.uint8)

        return out

    def apply_trimap(self):

        fg = self.image
        h, w, c = fg.shape

        trimap_3c = cv.cvtColor(self.trimap, cv.COLOR_GRAY2BGR)
        alpha = 1.0*trimap_3c/255

        out = alpha*fg
        out = np.array(out, dtype=np.uint8)

        return out
