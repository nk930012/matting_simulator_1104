import cv2 as cv
import os

class HaarFaceDetector:

    def __init__(self, width, height, ratio_target=0.5):

        self.width = width
        self.height = height
        self.ratio_target = ratio_target

        casc_path=os.path.dirname(cv.__file__)+"/data/haarcascade_frontalface_default.xml"
        self.cascade = cv.CascadeClassifier(casc_path)


    def detect(self, image, min_size=(30, 30)):

        width_target = int(self.width*self.ratio_target)
        height_target = int(self.height*self.ratio_target)

        img = cv.resize(image, (width_target, height_target))
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        detected = self.cascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=min_size,
            flags=cv.CASCADE_SCALE_IMAGE
        )

        bboxes = []
        for (x, y, w, h) in detected:

            x = int(x/self.ratio_target + 0.5)
            y = int(y/self.ratio_target + 0.5)
            w = int(w/self.ratio_target + 0.5)
            h = int(h/self.ratio_target + 0.5)

            bbox = [x, y, w, h]
            bboxes.append(bbox)

        return bboxes
