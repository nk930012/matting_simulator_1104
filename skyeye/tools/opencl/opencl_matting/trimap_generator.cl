__kernel void trimap_generator(
        __global unsigned char* p_src,
        __constant int* p_width,
        __constant int* p_height ,
        __constant int* p_size,
        __global unsigned char* p_dst)
{

    int width = *p_width;
    int height = *p_height;

    int c_x = get_global_id(0);
    int c_y = get_global_id(1);
    int c_index = c_y * width + c_x;

    bool zero_to_max = false;
    bool max_to_zero = false;

    int size = *p_size;

    int p_x = c_x - 1;
    int n_x = c_x + 1;
    int p_y = c_y - 1;
    int n_y = c_y + 1;

    unsigned char c_pixel = p_src[c_index];

    p_dst[c_index] = c_pixel;

    if (p_x >= 0){
        int p_index = c_y * width + p_x;
        unsigned char p_pixel = p_src[p_index];
        if (255 != p_pixel && 255 == c_pixel) {
            zero_to_max = true;
        }
    }

    if (n_x < width) {
        int n_index = c_y * width + n_x;
        unsigned char n_pixel = p_src[n_index];
        if (255 != n_pixel && 255 == c_pixel) {
            max_to_zero = true;
        }
    }

    if (zero_to_max || max_to_zero) {
        int i = 0;
        for (i = 0; i < size; ++i) {
            int w_x = c_x - i;
            unsigned char w_pixel = 127;
            if (w_x >= 0) {
                int w_index = c_y * width + w_x;
                p_dst[w_index] = w_pixel;
            }
            w_x = c_x + i;
            if (w_x < width) {
                int w_index = c_y * width + w_x;
                p_dst[w_index] = w_pixel;
            }
        }
    }

    zero_to_max = false;
    max_to_zero = false;

    if (p_y >= 0) {
        int p_index = p_y * width + c_x;
        unsigned char p_pixel = p_src[p_index];
        if (255 != p_pixel && 255 == c_pixel) {
            zero_to_max = true;
        }
    }
    if (n_y < height) {
        int n_index = n_y * width + c_x;
        unsigned char n_pixel = p_src[n_index];
        if (255 != n_pixel && 255 == c_pixel) {
            max_to_zero = true;
        }
    }

    if (zero_to_max || max_to_zero) {
        int i = 0;
        for (i = 0; i < size; ++i) {
            int w_y = c_y - i;
            unsigned char w_pixel = 127;
            if (w_y >= 0) {
                int w_index = w_y * width + c_x;
                p_dst[w_index] = w_pixel;
            }
            w_y = c_y + i;
            if (w_y < height) {
                int w_index = w_y * width + c_x;
                p_dst[w_index] = w_pixel;
            }
        }
    }
}