import numpy as np
import cv2 as cv


class ImageAverage:

    def __init__(self, num_frames=2):

        self.num_frames = num_frames  
        self.images = []

    def update(self, image):

        averaged = np.zeros(image.shape, dtype=np.float32)

        self.images.append(image)
        num_images = len(self.images)
        if num_images > self.num_frames:
            self.images.pop(0)
            num_images = self.num_frames

        for img in self.images:
            averaged += img

        averaged /= (1.0*num_images)
        averaged = np.array(averaged, dtype=np.uint8) 


        return averaged   

            


