import numpy as np


class HandPointer:

    def __init__(self):

        self.pos = None

    def project(self, hand):

        p0 = hand.WRIST
        p1 = hand.INDEX_FINGER_MCP
        p2 = hand.PINKY_MCP

        # Unit normal vector of the hand plane
        v1 = p1 - p0
        v2 = p2 - p0

        n_u = np.cross(v1, v2)
        n_u = n_u / np.linalg.norm(n_u)

        # Center point of hand
        center = 0.3*(p0+p1+p2)

        print('n_u: ', n_u)
        print('center: ', center)

        # Projected point on the projected plane
        # Assume the distance between projected plane and hand is d.
        d = 1.0

         





        return self.hands

    def get_palm_center(self, p0, p1, p2)