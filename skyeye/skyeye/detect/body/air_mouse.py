import numpy as np
from .get_distance import get_distance
from .vbox import Vbox


class AirMouse:

    def __init__(self, type_name='right'):

        self.type_name = type_name
        self.vbox = Vbox(type_name)
        self.shoulder = (0, 0)
        self.wrist = (0, 0)
        self.len_c = 100

        self.x = 0
        self.y = 0

    def update(self, shoulder, wrist, len_c):

        self.shoulder = shoulder
        self.wrist = wrist
        self.len_c = len_c

        self.vbox.update(shoulder, len_c)

        (self.x, self.y) = self.vbox.get_pos(wrist)

    def is_right(self):

        if self.x > 1:
            out = True
        else:
            out = False

        return out

    def is_left(self):

        if self.x < 0:
            out = True
        else:
            out = False

        return out

    def is_up(self):

        if self.y < 0:
            out = True
        else:
            out = False

        return out

    def is_down(self):

        if self.y > 1:
            out = True
        else:
            out = False

        return out

    def get_box(self):
        return self.vbox.box

    def get_pos(self):
        return (self.x, self.y)
