#include "init_program.h";


InitProgram::InitProgram() {}

InitProgram::~InitProgram() {}

void InitProgram::GetFirstImage(string inputs, string outputs, string videoName, int& width, int& height, cv::Mat& image, int& totalFrameCnt)
{

        cv::VideoCapture cap = cv::VideoCapture(inputs + videoName+".avi");

        bool success = true;

        cv::Mat frame;

        char imageName[128];// 這是buffer

        while (true)
        {
            success = cap.read(frame);

            if (!success)
            {
                
                break;
            }

            else if (totalFrameCnt == 0)// 第一張圖做trimap
            {
                //sprintf_s(imageName, "%s00000.png", outputs);
                sprintf_s(imageName, "%s00000.png", (outputs).c_str());
                cv::imwrite(imageName, frame);
                image = imread(outputs + "00000.png", cv::IMREAD_COLOR);
                width = image.cols;
                height = image.rows;
            }
            ++totalFrameCnt;
        }
        frame.release();
}

void InitProgram::CreateValuePath(int totalFrameCnt, std::vector<string>& value_path)
{
    char value[10000];
    for (int i = 0; i < totalFrameCnt; i++)
    {

        sprintf(value, "%05d", i);
        value_path.push_back(value);


    }
    //qDebug() << "matting圖片總數量為:" << value_path.capacity() - 3 << endl;
}









