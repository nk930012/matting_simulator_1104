import numpy as np
import dlib
import cv2 as cv
import imutils
from time import perf_counter
import yaml
from skyeye.detect.headpose import HeadposeTracker, WebcamPos, Annotator
from skyeye.utils.opencv import Webcam, wait_key
from skyeye.utils.opencv import TextDrawer
from skyeye.utils import Timer

def draw_msg(image, msg, x, y, y_shift=40, color=(255, 0, 0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 1.0, color, 2)
    y += y_shift

    return (x, y)

def draw_bbox(image, bbox):

    xa, ya, width, height = bbox
    xa = int(xa)
    ya = int(ya)
    xz = int(xa + width)
    yz = int(ya + height)
    cv.rectangle(image, (xa, ya), (xz, yz), (0, 255, 0), 2)

def draw_marker(image, id=None, point=None, width=None, height=None, color=(255, 0, 0)):

    u, v = point
    x = int(u*width + 0.5)
    y = int(v*height + 0.5)

    cv.circle(image, (x, y), 15, color, -1)

    if id is not None:
        msg = str(id)
        cv.putText(image, msg, (x, y), cv.FONT_HERSHEY_SIMPLEX, 1.0, (0, 0, 255), 2)


if __name__ == '__main__':

    input_file = "inputs.yaml"
    with open(input_file, 'r', encoding='utf-8') as f:
        inputs = yaml.load(f, Loader=yaml.Loader)

    # Set input parameters
    webcam_id = inputs['webcam_id']
    frame_width = inputs['frame_width']
    frame_height = inputs['frame_height']
    use_V4L2 = inputs['use_V4L2']
    theta_deg = inputs['theta_deg']
    len_x = inputs['len_x']
    len_y = inputs['len_y']
    dx = inputs['dx']
    dy = inputs['dy']
    num_bodies_max = inputs['num_bodies_max']
    disappeared_max = inputs['disappeared_max']
    (major, minor, _) = cv.__version__.split(".")
    if int(major) == 4:
        exposure = inputs['exposure_cv4']
    else:
        exposure = inputs['exposure_cv3']
    webcam_upsidedown = inputs['webcam_upsidedown']

    #frame_width = int(frame_width + 0.5)
    #frame_height = int(frame_height + 0.5) 

    use_fullscreen = True

    webcam = Webcam()
    if webcam.is_open():
        webcam.release()

    webcam.open(webcam_id, width=frame_width, height=frame_height, use_V4L2=use_V4L2, exposure=exposure)

    # Webcam position
    theta = theta_deg/180 * np.pi
    webcam_pos = WebcamPos(theta=theta, len_x=len_x, len_y=len_y, dx=dx, dy=dy)

    # Headpose detector
    tracker = HeadposeTracker(frame_width=frame_width, frame_height=frame_height,
        webcam_pos=webcam_pos, num_bodies_max= num_bodies_max, disappeared_max=disappeared_max)

    # Timer
    timer = Timer()

    if use_fullscreen:
        cv.namedWindow("win", cv.WND_PROP_FULLSCREEN)
        cv.setWindowProperty("win",cv.WND_PROP_FULLSCREEN, cv.WINDOW_FULLSCREEN)

    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        timer.tic('main_loop')

        frame = webcam.read()
        if frame is None:
            break
        if webcam_upsidedown:
            frame = cv.flip(frame, -1)

        image_out = frame.copy()
        bodies = tracker.update(frame)
        num_bodies = len(bodies)

        body_id_msg = ""
        gaze_u_msg = ""
        gaze_v_msg = ""
        distance_msg = ""

        for i, body in enumerate(bodies):

            # Draw gaze marker
            draw_marker(image_out, body.id, body.gaze_point, frame_width, frame_height, color=(255, 0, 0))
            draw_marker(image_out, body.id, body.face_point, frame_width, frame_height, color=(0, 255, 0))


            # Annotation
            annotator = Annotator(image_out, body.id, body.bbox, body.headpose, b=15.0)
            image_out = annotator.draw_all()
            body_id_msg += " {}".format(body.id)
            gaze_u_msg += " {}".format(round(body.gaze_point[0], 2))
            gaze_v_msg += " {}".format(round(body.gaze_point[1], 2))
            distance_msg += " {}".format(round(body.distance, 0))

        i = 0
        gaze_u = None
        gaze_v = None
        d = 0
        if num_bodies > 0:
            gaze_u = round(bodies[i].gaze_point[0], 1)
            gaze_v = round(bodies[i].gaze_point[1], 1)
            d = round(bodies[0].distance, 1)

        # Frame rate
        time_duration = timer.toc('main_loop')
        fps = int(1.0/time_duration)

        # Text drawer
        text_drawer = TextDrawer(image_out, x0=20, y0=40, y_shift=40)
        msg = "fps: {}".format(fps)
        text_drawer.draw(msg)

        msg = "id:[{}]".format(body_id_msg)
        text_drawer.draw(msg)

        msg = "u:[{}]".format(gaze_u_msg)
        text_drawer.draw(msg)

        msg = "v:[{}]".format(gaze_v_msg)
        text_drawer.draw(msg)

        msg = "d:[{}]".format(distance_msg)
        text_drawer.draw(msg)

        # show the frame and record if the user presses a key
        cv.imshow("win", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
