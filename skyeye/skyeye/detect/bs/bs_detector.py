import cv2 as cv
import imutils
from .body import Body

class BsDetector:
    '''
    BS(Background Subtraction) detector.
    '''

    def __init__(self, frame_width, frame_height, bs_name="MOG2", history=50):

        self.bs_name = bs_name
        if bs_name == "MOG2":
            self.subtractor = cv.createBackgroundSubtractorMOG2(history=history, detectShadows=False)
        else:
            self.subtractor = cv.createBackgroundSubtractorKNN(history=history, detectShadows=False)

        self.frame_width = frame_width
        self.frame_height = frame_height
        self.fg = None

    def detect(self, frame):

        frame_area = self.frame_width*self.frame_height
        area_min = frame_area*0.05
        area_max = frame_area*0.9

        # resize the frame, convert it to grayscale, and blur it
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        gray = cv.GaussianBlur(gray, (5, 5), 0)
        frame = gray

        self.fg = self.subtractor.apply(frame)
        thresh = cv.threshold(self.fg, 25, 255, cv.THRESH_BINARY)[1]
        thresh = cv.dilate(thresh, None, iterations=10)
        thresh = cv.erode(thresh, None, iterations=10)
        thresh = cv.dilate(thresh, None, iterations=10)

        cnts = cv.findContours(thresh.copy(), cv.RETR_EXTERNAL,
            cv.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)

        bboxes = []
        for c in cnts:
            area = cv.contourArea(c)
            if area > area_min and area < area_max:
                bbox = cv.boundingRect(c)
                bboxes.append(bbox)

        return bboxes
