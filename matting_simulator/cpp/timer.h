#include "opencv2/opencv.hpp"

using namespace cv;

class Timer 
{
public:
    void tic()
    {
        count_begin = getTickCount();
    };
    void toc()
    {
        count_end = getTickCount();
        count_delta = count_end - count_begin;
        fps = (float)getTickFrequency()/count_delta;
        dt = (float)(1.0/fps);
    };

    double get_fps() { return fps;};
    double get_dt() { return dt; };

private:
    int64 count_begin = 0;
    int64 count_end = 0;
    int64 count_delta = 0;

    float fps = 0.0;
    float dt = 0.0;

};
