﻿#include "simple_opencl_builder.h"

#include <opencv2/opencv.hpp>

#include <opencv2/core/ocl.hpp>

#include <opencv2/core/mat.hpp>

#include <opencv2/imgproc.hpp>

cv::Mat readImageFile(std::string fileName)
{

    cv::Mat bgr = cv::imread(fileName, cv::IMREAD_COLOR);

    cv::Mat rgba;

    cvtColor(bgr, rgba, cv::COLOR_BGR2RGBA);

    return rgba;

}

#include "opencl_matting_process.h"

int main(int argc, char** argv)
{
    OpenclMattingProcess mattingProcess;

    cv::Mat src = readImageFile("input.png");

    int width = src.cols;
    int height = src.rows;

    cv::Mat output;

    unsigned char hueMin[5] = { 40, 40, 40, 40, 40 };
    unsigned char hueMax[5] = { 80, 80, 80, 80, 80 };

    unsigned char satMin[5] = {55,55,55,55,55};
    unsigned char satMax[5] = {255,255,255,255,255};

    unsigned char valMin[5] = {85,85,85,85,85};
    unsigned char valMax[5] = {255,255,255,255,255};

    unsigned char* hueBound[2];
    hueBound[0] = hueMin;
    hueBound[1] = hueMax;

    unsigned char* satBound[2];
    satBound[0] = satMin;
    satBound[1] = satMax;

    unsigned char* valBound[2];
    valBound[0] = valMin;
    valBound[1] = valMax;

    char* outputBuffer = NULL;
    int count = 1000;
    int dropperSize = sizeof(hueMin) / sizeof(unsigned char);
    outputBuffer = mattingProcess.process(src, hueBound, satBound, valBound, dropperSize);
    free(outputBuffer);
    outputBuffer = NULL;


    chrono::steady_clock::time_point begin = chrono::steady_clock::now();
    for (int i = 0; i < count; ++i) {

        outputBuffer = mattingProcess.process(src, hueBound, satBound, valBound, 5);

        if (i != count - 1) {
            free(outputBuffer);
            outputBuffer = NULL;
        }

    }
    chrono::steady_clock::time_point end = chrono::steady_clock::now();

    std::cout << "Time("<<count<<") : " << chrono::duration_cast<chrono::microseconds>(end - begin).count() / count << "[us]" << std::endl;

    output = cv::Mat(height, width, CV_8UC1, outputBuffer).clone();

    free(outputBuffer);

    imshow("output", output);

    imwrite("output.png", output);


    cv::waitKey(0);

    system("pause");

    return 0;
}