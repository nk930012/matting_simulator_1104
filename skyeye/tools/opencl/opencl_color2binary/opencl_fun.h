#ifndef OPENCL_FUN_H
#define OPENCL_FUN_H

#include <iostream>
#ifdef __APPLE__
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.hpp>
#endif

//extern const char* trimap_generator_source;

cl_device_id create_device();

cl_program build_program(cl_context ctx, cl_device_id dev, const char* kernel);



#endif
