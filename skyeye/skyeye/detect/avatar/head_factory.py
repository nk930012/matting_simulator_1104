import numpy as np
from .head_model import HeadModel
from skyeye.utils.mediapipe import MpConvert
from skyeye.utils.camera import Camera

class HeadFactory:

    def __init__(self):

        self.heads = []

    def generate(self, results):

        self.heads = []
        if results.multi_face_landmarks is None:

            self.heads = []

        else:

            for landmarks in results.multi_face_landmarks:

                head = HeadModel()
                head.read_data(landmarks)  

                self.heads.append(head)

        return self.heads

    def generate_with_camera(self, results, camera):
        pass