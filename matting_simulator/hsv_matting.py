import numpy as np
import cv2 as cv

def estimate_hsv_bounds(pixels):

    h_min = 255
    h_max = 0
    s_min = 255
    s_max = 0
    v_min = 255
    v_max = 0

    for pixel in pixels:

        h, s, v = pixel

        if h < h_min:
            h_min = h

        if h > h_max:
            h_max = h

        if s < s_min:
            s_min = s

        if s > s_max:
            s_max = s

        if v < v_min:
            v_min = v

        if v > v_max:
            v_max = v

    lower_bound = np.array([h_min, s_min, v_min])
    upper_bound = np.array([h_max, s_max, v_max])

    return [lower_bound, upper_bound]


class HsvMatting: 

    def __init__(self):

        self.mask = None
        self.trimap = None

        self.pixels = []

        # Lower and upper bounds
        self.lower_bound_green = np.array([30, 43, 46])
        #self.lower_bound_green = np.array([35, 43, 46]) # Typical green
        self.upper_bound_green = np.array([77, 255, 255])

        self.lower_bound = self.lower_bound_green 
        self.upper_bound = self.upper_bound_green 

    def set_pixels(self, pixels):

        self.pixels= pixels

        if len(pixels) > 0:
            lower_bound, upper_bound = estimate_hsv_bounds(pixels)
            self.set_color_bounds(lower_bound, upper_bound)

    def use_green_bounds(self):

        self.lower_bound = self.lower_bound_green 
        self.upper_bound = self.upper_bound_green 

    def set_color_bounds(self, lower_bound=None, upper_bound=None):

        self.lower_bound = lower_bound
        self.upper_bound = upper_bound

    def estimate_mask(self, image):

        hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)

        lower_bound = np.array(self.lower_bound)
        upper_bound = np.array(self.upper_bound)

        print("hsv lower_bound: ", lower_bound)
        print("hsv upper_bound: ", upper_bound)

        mask = cv.inRange(hsv, lower_bound, upper_bound) 
        mask = cv.bitwise_not(mask)

        return mask

    def estimate_trimap(self, mask, blur_size=9):

        trimap = np.array(mask, dtype=np.float32)
        trimap = cv.blur(trimap, (blur_size, blur_size))

        trimap  = np.where((trimap < 255) & (trimap > 0), 127, trimap) # Unknown region
        trimap = np.array(trimap, dtype=np.uint8)

        return trimap 

    def estimate_alpha(self, image):
        '''
        Genetate the alpha channel.

        image: nd array
            Image with BGR channels.
        '''

        self.mask = self.estimate_mask(image)
        self.trimap = self.estimate_trimap(self.mask)
        alpha = self.trimap / 255.0

        return alpha

    def estimate_alpha_from_trimap(self, trimap):
        '''
        Genetate the alpha channel.

        image: nd array
            Image with BGR channels.
        '''

        alpha = trimap / 255.0

        return alpha
