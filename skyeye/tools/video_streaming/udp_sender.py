from __future__ import division
import cv2 as cv
import numpy as np
import socket
import struct
import math

from time import perf_counter
from skyeye.utils.opencv import Webcam, wait_key
from skyeye.utils.opencv import draw_msg

class FrameSegment(object):
    """ 
    Object to break down image frame segment
    if the size of image exceed maximum datagram size 
    """
    #MAX_DGRAM = 2**16
    MAX_DGRAM = 8192
    MAX_IMAGE_DGRAM = MAX_DGRAM - 64 # extract 64 bytes in case UDP frame overflown
    def __init__(self, sock, port, addr="127.0.0.1"):
        self.s = sock
        self.port = port
        self.addr = addr

    def udp_frame(self, img):
        """ 
        Compress image and Break down
        into data segments 
        """
        compress_img = cv.imencode('.jpg', img)[1]
        #dat = compress_img.tobytes()
        dat = compress_img.tostring()
        size = len(dat)
        count = math.ceil(size/(self.MAX_IMAGE_DGRAM))
        array_pos_start = 0
        #print(compress_img)
        #print(dat)
        #print(count)
        #print(struct.pack("B", count))
        #print(array_pos_start)
        #print(self.MAX_IMAGE_DGRAM)
        #print("size:"+str(size))
        #print(len(compress_img))
        while count:
            array_pos_end = min(size, array_pos_start + self.MAX_IMAGE_DGRAM)
            self.s.sendto(struct.pack("B", count) +
                dat[array_pos_start:array_pos_end], 
                (self.addr, self.port)
                )
            print(struct.pack("B", count) + dat[array_pos_start:array_pos_end])
            #print('\n\n')
            array_pos_start = array_pos_end
            count -= 1


def main():
    """ Top level main function """
    # Set up UDP socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    port = 8888

    fs = FrameSegment(s, port)

    frame_width = 640
    frame_height = 480
    use_V4L2 = True
    autofocus=False
    auto_exposure=False

    webcam = Webcam()
    if webcam.is_open():
        webcam.release()

    device = 0 # Device ID
    webcam.open(device, width=frame_width, height=frame_height,
        use_V4L2=use_V4L2, autofocus=autofocus, auto_exposure=auto_exposure)

    while True:

        time_start = perf_counter()

        frame = webcam.read()
        if frame is None:
            break

        fs.udp_frame(frame)
        break

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "fps: {}".format(fps)
        (text_x, text_y) = draw_msg(frame, msg, text_x, text_y)

        # show the frame and record if the user presses a key
        cv.imshow("Sender", frame)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
    s.close()

if __name__ == "__main__":
    main()