import socket
import uuid

class NetworkInfo():

    def __init__(self):
        self.host_name = socket.gethostname()
        self.ip = socket.gethostbyname_ex(self.host_name)
        self.mac=uuid.UUID(int = uuid.getnode()).hex[-12:]
        self.mac = self.mac.upper()
 
    def get_host_name(self):
        return self.host_name
    
    def get_ip(self):
        return self.ip
    
    def get_mac_address(self):
        return self.mac

    def get_serial_num(self):
        serial_num = str(self.mac)[0:6]
        return serial_num
