import numpy as np
import imutils
import cv2 as cv
import os
from time import perf_counter
from skyeye.detect.face import HaarFaceDetector
from skyeye.utils.opencv import Webcam, wait_key


def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

def draw_bbox(image, bbox):

    xa, ya, width, height = bbox
    xa = int(xa)
    ya = int(ya)
    xz = int(xa + width)
    yz = int(ya + height)
    cv.rectangle(image, (xa, ya), (xz, yz), (0, 255, 0), 2)

if __name__ == '__main__':


    frame_width = 640
    frame_height = 480
    use_V4L2 = True
    ratio_target = 0.5

    webcam = Webcam()
    if webcam.is_open():
        webcam.release()

    device = 0 # Device ID
    webcam.open(device, width=frame_width, height=frame_height, use_V4L2=use_V4L2)

    # Face detector
    detector = HaarFaceDetector(frame_width, frame_height, ratio_target=ratio_target)

    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        time_start = perf_counter()

        frame = webcam.read()
        if frame is None:
            break

        bboxes = detector.detect(frame)

        image_out = frame


        for bbox in bboxes:
            draw_bbox(image_out, bbox)

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)

        text_x = 20
        text_y = 20
        text_y_shift = 20

        msg = "fps: {}".format(fps)
        (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

        # show the frame and record if the user presses a key
        cv.imshow("win", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
