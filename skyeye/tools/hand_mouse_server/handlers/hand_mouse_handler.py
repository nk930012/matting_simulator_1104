# -*- coding: utf-8 -*-
import sys
import os
import argparse
import logging
import tornado.ioloop
import numpy as np
import time
import json 
import imutils
import cv2 as cv
from skyeye.detect.decorator import Mp3dHand
from skyeye.detect.decorator.mp_hands_decorate import MpHandsDecorate
from skyeye.detect.hand import HandFactory, GestureDetector, MouseSimulator
from skyeye.utils.camera import CameraBuilder


class HandMouseHandler():

    def __init__(self, WsHandler, debug=logging.ERROR):

        self.ws = WsHandler
        self.scheduler = None

        self.active = 0

        self.snap_count = 0
        

    def hand_scheduler(self):


        if self.active == 1:
            data = []
            
            self.detector.detect()
            hands = self.detector.mp_3d_hands

            gesture = None
            numeric_gesture = None
            mouse_box = None
            mouse_pos = None
            click_type = None
            depth = 0
            snap = False
            for hand in hands:
                if hand.get_handedness() == 'Right':
                    gesture = self.gesture_detector.detect(hand)
                    numeric_gesture = self.gesture_detector.detect_numeric(hand)
                    mouse_pos, click_type = self.mouse.detect(hand)
                    mouse_box = self.mouse.get_box()
                    depth = self.mouse.get_depth()
                    snap = self.gesture_detector.detect_snap(hand)

            #Snap
            snap_count_max = 100

            if snap:
                self.snap_count = snap_count_max
            else:
                self.snap_count -= 1

            send_snap = False
            if self.snap_count > 0:
                send_snap = True
            else:
                send_snap = False
            
            # Draw the mouse pointer
            #if not(mouse_pos is None):
                
            wsData = {'size': (self.frame_width, self.frame_height),'gesture':gesture, 'numeric_gesture':numeric_gesture, 'mouse_pos': mouse_pos, 'mouse_button': click_type,'mouse_depth':str(depth),'snap':send_snap}
                #wsData = {'size': (self.frame_width, self.frame_height),'gesture':gesture, 'numeric_gesture':numeric_gesture, 'mouse_pos': mouse_pos, 'mouse_button': click_type,'mouse_depth':str(depth)}
                
            self.ws.send_updates(wsData)


            

    def open(self, id = 0):
        
        if self.active == 0:
            self.camera = CameraBuilder.buildWithYaml('camera_config.yaml')
            self.camera.open()
            self.detector = Mp3dHand(MpHandsDecorate(self.camera,flip=1, max_num_hands=1))

            self.frame_width = self.camera.width
            self.frame_height = self.camera.height

            self.gesture_detector = GestureDetector()
            self.mouse = MouseSimulator()
            self.active = 1
            self.scheduler = tornado.ioloop.PeriodicCallback(self.hand_scheduler, 20)
            self.scheduler.start()
        ret = True
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)
        
    def close(self, id):
        
        self.active = 0
        if self.scheduler is not None:
            self.scheduler.stop()
            self.scheduler = None
            self.camera.close()
        status_str = "successful"
        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)
        
    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close cam device.')
                self.close(id=1)
            elif params["command"] == "open":
                if params["property"] == "hand_device":
                    self.open(id=id)
            elif params["command"] == "close":
                if params["property"] == "hand_device":
                    self.close(id=id)
        elif method == "get":
            pass
        elif method == "set":
            pass
