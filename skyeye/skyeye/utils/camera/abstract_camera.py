from .camera_constants import *
import abc


class AbstractCamera(metaclass=abc.ABCMeta):
    def __init__(self):
        self._camera_type = None
        self._width = 640
        self._height = 480
        self._depth_width = 640
        self._depth_height = 480
        self._fps = 30
        self._depth_fps = 30
        self._depth_intrin = 1
        self._depth_scale = 1

    def init(self, params):
        self._camera_type = params[CAMERA_TYPE]
        self._width = params[FRAME_WIDTH]
        self._height = params[FRAME_HEIGHT]
        self._depth_width = params[DEPTH_WIDTH]
        self._depth_height = params[DEPTH_HEIGHT]
        self._fps = params[FPS]
        self._depth_fps = params[DEPTH_FPS]
        self._depth_intrin = 1
        self._depth_scale = 1

    @abc.abstractmethod
    def open(self):
        return NotImplemented

    @abc.abstractmethod
    def close(self):
        return NotImplemented

    @abc.abstractmethod
    def process(self):
        return NotImplemented

    @abc.abstractmethod
    def getColorImage(self):
        return NotImplemented

    @abc.abstractmethod
    def getDepthInfo(self):
        return NotImplemented

    @abc.abstractmethod
    def getDepthValue(self, x, y):
        return NotImplemented

    @property
    def depth_scale(self):
        return self._depth_scale

    @property
    def depth_intrin(self):
        return self._depth_intrin

    @property
    def camera_type(self):
        return self._camera_type

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height