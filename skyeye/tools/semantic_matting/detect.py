
import numpy as np
import cv2 as cv
import yaml
from skyeye.utils import Timer
from skyeye.matting.semantic_matting import SemanticMatting

from skyeye.utils.opencv import Webcam, wait_key
from skyeye.utils.opencv import TextDrawer
from skyeye.utils import Timer



if __name__ == '__main__':

    input_file = "inputs.yaml"
    with open(input_file, 'r', encoding='utf-8') as f:
        inputs = yaml.load(f, Loader=yaml.Loader)

    # Set input parameters
    use_webcam = inputs['use_webcam']
    webcam_id = inputs['webcam_id']
    video_path = inputs['video_path']
    frame_width = inputs['frame_width']
    frame_height = inputs['frame_height']
    use_V4L2 = inputs['use_V4L2']
    matting_model_path = inputs['matting_model_path']
    confidence_threshold=inputs['confidence_threshold']

    
    if use_webcam:
        cap = Webcam()
        if cap.is_open():
            cap.release()

        cap.open(webcam_id, width=frame_width, height=frame_height, auto_exposure=True, use_V4L2=use_V4L2)

    else:    
        #video_path = 'video/insist_perseverance.mp4'
        cap = cv.VideoCapture(video_path)

    # Matting
    matting = SemanticMatting(matting_model_path=matting_model_path,
        confidence_threshold=confidence_threshold)

    # Background image
    bg_path = 'background.png'
    background = cv.imread(bg_path)
    background = cv.resize(background, (frame_width, frame_height))

    # Timer
    timer = Timer()

    frame_count = 0
    while True:

        frame_count += 1
        print("frame_count = {}".format(frame_count))
        timer.tic('main_loop')

        if use_webcam:
            frame = cap.read()
        else:
            success, frame = cap.read()
            if not success:
                print("Failed to open the video.")
                break

        if frame is None:
            break

        frame = cv.resize(frame, (frame_width, frame_height))
        image_out = frame.copy()

        # Predict
        matte = matting.predict(frame)
        with_mask = matting.apply_mask()
        with_trimap = matting.apply_trimap()
        mask = matting.mask
        trimap = matting.trimap
        composed = matting.compose(background)

        # Frame rate
        time_duration = timer.toc('main_loop')
        fps = int(1.0/time_duration)
        print('fps: ', fps)

        # Text drawer
        text_drawer = TextDrawer(image_out, x0=20, y0=40, y_shift=40)
        msg = "fps: {}".format(fps)
        text_drawer.draw(msg)


        # show the frame and record if the user presses a key
        matte_3c = cv.cvtColor(matte, cv.COLOR_GRAY2BGR)
        mask_3c = cv.cvtColor(mask, cv.COLOR_GRAY2BGR)
        #trimap_3c = cv.cvtColor(trimap, cv.COLOR_GRAY2BGR)

        out0 = np.concatenate((image_out, mask_3c, with_mask), axis=1)
        out1 = np.concatenate((with_trimap, matte_3c, composed), axis=1)
        image_out = np.concatenate((out0, out1), axis=0)

        #image_out = np.concatenate((image_out, trimap_3c, matte_3c, composed), axis=1)
        cv.imshow("win", image_out)

        # Exit while 'q' or 'Esc' is pressed
        key = wait_key(1)
        if key == ord("q") or key == 27: break


    # cleanup the camera and close any open windows
    cap.release()

    cv.destroyAllWindows()
