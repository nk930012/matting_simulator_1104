import cv2 as cv
import dlib
import numpy as np
import os
import os.path as osp

from .headpose import Headpose

class HeadposeDetector():

    # 3D facial model coordinates
    landmarks_3d_list = [
        np.array([
            [ 0.000,  0.000,   0.000],    # Nose tip
            [ 0.000, -8.250,  -1.625],    # Chin
            [-5.625,  4.250,  -3.375],    # Left eye left corner
            [ 5.625,  4.250,  -3.375],    # Right eye right corner
            [-3.750, -3.750,  -3.125],    # Left Mouth corner
            [ 3.750, -3.750,  -3.125]     # Right mouth corner
        ], dtype=np.double),
        np.array([
            [ 0.000000,  0.000000,  6.763430],   # 52 nose bottom edge
            [ 6.825897,  6.760612,  4.402142],   # 33 left brow left corner
            [ 1.330353,  7.122144,  6.903745],   # 29 left brow right corner
            [-1.330353,  7.122144,  6.903745],   # 34 right brow left corner
            [-6.825897,  6.760612,  4.402142],   # 38 right brow right corner
            [ 5.311432,  5.485328,  3.987654],   # 13 left eye left corner
            [ 1.789930,  5.393625,  4.413414],   # 17 left eye right corner
            [-1.789930,  5.393625,  4.413414],   # 25 right eye left corner
            [-5.311432,  5.485328,  3.987654],   # 21 right eye right corner
            [ 2.005628,  1.409845,  6.165652],   # 55 nose left corner
            [-2.005628,  1.409845,  6.165652],   # 49 nose right corner
            [ 2.774015, -2.080775,  5.048531],   # 43 mouth left corner
            [-2.774015, -2.080775,  5.048531],   # 39 mouth right corner
            [ 0.000000, -3.116408,  6.097667],   # 45 mouth central bottom corner
            [ 0.000000, -7.415691,  4.070434]    # 6 chin corner
        ], dtype=np.double),
        np.array([
            [ 0.000000,  0.000000,  6.763430],   # 52 nose bottom edge
            [ 5.311432,  5.485328,  3.987654],   # 13 left eye left corner
            [ 1.789930,  5.393625,  4.413414],   # 17 left eye right corner
            [-1.789930,  5.393625,  4.413414],   # 25 right eye left corner
            [-5.311432,  5.485328,  3.987654]    # 21 right eye right corner
        ], dtype=np.double)
    ]

    lm_2d_index_list = [
        [30, 8, 36, 45, 48, 54],
        [33, 17, 21, 22, 26, 36, 39, 42, 45, 31, 35, 48, 54, 57, 8], # 14 points
        [33, 36, 39, 42, 45] # 5 points
    ]

    def __init__(self, model_file=None, lm_type=1, use_dlib=True, verbose=True):


        self.use_dlib = use_dlib

        if self.use_dlib:
            if model_file is None:
                model_file =  "shape_predictor_68_face_landmarks.dat"

        else:

            from .mtcnn import TrtMtcnn
            self.mtcnn = TrtMtcnn() # init mtcnn

            if model_file is None:
                model_file =  "model/shape_predictor_68_face_landmarks.dat"

        self.face_detector = dlib.get_frontal_face_detector()
        self.landmark_predictor = dlib.shape_predictor(model_file)

        self.lm_2d_index = self.lm_2d_index_list[lm_type]
        self.landmarks_3d = self.landmarks_3d_list[lm_type]


        self.verbose = verbose


    def to_numpy(self, landmarks):
        coords = []
        for i in self.lm_2d_index:
            coords += [[landmarks.part(i).x, landmarks.part(i).y]]
        return np.array(coords).astype(np.float64)

    def get_landmarks(self, im):
        # Detect bounding boxes of faces

        landmarks_2d = []
        bboxes = []

        if self.use_dlib:

            # using dlib to get rects and calculte landmark        

            rects = self.face_detector(im, 0) if im is not None else []

            for rect in rects:
    
                # Detect landmarks
                lm_2d = self.landmark_predictor(im, rect)
                lm_2d = self.to_numpy(lm_2d)
                #lm_2d = lm_2d.astype(np.double)
                landmarks_2d.append(lm_2d)
    
                # Bounding box
                x = rect.left()
                y = rect.top()
                w = rect.right() - x
                h = rect.bottom() - y
                bbox = [x, y, w, h]
                bboxes.append(bbox)

        else:

            #rects = self.face_detector(im, 0) if im is not None else []
            rects, landmarks = self.mtcnn.detect(im, minsize=40) if im is not None else []

            # using mtcnn to get rects, dlib to calculte landmark
            for rect in rects:
                left = rect[0]
                top = rect[1]
                right = rect[2]
                bottom = rect[3]
                #convert nparry to dlib rect
                d_rect = dlib.rectangle(left, top, right, bottom) 
    
                # Detect landmarks
                lm_2d = self.landmark_predictor(im, d_rect)
                lm_2d = self.to_numpy(lm_2d)
                #lm_2d = lm_2d.astype(np.double)
                landmarks_2d.append(lm_2d)
    
                # Bounding box
                x = d_rect.left()
                y = d_rect.top()
                w = d_rect.right() - x
                h = d_rect.bottom() - y
                bbox = [x, y, w, h]
                bboxes.append(bbox)

        return landmarks_2d, bboxes

    def get_headpose(self, im, landmarks_2d, verbose=False):

        #h, w = im.shape
        h, w, c = im.shape
        f = w # column size = x axis length (focal length)
        u0, v0 = w / 2, h / 2 # center of image plane
        camera_matrix = np.array(
            [[f, 0, u0],
             [0, f, v0],
             [0, 0, 1]], dtype = np.double
         )

        # Assuming no lens distortion
        dist_coeffs = np.zeros((4,1))

        # Find rotation, translation
        (success, rotation_vector, translation_vector) = cv.solvePnP(self.landmarks_3d, landmarks_2d, camera_matrix, dist_coeffs)

        return rotation_vector, translation_vector, camera_matrix, dist_coeffs


    # rotation vector to euler angles
    def get_angles(self, rvec, tvec):
        rmat = cv.Rodrigues(rvec)[0]
        P = np.hstack((rmat, tvec)) # projection matrix [R | t]
        degrees = -cv.decomposeProjectionMatrix(P)[6]
        rx, ry, rz = degrees[:, 0]
        return [rx, ry, rz]

    # moving average history
    history = {'lm': [], 'bbox': [], 'rvec': [], 'tvec': [], 'cm': [], 'dc': []}

    def add_history(self, values):
        for (key, value) in zip(self.history, values):
            self.history[key] += [value]

    def pop_history(self):
        for key in self.history:
            self.history[key].pop(0)

    def get_history_len(self):
        return len(self.history['lm'])

    def get_ma(self):
        res = []
        for key in self.history:
            res += [np.mean(self.history[key], axis=0)]
        return res

    def detect(self, im, ma=3):
        # landmark Detection
        # mtcnn is using BGR image
        #gray = cv.cvtColor(im, cv.COLOR_BGR2GRAY)
        #landmarks_2d, bboxes = self.get_landmarks(gray)
        landmarks_2d, bboxes = self.get_landmarks(im)

        # if no face deteced, return original image
        if landmarks_2d is None:
            return None

        num_bodies = len(landmarks_2d)

        poses = []
        for i in range(num_bodies):

            # Headpose Detection
            rvec, tvec, cm, dc = self.get_headpose(im, landmarks_2d[i])
            angles = self.get_angles(rvec, tvec)

            # Save
            pose = Headpose(landmarks_2d[i], rvec, tvec, cm, dc, angles)
            poses.append(pose)

        return bboxes, poses
