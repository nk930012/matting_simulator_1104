#ifndef REMOTEDETECTOR_H
#define REMOTEDETECTOR_H

#include <QObject>
#include <QtNetwork>
#include "DetectorClient.h"

class RemoteDetector : public QObject
{
    Q_OBJECT

public:
    explicit RemoteDetector(QObject *parent = nullptr);
    ~RemoteDetector();

    static RemoteDetector& instance()
    {
        static RemoteDetector ins;
        return ins;
    }

    void connect(QString webServiceAddr, QString udpIP,int udpPort);
    void close();

    QString poseDetect(uchar* image,int length);

    void sendMessage(char* message);

    void sendImage(std::vector<uchar> imgBuf);

    //void processEvent();

signals:

public Q_SLOTS:
    void onConnected();
    void onTextMessageReceived(QString message);

private:
    QString m_webServiceAddress;
    QString m_udpIP;
    int m_udpPort;

    QUdpSocket *m_udpSender;
    QHostAddress m_udpAddress;

    DetectorClient m_client;
    QString m_receiveMsg;
    //QCoreApplication* app;
};

#define SRemoteDetector RemoteDetector::instance()

#endif // REMOTEDETECTOR_H
