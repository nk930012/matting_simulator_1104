import json
import trt_pose.coco
import trt_pose.models
import torch
import torch2trt
from torch2trt import TRTModule
import time, sys
import cv2
import torchvision.transforms as transforms
import PIL.Image
from trt_pose.draw_objects import DrawObjects
from trt_pose.parse_objects import ParseObjects
import argparse
import os.path
from touch.detect.body.nv_wrist_gesture import NvWristGesture
from touch.detect.body.nv_body_model import NvBodyModel
from touch.trackers.nv_body_tracker import NvBodyTracker
import numpy as np


body_tracker = NvBodyTracker()
body_model = NvBodyModel()

wrist_gesture = NvWristGesture(num_points_max=2)

# Frame size
frame_width = 1280
frame_height = 720


def gstreamer_pipeline(
    capture_width=1280,
    capture_height=720,
    display_width=1280,
    display_height=720,
    framerate=60,
    flip_method=0,
):
    return (
        "nvarguscamerasrc ! "
        "video/x-raw(memory:NVMM), "
        "width=(int)%d, height=(int)%d, "
        "format=(string)NV12, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink"
        % (
            capture_width,
            capture_height,
            framerate,
            flip_method,
            display_width,
            display_height,
        )
    )

'''
hnum: 0 based human index
kpoint : keypoints (float type range : 0.0 ~ 1.0 ==> later multiply by image width, height
'''
def get_keypoint(humans, hnum, peaks):
    #check invalid human index
    kpoint = []
    human = humans[0][hnum]
    C = human.shape[0]
    for j in range(C):
        k = int(human[j])
        if k >= 0:
            peak = peaks[0][j][k]   # peak[1]:width, peak[0]:height
            peak = (j, float(peak[0]), float(peak[1]))
            kpoint.append(peak)
            #print('index:%d : success [%5.3f, %5.3f]'%(j, peak[1], peak[2]) )
        else:    
            peak = (j, None, None)
            kpoint.append(peak)
            #print('index:%d : None %d'%(j, k) )
    return kpoint


parser = argparse.ArgumentParser(description='TensorRT pose estimation run')
parser.add_argument('--model', type=str, default='resnet', help = 'resnet or densenet' )
args = parser.parse_args()

with open('human_pose.json', 'r') as f:
    human_pose = json.load(f)

topology = trt_pose.coco.coco_category_to_topology(human_pose)

num_parts = len(human_pose['keypoints'])
num_links = len(human_pose['skeleton'])



# Parameter settings
if 'resnet' in args.model:
    print('------ model = resnet--------')
    MODEL_WEIGHTS = 'resnet18_baseline_att_224x224_A_epoch_249.pth'
    OPTIMIZED_MODEL = 'resnet18_baseline_att_224x224_A_epoch_249_trt.pth'
    model = trt_pose.models.resnet18_baseline_att(num_parts, 2 * num_links).cuda().eval()
    WIDTH = 224
    HEIGHT = 224

else:    
    print('------ model = densenet--------')
    MODEL_WEIGHTS = 'densenet121_baseline_att_256x256_B_epoch_160.pth'
    OPTIMIZED_MODEL = 'densenet121_baseline_att_256x256_B_epoch_160_trt.pth'
    model = trt_pose.models.densenet121_baseline_att(num_parts, 2 * num_links).cuda().eval()
    WIDTH = 256
    HEIGHT = 256


# CSI camera
pipeline = gstreamer_pipeline(capture_width=frame_width, capture_height=frame_height, display_width=frame_width, display_height=frame_height, flip_method=0)

cap = cv2.VideoCapture(pipeline, cv2.CAP_GSTREAMER)

#fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
#out_video = cv2.VideoWriter('/tmp/output.mp4', fourcc, cap.get(cv2.CAP_PROP_FPS), (640, 480))
count = 0

X_compress = 1.0*frame_width / WIDTH
Y_compress = 1.0*frame_height / HEIGHT

if cap is None:
    print("Camera Open Error")
    sys.exit(0)



# Model initialization
data = torch.zeros((1, 3, HEIGHT, WIDTH)).cuda()
if os.path.exists(OPTIMIZED_MODEL) == False:
    model.load_state_dict(torch.load(MODEL_WEIGHTS))
    model_trt = torch2trt.torch2trt(model, [data], fp16_mode=True, max_workspace_size=1<<25)
    torch.save(model_trt.state_dict(), OPTIMIZED_MODEL)

model_trt = TRTModule()
model_trt.load_state_dict(torch.load(OPTIMIZED_MODEL))

t0 = time.time()
torch.cuda.current_stream().synchronize()
for i in range(50):
    y = model_trt(data)
torch.cuda.current_stream().synchronize()
t1 = time.time()
fps = 50.0 / (t1 - t0)
print("Predict fps: ", fps)

mean = torch.Tensor([0.485, 0.456, 0.406]).cuda()
std = torch.Tensor([0.229, 0.224, 0.225]).cuda()
device = torch.device('cuda')


def preprocess(image):
    global device
    device = torch.device('cuda')
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = PIL.Image.fromarray(image)
    image = transforms.functional.to_tensor(image).to(device)
    image.sub_(mean[:, None, None]).div_(std[:, None, None])
    return image[None, ...]

def execute(img, src, t):
    color = (0, 255, 0)
    data = preprocess(img)
    cmap, paf = model_trt(data)
    cmap, paf = cmap.detach().cpu(), paf.detach().cpu()
    counts, objects, peaks = parse_objects(cmap, paf)#, cmap_threshold=0.15, link_threshold=0.15)
    fps = 1.0 / (time.time() - t)

    keypoints_all = []
    for i in range(counts[0]):
        keypoints = get_keypoint(objects, i, peaks)
        keypoints_all.append(keypoints)
        for j in range(len(keypoints)):
            if keypoints[j][1]:
                x = round(keypoints[j][2] * WIDTH * X_compress)
                y = round(keypoints[j][1] * HEIGHT * Y_compress)
                cv2.circle(src, (x, y), 3, color, 2)
                cv2.putText(src , "%d" % int(keypoints[j][0]), (x + 5, y),  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 2)
                cv2.circle(src, (x, y), 3, color, 2)
    print("FPS:%f "%(fps))

    cv2.putText(src , "FPS: %f" % (fps), (20, 20),  cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 1)

    cv2.imshow('demo', src)

    return keypoints_all


parse_objects = ParseObjects(topology)
draw_objects = DrawObjects(topology)

while cap.isOpened():
    t = time.time()
    ret_val, dst = cap.read()

    dst = cv2.flip(dst, 1) # Flip image horizontally

    if ret_val == False:
        print("Camera read Error")
        break

    img = cv2.resize(dst, dsize=(WIDTH, HEIGHT), interpolation=cv2.INTER_AREA)
    keypoints = execute(img, dst, t)
    count += 1

    print("keypoints: ", keypoints)

    has_people = False
    has_nose = False

    if len(keypoints) > 0:
        has_people = True
        body = keypoints[0]
        body_model.set_keypoints(body)
        nose = body_model.get_nose()
        neck = body_model.get_neck()

        if nose[0] is not None:
            has_nose = True


    if has_nose:

        body_tracker.update(keypoints)
        body = body_tracker.get_target_body()
        print("body: ", body)
        
        body_model.set_keypoints(body)

        wrist = body_model.get_rightwrist_mirrored() 

        gesture = None
        if wrist[0] is not None:
            x = wrist[0]*WIDTH
            y = wrist[1]*HEIGHT
            point = np.array([x, y])
            print("wrist: ", wrist, "point: ", point)
            wrist_gesture.update(point)
            wrist_gesture.set_dist_c(50)
            gesture = wrist_gesture.recognize()

        print("gesture: {}".format(gesture))


    # Stop the program on the q or ESC key
    code = cv2.waitKey(1) & 0xFF
    if code == ord('q') or code == 27:
        break

cv2.destroyAllWindows()
#out_video.release()
cap.release()
