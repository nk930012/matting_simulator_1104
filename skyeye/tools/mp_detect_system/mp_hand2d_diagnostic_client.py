import logging
import websocket
import optparse
import json
import websocket
import numpy as np
import cv2
from time import perf_counter
import logging
from time import sleep
import sys
import os



index_blue = 0
index_green = 1
index_red = 2
index_cyan = 3
index_magenta = 4
index_yellow = 5
index_black = 6

#(blue, green, read)
default_color = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255), (0 , 0, 0)]

def draw_msg(img, msg, x, y, color):

    cv2.putText(img, msg, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y_shift = 20
    y += y_shift

    return (x, y)


def formated_message(name, items):

    l = [tuple('{0:.2f}'.format(flt) for flt in sublist) for sublist in items]
    msg = "{}: {}".format(name, l)
    
    return msg


def draw_bbox(image, x, y, xw, yh):
    cv2.rectangle(image, (xw, y), (x, yh), (0, 255, 0), 2)
    
def drawline( img, pt1, pt2, color, thickness=1, style='dotted', gap=20 ):
    dist =((pt1[0]-pt2[0])**2+(pt1[1]-pt2[1])**2)**.5
    pts= []
    for i in  np.arange(0,dist,gap):
        r=i/dist
        x=int((pt1[0]*(1-r)+pt2[0]*r)+.5)
        y=int((pt1[1]*(1-r)+pt2[1]*r)+.5)
        p = (x,y)
        pts.append(p)

    if style=='dotted':
        for p in pts:
            cv2.circle(img,p,thickness,color,-1)
    else:
        s=pts[0]
        e=pts[0]
        i=0
        for p in pts:
            s=e
            e=p
            if i%2==1:
                cv2.line(img,s,e,color,thickness)
            i+=1

def draw_grid(img, width, height):
    division_x = 4
    division_y = 10
    increament_x = int( width / division_x )
    increament_y = int( height / division_y )
    color = (240, 32, 160)
    thickness = 1 
    lineType = 4
    gap = 10
    style='dashed'
    
    for i in range(division_x):
    
        x = (increament_x * i)
        if(i != 0):
            msg = "{:3.2f}".format(i * 0.25)
            draw_msg(img, msg, x, ( height - 30 ), color)
        
        drawline( img, (x, 0), (x, height), color, thickness, style, gap)
    
    for i in range(division_y):
        y = (increament_y * i)
        if(i != 0):
            msg = "{:3.1f}".format(i * 0.1)
            draw_msg(img, msg, (width - 30), y, color)
        drawline( img, (0, y), (width, y), color, thickness, style, gap)
    

def parse_arguments():
    parser = optparse.OptionParser()
    parser.add_option('-i', '--ip', dest='ip', default="127.0.0.1",
                    help=('the IP address of server'))
    parser.add_option('-p', '--port', dest='port', default=8050,
                    help=('the port of server'))

    parser.add_option('--width', dest='width', default=800,
                    help=('width of img'))

    parser.add_option('--height', dest='height', default=600,
                    help=('height of img'))


    (options, args) = parser.parse_args()
    return options, args

'''
class IDrawer(metaclass=abc.ABCMeta):
    def __init__(self):
        return NotImplemented
    def draw(self, img, data):
        return NotImplemented

class Hand2DDrawer(IDrawer):
    def __init__(self):
        super().__init__()
    def draw(self, img, data):
'''    


def convert_to_screen(part, width, height):
    x = int( part[0] * width )
    y = int( part[1] * height )
    return x, y


def drawHand2D(img, hand):
    
    width = 640
    height = 480

    wrist_x, wrist_y = convert_to_screen( hand['WRIST'], width, height )
    
    thumb_cmc_x, thumb_cmc_y = convert_to_screen( hand['THUMB_CMC'], width, height )
    thumb_mcp_x, thumb_mcp_y = convert_to_screen( hand['THUMB_MCP'], width, height )
    thumb_ip_x, thumb_ip_y = convert_to_screen( hand['THUMB_IP'], width, height )
    thumb_tip_x, thumb_tip_y = convert_to_screen( hand['THUMB_TIP'], width, height )

    index_finger_mcp_x, index_finger_mcp_y = convert_to_screen( hand['INDEX_FINGER_MCP'], width, height )
    index_finger_pip_x, index_finger_pip_y = convert_to_screen( hand['INDEX_FINGER_PIP'], width, height )
    index_finger_dip_x, index_finger_dip_y = convert_to_screen( hand['INDEX_FINGER_DIP'], width, height )
    index_finger_tip_x, index_finger_tip_y = convert_to_screen( hand['INDEX_FINGER_TIP'], width, height )


    middle_finger_mcp_x, middle_finger_mcp_y = convert_to_screen( hand['MIDDLE_FINGER_MCP'], width, height )
    middle_finger_pip_x, middle_finger_pip_y = convert_to_screen( hand['MIDDLE_FINGER_PIP'], width, height )
    middle_finger_dip_x, middle_finger_dip_y = convert_to_screen( hand['MIDDLE_FINGER_DIP'], width, height )
    middle_finger_tip_x, middle_finger_tip_y = convert_to_screen( hand['MIDDLE_FINGER_TIP'], width, height )

    ring_finger_mcp_x, ring_finger_mcp_y = convert_to_screen( hand['RING_FINGER_MCP'], width, height )
    ring_finger_pip_x, ring_finger_pip_y = convert_to_screen( hand['RING_FINGER_PIP'], width, height )
    ring_finger_dip_x, ring_finger_dip_y = convert_to_screen( hand['RING_FINGER_DIP'], width, height )
    ring_finger_tip_x, ring_finger_tip_y = convert_to_screen( hand['RING_FINGER_TIP'], width, height )
           
    pinky_finger_mcp_x, pinky_finger_mcp_y = convert_to_screen( hand['PINKY_FINGER_MCP'], width, height )
    pinky_finger_pip_x, pinky_finger_pip_y = convert_to_screen( hand['PINKY_FINGER_PIP'], width, height )
    pinky_finger_dip_x, pinky_finger_dip_y = convert_to_screen( hand['PINKY_FINGER_DIP'], width, height )
    pinky_finger_tip_x, pinky_finger_tip_y = convert_to_screen( hand['PINKY_FINGER_TIP'], width, height )

    #thumb
    drawline( img, (thumb_cmc_x, thumb_cmc_y), (thumb_mcp_x, thumb_mcp_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )
    drawline( img, (thumb_mcp_x, thumb_mcp_y), (thumb_ip_x, thumb_ip_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )
    drawline( img, (thumb_ip_x, thumb_ip_y), (thumb_tip_x, thumb_tip_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )

    #index finger
    drawline( img, (index_finger_mcp_x, index_finger_mcp_y), (index_finger_pip_x, index_finger_pip_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )
    drawline( img, (index_finger_pip_x, index_finger_pip_y), (index_finger_dip_x, index_finger_dip_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )
    drawline( img, (index_finger_dip_x, index_finger_dip_y), (index_finger_tip_x, index_finger_tip_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )


    #middle finger
    drawline( img, (middle_finger_mcp_x, middle_finger_mcp_y), (middle_finger_pip_x, middle_finger_pip_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )
    drawline( img, (middle_finger_pip_x, middle_finger_pip_y), (middle_finger_dip_x, middle_finger_dip_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )
    drawline( img, (middle_finger_dip_x, middle_finger_dip_y), (middle_finger_tip_x, middle_finger_tip_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )


    #ring finger
    drawline( img, (ring_finger_mcp_x, ring_finger_mcp_y), (ring_finger_pip_x, ring_finger_pip_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )
    drawline( img, (ring_finger_pip_x, ring_finger_pip_y), (ring_finger_dip_x, ring_finger_dip_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )
    drawline( img, (ring_finger_dip_x, ring_finger_dip_y), (ring_finger_tip_x, ring_finger_tip_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )

    #pinky_finger
    drawline( img, (pinky_finger_mcp_x, pinky_finger_mcp_y), (pinky_finger_pip_x, pinky_finger_pip_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )
    drawline( img, (pinky_finger_pip_x, pinky_finger_pip_y), (pinky_finger_dip_x, pinky_finger_dip_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )
    drawline( img, (pinky_finger_dip_x, pinky_finger_dip_y), (pinky_finger_tip_x, pinky_finger_tip_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )


    drawline( img, (wrist_x, wrist_y), (index_finger_mcp_x, index_finger_mcp_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )
    drawline( img, (wrist_x, wrist_y), (pinky_finger_mcp_x, pinky_finger_mcp_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )
    drawline( img, (index_finger_mcp_x, index_finger_mcp_y), (middle_finger_mcp_x, middle_finger_mcp_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )
    drawline( img, (ring_finger_mcp_x, ring_finger_mcp_y), (middle_finger_mcp_x, middle_finger_mcp_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )
    drawline( img, (ring_finger_mcp_x, ring_finger_mcp_y), (pinky_finger_mcp_x, pinky_finger_mcp_y), default_color[ index_green ], thickness=1, style='dotted', gap=1 )


    cv2.circle(img, (wrist_x, wrist_y), 3, default_color[ index_red ], -1 )
    cv2.circle(img, (thumb_cmc_x, thumb_cmc_y), 3, default_color[ index_red ], -1 )
    cv2.circle(img, (thumb_mcp_x, thumb_mcp_y), 3, default_color[ index_red ], -1 )    
    cv2.circle(img, (thumb_ip_x, thumb_ip_y), 3, default_color[ index_red ], -1 )    
    cv2.circle(img, (thumb_tip_x, thumb_tip_y), 3, default_color[ index_red ], -1 )

    cv2.circle(img, (index_finger_mcp_x, index_finger_mcp_y), 3, default_color[ index_red ], -1 )    
    cv2.circle(img, (index_finger_pip_x, index_finger_pip_y), 3, default_color[ index_red ], -1 )
    cv2.circle(img, (index_finger_dip_x, index_finger_dip_y), 3, default_color[ index_red ], -1 )
    cv2.circle(img, (index_finger_tip_x, index_finger_tip_y), 3, default_color[ index_red ], -1 )

    cv2.circle(img, (middle_finger_mcp_x, middle_finger_mcp_y), 3, default_color[ index_red ], -1 )
    cv2.circle(img, (middle_finger_pip_x, middle_finger_pip_y), 3, default_color[ index_red ], -1 )
    cv2.circle(img, (middle_finger_dip_x, middle_finger_dip_y), 3, default_color[ index_red ], -1 )
    cv2.circle(img, (middle_finger_tip_x, middle_finger_tip_y), 3, default_color[ index_red ], -1 )

    cv2.circle(img, (ring_finger_mcp_x, ring_finger_mcp_y), 3, default_color[ index_red ], -1 )
    cv2.circle(img, (ring_finger_pip_x, ring_finger_pip_y), 3, default_color[ index_red ], -1 )
    cv2.circle(img, (ring_finger_dip_x, ring_finger_dip_y), 3, default_color[ index_red ], -1 )
    cv2.circle(img, (ring_finger_tip_x, ring_finger_tip_y), 3, default_color[ index_red ], -1 )
   
    cv2.circle(img, (pinky_finger_mcp_x, pinky_finger_mcp_y), 3, default_color[ index_red ], -1 )
    cv2.circle(img, (pinky_finger_pip_x, pinky_finger_pip_y), 3, default_color[ index_red ], -1 )
    cv2.circle(img, (pinky_finger_dip_x, pinky_finger_dip_y), 3, default_color[ index_red ], -1 ) 
    cv2.circle(img, (pinky_finger_tip_x, pinky_finger_tip_y), 3, default_color[ index_red ], -1 )


class DiagnosisWebsocketData:

    def __init__(self, ip="127.0.0.1", port=8050, debug=logging.ERROR, width=640, height=680):
        self.fps = 0
        self.frame_count = 0
        self.ip = ip
        self.port = port
        self.debug = debug
        logging.basicConfig(level=self.debug, format='%(asctime)s -(%(filename)s)%(levelname)s : %(message)s')
        self.id = 0
        self.width = width
        self.height = height

        self.img = np.zeros((height, width, 3), np.uint8)
        self.url = url = "ws://"+ip+":"+str(port)+"/ws"

        self.time_start = 0
        self.time_end = 0

        websocket.enableTrace(True)
        self.ws = websocket.WebSocketApp(self.url,
        on_message = lambda ws,msg: self.message(ws, msg),
        on_error   = lambda ws,msg: self.error(ws, msg),
        on_close   = lambda ws:     self.close(ws),
        on_open    = lambda ws:     self.open(ws))
    

    def message(self, ws, message):
        self.time_end = perf_counter()
        data = json.loads( message )
        if('TYPE' in data):
            data_type = data['TYPE']
            data_len = data['LEN']
            self.img.fill( 255 )

            numeric_msg =''
            id_msg = ''
            depth_msg = ''
            gesture_msg = ''
            click_type_msg = ''

            for i in range( data_len ):
                hand = data['DATA'][i]
                side = hand['SIDE']
                depth = float(hand['DEPTH'])
                drawHand2D(self.img, hand)
                gesture_msg = "{} ".format(hand['GESTURE'])
                numeric_msg = "{} ".format(hand['NUMERIC'])
                click_type_msg = "{} ".format(hand['CLICK_TYPE'])
                depth_msg = "{} ".format(depth)

                if('MOUSE_POS' in hand):
                    mouse_pos = hand['MOUSE_POS']
                    if mouse_pos is None:
                        mouse_pos = (0, 0)
                    radius = int( 20 + depth * 20 )
                    if radius < 1:
                        radius = 1
                    x = int( mouse_pos[0] * self.width ) 
                    y = int( mouse_pos[1] * self.height )

                cv2.circle(self.img, (x, y), radius, (255, 0, 0), -1)


            num_hand_msg = "num_hands: {}".format(data_len)
            gesture_msg = "gesture: [ {}]".format(gesture_msg)
            numeric_msg = "numeric: [ {}]".format(numeric_msg)            
            click_type_msg = "click_type: [ {}]".format(click_type_msg)
            depth_msg = "depth: [ {}]".format(depth_msg)
           
            time_duration = self.time_end - self.time_start
            fps = int( 1.0 / time_duration )
            fps_msg = "fps: {}".format(fps)



            text_x = 5
            text_y = 20
            ( text_x, text_y ) = draw_msg( self.img, fps_msg, text_x, text_y, default_color[ index_blue ] )
            ( text_x, text_y ) = draw_msg( self.img, num_hand_msg, text_x, text_y, default_color[ index_blue ] )
            ( text_x, text_y ) = draw_msg( self.img, gesture_msg, text_x, text_y, default_color[ index_blue ] )
            ( text_x, text_y ) = draw_msg( self.img, numeric_msg, text_x, text_y, default_color[ index_blue ] )
            ( text_x, text_y ) = draw_msg( self.img, click_type_msg, text_x, text_y, default_color[ index_blue ] )    
            ( text_x, text_y ) = draw_msg( self.img, depth_msg, text_x, text_y, default_color[ index_blue ] )
            self.time_start = self.time_end
        cv2.imshow( 'diagnosis image', self.img )       
        key = cv2.waitKey( 1 )
        if key == ord( "q" ) or 27 == key:
            self.close( websocket )

    def error( self, ws, error ):
        print( error )


    def close(self, ws):
        print( "close" )
        cv2.destroyAllWindows()
        self.id += 1
        id_string = '{id}'.format( id = self.id )
        close_jetson_cmd = '{"id":'+ id_string +', "method": "command","params": {"command":"close", "property":"jetson_device"}}'
        self.ws.send( close_jetson_cmd )        
        self.ws.keep_running = False
        self.ws.close()

    def open(self, ws):
        self.id += 1
        print( 'Open websocket server ip: {}, port: {}'.format(self.ip, self.port) )
        id_string = '{id}'.format( id = self.id )
        open_jetson_cmd = '{"id":'+ id_string +', "method": "command","params": {"command":"open", "property":"jetson_device"}}'
        self.ws.send( open_jetson_cmd )
        sleep(0.05)

    def start(self):
        if None != self.ws:
            self.ws.run_forever()


if __name__ == "__main__":
    options, args = parse_arguments()
    ip = options.ip
    port = int(options.port)
    debug = logging.ERROR
    width = int(options.width)
    height = int(options.height)
    websocket = DiagnosisWebsocketData(ip, port, debug, width, height)
    websocket.start()
