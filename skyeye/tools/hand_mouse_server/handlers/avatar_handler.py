# -*- coding: utf-8 -*-
import sys
import os
import argparse
import logging
import tornado.ioloop
import numpy as np
import time
import json 
import imutils
import cv2 as cv
from skyeye.utils.mediapipe import MpConvert
from skyeye.utils.camera import CameraBuilder
from skyeye.detect.avatar.avatar_detector import AvatarDetector

import yaml


class AvatarHandler():

    def __init__(self, WsHandler, debug=logging.ERROR):

        self.ws = WsHandler
        self.scheduler = None

        self.active = 0

        self.snap_count = 0
        

    def dectet_scheduler(self):


        if self.active == 1:
            
            self.camera.process()
            frame = self.camera.color_image.copy()
            
            if frame is None:
                return

            frame = cv.flip(frame, 1)
            image = frame.copy()
            image_out = frame.copy()

            #image = cv.cvtColor(image, cv.COLOR_BGR2RGB)

            if self.enable_gpu == 1:
                # the BGR image to RGBA.
                image = cv.cvtColor(image, cv.COLOR_BGR2RGBA)
            else:    
                # the BGR image to RGB.
                image = cv.cvtColor(image, cv.COLOR_BGR2RGB)

            avatars = self.avatar_detector.detect(image)

            data_avatars= []

            for avatar in avatars:
                roll = 0
                pitch = 0
                yaw = 0
                face_dist = 0
                left_hand_dist = 0
                right_hand_dist = 0
                face_ary = []
                left_hand_ary = []
                right_hand_ary = []
                body_ary = []

                left_gesture = None
                left_numeric_gesture = None

                right_gesture = None
                right_numeric_gesture = None
                
                head = avatar.get_head()
                left_hand = avatar.get_left_hand()
                right_hand = avatar.get_right_hand()
                body = avatar.get_body()

                if head:
                    quat = head.get_quat()

                    # Headpose
                    yaw, pitch, roll = quat.yaw_pitch_roll
                    roll = roll / np.pi * 180
                    pitch = pitch / np.pi * 180
                    yaw = yaw / np.pi * 180

                    # Face distance
                    face_dist = head.get_face_distance()
                    face_ary = MpConvert.convert_landmark_to_ary(head.get_landmarks())

                if left_hand:
                    left_hand_dist = left_hand.get_hand_distance()
                    left_hand_ary = MpConvert.convert_landmark_to_ary(left_hand.get_landmarks())
                    left_gesture = left_hand.get_gesture()
                    left_numeric_gesture = left_hand.get_numeric_gesture()

                if right_hand:
                    right_hand_dist = right_hand.get_hand_distance()
                    right_hand_ary = MpConvert.convert_landmark_to_ary(right_hand.get_landmarks())
                    right_gesture = right_hand.get_gesture()
                    right_numeric_gesture = right_hand.get_numeric_gesture()

                if body:
                    body_ary = MpConvert.convert_landmark_to_ary(body.get_landmarks())

                avatarData = {'roll':roll, 'pitch':pitch, 'yaw':yaw, 'face_dist':face_dist,'left_gesture':left_gesture,'left_numeric_gesture':left_numeric_gesture, 'left_hand_dist':left_hand_dist,'right_gesture':right_gesture,'right_numeric_gesture':right_numeric_gesture, 'right_hand_dist':right_hand_dist, 'face_ary':face_ary, 'left_hand_ary':left_hand_ary, 'right_hand_ary':right_hand_ary, 'body_ary':body_ary}

                data_avatars.append(avatarData)
                
            wsData = {'size': (self.frame_width, self.frame_height),'avatars':data_avatars}
            self.ws.send_updates(wsData)
            

    def open(self, id = 0):
        
        if self.active == 0:
            self.camera = CameraBuilder.buildWithYaml('camera_config.yaml')
            self.camera.open()
            self.avatar_detector = AvatarDetector(isDetectFace=False,isDetectHand=True,isDetectBody=True)

            self.frame_width = self.camera.width
            self.frame_height = self.camera.height

            self.active = 1
            self.scheduler = tornado.ioloop.PeriodicCallback(self.dectet_scheduler, 20)
            self.scheduler.start()

        with open(r'mediapipe.yaml', 'r', encoding='utf-8') as f:
            inputs = yaml.load(f, Loader=yaml.Loader)
        self.enable_gpu = inputs['enable_gpu']

        ret = True
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)
        
    def close(self, id):
        
        self.active = 0
        if self.scheduler is not None:
            self.scheduler.stop()
            self.scheduler = None
            self.camera.close()
        status_str = "successful"
        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)
        
    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close cam device.')
                self.close(id=1)
            elif params["command"] == "open":
                if params["property"] == "avatar":
                    self.open(id=id)
            elif params["command"] == "close":
                if params["property"] == "avatar":
                    self.close(id=id)
        elif method == "get":
            pass
        elif method == "set":
            pass
