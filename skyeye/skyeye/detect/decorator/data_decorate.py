from .component import Component
import abc

class DataDecorate(Component):

    def __init__(self, component : Component, postProcess=True):
        super().__init__(postProcess)
        self.component = component
        self._imageOut = None

    def detect(self):
        self.component.detect()

    @abc.abstractmethod
    def postProcess(self):
        return NotImplemented