import numpy as np


def get_op_position(obj):

    x = obj[0]
    y = obj[1]

    return [x, y]


def get_tp_position(obj):

    x = obj[2]
    y = obj[1]

    if x is None:
        x = 0.0

    if y is None:
        y = 0.0


    return [x, y]


def to_keypoints(kps_in, fmt="openpose"):
    '''
    Return the keypoints with standard format.

    kp_in: list
        Input keypoints.
    fmt: str
        Format of keypoints. "openpose" or "trt_pose"
    '''

    kps_out = []
    # Take care anormal keypoints
    if np.ndim(kps_in) == 0:
        return kps_out

    if fmt == "openpose":

        get_position = get_op_position
        for kp_in in kps_in:

            kp_out = []

            # nose
            kp_out.append(get_position(kp_in[0]))
            # neck
            kp_out.append(get_position(kp_in[1]))
            # right shoulder
            kp_out.append(get_position(kp_in[2]))
            # right elbow
            kp_out.append(get_position(kp_in[3]))
            # right wrist
            kp_out.append(get_position(kp_in[4]))
            # left shoulder
            kp_out.append(get_position(kp_in[5]))
            # left elbow
            kp_out.append(get_position(kp_in[6]))
            # left wrist
            kp_out.append(get_position(kp_in[7]))
            # right hip
            kp_out.append(get_position(kp_in[8]))
            # right knee
            kp_out.append(get_position(kp_in[9]))
            # right ankle
            kp_out.append(get_position(kp_in[10]))
            # left hip
            kp_out.append(get_position(kp_in[11]))
            # left knee
            kp_out.append(get_position(kp_in[12]))
            # left ankle
            kp_out.append(get_position(kp_in[13]))
            # right eye
            kp_out.append(get_position(kp_in[14]))
            # left eye
            kp_out.append(get_position(kp_in[15]))
            # right ear
            kp_out.append(get_position(kp_in[16]))
            # left ear
            kp_out.append(get_position(kp_in[17]))

            # Save
            kps_out.append(kp_out)

    else:

        get_position = get_tp_position
        for kp_in in kps_in:

            kp_out = []

            # nose
            kp_out.append(get_position(kp_in[0]))
            # neck
            kp_out.append(get_position(kp_in[17]))
            # right shoulder
            kp_out.append(get_position(kp_in[6]))
            # right elbow
            kp_out.append(get_position(kp_in[8]))
            # right wrist
            kp_out.append(get_position(kp_in[10]))
            # left shoulder
            kp_out.append(get_position(kp_in[5]))
            # left elbow
            kp_out.append(get_position(kp_in[7]))
            # left wrist
            kp_out.append(get_position(kp_in[9]))
            # right hip
            kp_out.append(get_position(kp_in[12]))
            # right knee
            kp_out.append(get_position(kp_in[14]))
            # right ankle
            kp_out.append(get_position(kp_in[16]))
            # left hip
            kp_out.append(get_position(kp_in[11]))
            # left knee
            kp_out.append(get_position(kp_in[13]))
            # left ankle
            kp_out.append(get_position(kp_in[15]))
            # right eye
            kp_out.append(get_position(kp_in[2]))
            # left eye
            kp_out.append(get_position(kp_in[1]))
            # right ear
            kp_out.append(get_position(kp_in[4]))
            # left ear
            kp_out.append(get_position(kp_in[3]))

            # Save
            kps_out.append(kp_out)

    return kps_out
