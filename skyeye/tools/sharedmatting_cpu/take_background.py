import numpy as np
import cv2 as cv
from time import perf_counter

from skyeye.utils.opencv import wait_key
from skyeye.utils.camera import CameraBuilder

def draw_msg(image, msg, x, y, y_shift=20, color=(0, 255,0)):

    cv.putText(image, msg, (x, y),
        cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    y += y_shift

    return (x, y)

if __name__ == '__main__':

    camera = CameraBuilder.buildWithYaml('camera.yaml')
    camera.open()
    frame_width = camera.width
    frame_height = camera.height

    frame_count = 0
    while True:

        bg = None
        while True: # Grab background

            camera.process()
            frame = camera.color_image.copy()

            cv.imshow("Win", frame)

            # Press 'b' to take a snapshot as the background 
            key = wait_key(1)
            if key == ord('b'):
                cv.imwrite('background.png', frame)
                break
            elif key == ord('q'):
                exit()

        while True: # Matting

            camera.process()
            frame = camera.color_image.copy()

            frame_count += 1
            print("frame_count = {}".format(frame_count))
            time_start = perf_counter()

            time_end = perf_counter()
            time_duration = time_end - time_start
            fps = int(1.0/time_duration)

            image_out = frame

            # Show image 
            text_x = 20
            text_y = 20
            text_y_shift = 20

            msg = "fps: {}".format(fps)
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "Press {} to take a snapshot.".format('s')
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)
            msg = "Press {} to quit.".format('q')
            (text_x, text_y) = draw_msg(image_out, msg, text_x, text_y)

            #cv.imshow("fg_mask", fg_mask)
            cv.imshow("win", image_out)

            # Exit while 'q' is pressed
            key = wait_key(1)
            if key == ord('b'):
                break
            elif key == ord('q'):
                exit()


    # cleanup the camera and close any open windows
    camera.close()

    cv.destroyAllWindows()
